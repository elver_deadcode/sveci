<?php
namespace App;
class Api{
    private $url_sus='http://suis-ws.minsalud.gob.bo/api/buscar_adscrito';

    public  function buscarCarnetSus($numero_carnet,$fecha_nacimiento,$complemento){
        // $numero_carnet='13220985899';

        // $fecha_nacimiento='14/04/2007';
        // $complemento='';

        $client = new \GuzzleHttp\Client();
        $url=$this->url_sus.'?ci='.$numero_carnet.'&fechanacimiento='.$fecha_nacimiento;

     if($complemento!=''){
         $url.='&complemento='.$complemento;
     }

        try{
         $request = $client->get($url,[ 'timeout' => 2]);
        $response = $request->getBody();
        $statuscode = $request->getStatusCode();
        $obj = json_decode($response);
        return[
            'status'=>$statuscode,
            'response'=>$obj,
            'message'=>''
        ];

        }
        catch(\Exception $e){
        // dd($e);
          return [
              'status'=>400,
              'response'=>[],
              'message'=>$e->getMessage()
          ];
        }

    }
}
?>
