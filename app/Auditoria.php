<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
    protected $table = "auditoria";

    protected $fillable = [
        'Id_tabla',
        'Nombre_tabla',
        'Tipo_usuario',
        'Fecha_ing',
        'Hora_ing',
        'Fecha_sal',
        'Hora_sal',
        'IP_terminal',
        'Evento',
        'Submenu',
        'Modulo',
        'Menu',
        'Item',
    ];
    public $timestamps = false;

    public static function guardar(
        $id_tabla,
        $evento,
        $modulo = '',
        $menu = '',
        $submenu = '',
        $item = '',
        $sw_ingreso = true,
        $request,
        $nombre_tabla = 'usuario'
    ) {


        try {

//            $evento       = self::obtenerIdEvento($evento);
            $tipo_usuario = self::obtenerTipoUsuario($id_tabla, $nombre_tabla);
            $ip           = $request->ip();
            $fecha_act    = date('Y-m-d');
            $hora_act     = date('H:i:s');
            $fecha_ing    = '';
            $hora_ing     = '';
            $fecha_sal    = '';
            $hora_sal     = '';
            $data         = [];
            if ($sw_ingreso) {
                $fecha_ing = $fecha_act;
                $hora_ing  = $hora_act;
                $fecha_sal = null;
                $hora_sal  = null;
            } else {
                $fecha_ing = null;
                $hora_ing  = null;
                $fecha_sal = $fecha_act;
                $hora_sal  = $hora_act;
            }

            if ($tipo_usuario) {

                $au = new Auditoria();

                $au->Id_tabla     = $tipo_usuario['id'];
                $au->Tipo_usuario = $tipo_usuario['tipo_usuario'];
                $au->Nombre_tabla = $tipo_usuario['nombre_tabla'];
                $au->Fecha_ing    = $fecha_ing;
                $au->Hora_ing     = $hora_ing;
                $au->Fecha_sal    = $fecha_sal;
                $au->Hora_sal     = $hora_sal;
                $au->IP_terminal  = $ip;
                $au->Evento       = $evento;
                $au->Submenu      = $submenu;
                $au->Modulo       = $modulo;
                $au->Menu         = $menu;
                $au->Item         = $item;
                try {
                    $au->save();
                } catch (\Exception $e) {
                    return dd($e);
                }

            }
        } catch (Exception $e) {

        }


    }

    private function getIpTerminal()
    {

        return request()->ip();


    }

    public static function obtenerIdEvento($evento)
    {
        $evento = Evento::inDescripcion($evento);

        if ($evento->first() == null) {
            return false;

        }
        return $evento->first()->Id_evento;

    }

    public static function obtenerTipoUsuario($id_tabla, $nombre_tabla)
    {
        $tipo_usuario = [];
        switch ($nombre_tabla) {
            case 'usuario':
                $user = User::find($id_tabla);
                if (isset($user->roles[0])) {
                    $rol = $user->roles[0];
                    if ($rol == null) {
                        return [];
                    }
                    return $tipo_usuario = [
                        'id'           => $user->id,
                        'tipo_usuario' => $rol->name,
                        'nombre_tabla' => 'users'
                    ];


                }
                break;

            case 'vigilantes':
                $vigilante = Vigilante::find($id_tabla);
                if (isset($vigilante)) {
                    return $tipo_usuario = [
                        'id'           => $id_tabla,
                        'tipo_usuario' => 'vigilante',
                        'nombre_tabla' => 'vigilantes'
                    ];
                }

                break;
        }
        return $tipo_usuario;
    }


}
