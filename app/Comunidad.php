<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    protected $table = 'comunidades';
    protected $primaryKey = "IdComunidad";
    protected $connection = 'sveci';

    protected $fillable = [
        "codLocalidad",
        "codMunicipio",
        "nomLocalidad",
        "nomComunidad",

    ];

    public function municipio()
    {
        return $this->belongsTo(municipio::class, 'codMunicipio', 'mnc_codigo');
    }
}