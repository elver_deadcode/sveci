<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class ContraseniaUsuario extends Model
{
    protected $table = 'contraseniaUsuario';
    protected $fillable = [
        'user_id',
        'FechaDeCambio',
        'Contrasenia'
    ];
    protected static  $numero_veces_repeticion_contrasenia=5;

    public  static function validarUsuarioContrasenia(User $user,$password)
    {

        $ultimas_contrasenias=self::select('Contrasenia','id')->where('user_id',$user->id)->orderBy('id','DESC')->limit(5)->get();
        $result = $ultimas_contrasenias->filter(function ($item, $key)use($password)
        {

            return Hash::check($password, $item->Contrasenia);
        });
        if(count($result)>0){
            return false;
        }
        return true;
    }
}
