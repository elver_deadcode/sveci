<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Provincia;
use App\Municipio;
/**

 * @property string dpt_codigo
 * @property string dpt_nombre
 * @property string sigla
 * @property string corr_est_salud


 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="Departamento",
 *     @OA\Property(property="dpt_codigo", type="string",example="4"),
 *     @OA\Property(property="dpt_nombre", type="string",example="ORURO"),
 *     @OA\Property(property="sigla", type="string",example="ORU"),
 *     @OA\Property(property="corr_est_salud", type="string",example="230"),
 *
 * ),
 * @OA\Schema(
 *     schema="DepartamentoConProvicinasMunicipios",
 *     @OA\Property(property="dpt_codigo", type="string",example="4"),
 *     @OA\Property(property="dpt_nombre", type="string",example="ORURO"),
 *     @OA\Property(property="sigla", type="string",example="ORU"),
 *     @OA\Property(property="corr_est_salud", type="string",example="230"),
 *     @OA\Property(property="provincias", type="array",

 *     @OA\Items(ref="#/components/schemas/ProvinciaMunicipios")),
 *
 * ),
 */
class Departamento extends Model
{
    protected $table = 'ctg_departamentos';
    public $keyType = 'string';

    protected $fillable = [
        "dpt_codigo",
        "dpt_nombre",
        "dpt_sigla",
        "corr_est_salud",

    ];
    protected $connection = 'snis';
    public $timestamps = false;
    protected $primaryKey = "dpt_codigo";

    public function provincias()
    {
        return $this->hasMany(Provincia::class, 'coddepto');
    }

    public function municipios()
    {
        return $this->hasManyThrough(Municipio::class, Provincia::class, 'coddepto', 'prv_codigo', 'dpt_codigo');
        // return $this->hasMany(Municipio::class,'departamento_id');
    }
    public function getCodigoAttribute()
    {
        return $this->dpt_codigo;
    }
    public function getNombreAttribute()
    {
        return $this->dpt_nombre;
    }

    public  function obtenerMunicipios()
    {

        $municipios = Municipio::join('ctg_provincia as p', 'p.codprovi', '=', 'ctg_municipios.prv_codigo')
            ->join('ctg_departamentos as d', 'd.dpt_codigo', '=', 'p.coddepto')
            ->where('d.dpt_codigo', $this->dpt_codigo)
            ->get();

        return $municipios;
    }

    public  static function obtenerEstablecimientos($cod_depto)
    {

        $establecimientos = Establecimiento::join('ctg_municipios as m', 'm.mnc_codigo', '=', 'tbl_estabgest.codmunicip')
            ->join('ctg_provincia as p', 'p.codprovi', '=', 'm.prv_codigo')
            ->join('ctg_departamentos as d', 'd.dpt_codigo', '=', 'p.coddepto')
            ->where('d.dpt_codigo', $cod_depto)
            ->select('tbl_estabgest.codestabl')->distinct()->get();

        return $establecimientos;
    }
    // public function establecimientos()
    // {
    //     return $this->hasManyThrough(Establecimiento::class, Municipio::class);
    // }
}
