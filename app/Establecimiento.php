<?php

namespace App;

use App\Http\Controllers\EstablecimientoController;
use Illuminate\Database\Eloquent\Model;
/**
 * @property string codestabl
 * @property string codinstit
 * @property string codclsest
 * @property string codmunicip
 * @property string idgestion
 * @property string codarea
 * @property string nomestabl
 * @property string codurbrur
 * @property string nomrespon
 * @property string seguro
 * @property string num_camas
 * @property string bajalogica
 * @property string poblacion
 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="Establecimiento",
 *     @OA\Property(property="codestabl", type="string",example="200711"),
 *     @OA\Property(property="codinstit", type="string",example="0101"),
 *     @OA\Property(property="codclsest", type="string",example="J"),
 *     @OA\Property(property="codmunicip", type="string",example="22002"),
 *     @OA\Property(property="idgestion", type="string",example="2018"),
 *     @OA\Property(property="codarea", type="string",example="2082"),
 *     @OA\Property(property="nomestabl", type="string",example="MERCEDES"),
 *     @OA\Property(property="codurbrur", type="string",example="R"),
 *     @OA\Property(property="nomrespon", type="string",example=""),
 *     @OA\Property(property="seguro", type="string",example="S"),
 *     @OA\Property(property="num_camas", type="string",example="0"),
 *     @OA\Property(property="bajalogica", type="string",example="N"),
 *     @OA\Property(property="poblacion", type="string",example=""),
 *     @OA\Property(property="nombre", type="string",example="MERCEDES")

 * ),
 * @OA\Schema(
 *     schema="EstablecimientoMedicos",
 *     @OA\Property(property="codestabl", type="string",example="200711"),
 *     @OA\Property(property="codinstit", type="string",example="0101"),
 *     @OA\Property(property="codclsest", type="string",example="J"),
 *     @OA\Property(property="codmunicip", type="string",example="22002"),
 *     @OA\Property(property="idgestion", type="string",example="2018"),
 *     @OA\Property(property="codarea", type="string",example="2082"),
 *     @OA\Property(property="nomestabl", type="string",example="MERCEDES"),
 *     @OA\Property(property="codurbrur", type="string",example="R"),
 *     @OA\Property(property="nomrespon", type="string",example=""),
 *     @OA\Property(property="seguro", type="string",example="S"),
 *     @OA\Property(property="num_camas", type="string",example="0"),
 *     @OA\Property(property="bajalogica", type="string",example="N"),
 *     @OA\Property(property="poblacion", type="string",example=""),
 *     @OA\Property(property="municipio", ref="#/components/schemas/Municipio"),
 *     @OA\Property(property="medicos", type="array",
 *     @OA\Items(ref="#/components/schemas/PersonaUsuario")
 * )

 * ),
 *
 */
class Establecimiento extends Model
{
    protected $table = 'tbl_estabgest';
    protected $connection = 'snis';
    public $keyType = 'string';
    protected $fillable = [
        "codestabl",
        "codinstit",
        "codclsest",
        "codmunicip",
        "idgestion",
        "codarea",
        "nomestabl",
        "codurbrur",
        "nomrespon",
        "seguro",
        "num_camas",
        "bajalogica",
        "poblacion",
    ];
    public $timestamps = false;
    protected $primaryKey = "codestabl";
    protected  $appends = ['nombre'];

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'codmunicip', 'mnc_codigo');
    }
    public function getCodigoAttribute()
    {
        return $this->codestabl;
    }
    public function getNombreAttribute()
    {
        return $this->nomestabl;
    }


    // public function esta

    public function self()
    {
        $string="'".$this->codestabl."'";
        return $this->with('municipio', 'municipio.provincia.departamento')->where("codestabl", $string)
            ->orderBy("idgestion", 'DESC')
            ->first();
    }

    public function red()
    {
        return $this->belongsTo(Red::class, 'are_codigo');
    }
    public  static function obtenerUltimoEstablecimiento($codigo)
    {
        return  self::where('codestabl', $codigo)->orderBy('idgestion', 'DESC')->first();
    }
    public function obtenerMedicosEstablecimiento(){
          return $this->hasMany(Persona::class)->where('codEstablecimiento',$this->codestabl);
    }
}
