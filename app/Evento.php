<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'evento';
    protected $primaryKey = "Id_evento";
    protected $fillable = [
        'descripcion'
    ];
    public $timestamps = false;

    public  function scopeInDescripcion($query,$desc)
    {
        return $query->where('Descripcion',$desc);

    }
}
