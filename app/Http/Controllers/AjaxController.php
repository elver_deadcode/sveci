<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificacionParto;
use App\NotificacionMuerteMujer;
use App\NotificacionMuerteBebe;
use App\NotificacionEmbarazo;
use App\Persona;
use App\Mujer;
use App\Vigilante;

class AjaxController extends Controller
{
    public function anularNotificacion(Request $request)
    {
        $notificacion = \App\Notificacion::find($request->id);
        $notificable;


        switch ($notificacion->notificable_type) {
            case 'App\NotificacionEmbarazo':
                $notificable = \App\NotificacionEmbarazo::where('IdNotificacionEmbarazo',
                    $notificacion->notificable_id)->first();
                break;

            case 'App\NotificacionMuerteMujer':
                $notificable = \App\NotificacionMuerteMujer::where('IdNotificacionMuerteMujer',
                    $notificacion->notificable_id)->first();
                break;

            case 'App\NotificacionMuerteBebe':
                $notificable = \App\NotificacionMuerteBebe::where('IdNotificacionMuerteBebe',
                    $notificacion->notificable_id)->first();
                break;

            case 'App\NotificacionParto':
                $notificable = \App\NotificacionParto::where('IdNotificacionParto',
                    $notificacion->notificable_id)->first();
                break;
        }

        if (!is_null($notificable)) {

            $notificable->estado = 'anulado';
            $notificable->save();

            return response()->json([
                'success' => true,


            ]);
        } else {
            return response()->json([
                'success' => false,

            ]);
        }
    }

    public function establecimientosMunicipio(Request $request)
    {

        $options = '';

        try {

            $establecimientos    = \App\Municipio::obtenerEstablecimientosPorMunicipio($request->idmuni);
            $comunidades         = [];
            $muni                = \App\Municipio::where('mnc_codigo', $request->idmuni)->first();
            $options_comunidades = "";
            if (!is_null($muni)) {

                $comunidades = $muni->comunidades()->orderBy('nomComunidad', 'ASC')->get();

                foreach ($comunidades as $c) {
                    $options_comunidades .= '<option value="'.$c->IdComunidad.'">'.$c->nomComunidad.'</option>';
                }
            }

            foreach ($establecimientos as $e) {

                $options .= '<option value="'.$e->codestabl.'">'.$e->nomestabl.'</option>';
            }
        } catch (Exception $e) {
            return response()->json([], 500);
        }

        // return dd($establecimientos);

        return response()->json([
            'status' => true,
            'options' => $options,
            'options_comunidades' => $options_comunidades
        ], 200);
    }

    public function establecimientosRed(Request $request)
    {
        $options = '';

        try {

            $establecimientos = \App\Red::obtenerEstablecimientosPorRed($request->idred);

            foreach ($establecimientos as $e) {
                $aux     = $e->self();
                $options .= '<option value="'.$e->codestabl.'">'.$aux->nomestabl.'</option>';
            }
        } catch (Exception $e) {
            return response()->json([], 500);
        }

        // return dd($establecimientos);

        return response()->json([
            'status' => true,
            'options' => $options
        ], 200);
    }

    public function buscarMujer(Request $request)
    {
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;
        $tipo    = $request->tipo;
        //   $tipo='nombres';
        $palabra = strtoupper($request->phrase);
        $mujeres = [];


        switch ($nivel) {
            case 'nacional':
                if ($tipo == 'nombres') {

                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->orWhere('p.primerApellido', 'LIKE', '%'.$palabra.'%')
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                    // return dd($mujeres);
                }
                if ($tipo == 'carnet') {
                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.numeroCarnet', 'like', $palabra.'%')
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                break;

            case 'depto':

                if ($tipo == 'nombres') {


                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('mujeres.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('p.dpt_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('p.dpt_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
            case 'red':

                if ($tipo == 'nombres') {


                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('p.are_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('p.are_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;

            case 'muni':

                if ($tipo == 'nombres') {


                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('p.mnc_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('p.mnc_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
            case 'establ':

                if ($tipo == 'nombres') {

                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('p.codEstablecimiento', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $mujeres = Mujer::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('p.codEstablecimiento', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
        }

        $result = [];
        foreach ($mujeres as $v) {
            $result[] = [
                "value" => $v['IdMujer'],
                "label" => $v['nombres']." ".$v['primerApellido']." ".$v['segundoApellido'],
            ];
        }


        return response()->json($result);
    }

    public function buscarVigilante(Request $request)
    {
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;
        $tipo    = $request->tipo;
        $palabra = strtoupper($request->phrase);
        //   $palabra=$request->phrase;
        $vigilantes = [];
        switch ($nivel) {
            case 'nacional':
                if ($tipo == 'nombres') {


                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        //   ->where('activado','true')
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.numeroCarnet', 'like', $palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        //   ->where('activado','true')
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                break;

            case 'depto':

                if ($tipo == 'nombres') {


                    $vigilante = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        //   ->where('activado','true')
                        ->where('p.dpt_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        //   ->where('activado','true')
                        ->where('p.dpt_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
            case 'red':

                if ($tipo == 'nombres') {


                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.are_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.are_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;

            case 'muni':

                if ($tipo == 'nombres') {


                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.mnc_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.mnc_codigo', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
            case 'establ':

                if ($tipo == 'nombres') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.nombres', 'LIKE', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.codEstablecimiento', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }
                if ($tipo == 'carnet') {
                    $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                        ->where('p.numeroCarnet', 'LIKE ', '%'.$palabra.'%')
                        ->where('vigilantes.estado', 'true')
                        ->where('p.codEstablecimiento', $idnivel)
                        ->orderBy('p.primerApellido', 'ASC')
                        ->take(20)
                        ->get()
                        ->toArray();
                }

                break;
        }
        $result = [];
        foreach ($vigilantes as $v) {
            $result[] = [
                "value" => $v['IdVigilante'],
                "label" => $v['nombres']." ".$v['primerApellido']." ".$v['segundoApellido'],
            ];
        }
        return response()->json($result);
    }

    public function guardarDatosMujer(Request $request)
    {

        switch ($request->tiponotificacion) {
            case 'App\NotificacionParto':
                $notificacion = NotificacionParto::find($request->notificacionid);
                if ($notificacion) {

                    $notificacion->nombres         = $request->nombrecompleto;
                    $notificacion->primerApellido  = '';
                    $notificacion->segundoApellido = '';
                    $notificacion->edad            = $request->edad;
                    $notificacion->direccion       = $request->direccion;
                    $notificacion->telefono        = $request->telefono;
                    $notificacion->save();

                }

                break;


            case 'App\NotificacionMuerteMujer':
                $notificacion = NotificacionMuerteMujer::find($request->notificacionid);

                if ($notificacion) {

                    $notificacion->nombres         = $request->nombrecompleto;
                    $notificacion->primerApellido  = '';
                    $notificacion->segundoApellido = '';
                    $notificacion->edad            = $request->edad;
                    $notificacion->direccion       = $request->direccion;
                    $notificacion->telefono        = $request->telefono;
                    $notificacion->save();
                }

                break;


            case 'App\NotificacionMuerteBebe':
                $notificacion = NotificacionMuerteBebe::find($request->notificacionid);

                if ($notificacion) {

                    $notificacion->nombres         = $request->nombrecompleto;
                    $notificacion->primerApellido  = '';
                    $notificacion->segundoApellido = '';
                    $notificacion->edad            = $request->edad;
                    $notificacion->direccion       = $request->direccion;
                    $notificacion->telefono        = $request->telefono;
                    $notificacion->save();
                }

                break;

            case 'App\NotificacionEmbarazo':
                $notificacion = NotificacionEmbarazo::find($request->notificacionid);

                if ($notificacion) {

                    $notificacion->nombres         = $request->nombrecompleto;
                    $notificacion->primerApellido  = '';
                    $notificacion->segundoApellido = '';
                    $notificacion->edad            = $request->edad;
                    $notificacion->direccion       = $request->direccion;
                    $notificacion->telefono        = $request->telefono;
                    $notificacion->save();
                }

                break;
        }
    }
}
