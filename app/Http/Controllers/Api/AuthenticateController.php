<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthenticateController extends Controller
{
        /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return ApiResource
     *
     * @throws Throwable
     *
     * @OA\Post(
     *     path="/authenticate",
     *     summary="login  pora  autenticarse en el API",
     *     operationId="autenticacionAPI",
     *     tags={"login"},
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\JsonContent(ref="#/components/schemas/UserCredentials")

    ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(ref="#/components/schemas/UserCredentialsResponse")
     *     ),
     * )
     */
     public function authenticate(Request $request)
     {

         $request->validate([
             'username' => 'required',
             'password' => 'required',
         ]);


         $user = User::where('username', $request->username)->first();

         if (! $user || ! Hash::check($request->password, $user->password)) {

            return response()->json([
                 'email' => 'las credenciales son incorrectas.',
             ],404);
         }
         if ($user->nivel!='api') {

             return response()->json([
                 'message' => 'Este usuario no esta autorizado ',
             ],404);
         }
         return response()->json([
             'user' => $user,
             'token'=>$user->createToken($request->username)->plainTextToken
         ],200);
//         return $user->createToken($request->username)->plainTextToken;
     }
}
