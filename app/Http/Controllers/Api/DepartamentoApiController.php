<?php

namespace App\Http\Controllers\Api;

use App\Departamento;
use App\Http\Controllers\Controller;
use App\Municipio;
use Illuminate\Http\Request;

class DepartamentoApiController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Departamento $departamento
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Get(
     *     path="/departamentos/{departamento}",
     *     summary="Obtiene la informacion de un departamento en particular pasandole en codigo del departamento",
     *     operationId="obtenerDepartamentoPorCodigo",
     *     tags={"Departamentos"},
     *     @OA\Parameter(
     *         description="codigo del departamento",
     *         in="path",
     *         name="dpt_codigo",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: provincia,municipios",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Departamento encontrado",
     *         @OA\JsonContent(ref="#/components/schemas/DepartamentoConProvicinasMunicipios")
     *     ),
     * )
     */
    public function obtenerDepartamento(Departamento $departamento, Request $request)
    {

        $include = explode(',', $request->input('include', ''));
        if (in_array('municipios', $include)) {
            $departamento->municipios = $departamento->municipios;
//            $departamento->provincia->departamento = $departamento->provincia->departamento;
        }

        return new \App\Http\Resources\ApiResource($departamento);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return ApiCollection
     *
     * @OA\Get(
     *     path="/departamentos/listar",
     *     summary="Obtener los Departamentos",
     *     operationId="ListaDeDepartamento",
     *     tags={"Departamentos"},
     *
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: provincias,municipios",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de Departamentos",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/DepartamentoConProvicinasMunicipios"),
     *             ),
     *         ),
     *     ),
     * )
     */
    public function listar(Request $request)
    {
        $query = Departamento::query();

        $include = explode(',', $request->input('include', ''));
        if (in_array('provincias', $include)) {
            $query->with('provincias');
        }
        if (in_array('municipios', $include)) {
            $query->with('provincias.municipios');
        }
        //load resources
        $response = $query->orderBy(
            $request->input('order_by', 'dpt_nombre'),
            $request->input('order_direction', 'ASC')
        )
            ->get();
        return new \App\Http\Resources\ApiCollection($response);

    }

}
