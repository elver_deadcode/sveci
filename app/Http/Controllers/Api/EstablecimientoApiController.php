<?php

namespace App\Http\Controllers\Api;

use App\Establecimiento;
use App\Http\Controllers\Controller;
use App\Persona;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Omnio\Http\Resources\ApiCollection;

class EstablecimientoApiController extends Controller
{
    public static function paginate(Collection $results, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');

        $total = $results->count();

        return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);

    }
    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return ApiCollection
     *
     * @OA\Get(
     *     path="/establecimientos/listar",
     *     summary="Obtener los establecimientos po nivel de ubicacion",
     *     operationId="ListaDeEstablecimientosPorNivel",
     *     tags={"Establecimientos de Salud"},
     *     @OA\Parameter(
     *     description="Nivel de localizacion:ejm(nacional,departamento,red,municipio,establecimiento)",
     *     name="nivel",
     *     required=true,
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         )
     * ),
     *      @OA\Parameter(
     *         description="codigo del nivel de localizacion ejm(41503 codigo de municipio del establecimiento, si el nivel es nacional dejar vacio)",
     *         in="query",
     *         name="codigo",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/order_by"),
     *     @OA\Parameter(ref="#/components/parameters/order_direction"),
     *     @OA\Parameter(ref="#/components/parameters/per_page"),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de Establecimientos de salud",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Establecimiento"),
     *             ),
     *             @OA\Property(
     *                 property="meta",
     *                 type="object",
     *                 ref="#/components/schemas/metadata",
     *             ),
     *         ),
     *     ),
     * )
     */

    public function listar(Request $request){
        $request->validate([
            'nivel' => 'required'
        ]);
        $query=[];
        try {
            switch ($request->nivel) {
                case 'departamento':

                    $query = Establecimiento::join('ctg_municipios as m', 'm.mnc_codigo', '=',
                        'tbl_estabgest.codmunicip')
                        ->join('ctg_provincia as p', 'p.codprovi', '=', 'm.prv_codigo')
                        ->join('ctg_departamentos as d', 'd.dpt_codigo', '=', 'p.coddepto')
                        ->where('d.dpt_codigo', $request->codigo)->get();
                    break;
                case 'municipio';
                    $query = Establecimiento::where('codmunicip', $request->codigo)->get();
                    break;
                case 'red';
                    $query = Establecimiento::where('codarea', $request->codigo)->get();

                    break;

            }
            if($query==null || !$query->count()){
                throw new \Exception('');
            }


            $query           = $query->sortByDesc('idgestion')->groupby('idgestion')->first();
            $order_by        = 'nomestabl';
            $order_direction = 'ASC';

            if ($request->has('order_by')) {
                $order_by = $request->input('order_by');
            }

            if ($request->has('order_direction')) {
                $order_direction = $request->input('order_direction');
            }
            $perPage = $request->input('per_page', $query->count());
            if (strtoupper($order_direction) == 'ASC') {
                $query = $query->sortBy($order_by);

            }

            if (strtoupper($order_direction) == 'DESC') {

                $query = $query->sortByDesc($order_by);
            }
            $query = $this->paginate($query, $perPage);

            return new \App\Http\Resources\ApiCollection($query);
        }
        catch (\Exception $e){
            return new \App\Http\Resources\ApiCollection([]);

        }



    }

    /**
     * Display the specified resource.
     *
     * @param $establecimiento
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Get(
     *     path="/establecimientos/{establecimiento}",
     *     summary="Obtiene la informacion de un establecimiento de salud en particular",
     *     operationId="obtenerEstablecimientoPorCodigo",
     *     tags={"Establecimientos de Salud"},
     *     @OA\Parameter(
     *         description="codigo del establecimiento de salud",
     *         in="path",
     *         name="codestabl",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: medicos,municipio",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Establecimiento encontrado",
     *         @OA\JsonContent(ref="#/components/schemas/EstablecimientoMedicos")
     *     ),
     * )
     */
    public function obtenerEstablecimiento($establecimiento,Request $request){
        $query=Establecimiento::where('codestabl',$establecimiento)->orderBy('idgestion','DESC')->first();
        $include = explode(',', $request->input('include', ''));
        if (in_array('municipio', $include)) {
            $query->municipio=$query->municipio;
        }
            if (in_array('medicos', $include)) {
            $codigo=$query->codestabl;
            $personas=Persona::whereHas('user',function($query)use($codigo){
                $query->where('nivel','establ')->where('idnivel',$codigo);
            })->with('user')->get();
            $query->medicos = $personas;

        }
        return new \App\Http\Resources\ApiResource($query);

    }
}
