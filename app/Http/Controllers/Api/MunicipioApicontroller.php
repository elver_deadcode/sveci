<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Municipio;
use Illuminate\Http\Request;
use Omnio\Http\Resources\ApiCollection;
use Omnio\Http\Resources\ApiResource;

class MunicipioApicontroller extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Municipio $municipio
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Get(
     *     path="/municipios/{municipio}",
     *     summary="Obtiene la informacion de un municipio en particular pasandole en codigo del municipio",
     *     operationId="obtenerMunicipioPorCodigo",
     *     tags={"Municipios"},
     *     @OA\Parameter(
     *         description="codigo del municipio",
     *         in="path",
     *         name="mnc_codigo",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: provincia,establecimientos",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Municipio encontrado",
     *         @OA\JsonContent(ref="#/components/schemas/MunicipioConDEpartamentoEstablecimientos")
     *     ),
     * )
     */
    public function obtenerMunicipio(Municipio $municipio, Request $request)
    {

        $include = explode(',', $request->input('include', ''));
        if (in_array('provincia', $include)) {
            $municipio->provincia               = $municipio->provincia;
            $municipio->provincia->departamento = $municipio->provincia->departamento;
        }
        if (in_array('establecimientos', $include)) {
            $municipio->establecimientos = Municipio::obtenerEstablecimientosPorMunicipio($municipio->mnc_codigo);
        }
        return new \App\Http\Resources\ApiResource($municipio);
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return ApiCollection
     *
     * @OA\Get(
     *     path="/municipios/listar",
     *     summary="Obtener los Municipios",
     *     operationId="ListaDeMunicipios",
     *     tags={"Municipios"},
     *     @OA\Parameter(
     *     description="Codigo del departamento ejm: codigo=2 LAPAZ, si esta vacio lista los municipios de toda bolivia",
     *     name="departamento",
     *     required=false,
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         )
     * ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: provincia,establecimientos",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/order_by"),
     *     @OA\Parameter(ref="#/components/parameters/order_direction"),
     *     @OA\Parameter(ref="#/components/parameters/per_page"),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de Municipios",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/MunicipioConDEpartamentoEstablecimientos"),
     *             ),
     *             @OA\Property(
     *                 property="meta",
     *                 type="object",
     *                 ref="#/components/schemas/metadata",
     *             ),
     *         ),
     *     ),
     * )
     */
    public function listar(Request $request)
    {
        $query        = Municipio::query();
        $departamento = $request->input('departamento', '');

        if ($departamento != '') {
            $query = $query->whereHas('provincia', function ($query) use ($departamento) {
                $query->where('coddepto', $departamento);
            });

        }
        $include = explode(',', $request->input('include', ''));

        //load resources
        if (in_array('provincia', $include)) {
            $query->with('provincia.departamento');
        }
        $filter = $request->input('filter', '');

        if (!empty($filter)) {
            $filter = '%'.$filter.'%';
            $query->where(function ($query) use ($filter) {
                $query->Where('mnc_nombre', 'like', $filter)
                    ->orWhere('mnc_codigo', 'like', $filter);

            });
        }

        $perPage  = $request->input('per_page', $query->count());
        $response = $query->orderBy(
            $request->input('order_by', 'mnc_codigo'),
            $request->input('order_direction', 'ASC')
        )
            ->paginate($perPage);
        if (in_array('establecimientos', $include)) {
            foreach ($response as $municipio) {
                $municipio->establecimientos = Municipio::obtenerEstablecimientosPorMunicipio($municipio->mnc_codigo);
            }
        }


        return new \App\Http\Resources\ApiCollection($response);

    }
}
