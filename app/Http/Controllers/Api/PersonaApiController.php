<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiClientController;
use App\Http\Controllers\Controller;
use App\Persona;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Omnio\Http\Resources\ApiCollection;
use Omnio\Http\Resources\ApiResource;

class PersonaApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return ApiCollection
     *
     * @OA\Get(
     *     path="/personas/mujeres",
     *     summary="Obtener mujeres por nivel de ubicacion",
     *     operationId="ListaDeMujeresPorNivel",
     *     tags={"Personas"},
     *     @OA\Parameter(
     *     description="Nivel de localizacion:ejm(nacional,departamento,red,municipio,establecimiento)",
     *     name="nivel",
     *     required=true,
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         )
     * ),
     *      @OA\Parameter(
     *         description="codigo del nivel de localizacion ejm(1000 codigo del establecimiento, si el nivel es nacional dejar vacio)",
     *         in="query",
     *         name="codigo",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/order_by"),
     *     @OA\Parameter(ref="#/components/parameters/order_direction"),
     *     @OA\Parameter(ref="#/components/parameters/per_page"),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: user,establecimiento,departamento,municipio,red",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de Mujeres",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/PersonaMedicoConLocalizacion"),
     *             ),
     *             @OA\Property(
     *                 property="meta",
     *                 type="object",
     *                 ref="#/components/schemas/metadata",
     *             ),
     *         ),
     *     ),
     * )
     */

    public function obtenerMujeresPorNivel(Request $request)
    {
        $request->validate([
            'nivel' => 'required'
        ]);

        $query = Persona::whereHas('mujer', function ($query) {
            $query->whereNotNull('IdPersona');
        });

        $nivel = 'nacional';
        switch ($request->nivel) {
            case 'departamento':
                $nivel = 'dpt_codigo';
                break;
            case 'municipio';
                $nivel = 'mnc_codigo';
                break;
            case 'red';
                $nivel = 'are_codigo';
                break;
            case 'establecimiento';
                $nivel = 'codEstablecimiento';
                break;
        }
        if ($nivel != 'nacional') {

            $query->where($nivel, $request->codigo);
        }
        $includes = $request->input('include', 'user');
        foreach (array_filter(explode(',', $includes)) as $include) {
            if (in_array($include, Persona::INCLUDE)) {
                $query->with($include);
            }
        }

        $filter = $request->input('filter', '');
        if (!empty($filter)) {
            $filter = '%'.$filter.'%';
            $query->where(function ($query) use ($filter) {
                $query->Where('nombres', 'like', $filter)
                    ->orWhere('primerApellido', 'like', $filter)
                    ->orWhere('segundoApellido', 'like', $filter)
                    ->orWhere('numeroCarnet', 'like', $filter)
                    ->orWhere('fechaNacimiento', 'like', $filter)
                    ->orWhere('complemento', 'like', $filter);
            });
        }

        $order_by        = 'primerApellido';
        $order_direction = 'ASC';

        if ($request->has('order_by')) {
            $order_by = $request->input('order_by');
        }

        if ($request->has('order_direction')) {
            $order_direction = $request->input('order_direction');
        }

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->orderBy(
                $request->input('order_by', $order_by),
                $request->input('order_direction', $order_direction)
            )
                ->paginate($perPage);

        return new \App\Http\Resources\ApiCollection($response);

    }
    /**
     * Display the specified resource.
     *
     * @param Persona $persona
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Get(
     *     path="/personas/{persona}/mujer",
     *     summary="Obtiene la informacion de una mujer pasandole la id de persona",
     *     operationId="obtenerMujerPorIdPersona",
     *     tags={"Personas"},
     *     @OA\Parameter(
     *         description="id de la persona",
     *         in="path",
     *         name="IdPersona",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: user,establecimiento,departamento,municipio,red",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Persona encontrada",
     *         @OA\JsonContent(ref="#/components/schemas/PersonaMedicoConLocalizacion")
     *     ),
     * )
     */

    public function obtenerMujer(Persona $persona, Request $request)
    {
        try {
            if ($persona->mujer == null) {
                throw new \Exception("Esta persona no esta registrada como mujer en el sistema", 404);

            }

            $include = explode(',', $request->input('include', ''));
            if (in_array('user', $include)) {
                $persona->user = $persona->user()->get();
            }
            if (in_array('establecimiento', $include)) {
                $persona->establecimiento = $persona->establecimiento()->get()->first();
            }
            if (in_array('municipio', $include)) {
                $persona->municipio = $persona->municipio()->get()->first();
            }
            if (in_array('red', $include)) {
                $persona->red = $persona->red()->get()->first();
            }
            if (in_array('departamento', $include)) {
                $persona->departamento = $persona->departamento()->get()->first();
            }
            return new \App\Http\Resources\ApiResource($persona);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], $e->getCode());
        }

    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return ApiCollection
     *
     * @OA\Get(
     *     path="/personas/medicos",
     *     summary="Obtener medicos por nivel de ubicacion",
     *     operationId="ListaDeMedicosPorNivel",
     *     tags={"Personas"},
     *     @OA\Parameter(
     *     description="Nivel de localizacion:ejm(nacional,departamento,red,municipio,establecimiento)",
     *     name="nivel",
     *     required=true,
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         )
     * ),
     *      @OA\Parameter(
     *         description="codigo del nivel de localizacion ejm(1000 codigo del establecimiento, si el nivel es nacional dejar vacio)",
     *         in="query",
     *         name="codigo",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/order_by"),
     *     @OA\Parameter(ref="#/components/parameters/order_direction"),
     *     @OA\Parameter(ref="#/components/parameters/per_page"),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: user,establecimiento,departamento,municipio,red",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de Personas medicos",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/PersonaMedicoConLocalizacion"),
     *             ),
     *             @OA\Property(
     *                 property="meta",
     *                 type="object",
     *                 ref="#/components/schemas/metadata",
     *             ),
     *         ),
     *     ),
     * )
     */
    public function obtenerMedicosPorNivel(Request $request)
    {
        $request->validate([
            'nivel' => 'required'
        ]);
        $query = Persona::whereHas('user', function ($query) {
            $query->where('nivel', 'establ');
        });
        $nivel = 'nacional';
        switch ($request->nivel) {
            case 'departamento':
                $nivel = 'dpt_codigo';
                break;
            case 'municipio';
                $nivel = 'mnc_codigo';
                break;
            case 'red';
                $nivel = 'are_codigo';
                break;
            case 'establecimiento';
                $nivel = 'codEstablecimiento';
                break;
        }
        if ($nivel != 'nacional') {

            $query->where($nivel, $request->codigo);
        }
        $includes = $request->input('include', 'user');
        foreach (array_filter(explode(',', $includes)) as $include) {
            if (in_array($include, Persona::INCLUDE)) {
                $query->with($include);
            }
        }

        $filter = $request->input('filter', '');
        if (!empty($filter)) {
            $filter = '%'.$filter.'%';
            $query->where(function ($query) use ($filter) {
                $query->Where('nombres', 'like', $filter)
                    ->orWhere('primerApellido', 'like', $filter)
                    ->orWhere('segundoApellido', 'like', $filter)
                    ->orWhere('numeroCarnet', 'like', $filter)
                    ->orWhere('fechaNacimiento', 'like', $filter)
                    ->orWhere('complemento', 'like', $filter);
            });
        }

        $order_by        = 'primerApellido';
        $order_direction = 'ASC';

        if ($request->has('order_by')) {
            $order_by = $request->input('order_by');
        }

        if ($request->has('order_direction')) {
            $order_direction = $request->input('order_direction');
        }

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->orderBy(
                $request->input('order_by', $order_by),
                $request->input('order_direction', $order_direction)
            )
                ->paginate($perPage);

        return new \App\Http\Resources\ApiCollection($response);


    }
    /**
     * Display the specified resource.
     *
     * @param Persona $persona
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Get(
     *     path="/personas/{persona}/medico",
     *     summary="Obtiene la informacion de un medico pasandole la id de persona",
     *     operationId="obtenerMedicoPorIdPersona",
     *     tags={"Personas"},
     *     @OA\Parameter(
     *         description="id de la persona",
     *         in="path",
     *         name="IdPersona",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Incluir relaciones complementarias separadas por comas: user,establecimiento,departamento,municipio,red",
     *         in="query",
     *         name="include",
     *         required=false,
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Persona encontrada",
     *         @OA\JsonContent(ref="#/components/schemas/PersonaMedicoConLocalizacion")
     *     ),
     * )
     */
    public function showMedico(Persona $persona, Request $request)
    {
        try {
            if ($persona->user == null) {
                throw new \Exception("Esta persona no esta registrada como medico", 404);

            }
            if ($persona->user->nivel != 'establ') {
                throw new \Exception("Esta persona no es medico de establecimiento de salud", 404);
            }
            $include = explode(',', $request->input('include', ''));
            if (in_array('user', $include)) {
                $persona->user = $persona->user()->get();
            }
            if (in_array('establecimiento', $include)) {
                $persona->establecimiento = $persona->establecimiento()->get()->first();
            }
            if (in_array('municipio', $include)) {
                $persona->municipio = $persona->municipio()->get()->first();
            }
            if (in_array('red', $include)) {
                $persona->red = $persona->red()->get()->first();
            }
            if (in_array('departamento', $include)) {
                $persona->departamento = $persona->departamento()->get()->first();
            }
            return new \App\Http\Resources\ApiResource($persona);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], $e->getCode());
        }

    }
    /**
     * Display the specified resource.
     *
     * @param Persona $persona
     * @param Request $request
     *
     * @return ApiResource
     *
     * @OA\Post(
     *     path="/personas/buscar",
     *     summary="busca a una persona en el SVEC si no lo encuentra la busca en en SUS",
     *     operationId="Buscar persona",
     *     tags={"Personas"},
     *     @OA\RequestBody(
     *      required=true,
     *     @OA\JsonContent(ref="#/components/schemas/BuscarPersonaRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Persona encontrada",
     *         @OA\JsonContent(ref="#/components/schemas/PersonaEditable")
     *     ),
     * )
     */
    public function buscarPersona(Request $request){
            $controller=new ApiClientController();
            return $controller->buscarPersonaApi($request);
    }

}
