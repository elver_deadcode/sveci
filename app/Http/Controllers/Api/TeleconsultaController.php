<?php

namespace App\Http\Controllers\Api;

use App\Establecimiento;
use App\Http\Controllers\Controller;
use App\Notificacion;
use App\NotificacionTeleconsulta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeleconsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $request->validate(
            [
                'codEstablecimiento' => 'required',
                'numeroTeleconsulta' => 'required|unique:notificacionTeleconsulta',
                'fechaTeleconsulta'  => 'required|date_format:Y-m-d H:i:s',
                'fechaNacimientoPaciente'  => 'date_format:Y-m-d',
                'estadoTeleconsulta' => ['required', 'in:confirmada,cancelada,atendida'],
                'nombresPaciente'    => 'required',
                'apellidosPaciente'  => 'required',
                'nombresMedico'      => 'required',
            ]
        );
        try {
            DB::beginTransaction();
            $establecimiento = Establecimiento::obtenerUltimoEstablecimiento(
                $request->codEstablecimiento
            );
            if ($establecimiento == null) {
                return response()->json(
                    [
                        'message' => 'no se encontro el establecimiento de salud'
                    ],
                    404
                );
            }
            $notificacionTeleconsulta = new NotificacionTeleconsulta();
            $notificacionTeleconsulta->fill($request->input());
            $notificacionTeleconsulta->saveOrFail();
            $notificacionTeleconsulta->notificable()->create(
                [
                    "notificable_id"     => $notificacionTeleconsulta->id,
                    "mensaje"            => "Se registro una notificacion de Teleconsulta en el sistema",
                    "codEstablecimiento" => $establecimiento->codestabl,
                    "tipo"               => "teleconsulta",
                    "prioridad"          => "normal",
                    "mnc_codgo"          => $establecimiento->codmunicip,
                    "dpt_codigo"         => $establecimiento->municipio->provincia->departamento->dpt_codigo,
                    "are_codigo"         => $establecimiento->codarea,
                    "idComunidad"        => 0,
                ]
            );
            DB::commit();

            return response()->json([], 204);
        }
        catch (\Exception $e){
            DB::rollBack();
            return response()->json([], 500);

        }
    }


    public function actualizarEstado($numeroTeleconsulta,Request $request){

        $request->validate(
            [
                'estadoTeleconsulta' => ['required', 'in:confirmada,cancelada,atendida'],
            ]
        );
        $teleconsulta=NotificacionTeleconsulta::where('numeroTeleconsulta',$numeroTeleconsulta)->first();

        if($teleconsulta==null){
            return response()->json([
                'message'=>'no se encontro la teleconsulta'
                                    ],404);
        }
        $teleconsulta->fill($request->input());
        $teleconsulta->saveOrFail();
         return response()->json([],204);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
