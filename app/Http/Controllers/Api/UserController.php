<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Omnio\Http\Resources\ApiCollection;
use Omnio\Http\Resources\ApiResource;
use Omnio\Models\Omnio\Warehouse;
use Throwable;

class UserController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return ApiResource
     *
     * @throws Throwable
     *
     * @OA\Post(
     *     path="/user/credentials",
     *     summary="autenticacion de un usuario medico",
     *     operationId="autenticacionUsuario",
     *     tags={"Usuarios"},
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\JsonContent(ref="#/components/schemas/UserCredentials")

    ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(ref="#/components/schemas/UserCredentialsResponse")
     *     ),
     * )
     */
    public function userCredentials(Request $request){
        $request->validate([
             'username' => 'required',
             'password' => 'required',
        ]);
        $user = User::where('username', $request->username)->with('persona')->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            //         return response()->json($request->all());

            return response()->json([
                'message' => 'Las credenciales  no son correctas.',
            ],404);
        }
        $user->localizacion=$user->obtenerLocalizacion();
        return response()->json(
            $user,200);


    }
}
