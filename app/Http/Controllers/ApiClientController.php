<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use App\Persona;

class ApiClientController extends Controller
{
    private $url_sus = 'http://suis-ws.minsalud.gob.bo/api/buscar_adscrito';



    public function buscarPersonaApi(Request $request)
    {
        $api = new Api();

        $numcarnet        = isset($request->num_carnet) ? $request->num_carnet : '0';
        $fecha_nacimiento = isset($request->fecha_nacimiento) ? $request->fecha_nacimiento : '';
        $complemento      = isset($request->complemento) ? $request->complemento : '';


//        if ($numcarnet > 0 && $fecha_nacimiento != '') {
        if ($numcarnet > 0 ) {

            $buscar_base_datos = Persona::where('numeroCarnet', $numcarnet)->first();
            if (!is_null($buscar_base_datos)) {
                
                $buscar_base_datos['origen']='svec';
                return response()->json([
                    'success' => true,
                    'data' => $buscar_base_datos

                ]);
            }

            $buscar_sus = $api->buscarCarnetSus($numcarnet, $fecha_nacimiento, $complemento);
            if ($buscar_sus['status'] == 200) {
                $buscar_sus['origen']='sus';

                return response()->json([
                    'success' => true,
                    'data' => $buscar_sus['response']

                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'data' => $buscar_sus['message'],
                    'status' => 500
                ]);
            }
        } else {

            return response()->json([
                'success' => false,
                'data' => 'datos invalidos',
                'status' => 500
            ]);
        }
    }
}
