<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\Mujer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Persona;
use App\NotificacionEmbarazo;
use App\NotificacionMuerteBebe;
use App\NotificacionMuerteMujer;
use App\NotificacionParto;
use App\Vigilante;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ApiController extends Controller
{




    public function activarTelefono(Request $request)
    {

        $status  = false;
        $msg     = "Ocurrio un error";
        $codigo  = "";
        $rcodigo = trim(strtoupper($request->codigo));
        // \Log::error($request->codigo);

        try {

            DB::beginTransaction();

            if ($rcodigo === "" || !isset($rcodigo) || is_null($rcodigo)) {
                $status = false;
                $msg    = "El codigo que envio no se encuentra registrado. Intente nuevamente Porfavor";
            } else {

                $vigilante = \App\Vigilante::where('codigo', $rcodigo)->first();
                if (is_null($vigilante)) {
                    $status = false;
                    $msg    = "El codigo que envio es invalido. Intente nuevamente Porfavor";
                } else {

                    if ($vigilante->estado == false) {
                        $status = false;
                        $msg    = "Este codigo pertenece a un vigilante que esta dado de baja en el sistema.";
                    }

                    $status                     = true;
                    $vigilante->activado        = true;
                    $vigilante->fechaActivacion = date("Y-m-d");
                    $codigo                     = $vigilante->codigo;
                    $vigilante->save();
                    $msg = "El telefono se activo correctamente";


                }
            }


           try{
                Auditoria::guardar($vigilante->IdVigilante,65,'aplicacion android','activar celular','','boton activar',1,$request,'vigilantes');
           }
            catch (\Exception $e){}
            DB::commit();


            return response()->json([
                'status'  => $status,
                'mensaje' => $msg,
                'codigo'  => $codigo
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'status'  => false,
                'mensaje' => "Error en la activacion",
                'codigo'  => $codigo
            ]);
        }
    }


    public function guadarUnaNotificacionEmbarazo(Request $request,$notificacion, $codigo)
    {


        try {
            DB::beginTransaction();

            $noti_id                   = $notificacion["id"];
            $tipo                      = $notificacion["tipo"];
            $fecha                     = $notificacion["fecha"];
            $nombres                   = $notificacion["nombres"];
            $apellidos                 = $notificacion["apellidos"];
            $direccion                 = $notificacion["direccion"];
            $telefono                  = $notificacion["telefono"];
            $edad                      = $notificacion["edad"];
            $latitud                   = $notificacion["latitud"];
            $longitud                  = $notificacion["longitud"];
            $mujer_id                  = $notificacion["mujer_id"];
            $codigo_vigilante          = $codigo;
            $notificacion['IdAndroid'] = $noti_id;

            $return = [];
            $noti   = \App\NotificacionEmbarazo::where('IdAndroid', $noti_id)->first();
            if (!is_null($noti)) {
                // \Log::debug('IdAndroid=>' . $noti_id);

                DB::rollBack();
                return [
                    "status"  => true,
                    "mensaje" => "La notificacion de Embarazo se ha registrado correctamente",
                    "data"    => $noti->toArray()
                ];
                // throw new Exception('Ya Existe una notificacion de embarazo para esta persona');
            }
            if (!is_null($mujer_id) && $mujer_id != '') {


                if (NotificacionEmbarazo::validarNotificacionMismoAnioAndroid($mujer_id)) {

                    throw new Exception('Ya Existe una notificacion  para esta persona este año');
                }
                if (NotificacionMuerteMujer::existeNotificacionMuerteAndroid($mujer_id)) {
                    throw new Exception('Ya Existe una notificacion de muerte de mujer para esta persona');
                }
            } else {
                throw new Exception("No se puede recuperar  a la mujer del celular . intente nuevamente, o  borre la notificacion en su celular y registre otra",
                    1);

            }

            $vigilante = \App\Vigilante::where('codigo', $codigo_vigilante)->first();


            if (!is_null($vigilante) && $vigilante->estado == false) {

                throw new Exception('Usted esta dado de baja del sistema no puede enviar Informacion. comuniquese con su centro de salud');
            } else {
                if (is_null($vigilante)) {
                    throw new Exception('El codigo de su telefono es incorrecto');
                } else {
                    if (!is_null($vigilante)) {
                        $cod_establecimiento = $vigilante->persona->codEstablecimiento;
                    } else {
                        $cod_establecimiento = "";
                    }
                }
            }

            $noti                     = new NotificacionEmbarazo();
            $noti->IdAndroid          = $noti_id;
            $noti->codVigilante       = $codigo_vigilante;
            $noti->codEstablecimiento = $cod_establecimiento;
            $noti->estado             = "recibido";
            $noti->nombres            = $nombres;
            $noti->primerApellido     = $apellidos;
            $noti->segundoApellido    = "";
            $noti->direccion          = $direccion;
            $noti->telefono           = "";
            $noti->latitud            = $latitud;
            $noti->longitud           = $longitud;
            $noti->edad               = $edad;
            $noti->semanasEmbarazo    = 0;


            if (!is_null($fecha) && $fecha != '') {
                try {
                    $noti->fechaRegistro = $fecha;
                } catch (Exception $e) {
                    $noti->fechaRegistro = date('Y-m-d H:i:s');
                }
            }
            $noti->idMujerAndroid = $mujer_id;
            $noti->save();
            $noti->notificable()->create([
                "notificable_id"     => $noti->id,
                "mensaje"            => "Se registro una notificacion de Embarazo en el sistema",
                "codEstablecimiento" => $noti->codEstablecimiento,
                "tipo"               => "android",
                "prioridad"          => "normal",
                "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                "are_codigo"         => $vigilante->persona->are_codigo,
                "idComunidad"        => $vigilante->persona->IdComunidad,
                "idVigilante"        => $vigilante->IdVigilante
            ]);

            try{
                Auditoria::guardar($vigilante->IdVigilante,66,'aplicacion android','notificar mujer embarazada','','boton enviar',1,$request,'vigilantes');
            }
            catch (\Exception $e){}
            DB::commit();
            return $return = [
                "status"  => true,
                "mensaje" => "La notificacion de Embarazo se ha registrado correctamente",
                "data"    => $noti->toArray(),
                "evento"  => "inserto"
            ];
        } catch (\Exception $e) {

            DB::rollBack();
            return $return = [
                "status"  => false,
                "mensaje" => "error::".$e->getMessage(),
                "data"    => $notificacion
            ];
        }
    }

    public function notificarEmbarazo(Request $request)
    {


        $notificacion = [];

        $notificacion["tipo"]      = trim($request->tipo);
        $notificacion["fecha"]     = trim($request->fecha);
        $notificacion["nombres"]   = strtoupper(trim($request->nombres));
        $notificacion["apellidos"] = strtoupper(trim($request->apellidos));
        $notificacion["direccion"] = strtoupper(trim($request->direccion));
        $notificacion["telefono"]  = trim($request->telefono);
        $notificacion["edad"]      = trim($request->edad);
        $notificacion["latitud"]   = trim($request->latitud);
        $notificacion["longitud"]  = trim($request->longitud);
        $notificacion["mujer_id"]  = trim($request->mujer_id);
        $codigo                    = trim($request->codigo_vigilante);
        $notificacion["id"]        = trim($request->id);


        return response()->json($this->guadarUnaNotificacionEmbarazo($request,$notificacion, $codigo));
    }

    public function guardarUnaNotificacionMuerteMujer(Request $request,$notificacion, $codigo)
    {


        try {

            DB::beginTransaction();
            $embarazada                = $notificacion["embarazada"];
            $nombres                   = $notificacion["nombres"];
            $fecha                     = $notificacion["fecha"];
            $apellidos                 = $notificacion["apellidos"];
            $direccion                 = $notificacion["direccion"];
            $telefono                  = $notificacion["telefono"];
            $edad                      = $notificacion["edad"];
            $latitud                   = $notificacion["latitud"];
            $longitud                  = $notificacion["longitud"];
            $mujer_id                  = $notificacion["mujer_id"];
            $id                        = $notificacion["id"];
            $codigo_vigilante          = $codigo;
            $notificacion['IdAndroid'] = $id;

            $return = [];
            if (!is_null($id) && $id != '') {

                $noti = \App\NotificacionMuerteMujer::where('IdAndroid', $id)->first();


                if (!is_null($noti)) {
                    DB::commit();
                    return $return = [
                        "status"  => true,
                        "mensaje" => "Ya Existe una notificacion  para esta persona",
                        "data"    => $noti->toArray()
                    ];
                }
            }


            if (!is_null($mujer_id) && $mujer_id != '') {


                $noti = \App\NotificacionMuerteMujer::where('idMujerAndroid', $mujer_id)->orWhere('IdAndroid',
                    $id)->first();
                if (!is_null($noti)) {
                    throw new Exception("Ya Existe una notificacion de muerte  para esta persona", 1);
                }
            } else {
                throw new Exception("No se puede recuperar  a la mujer del celular . intente nuevamente, o  borre la notificacion en su celular y registre otra",
                    1);

            }


            $vigilante = \App\Vigilante::where('codigo', $codigo_vigilante)->first();

            if (!is_null($vigilante) && $vigilante->estado == false) {
                throw new Exception("Usted esta dado de baja del sistema no puede enviar Informacion. comuniquese con su centro de salud",
                    1);
            } else {
                if (is_null($vigilante)) {
                    throw new Exception("El codigo de su telefono es incorrecto", 1);
                } else {
                    if (!is_null($vigilante)) {
                        $cod_establecimiento = $vigilante->persona->codEstablecimiento;
                    } else {
                        $cod_establecimiento = "";
                    }
                }
            }
            $noti                     = new \App\NotificacionMuerteMujer();
            $noti->IdAndroid          = $id;
            $noti->codVigilante       = $codigo_vigilante;
            $noti->codEstablecimiento = $cod_establecimiento;
            $noti->estado             = "recibido";
            $noti->nombres            = $nombres;
            $noti->primerApellido     = $apellidos;
            $noti->segundoApellido    = "";
            $noti->direccion          = $direccion;
            $noti->telefono           = "";
            $noti->latitud            = $latitud;
            $noti->longitud           = $longitud;
            $noti->edad               = $edad;
            $noti->embarazada         = $embarazada;

            if (!is_null($fecha) && $fecha != '') {
                try {
                    $noti->fechaRegistro = $fecha;
                } catch (\Exception $e) {

                    $noti->fechaRegistro = date('Y-m-d H:i:s');
                }
            }
            $noti->idMujerAndroid = $mujer_id;
            $noti->IdAndroid;
            $noti->save();
            $noti->notificable()->create([


                "notificable_id"     => $noti->id,
                "mensaje"            => "Se registro Nueva notificacion de Muerte de una Mujer  en el sistema",
                "codEstablecimiento" => $noti->codEstablecimiento,
                "tipo"               => "android",
                "prioridad"          => "normal",
                "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                "are_codigo"         => $vigilante->persona->are_codigo,
                "idComunidad"        => $vigilante->persona->IdComunidad,
                "idVigilante"        => $vigilante->IdVigilante
            ]);
            try{

            Auditoria::guardar($vigilante->IdVigilante,67,'aplicacion android','notificar muerte mujer','','boton enviar',1,$request,'vigilantes');
            }
            catch(\Exception $e){}

            DB::commit();

            return $return = [
                "status"  => true,
                "mensaje" => "La notificacion  se ha registrado correctamente",
                "data"    => $noti->toArray(),
                "evento"  => "inserto"
            ];
        } catch (\Exception $e) {

            DB::rollBack();
            return $return = [
                "status"  => false,
                "mensaje" => $e->getMessage(),
                "data"    => $notificacion
            ];
        }
    }

    public function notificarMuerteMujer(Request $request)
    {


        $notificacion = [];

        $notificacion["embarazada"] = trim($request->embarazada);
        $notificacion["nombres"]    = strtoupper(trim($request->nombres));
        $notificacion["fecha"]      = trim($request->fecha);
        $notificacion["apellidos"]  = strtoupper(trim($request->apellidos));
        $notificacion["direccion"]  = strtoupper(trim($request->direccion));
        $notificacion["telefono"]   = trim($request->telefono);
        $notificacion["edad"]       = trim($request->edad);
        $notificacion["latitud"]    = trim($request->latitud);
        $notificacion["longitud"]   = trim($request->longitud);
        $notificacion["mujer_id"]   = trim($request->mujer_id);
        $notificacion["id"]         = trim($request->id);
        $codigo = trim($request->codigo_vigilante);

        return response()->json($this->guardarUnaNotificacionMuerteMujer($request,$notificacion, $codigo));
    }


    public function guardarUnaNotificacionMuerteBebe(Request $request,$notificacion, $codigo)
    {


        try {

            DB::beginTransaction();
            $notificacion_id           = $notificacion["id"];
            $nacido                    = $notificacion["nacido"];
            $fecha                     = $notificacion["fecha"];
            $nombres                   = $notificacion["nombres"];
            $apellidos                 = $notificacion["apellidos"];
            $direccion                 = $notificacion["direccion"];
            $telefono                  = $notificacion["telefono"];
            $edad                      = $notificacion["edad"];
            $latitud                   = $notificacion["latitud"];
            $longitud                  = $notificacion["longitud"];
            $mujer_id                  = $notificacion["mujer_id"];
            $codigo_vigilante          = $codigo;
            $notificacion['IdAndroid'] = $notificacion_id;
            $result                    = [];

            if (!is_null($notificacion_id) && $notificacion_id != '') {

                $noti = \App\NotificacionMuerteBebe::where('IdAndroid', $notificacion_id)->first();
                if (!is_null($noti)) {
                    DB::commit();

                    return [
                        "status"  => true,
                        "mensaje" => "Ya Existe una notificacion para esta persona",
                        "data"    => $noti->toArray(),
                        "evento"  => "inserto"
                    ];
                }
            }

            if (!is_null($mujer_id) && $mujer_id != '') {
                if (NotificacionMuerteMujer::existeNotificacionMuerteAndroid($mujer_id)) {

                    throw new Exception("Esta persona tiene una notificacion de muerte", 1);
                }
                $noti = \App\NotificacionMuerteBebe::where('idMujerAndroid', $mujer_id)->first();
                if (!is_null($noti)) {
                    throw new Exception("Ya Existe una notificacion para esta persona", 1);
                }
            } else {
                throw new Exception("No se puede recuperar  a la mujer del celular . intente nuevamente, o  borre la notificacion en su celular y registre otra",
                    1);

            }


            $vigilante = \App\Vigilante::where('codigo', $codigo_vigilante)->first();

            if (!is_null($vigilante) && $vigilante->estado == false) {

                throw new Exception("Usted esta dado de baja del sistema no puede enviar Informacion. comuniquese con su centro de salud",
                    1);
            } else {
                if (is_null($vigilante)) {

                    throw new Exception("El codigo de su telefono es incorrecto", 1);
                } else {
                    if (!is_null($vigilante)) {
                        $cod_establecimiento = $vigilante->persona->codEstablecimiento;
                    } else {
                        $cod_establecimiento = "";
                    }
                }
            }
            $noti                     = new \App\NotificacionMuerteBebe();
            $noti->IdAndroid          = $notificacion_id;
            $noti->codVigilante       = $codigo_vigilante;
            $noti->codEstablecimiento = $cod_establecimiento;
            $noti->estado             = "recibido";
            $noti->nombres            = $nombres;
            $noti->primerApellido     = $apellidos;
            $noti->segundoApellido    = "";

            $noti->direccion = $direccion;
            $noti->telefono  = "";
            $noti->latitud   = $latitud;
            $noti->longitud  = $longitud;
            $noti->edad      = $edad;
            $noti->nacido    = $nacido;


            if (!is_null($fecha) && $fecha != '') {

                try {
                    $noti->fechaRegistro = $fecha;
                } catch (\Exception $e) {
                    $noti->fechaRegistro = date('Y-m-d H:i:s');
                }
            }
            $noti->idMujerAndroid = $mujer_id;
            $noti->save();
            $noti->notificable()->create([


                "notificable_id"     => $noti->id,
                "mensaje"            => "Se registro Nueva notificacion de Muerte de un Bebe  en el sistema",
                "codEstablecimiento" => $noti->codEstablecimiento,
                "tipo"               => "android",
                "prioridad"          => "normal",
                "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                "are_codigo"         => $vigilante->persona->are_codigo,
                "idComunidad"        => $vigilante->persona->IdComunidad,
                "idVigilante"        => $vigilante->IdVigilante
            ]);
            try{
                Auditoria::guardar($vigilante->IdVigilante,68,'aplicacion android','notificar  muerte bebe','','boton enviar',1,$request,'vigilantes');
            }
            catch (\Exception $e){}

            DB::commit();
            return $result = [
                "status"  => true,
                "mensaje" => "La notificacion  se ha registrado correctamente",
                "data"    => $noti->toArray(),
                "evento"  => "inserto"
            ];
        } catch (\Exception $e) {

            DB::rollBack();
            return $result = [
                "status"  => false,
                "mensaje" => $e->getMessage(),
                "data"    => $notificacion
            ];
        }
    }

    public function notificarMuerteBebe(Request $request)
    {

        $notificacion = [];

        $notificacion["id"]        = trim($request->id);
        $notificacion["nacido"]    = trim($request->nacido);
        $notificacion["fecha"]     = trim($request->fecha);
        $notificacion["nombres"]   = strtoupper(trim($request->nombres));
        $notificacion["apellidos"] = strtoupper(trim($request->apellidos));
        $notificacion["direccion"] = strtoupper(trim($request->direccion));
        $notificacion["telefono"]  = trim($request->telefono);
        $notificacion["edad"]      = trim($request->edad);
        $notificacion["latitud"]   = trim($request->latitud);
        $notificacion["longitud"]  = trim($request->longitud);
        $notificacion["mujer_id"]  = trim($request->mujer_id);
        $codigo                    = trim($request->codigo_vigilante);

        return response()->json($this->guardarUnaNotificacionMuerteBebe($request,$notificacion, $codigo));
    }

    public function guardarUnaNotificacionParto(Request $request,$notificacion, $codigo)
    {
        try {

            DB::beginTransaction();
            $notificacion_id           = $notificacion["id"];
            $atendido                  = $notificacion["atendido_por"];
            $fecha                     = $notificacion["fecha"];
            $nombres                   = $notificacion["nombres"];
            $apellidos                 = $notificacion["apellidos"];
            $direccion                 = $notificacion["direccion"];
            $telefono                  = $notificacion["telefono"];
            $edad                      = $notificacion["edad"];
            $latitud                   = $notificacion["latitud"];
            $longitud                  = $notificacion["longitud"];
            $mujer_id                  = $notificacion["mujer_id"];
            $codigo_vigilante          = $codigo;
            $notificacion['IdAndroid'] = $notificacion_id;

            $result = [];

            if (!is_null($notificacion_id) && $mujer_id != '') {


                $noti = \App\NotificacionParto::where('IdAndroid', $notificacion_id)->first();
                if (!is_null($noti)) {
                    DB::commit();
                    return [
                        "status"  => true,
                        "mensaje" => "Ya Existe una notificacion de  para esta persona",
                        "data"    => $noti->toArray(),
                        "evento"  => "inserto"
                    ];
                }
            }

            if (!is_null($mujer_id) && $mujer_id != '') {
                if (NotificacionMuerteMujer::existeNotificacionMuerteAndroid($mujer_id)) {

                    throw new Exception("Esta persona ya  tiene una notificacion de muerte", 1);
                }

                $noti = \App\NotificacionParto::where('idMujerAndroid', $mujer_id)->first();
                if (!is_null($noti)) {
                    throw new Exception("Ya Existe una notificacion de parto  para esta persona", 1);
                }
            } else {
                throw new Exception("No se puede recuperar  a la mujer del celular . intente nuevamente, o  borre la notificacion en su celular y registre otra",
                    1);

            }

            $vigilante = \App\Vigilante::where('codigo', $codigo_vigilante)->first();

            if (!is_null($vigilante) && $vigilante->estado == false) {

                throw new Exception("Usted esta dado de baja del sistema no puede enviar Informacion. comuniquese con su centro de saludt",
                    1);
            } else {
                if (is_null($vigilante)) {
                    throw new Exception("El codigo de su telefono es incorrecto", 1);
                } else {
                    if (!is_null($vigilante)) {
                        $cod_establecimiento = $vigilante->persona->codEstablecimiento;
                    } else {
                        $cod_establecimiento = "";
                    }
                }
            }
            $noti                     = new \App\NotificacionParto();
            $noti->IdAndroid          = $notificacion_id;
            $noti->codVigilante       = $codigo_vigilante;
            $noti->codEstablecimiento = $cod_establecimiento;
            $noti->estado             = "recibido";
            $noti->nombres            = $nombres;
            $noti->primerApellido     = $apellidos;
            $noti->segundoApellido    = "";

            $noti->direccion = $direccion;
            $noti->telefono  = "";
            $noti->latitud   = $latitud;
            $noti->longitud  = $longitud;
            $noti->edad      = $edad;
            $noti->atendido  = $atendido;


            if (!is_null($fecha) && $fecha != '') {
                try {
                    $noti->fechaRegistro = $fecha;
                } catch (Exception $e) {
                    $noti->fechaRegistro = date('Y-m-d H:i:s');
                }
            }
            $noti->idMujerAndroid = $mujer_id;
            $noti->save();
            $noti->notificable()->create([


                "notificable_id"     => $noti->id,
                "mensaje"            => "Se registro Nueva notificacion de Parto  en el sistema",
                "codEstablecimiento" => $noti->codEstablecimiento,
                "tipo"               => "android",
                "prioridad"          => "normal",
                "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                "are_codigo"         => $vigilante->persona->are_codigo,
                "idComunidad"        => $vigilante->persona->IdComunidad,
                "idVigilante"        => $vigilante->IdVigilante
            ]);
            try{
                Auditoria::guardar($vigilante->IdVigilante,69,'aplicacion android','notificar parto','','boton enviar',1,$request,'vigilantes');
            }
            catch (\Exception $e){}

            DB::commit();

            return $result = [
                "status"  => true,
                "mensaje" => "La notificacion  se ha registrado correctamente",
                "data"    => $noti->toArray(),
                "evento"  => "inserto"
            ];
        } catch (\Exception $e) {

            DB::rollBack();

            return $result = [
                "status"  => false,
                "mensaje" => $e->getMessage(),
                "data"    => $notificacion
            ];
        }
    }

    public function notificarParto(Request $request)
    {


        $notificacion                 = [];
        $notificacion["id"]           = trim($request->id);
        $notificacion["atendido_por"] = trim($request->atendido_por);
        $notificacion["fecha"]        = trim($request->fecha);
        $notificacion["nombres"]      = strtoupper(trim($request->nombres));
        $notificacion["apellidos"]    = strtoupper(trim($request->apellidos));
        $notificacion["direccion"]    = strtoupper(trim($request->direccion));
        $notificacion["telefono"]     = trim($request->telefono);
        $notificacion["edad"]         = trim($request->edad);
        $notificacion["latitud"]      = trim($request->latitud);
        $notificacion["longitud"]     = trim($request->longitud);
        $notificacion["mujer_id"]     = trim($request->mujer_id);
        $codigo                       = trim($request->codigo_vigilante);

        return response()->json($this->guardarUnaNotificacionParto($request,$notificacion, $codigo));
    }

    private function guardarUnaMujer(Request $request,$datos_mujer, $codigo)
    {

        try {

            \DB::beginTransaction();
            $foto            = $datos_mujer['foto'];
            $nombres         = $datos_mujer['nombres'];
            $apellidomaterno = $datos_mujer['apellidoMaterno'];
            $apellidopaterno = $datos_mujer['apellidoPaterno'];
            $fechanacimiento = $datos_mujer['fechanacimiento'];
            $carnet          = $datos_mujer['carnet'];

            $direccion                = $datos_mujer['direccion'];
            $telefono                 = $datos_mujer['telefono'];
            $codigo_vigilante         = $codigo;
            $idAndroid                = $datos_mujer['id'];
            $edit                     = $datos_mujer['edit'];
            $adscrito                 = $datos_mujer['adscrito'];
            $complemento              = $datos_mujer['complemento'];
            $return                   = [];
            $datos_mujer['idAndroid'] = $idAndroid;

            // \Log::debug('mujer=>' . $idAndroid);


            if (is_null($codigo_vigilante) || $codigo_vigilante == '') {

                throw new Exception("Su telefono no esta activado", 1);
            } else {

                $vigilante = \App\Vigilante::where('codigo', $codigo_vigilante)->first();
                if (!is_null($vigilante)) {
                    if ($vigilante->estado == false) {
                        throw new Exception("Usted esta dado de baja en el sistema, no puede enviar informacion. Contactese con el centro de Salud",
                            1);
                    } else {

                        $mujer = \App\Mujer::where('idAndroid', $idAndroid)->first();

                        if (!is_null($mujer)) {
                            DB::commit();
                            return [
                                'status'  => true,
                                'mensaje' => "Este registro  de mujer ya se encuentra en el sistema",
                                'data'    => $mujer->toArray()
                            ];
                        }

                        if ($edit == 0) {


                            $persona = Persona::where('numeroCarnet', $carnet)->first();
                            if (!is_null($persona) && $carnet != '') {
                                $msg = "Esta Persona con carnet ".$carnet." Ya se encuentra registrado en el sistema";
                                throw new Exception($msg, 1);
                            }
                            $persona = new Persona();

                            $persona->nombres            = $nombres;
                            $persona->primerApellido     = $apellidopaterno;
                            $persona->segundoApellido    = $apellidomaterno;
                            $persona->numeroCarnet       = $carnet;
                            $persona->complemento        = $complemento;
                            $persona->nacionalidad       = 'BOlIVIANA';
                            $persona->direccion          = $direccion;
                            $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                                $fechanacimiento)->format('Y-m-d');
                            $persona->sexo               = 'F';
                            $persona->codEstablecimiento = $vigilante->persona->codEstablecimiento;
                            $persona->mnc_codigo         = $vigilante->persona->mnc_codigo;
                            $persona->dpt_codigo         = $vigilante->persona->dpt_codigo;
                            $persona->are_codigo         = $vigilante->persona->are_codigo;
                            $persona->IdComunidad        = $vigilante->persona->IdComunidad;
                            $persona->celular            = $telefono;
                            if ($adscrito != 0) {
                                $persona->verificado  = true;
                                $persona->adscrito_id = $adscrito;
                            } else {
                                $persona->verificado = false;
                            }

                            $persona->save();

                            $mujer              = new \App\Mujer();
                            $mujer->IdVigilante = $vigilante->IdVigilante;
                            $mujer->estado      = true;
                            $mujer->idAndroid   = $idAndroid;
                            $mujer->IdUsuario   = 0;
                            $mujer->IdPersona   = $persona->IdPersona;
                            $mujer->save();

                            $status = true;
                            $msg    = "El registro de la persona se guardo correctamente en el servidor.";
                            // \Log::debug('mujer=>' . $mujer->id);
                            $return = [
                                'status'  => $status,
                                'mensaje' => $msg,
                                'data'    => [
                                    'action'    => 'insertado',
                                    'persona'   => $persona->toArray(),
                                    'IdAndroid' => $mujer->idAndroid
                                ]
                            ];
                            try{
                                Auditoria::guardar($vigilante->IdVigilante,71,'aplicacion android','mujeres','registrar mujeres','boton guardar',1,$request,'vigilantes');
                            }
                            catch (\Exception $e){}
                            DB::commit();
                            return $return;
                        } // finde edit==0?
                        elseif ($edit == 1) {

                            $mujer = \App\Mujer::where('idAndroid', $idAndroid)->first();


                            if (!is_null($mujer)) {
                                $persona = $mujer->persona;
                                if (!is_null($persona) && !$persona->verficado) {
                                    $persona->nombres         = $nombres;
                                    $persona->primerApellido  = $apellidopaterno;
                                    $persona->segundoApellido = $apellidomaterno;
                                    $persona->numeroCarnet    = $carnet;
                                    $persona->complemento     = $complemento;
                                    $persona->direccion       = $direccion;
                                    $persona->celular         = $telefono;
                                    $persona->fechaNacimiento = Carbon::createFromFormat('d/m/Y',
                                        $fechanacimiento)->format('Y-m-d');
                                    $persona->save();
                                    DB::commit();

                                    $status = true;
                                    $msg    = "El registro de la persona se guardo correctamente en el servidor.";
                                    $return = [
                                        'status'  => $status,
                                        'mensaje' => $msg,
                                        'data'    => $mujer->toArray()
                                    ];

                                    return $return;
                                } else {
                                    throw new Exception("la persona ya esta verificada", 1);


                                }
                            }
                        }
                    } //fin de else
                } else {
                    throw new Exception("El codigo de su telefono es incorrecto", 1);


                }
            }
        } catch (\Exception $e) {

            DB::rollBack();
            $return = [
                'status'  => false,
                'mensaje' => $e->getMessage(),
                "data"    => $datos_mujer

            ];
            return $return;
        }
    }


    public function registrarMujer(Request $request)
    {
        $mujer                    = [];
        $mujer["foto"]            = is_null($request->foto) ? "" : strtolower(trim($request->foto));
        $mujer["nombres"]         = is_null($request->nombres) ? "" : strtoupper(trim($request->nombres));
        $mujer["apellidoMaterno"] = is_null($request->apellidomaterno) ? "" : strtoupper(trim($request->apellidomaterno));
        $mujer["apellidoPaterno"] = is_null($request->apellidopaterno) ? "" : strtoupper(trim($request->apellidopaterno));
        $mujer["fechanacimiento"] = is_null($request->fechanacimiento) ? "" : strtoupper(trim($request->fechanacimiento));
        $mujer["carnet"]          = is_null($request->carnet) ? "" : strtoupper(trim($request->carnet));
        $mujer["direccion"]       = is_null($request->direccion) ? "" : strtoupper(trim($request->direccion));
        $mujer["telefono"]        = is_null($request->telefono) ? "" : strtoupper(trim($request->telefono));
        $mujer['edit']            = is_null($request->edit) ? 0 : $request->edit;
        $codigo                   = is_null($request->codigo_vigilante) ? "" : strtoupper(trim($request->codigo_vigilante));
        $mujer["id"]              = trim($request->idmujer);
        $mujer["complemento"]     = trim($request->complemento);
        $mujer["adscrito"]        = $request->adscrito;


        $result = $this->guardarUnaMujer($request,$mujer, $codigo);

        return response()->json($result);
    }




    public function recuperarRegistrosPorVigilante(Request $request)
    {

        $codigo_vigilante           = $request->codigo;
        $registros_mujeres          = [];
        $notificaciones_embarazo    = [];
        $notificaciones_muertemujer = [];
        $notificaciones_muertebebe  = [];
        $notificaciones_parto       = [];
        try {
            if (!is_null($codigo_vigilante) && $codigo_vigilante != '') {
                $vigilante = Vigilante::where('codigo', $codigo_vigilante)->first();

                if (!is_null($vigilante) && $vigilante->estado && $vigilante->activado) {
                    $registros_mujeres          = $this->obtenerRegistrosMujeresPorVigilante($vigilante->IdVigilante);
                    $notificaciones_embarazo    = $this->obtenerRegistrosNotificacionesEmbarazoPorVigilante($vigilante->codigo);
                    $notificaciones_muertemujer = $this->obtenerRegistrosNotificacionesMuerteMujerPorVigilante($vigilante->codigo);
                    $notificaciones_muertebebe  = $this->obtenerRegistrosNotificacionesMuerteBebePorVigilante($vigilante->codigo);
                    $notificaciones_parto       = $this->obtenerRegistrosNotificacionesPartoPorVigilante($vigilante->codigo);
                    return [
                        'status'                     => true,
                        'registros_mujeres'          => $registros_mujeres,
                        'notificaciones_embarazo'    => $notificaciones_embarazo,
                        'notificaciones_muertemujer' => $notificaciones_muertemujer,
                        'notificaciones_muertebebe'  => $notificaciones_muertebebe,
                        'notificaciones_parto'       => $notificaciones_parto
                    ];
                }
                throw new Exception("Este vigilante no esta habilitado en el sistema", 1);
            }
            throw new Exception("No existe registrado este Vigilante en el sistema", 1);
        } catch (Exception $e) {
            return [
                'status' => false,
                'error'  => $e->getMessage()
            ];
        }
    }
    private function buscarCarnetPorCodigoVigilante($codigo){
                 $numcarnet= substr($codigo, 3,strlen($codigo) );
                 $persona=Persona::where('numeroCarnet',$numcarnet)->first();
                 if($persona!=null)
                 return true;
                 else
                 return $numcarnet;
       }

    public function Sincronizar(Request $request)
    {
     
        $existeElVigilante=true;
        
        try {

            $codigo                      = trim($request->codigo);
            $mujeres                     = json_decode($request->mujeres, true);
            $notificaciones_embarazos    = json_decode($request->notificacion_embarazo, true);
            $notificaciones_muerte_mujer = json_decode($request->notificacion_muerte_mujer, true);
            $notificaciones_muerte_bebe  = json_decode($request->notificacion_muerte_bebe, true);
            $notificaciones_parto        = json_decode($request->notificacion_parto, true);


            $response_mujeres           = [];
            $response_noti_embarazo     = [];
            $response_noti_muerte_mujer = [];
            $response_noti_muerte_bebe  = [];
            $response_noti_parto        = [];
            $existeElVigilante=$this->buscarCarnetPorCodigoVigilante($codigo);

           
            if (count($mujeres) > 0) {
                foreach ($mujeres as $k => $mujer) {
                   
                    $response_mujeres[] = $this->guardarUnaMujer($request,$mujer, $codigo);
                }
            }
            if (count($notificaciones_embarazos) > 0) {
                foreach ($notificaciones_embarazos as $k => $notificacion) {
                    if ($notificacion['sync'] == '0') {

                        $response_noti_embarazo[] = $this->guadarUnaNotificacionEmbarazo($request,$notificacion, $codigo);
                    } else {
                        if ($notificacion['sync'] == '1') {
                            $response_noti_embarazo[] = $this->consultarEstadoNotificacion($notificacion, $codigo,
                                'embarazo');
                        }
                    }
                }
            }
            // \Log::debug('notificaciones de muerte mujer=>'.count($notificaciones_muerte_mujer));

            if (count($notificaciones_muerte_mujer) > 0) {
                foreach ($notificaciones_muerte_mujer as $k => $notificacion) {
                    if ($notificacion['sync'] == '0') {

                        $response_noti_muerte_mujer[] = $this->guardarUnaNotificacionMuerteMujer($request,$notificacion,
                            $codigo);
                    } else {
                        if ($notificacion['sync'] == '1') {
                            $response_noti_muerte_mujer[] = $this->consultarEstadoNotificacion($notificacion, $codigo,
                                'muertemujer');
                        }
                    }
                }
            }

            if (count($notificaciones_muerte_bebe) > 0) {
                foreach ($notificaciones_muerte_bebe as $k => $notificacion) {
                    \Log::debug('sync=>'.json_encode($notificacion));
                    if ($notificacion['sync'] == '0') {
                        $response_noti_muerte_bebe[] = $this->guardarUnaNotificacionMuerteBebe($request,$notificacion, $codigo);
                    } else {
                        if ($notificacion['sync'] == '1') {
                            $response_noti_muerte_bebe[] = $this->consultarEstadoNotificacion($notificacion, $codigo,
                                'muertebebe');
                        }
                    }
                }
                \Log::debug('json=>'.json_encode($response_noti_muerte_bebe));
            }

            if (count($notificaciones_parto) > 0) {
                foreach ($notificaciones_parto as $k => $notificacion) {
                    if ($notificacion['sync'] == '0') {
                        $response_noti_parto[] = $this->guardarUnaNotificacionParto($request,$notificacion, $codigo);
                    } else {
                        if ($notificacion['sync'] == '1') {
                            $response_noti_parto[] = $this->consultarEstadoNotificacion($notificacion, $codigo,
                                'parto');
                        }
                    }
                }
            }

            // if($existeElVigilante==false){
               
            // }

            return response()->json([
                "status"                         => true,
                "mensaje"                        => "Se sincronizo la informacion con el centro de salud.",
                "response_mujeres"               => $response_mujeres,
                "response_notificacion_embarazo" => $response_noti_embarazo,
                'response_noti_muerte_mujer'     => $response_noti_muerte_mujer,
                'response_noti_muerte_bebe'      => $response_noti_muerte_bebe,
                'response_noti_parto'            => $response_noti_parto


            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status"                         => false,
                "mensaje"                        => "Ocurrio un error al sincronizar la informacion.",
                "response_mujeres"               => [],
                "response_notificacion_embarazo" => [],
                'response_noti_muerte_mujer'     => [],
                'response_noti_muerte_bebe'      => [],
                'response_noti_parto'            => []


            ]);
        }
    }

    private function procesarListaMujeres($lista_mujeres, $codigo)
    {

        $response = [];

        return $response;
    }

    public function testconsultar(Request $request)
    {
        $notificacion = ["id_api" => $request->id];
        $codigo       = 0;
        $tipo         = $request->tipo;
        dd($this->consultarEstadoNotificacion($notificacion, $codigo, $tipo));
    }

    private function consultarEstadoNotificacion($notificacion, $codigo_vigilante, $tipo_notificacion)
    {

        try {
            $id     = $notificacion['id_api'];
            $noti   = [];
            $evento = "inserto";
            switch ($tipo_notificacion) {
                case 'embarazo':
                    $noti = NotificacionEmbarazo::find($id);
                    break;
                case 'muertebebe':
                    $noti = NotificacionMuerteBebe::find($id);
                    break;
                case 'muertemujer':
                    $noti = NotificacionMuerteMujer::find($id);
                    break;
                case 'parto':
                    $noti = NotificacionParto::find($id);
                    break;
            }
            if (isset($noti) && isset($noti->notificable[0])) {
                // return dd($noti->notificable[0]->vistoEstablecimiento);
                if ($noti->estado == 'anulado') {

                    $evento = "anulado";
                } else {
                    if ($noti->notificable[0]->vistoEstablecimiento == true) {
                        $evento = "visto";
                    }
                }
                return [
                    "status"  => true,
                    "mensaje" => "",
                    "data"    => $noti->toArray(),
                    "evento"  => $evento

                ];
            }
            throw new Exception("no existe la notificacion");
        } catch (Exception $e) {
            return [
                "status"  => false,
                "mensaje" => "error::".$e->getMessage(),
                "data"    => $notificacion,
                "evento"  => $evento
            ];
        }
    }

    public function obtenerRegistrosMujeresPorVigilante($idVigilante)
    {

        $mujeres          = [];
        $codigo_vigilante = '';
        $vigilante        = Vigilante::where('IdVigilante', $idVigilante)->first();
        if (!is_null($vigilante)) {

            $codigo_vigilante = $vigilante->codigo;

            $mujeres = Mujer::where('IdVigilante', $vigilante->IdVigilante)
                ->with('persona')
                ->get()
                ->toArray();
        }
        return $mujeres;
    }

    public function obtenerRegistrosNotificacionesEmbarazoPorVigilante($codigo_vigilante)
    {
        $datos = [];
        $notis = NotificacionEmbarazo::where('codVigilante', $codigo_vigilante)->get();

        if (count($notis) > 0) {
            $datos = $notis->toArray();
        }
        return $datos;
    }

    public function obtenerRegistrosNotificacionesMuerteMujerPorVigilante($codigo_vigilante)
    {
        $datos = [];
        $notis = NotificacionMuerteMujer::where('codVigilante', $codigo_vigilante)->get();

        if (count($notis) > 0) {
            $datos = $notis->toArray();
        }
        return $datos;
    }

    public function obtenerRegistrosNotificacionesMuerteBebePorVigilante($codigo_vigilante)
    {
        $datos = [];
        $notis = NotificacionMuerteBebe::where('codVigilante', $codigo_vigilante)->get();

        if (count($notis) > 0) {
            $datos = $notis->toArray();
        }
        return $datos;
    }

    public function obtenerRegistrosNotificacionesPartoPorVigilante($codigo_vigilante)
    {
        $datos = [];
        $notis = NotificacionParto::where('codVigilante', $codigo_vigilante)->get();

        if (count($notis) > 0) {
            $datos = $notis->toArray();
        }
        return $datos;
    }
}
