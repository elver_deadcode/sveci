<?php

namespace App\Http\Controllers\Auth;

use App\Auditoria;
use App\Evento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Persona;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $maxAttempts=2;
    public $decayMinutes=2;
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function validateLogin(Request $request)
    {

        // return dd($request->all());
        $this->validate(
            $request,
            [
                'username' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'username.required' => 'El nombre de usuario es necesario',
                'password.required' => 'La contraseña es necesario',
            ]
        );
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);


        $remember = $request->remember;
        if (\Auth::attempt(['username' => $request->username, 'password' => $request->password], $remember)) {
        Auditoria::guardar(\Auth::user()->id, 1, 'login', 'login', 'login', 'login', true,$request);

            return redirect()->intended('/home');
        }

        return $this->sendFailedLoginResponse($request);
    }


    protected function sendFailedLoginResponse(Request $request, $trans = 'auth.failed')
    {
        $errors = [$this->username() => trans($trans)];

        // return dd($errors);
        // if ($request->expectsJson()) {
        //     return response()->json($errors, 422);
        // }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);

    }


    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }


    public function logout(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 64, 'logout',
            'salir del sistema', '', 'boton salir del sistema', false, $request);

        \Auth::logout();

        return redirect('/');
    }

    public function generarCodigo(Request $request)
    {

        $fecha_nacimiento = \Carbon\Carbon::createFromFormat('d/m/Y', $request->fecha_nacimiento)->format('Y-m-d');
        // $fecha_nacimiento=$request->fecha_nacimiento;
        // return $fecha_nacimiento;
        $numcarnet = $request->carnet;
        $email     = '';
        $celular   = '';
        $codigo    = '';
        try {

            $user = User::join('personas as p', 'p.IdPersona', '=', 'users.IdPersona')
                ->select(
                    'p.email',
                    'p.celular',
                    'p.nombres',
                    'p.primerApellido',
                    'p.segundoApellido',
                    'p.numeroCarnet',

                    'users.estado',
                    'users.id'
                )
                ->where('p.numeroCarnet', $numcarnet)
                ->where('p.fechaNacimiento', $fecha_nacimiento)
                ->first();


            if (is_null($user)) {
                $return = [
                    'success' => true,
                    'status'  => 400,
                    'msg'     => 'No se encontro a ninguna persona con estos datos'
                ];
            } elseif (!is_null($user) && $user->estado == false) {
                $return = [
                    'success' => true,
                    'status'  => 400,
                    'msg'     => 'Esta persona esta dado de baja del sistema. Comuniquese con la administracion para recuperar sus credenciales'
                ];
            } elseif (!is_null($user) && $user->email == '') {
                $return = [
                    'success' => true,
                    'status'  => 400,
                    'msg'     => 'Esta persona no tiene cuenta de email  registrado. Comuniquese con la administracion para recuperar sus credenciales'
                ];
            } else {
                $email                       = $user->email;
                $celular                     = $user->celular;
                $codigo                      = strtolower(str_random(5));
                $user_aux                    = User::find($user->id);
                $user_aux->codigo_credencial = $codigo;
                $user_aux->save();


                if ($email != '') {

                    // return (new \App\Mail\ResetPassword($user,$codigo))->render();
                    \Mail::to($user->email)->send(new \App\Mail\ResetPassword($user, $codigo));

                }
                if ($celular != '') {
                    // $this->enviarSms($user,$codigo);
                }
                $return = [
                    'success' => true,
                    'status'  => 200,
                    'msg'     => 'El codigo de recuperacion se envio a su correo electronico: '.$user->email.'<br> REVISA TU CORREO NO DESEADO O LA CARPETA DE SPAN.'
                ];


            }
            return response()->json($return);
        } catch (\Exception $e) {
            return $e->getMessage();

            return response()->json([], 500);

        }

    }

    public function validarCodigoRecuperacion(Request $request)
    {
        $user   = User::where('codigo_credencial', $request->codigo)
            ->with('persona')
            ->first();
        $return = [];
        if (!is_null($user)) {
            $return = [
                'success' => true,
                'status'  => 200,
                'user'    => $user

            ];
        } else {
            $return = [
                'success' => true,
                'status'  => 400,

            ];
        }
        return response()->json($return);

    }

    public function guardarCredenciales(Request $request)
    {

        $user = User::find($request->id);
        if (!is_null($user)) {
            $user->password          = bcrypt($request->password);
            $user->codigo_credencial = '';
            $user->save();
            \Auth::login($user);
            session()->flash('success', ' Su contraseña se registro correctamente');
            return redirect('home');


        } else {
            return redirect()->back();
        }

    }


}
