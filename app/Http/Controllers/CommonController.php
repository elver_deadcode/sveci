<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{


    public function error($tipo){
        switch($tipo){
            case "estado": return view('errors.estado'); break;
            case "permission": return view('errors.permission'); break;
        }
    }
    
}
