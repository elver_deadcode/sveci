<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\ContraseniaUsuario;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       Auditoria::guardar(\Auth::user()->id, 2, 'inicio', 'inicio', 'inicio', 'inicio', true,$request);

        return view('home');
    }
}
