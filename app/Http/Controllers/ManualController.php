<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;

class ManualController extends Controller
{


    public function index(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 'manual_usuario', 'manual de usuario', 'manual del usuario', '', '', true,$request);

        return view('manual.index');
    }
}
