<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;
use App\Mujer;
use App\User;
use App\Departamento;
use App\Persona;
use App\Municipio;
use App\Establecimiento;
use Illuminate\Support\Str;

class MujerController extends Controller
{
    public function index(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 35, 'mujeres',
            'mujeres', 'lista de mujeres', '', true, $request);

        return view('mujeres.index');
    }

    public function create(Request $request)
    {

        $niveles = auth()->user()->getNiveles();
        Auditoria::guardar(\Auth::user()->id, 38, 'mujeres',
            'mujeres', 'registro de mujer', '', true, $request);


        return view('mujeres.create_edit', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'comunidades'      => $niveles['comunidades']
        ]);
    }

    public function editar(Request $request, Mujer $mujer)
    {
        $niveles          = auth()->user()->getNiveles();
        $user             = auth()->user();
        $nacional         = 0;
        $dpt_codigo       = 0;
        $mnc_codigo       = 0;
        $are_codigo       = 0;
        $codestabl        = 0;
        $establecimientos = $niveles['establecimientos'];

        $comunidades = [];
        switch ($user->nivel) {

            case 'depto':
                $dpt_codigo = $user->idnivel;
                break;
            case 'muni':
                $muni       = Municipio::where('cod_muni', $user->idnivel)->first();
                $dpt_codigo = $muni->departamento->dpt_codigo;
                $mnc_codigo = $muni->mnc_codigo;
                break;
            case 'red':
                $red = \App\Red::where('are_codigo', $user->idnivel)->first();
                //  return dd($red);
                $are_codigo = $red->are_codigo;
                $dpt_codigo = $red->depto->dpt_codigo;
                break;

            case 'establ':
                $esta = Establecimiento::where('codestabl', $user->idnivel)->orderBy('idgestion', 'DESC')->first();


                $codestabl          = $esta->codestabl;
                $mnc_codigo         = $esta->municipio->mnc_codigo;
                $dpt_codigo         = $esta->municipio->departamento()->dpt_codigo;
                $establecimientos[] = $esta;
                $muni               = $esta->municipio;
                $comunidades        = $muni->comunidades()->orderBy('nomComunidad', 'ASC')->get();

                //  $comunidades=
                break;
        }
        $establecimientos = Municipio::where('mnc_codigo', $mujer->persona->mnc_codigo)->first()
            ->establecimientos;

        Auditoria::guardar(\Auth::user()->id, 36, 'mujeres',
            'mujeres', 'lista de mujeres', 'boton editar mujer', true, $request);


        return view('mujeres.create_edit', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $establecimientos,
            'mujer'            => $mujer,
            'comunidades'      => $comunidades,

        ]);
    }

    public function guardar(Request $request)
    {

        // return dd($request->all());
        $fecha_actual = \Carbon\Carbon::now();
        try {
            \DB::beginTransaction();
            if (isset($request->mujer_id)) {
                Auditoria::guardar(\Auth::user()->id, 37, 'mujeres',
                    'mujeres', 'editar mujer', 'boton  guardar', true, $request);

                $mujer   = Mujer::find($request->mujer_id);
                $persona = $mujer->persona;
            } else {
                Auditoria::guardar(\Auth::user()->id, 39, 'mujeres',
                    'mujeres', 'registro de nueva mujer', 'boton  guardar', true, $request);

                $mujer   = new Mujer();
                $persona = Persona::where('numeroCarnet', $request->numero_carnet)->first();
                if (!is_null($persona)) {
                    // return dd('ya hay persona');
                    session()->flash('warning',
                        ' Esta Persona con Numero de carnet: '.$request->numero_carnet.' ya se encuentra registrada');
                    return back()->withInput($request->input());
                } else {
                    // return dd('NO hay persona');
                    $persona = new Persona();
                }
            }

            $establecimiento = \App\Establecimiento::find($request->establecimiento);

            $persona->nombres            = strtoupper($request->nombres);
            $persona->primerApellido     = strtoupper($request->primer_apellido);
            $persona->segundoApellido    = strtoupper($request->segundo_apellido);
            $persona->numeroCarnet       = strtoupper($request->numero_carnet);
            $persona->complemento        = strtoupper($request->complemento);
            $persona->nacionalidad       = 'BOlIVIANA';
            $persona->direccion          = $request->direccion;
            $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                $request->fecha_nacimiento)->format('Y-m-d');
            $persona->sexo               = 'F';
            $persona->codEstablecimiento = $request->establecimiento;
            $persona->mnc_codigo         = $establecimiento->codmunicip;
            $persona->dpt_codigo         = $establecimiento->municipio->departamento()->dpt_codigo;
            $persona->are_codigo         = $establecimiento->codarea;
            $persona->IdComunidad        = $request->comunidad;
            $persona->celular            = $request->numero_celular;


            if (!is_null($request->adscrito_id)) {

                $persona->adscrito_id = $request->adscrito_id;
                $persona->verificado  = 1;
            } else {

                $persona->verificado = 0;
            }
            $anios = $fecha_actual->diffInYears($persona->fechaNacimiento);
            if ($anios < 10 || $anios > 59) {
                session()->flash('warning',
                    'La edad de la mujer se calculo en  '.$anios.' años, introduzca una fecha de nacimiento  entre los 10 a 59 años de edad');
                return back()->withInput($request->input());
            }
            $uuid = (string)Str::uuid();
            $persona->save();
            $mujer->estado      = true;
            $mujer->IdVigilante = 0;
            $mujer->longitud    = 0;
            $mujer->latitud     = 0;
            if (!isset($request->mujer_id)) {
                $mujer->idAndroid = 'web-'.$uuid;
            }
            $mujer->IdUsuario = auth()->user()->id;
            $mujer->IdPersona = $persona->IdPersona;
            $mujer->save();


            \DB::commit();
            session()->flash('success', 'Los datos de la mujer '.$mujer->nombre_completo.', se guardo correctamente');

            return redirect('mujeres');
        } catch (\Exception $e) {
            \DB::rollBack();
            session()->flash('error', 'Ocurrio un error en el servidor al guardar registrar el vigilante');
            return redirect('mujeres/create');
        }
    }

    public function borrar(Mujer $mujer)
    {


        // try{

        //   }


    }

    public function listarMujeres()
    {

        $nivel                    = auth()->user()->nivel;
        $idnivel                  = auth()->user()->idnivel;
        $codigos_establecimientos = [];

        switch ($nivel) {
            case 'nacional':
                $mujeres = Mujer::latest()->with('persona')->orderBy('created_at', 'DESC')->get();

                $codigos_establecimientos = [];
                break;

            case 'depto':
                $codigos_establecimientos = array_values(\App\Departamento::obtenerEstablecimientos($idnivel)->map->only(['codestabl'])->toArray());
                break;

            case 'red':
                $codigos_establecimientos = array_values(\App\Red::obtenerEstablecimientosPorRed($idnivel)->map->only(['codestabl'])->toArray());
                break;

            case 'muni':
                $codigos_establecimientos = \App\Municipio::obtenerCodigosEstablecimientosPorMunicipio($idnivel);
                break;
            case 'establ':
                $codigos_establecimientos = [$idnivel];
                break;
        }

        if (count($codigos_establecimientos) > 0) {
            $mujeres = Mujer::MujeresPorEstablecimiento($codigos_establecimientos);
        }
        return \DataTables::of($mujeres)
            ->addColumn('establecimiento', function ($mujer) {

                return $mujer->persona->obtenerEstablecimiento()->nomestabl;
            })
            ->addColumn('fechanacimiento', function ($mujer) {

                return $mujer->persona->fechaNacimiento->format('d/m/Y');
            })
            ->addColumn('fecharegistro', function ($mujer) {

                return $mujer->created_at->format('d/m/Y');
            })
            ->addColumn('edad', function ($mujer) {

                return $mujer->persona->edad;
            })
            ->addColumn('vigilante', function ($mujer) {

                if (isset($mujer->vigilante)) {

                    return $mujer->vigilante->nombre_completo;
                } else {
                    return '';
                }
            })
            ->addColumn('municipio', function ($mujer) {


                return $mujer->persona->obtenerMunicipio()->mnc_nombre;
            })
            ->addColumn('departamento', function ($mujer) {

                return $mujer->persona->obtenerDepartamento()->dpt_nombre;
            })
            ->addColumn('red', function ($mujer) {

                return $mujer->persona->obtenerRed()->are_nombre;
            })
            ->addColumn('fecharegistro', function ($mujer) {

                return $mujer->created_at->format('d/m/Y H:i:s');
            })
            ->addColumn('verificado', function ($mujer) {

                if ($mujer->persona->verificado) {
                    return '<span class="badge badge-success">SI</span>';
                } else {
                    return '<span class="badge badge-danger">NO</span>';
                }
            })
            ->addColumn('acciones', function ($mujer) {

                return view('mujeres.acciones', compact('mujer'))->render();
            })
            ->rawColumns(['acciones', 'verificado', 'fechanacimiento', 'fecharegistro', 'edad'])
            ->make(true);
    }
}
