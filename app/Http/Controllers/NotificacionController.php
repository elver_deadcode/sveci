<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NotificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {

        // return dd($request->all());

        $status = true;
        try {
            \DB::beginTransaction();

            $idmujerandroid   = 0;
            $nombres          = '';
            $primer_apellido  = '';
            $segundo_apellido = '';
            $edad             = "";
            $direccion        = "";
            $telefono         = "";
            $uuid             = (string)Str::uuid();
            $idAndroid        = 'web-'.$uuid;

            if (isset($request->idmujer)) {

                $mujer            = \App\Mujer::find($request->idmujer);
                $nombres          = $mujer->persona->nombres;
                $primer_apellido  = $mujer->persona->primerApellido;
                $segundo_apellido = $mujer->persona->segundoApellido;
                $edad             = $mujer->persona->edad;
                $direccion        = $mujer->persona->direccion;
                $telefono         = $mujer->persona->celular;
                $idmujerandroid   = $mujer->idAndroid;
            } else {

                $nombres          = $request->nombres_mujer;
                $primer_apellido  = $request->primer_apellido;
                $segundo_apellido = $request->segundo_apellido;
                $edad             = $request->edad;
                $direccion        = $request->direccion;
            }
            $vigilante = \App\Vigilante::find($request->id_vigilante);
            switch ($request->tipo) {
                case 'embarazo':
                    $noti                     = new \App\NotificacionEmbarazo();
                    $noti->codVigilante       = $vigilante->codigo;
                    $noti->codEstablecimiento = $vigilante->persona->codEstablecimiento;
                    $noti->estado             = "recibido";
                    $noti->IdAndroid          = $idAndroid;
                    $noti->fechaRegistro      = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_generacion)->format('Y-m-d');
                    $noti->created_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->updated_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->idMujerAndroid     = $idmujerandroid;
                    $noti->nombres            = $nombres;
                    $noti->primerApellido     = $primer_apellido;
                    $noti->segundoApellido    = $segundo_apellido;
                    $noti->edad               = $edad;
                    $noti->direccion          = $direccion;
                    $noti->telefono           = $telefono;

                    $noti->save();
                    $noti->notificable()->create([


                        "notificable_id"     => $noti->id,
                        "mensaje"            => "Se registro una notificacion de Embarazo en el sistema",
                        "codEstablecimiento" => $noti->codEstablecimiento,
                        "tipo"               => "web",
                        "prioridad"          => "normal",
                        "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                        "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                        "are_codigo"         => $vigilante->persona->are_codigo,
                        "user_id"            => \Auth::user()->id,
                        "idComunidad"        => $vigilante->persona->IdComunidad,
                        "idVigilante"        => $vigilante->IdVigilante

                    ]);


                    break;


                case 'muertemujer':
                    $noti                     = new \App\NotificacionMuerteMujer();
                    $noti->codVigilante       = $vigilante->codigo;
                    $noti->codEstablecimiento = $vigilante->persona->codEstablecimiento;
                    $noti->estado             = "recibido";
                    $noti->IdAndroid          = $idAndroid;
                    $noti->fechaRegistro      = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_generacion)->format('Y-m-d');
                    $noti->created_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->updated_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->idMujerAndroid     = $idmujerandroid;
                    $noti->nombres            = $nombres;
                    $noti->primerApellido     = $primer_apellido;
                    $noti->segundoApellido    = $segundo_apellido;
                    $noti->edad               = $edad;
                    $noti->direccion          = $direccion;
                    $noti->telefono           = $telefono;
                    $noti->save();
                    $noti->notificable()->create([


                        "notificable_id"     => $noti->id,
                        "mensaje"            => "Se registro una notificacion de Muerte Mujer en el sistema",
                        "codEstablecimiento" => $noti->codEstablecimiento,
                        "tipo"               => "web",
                        "prioridad"          => "normal",
                        "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                        "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                        "are_codigo"         => $vigilante->persona->are_codigo,
                        "user_id"            => \Auth::user()->id,
                        "idComunidad"        => $vigilante->persona->IdComunidad,
                        "idVigilante"        => $vigilante->IdVigilante

                    ]);

                    break;

                case 'muertebebe':
                    $noti                     = new \App\NotificacionMuerteBebe();
                    $noti->codVigilante       = $vigilante->codigo;
                    $noti->codEstablecimiento = $vigilante->persona->codEstablecimiento;
                    $noti->estado             = "recibido";
                    $noti->IdAndroid          = $idAndroid;
                    $noti->fechaRegistro      = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_generacion)->format('Y-m-d');
                    $noti->created_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->updated_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->idMujerAndroid     = $idmujerandroid;
                    $noti->nombres            = $nombres;
                    $noti->primerApellido     = $primer_apellido;
                    $noti->segundoApellido    = $segundo_apellido;
                    $noti->edad               = $edad;
                    $noti->direccion          = $direccion;
                    $noti->telefono           = $telefono;
                    $noti->save();
                    $noti->notificable()->create([
                        "notificable_id"     => $noti->id,
                        "mensaje"            => "Se registro una notificacion de Muerte Bebe en el sistema",
                        "codEstablecimiento" => $noti->codEstablecimiento,
                        "tipo"               => "web",
                        "prioridad"          => "normal",
                        "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                        "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                        "are_codigo"         => $vigilante->persona->are_codigo,
                        "user_id"            => \Auth::user()->id,
                        "idComunidad"        => $vigilante->persona->IdComunidad,
                        "idVigilante"        => $vigilante->IdVigilante

                    ]);


                    break;
                case 'parto':
                    $noti                     = new \App\NotificacionParto();
                    $noti->codVigilante       = $vigilante->codigo;
                    $noti->codEstablecimiento = $vigilante->persona->codEstablecimiento;
                    $noti->estado             = "recibido";
                    $noti->IdAndroid          = $idAndroid;
                    $noti->fechaRegistro      = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_generacion)->format('Y-m-d');
                    $noti->created_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->updated_at         = \carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_registro)->format('Y-m-d');
                    $noti->idMujerAndroid     = $idmujerandroid;
                    $noti->nombres            = $nombres;
                    $noti->primerApellido     = $primer_apellido;
                    $noti->segundoApellido    = $segundo_apellido;
                    $noti->edad               = $edad;
                    $noti->direccion          = $direccion;
                    $noti->telefono           = $telefono;
                    $noti->save();
                    $noti->notificable()->create([
                        "notificable_id"     => $noti->id,
                        "mensaje"            => "Se registro una notificacion de Parto en el sistema",
                        "codEstablecimiento" => $noti->codEstablecimiento,
                        "tipo"               => "web",
                        "prioridad"          => "normal",
                        "mnc_codgo"          => $vigilante->persona->mnc_codigo,
                        "dpt_codigo"         => $vigilante->persona->dpt_codigo,
                        "are_codigo"         => $vigilante->persona->are_codigo,
                        "user_id"            => \Auth::user()->id,
                        "idComunidad"        => $vigilante->persona->IdComunidad,
                        "idVigilante"        => $vigilante->IdVigilante

                    ]);

                    break;
            }
            $id = $noti->notificable[0]->id;
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return dd($e);
            $status = false;
        }
        return response()->json([
            'status'          => $status,
            'tipo'            => $request->tipo,
            'notificacion_id' => $id

        ]);
    }


    public function listarNotificaciones(Request $request)
    {
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;

        $numero_registros  = 20;
        $tipo_notificacion = is_null($request->tipo_notificacion) ? 'todas' : $request->tipo_notificacion;

        $where = '';


        switch ($tipo_notificacion) {
            case 'embarazo':
                $where = 'App\NotificacionEmbarazo';
                break;
            case 'muertemujer':
                $where = 'App\NotificacionMuerteMujer';
                break;
            case 'muertebebe':
                $where = 'App\NotificacionMuerteBebe';
                break;
            case 'parto':
                $where = 'App\NotificacionParto';
                break;
            case 'registromujer':
                $where = 'App\Mujer';
                break;
            case 'teleconsulta':
                $where = 'App\NotificacionTeleconsulta';
                break;
        }

        if (!is_null($request->buscar)) {

            $nivel_busqueda           = $request->buscar[0]['value'];
            $fecha_ini_busqueda       = $request->buscar[1]['value'];
            $fecha_fin_busqueda       = $request->buscar[2]['value'];
            $depto_busqueda           = $request->buscar[3]['value'];
            $red_busqueda             = $request->buscar[4]['value'];
            $municipio_busqueda       = $request->buscar[5]['value'];
            $establecimiento_busqueda = $request->buscar[6]['value'];
            $where_busqueda           = '';
            if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                $fecha_ini_busqueda = \Carbon\Carbon::createFromformat('d/m/Y', $fecha_ini_busqueda)->format('Y-m-d');
                $fecha_fin_busqueda = \Carbon\Carbon::createFromformat('d/m/Y', $fecha_fin_busqueda)->format('Y-m-d');
            }
        }

        switch ($nivel) {

            case 'nacional':

                if ($tipo_notificacion == 'todas') {

                    if (!is_null($request->buscar)) {


                        switch ($nivel_busqueda) {
                            case 'nacional':
                                if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {


                                    $where_busqueda = "created_at BETWEEN '".$fecha_ini_busqueda."' AND  '".$fecha_fin_busqueda."'";
                                    $notificaciones = Notificacion::whereBetween('created_at',
                                        [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                        ->orderBy('created_at', 'DESC')
                                        ->paginate($numero_registros);
                                } else {

                                    $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                                        ->paginate($numero_registros);
                                }

                                break;
                            case 'depto':

                                if (!is_null($depto_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('dpt_codigo', $depto_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('dpt_codigo', $depto_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                } else {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {


                                        $where_busqueda = "created_at BETWEEN '".$fecha_ini_busqueda."' AND  '".$fecha_fin_busqueda."'";
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }
                                break;
                            case 'red':
                                if (!is_null($red_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('are_codigo', $red_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('are_codigo', $red_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;

                            case 'muni':
                                if (!is_null($municipio_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('mnc_codgo', $municipio_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('mnc_codgo', $municipio_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                            case 'establ':

                                if (!is_null($establecimiento_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('codEstablecimiento', $establecimiento_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('codEstablecimiento',
                                            $establecimiento_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                        }
                    } else {

                        $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                            ->paginate($numero_registros);
                    }
                } else {
                    $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                        ->where('notificable_type', $where)
                        ->paginate($numero_registros);
                }
                break;

            case 'depto':


                if ($tipo_notificacion == 'todas') {
                    if (!is_null($request->buscar)) {


                        switch ($nivel_busqueda) {

                            case 'depto':


                                if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {


                                    $notificaciones = Notificacion::whereBetween('created_at',
                                        [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                        ->where('dpt_codigo', $idnivel)
                                        ->orderBy('created_at', 'DESC')
                                        ->paginate($numero_registros);
                                } else {
                                    $notificaciones = Notificacion::where('dpt_codigo', $idnivel)
                                        ->orderBy('created_at', 'DESC')
                                        ->paginate($numero_registros);
                                }

                                break;
                            case 'red':
                                if (!is_null($red_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('are_codigo', $red_busqueda)
                                            ->where('dpt_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('are_codigo', $red_busqueda)
                                            ->where('dpt_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;

                            case 'muni':
                                if (!is_null($municipio_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('dpt_codigo', $idnivel)
                                            ->where('mnc_codgo', $municipio_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('mnc_codigo', $municipio_busqueda)
                                            ->where('dpt_codgo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                            case 'establ':
                                if (!is_null($establecimiento_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('dpt_codigo', $idnivel)
                                            ->where('codEstablecimiento', $establecimiento_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('codEstablecimiento',
                                            $establecimiento_busqueda)
                                            ->where('dpt_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                        }
                    } else {
                        $notificaciones = Notificacion::where('dpt_codigo', $idnivel)
                            ->orderBy('created_at', 'DESC')
                            ->paginate($numero_registros);
                    }
                } else {
                    $notificaciones = Notificacion::where('dpt_codigo', $idnivel)
                        ->where('notificable_type', $where)
                        ->orderBy('created_at', 'DESC')
                        ->paginate($numero_registros);
                }
                break;
            // ********************usuario de nivel red*************************************
            case 'red':


                if ($tipo_notificacion == 'todas') {
                    if (!is_null($request->buscar)) {


                        switch ($nivel_busqueda) {


                            case 'red':

                                if (!is_null($red_busqueda)) {

                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('are_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('are_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                } else {

                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {

                                        if ($fecha_ini_busqueda == $fecha_fin_busqueda) {
                                            $notificaciones = Notificacion::whereDate('created_at', '=',
                                                $fecha_ini_busqueda)
                                                ->where('are_codigo', $idnivel)
                                                ->orderBy('created_at', 'DESC')
                                                ->paginate($numero_registros);
                                        } else {
                                            $notificaciones = Notificacion::whereBetween('created_at',
                                                [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                                ->where('are_codigo', $idnivel)
                                                ->orderBy('created_at', 'DESC')
                                                ->paginate($numero_registros);
                                            // return dd($fecha_ini_busqueda,$fecha_fin_busqueda,$numero_registros);
                                        }
                                    } else {
                                        $notificaciones = Notificacion::where('are_codigo', auth()->user()->idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;


                            case 'establ':

                                if (!is_null($establecimiento_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        if ($fecha_ini_busqueda == $fecha_fin_busqueda) {
                                            // return dd($establecimiento_busqueda,$idnivel);

                                            $notificaciones = Notificacion::whereDate('created_at', '=',
                                                $fecha_ini_busqueda)
                                                ->where('are_codigo', auth()->user()->idnivel)
                                                ->where('codEstablecimiento', $establecimiento_busqueda)
                                                ->orderBy('created_at', 'DESC')
                                                ->paginate($numero_registros);
                                        } else {
                                            $notificaciones = Notificacion::whereBetween('created_at',
                                                [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                                ->where('are_codigo', $idnivel)
                                                ->where('codEstablecimiento', $establecimiento_busqueda)
                                                ->orderBy('created_at', 'DESC')
                                                ->paginate($numero_registros);
                                        }
                                    } else {
                                        $notificaciones = Notificacion::where('codEstablecimiento',
                                            $establecimiento_busqueda)
                                            ->where('are_codigo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                        }
                    } else {


                        $notificaciones = Notificacion::where('are_codigo', $idnivel)
                            ->orderBy('created_at', 'DESC')
                            ->paginate($numero_registros);
                    }
                } else {
                    $notificaciones = Notificacion::where('are_codigo', $idnivel)
                        ->where('notificable_type', $where)
                        ->orderBy('created_at', 'DESC')
                        ->paginate($numero_registros);
                }
                break;
            // ***********************usuario nivel municipal***********************************

            case 'muni':

                if ($tipo_notificacion == 'todas') {

                    if (!is_null($request->buscar)) {


                        switch ($nivel_busqueda) {


                            case 'muni':
                                if (!is_null($idnivel)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('mnc_codgo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('mnc_codgo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                            case 'establ':
                                if (!is_null($establecimiento_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('mnc_codgo', $idnivel)
                                            ->where('codEstablecimiento', $establecimiento_busqueda)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('codEstablecimiento',
                                            $establecimiento_busqueda)
                                            ->where('mnc_codgo', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                        }
                    } else {


                        $notificaciones = Notificacion::where('mnc_codgo', $idnivel)
                            ->orderBy('created_at', 'DESC')
                            ->paginate($numero_registros);
                    }
                } else {
                    $notificaciones = Notificacion::where('mnc_codgo', $idnivel)
                        ->where('notificable_type', $where)
                        ->orderBy('created_at', 'DESC')
                        ->paginate($numero_registros);
                }

                break;

            // **********************usuario nivel establecimiento*******************

            case 'establ':


                if ($tipo_notificacion == 'todas') {

                    if (!is_null($request->buscar)) {

                        switch ($nivel_busqueda) {

                            case 'establ':
                                return dd($establecimiento_busqueda);
                                if (!is_null($establecimiento_busqueda)) {
                                    if (!is_null($fecha_ini_busqueda) && !is_null($fecha_fin_busqueda)) {
                                        $notificaciones = Notificacion::whereBetween('created_at',
                                            [$fecha_ini_busqueda, $fecha_fin_busqueda])
                                            ->where('codEstablecimiento', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    } else {
                                        $notificaciones = Notificacion::where('codEstablecimiento', $idnivel)
                                            ->orderBy('created_at', 'DESC')
                                            ->paginate($numero_registros);
                                    }
                                }

                                break;
                        }
                    } else {


                        $notificaciones = Notificacion::where('codEstablecimiento', $idnivel)
                            ->orderBy('created_at', 'DESC')
                            ->paginate($numero_registros);
                    }
                } else {

                    //    return dd($notificaciones);

                    $notificaciones = Notificacion::where('codEstablecimiento', $idnivel)
                        ->where('notificable_type', $where)
                        ->orderBy('created_at', 'DESC')
                        ->paginate($numero_registros);
                }
                break;
        }

        return view('notificaciones.item-noti-embarazo', [
            'notificaciones' => $notificaciones,
            'tipo'           => $tipo_notificacion
        ]);
    }

    public function index(Request $request)
    {
        $niveles = auth()->user()->getNiveles();

        $roles = \App\Role::all();

        Auditoria::guardar(\Auth::user()->id, 40, 'notificaciones',
            'notificaciones', '', '', true, $request);

        return view('notificaciones.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function storeAndroid(Request $request)
    {
        return response()->json([
            'status'  => true,
            'mensaje' => "La notificacion se guardo correctamente",
            'data'    => $request->all()
        ]);
    }

    public function contarNotificaciones(Request $request)
    {
        $status = 0;
        try {
            $contador_notificaciones = \Auth::user()->contarNotificaciones();
            return response()->json([
                'status'   => true,
                'contador' => $contador_notificaciones
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'status'   => false,
                'contador' => []
            ], 500);
        }
    }

    public function contarTodasNotificaciones(Request $request)
    {
        $status = 0;
        try {
            $contador_notificaciones = \Auth::user()->contarTodasNotificaciones();
            return response()->json([
                'status'   => true,
                'contador' => $contador_notificaciones
            ]);
        } catch (\Exception $e) {
            return dd($e);

            return response()->json([
                'status'   => false,
                'contador' => []
            ], 500);
        }
    }

    public function marcarVisto(Request $request)
    {

        try {


            $nivel        = \Auth::user()->nivel;
            $idnivel      = auth()->user()->idnivel;
            $notificacion = Notificacion::find($request->id);

            if (!is_null($notificacion)) {

                switch ($nivel) {
                    case 'nacional':
                        $notificacion->update(['vistoNacional' => true]);
                        break;
                    case 'depto':
                        $notificacion->update(['vistoDepartamento' => true]);
                        break;
                    case 'red':
                        $notificacion->update(['vistoRed' => true]);
                        break;
                    case 'muni':
                        $notificacion->update(['vistoMunicipio' => true]);
                        break;
                    case 'establ':
                        $notificacion->update(['vistoEstablecimiento' => true]);
                        break;
                        break;
                }

                return response()->json([
                    'status' => true
                ]);
            } else {
                return response()->json([
                    'status' => false
                ]);
            }
        } catch (\Exception $e) {
            return dd($e);
            return response()->json([
                'status' => false

            ], 505);
        }
    }


    public function mostrarNotificacionesEnCabecera()
    {
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;

        $numero_registros = 10;

        //   $user=\Auth::user();
        //   $cod_establecimiento=$user->

        switch ($nivel) {

            case 'nacional':

                $notificaciones_embarazo = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\NotificacionEmbarazo')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertemujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertebebe = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_parto = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\NotificacionParto')
                    ->take($numero_registros)
                    ->get();
                $notificaciones_mujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\Mujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_activacion = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->where('notificable_type', 'App\Vigilante')
                    ->take($numero_registros)
                    ->get();

                break;

            case 'depto':


                $notificaciones_embarazo = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionEmbarazo')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertemujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertebebe = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_parto = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionParto')
                    ->take($numero_registros)
                    ->get();
                $notificaciones_mujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\Mujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_activacion = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->where('notificable_type', 'App\Vigilante')
                    ->take($numero_registros)
                    ->get();
                break;

            case 'red':


                $notificaciones_embarazo = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionEmbarazo')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertemujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertebebe = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_parto = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionParto')
                    ->take($numero_registros)
                    ->get();
                $notificaciones_mujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\Mujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_activacion = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->where('notificable_type', 'App\Vigilante')
                    ->take($numero_registros)
                    ->get();
                break;


            case 'muni':
                $notificaciones_embarazo = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionEmbarazo')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertemujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertebebe = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_parto = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\NotificacionParto')
                    ->take($numero_registros)
                    ->get();
                $notificaciones_mujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\Mujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_activacion = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->where('notificable_type', 'App\Vigilante')
                    ->take($numero_registros)
                    ->get();
                break;
            case 'establ':
                $notificaciones_embarazo = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\NotificacionEmbarazo')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertemujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_muertebebe = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_parto = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\NotificacionParto')
                    ->take($numero_registros)
                    ->get();
                $notificaciones_mujer = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\Mujer')
                    ->take($numero_registros)
                    ->get();

                $notificaciones_activacion = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->where('notificable_type', 'App\Vigilante')
                    ->take($numero_registros)
                    ->get();
                break;
        }
        $lista_embarazos   = [];
        $lista_muertemujer = [];
        $lista_muertebebe  = [];
        $lista_parto       = [];
        $lista_mujeres     = [];
        $lista_activacion  = [];
        \Carbon\Carbon::setLocale('es');

        foreach ($notificaciones_embarazo as $k => $v) {
            $class = $v->obtenerRelacion();


            $nombres = is_null($class->vigilante->nombre_completo) ? '' : $class->vigilante->nombre_completo;

            $lista_embarazos[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }

        foreach ($notificaciones_muertemujer as $k => $v) {
            $class               = $v->obtenerRelacion();
            $nombres             = is_null($class->vigilante->nombre_completo) ? '' : $class->vigilante->nombre_completo;
            $lista_muertemujer[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }

        foreach ($notificaciones_muertebebe as $k => $v) {
            $class              = $v->obtenerRelacion();
            $nombres            = is_null($class->vigilante->nombre_completo) ? '' : $class->vigilante->nombre_completo;
            $lista_muertebebe[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }

        foreach ($notificaciones_parto as $k => $v) {
            $class         = $v->obtenerRelacion();
            $nombres       = is_null($class->vigilante->nombre_completo) ? '' : $class->vigilante->nombre_completo;
            $lista_parto[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }

        foreach ($notificaciones_mujer as $k => $v) {
            $class           = $v->obtenerRelacion();
            $nombres         = is_null($class->vigilante->nombre_completo) ? '' : $class->vigilante->nombre_completo;
            $lista_mujeres[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }

        foreach ($notificaciones_activacion as $k => $v) {
            $class              = $v->obtenerRelacion();
            $nombres            = is_null($class->nombre_completo) ? '' : $class->nombre_completo;
            $lista_activacion[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }


        return response()->json([
            'status'      => true,
            'embarazos'   => $lista_embarazos,
            'muertemujer' => $lista_muertemujer,
            'muertebebe'  => $lista_muertebebe,
            'parto'       => $lista_parto,
            'mujer'       => $lista_mujeres,
            'activacion'  => $lista_activacion,
        ]);
    }

    public function mostrarTodasNotificacionesEnCabecera()
    {
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;

        $numero_registros = 10;
        switch ($nivel) {
            case 'nacional':
                $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoNacional', 'false')
                    ->take($numero_registros)
                    ->get();
                break;
            case 'depto':
                $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoDepartamento', 'false')
                    ->where('dpt_codigo', $idnivel)
                    ->take($numero_registros)
                    ->get();
                break;
            case 'red':
                $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoRed', 'false')
                    ->where('are_codigo', $idnivel)
                    ->take($numero_registros)
                    ->get();
                break;
            case 'muni':
                $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoMunicipio', 'false')
                    ->where('mnc_codigo', $idnivel)
                    ->take($numero_registros)
                    ->get();
                break;
            case 'establ':
                $notificaciones = Notificacion::orderBy('created_at', 'DESC')
                    ->select('id', 'notificable_type', 'notificable_id', 'updated_at', 'mensaje')
                    ->where('vistoEstablecimiento', 'false')
                    ->where('codEstablecimiento', $idnivel)
                    ->take($numero_registros)
                    ->get();
                break;
        }
        $lista_notificaciones = [];
        \Carbon\Carbon::setLocale('es');
        foreach ($notificaciones as $k => $v) {
            $class                  = $v->obtenerRelacion();
//            $nombres                = is_null($class->nombre_completo) ? '' : $class->nombre_completo;
            $nombres                = !isset($class->nombre_completo) ? '' : $class->nombre_completo;
            $lista_notificaciones[] = [
                'id'        => $v->id,
                'fecha'     => $v->updated_at->format('d/m/Y H:i:s'),
                'fecha2'    => $v->updated_at->diffForHumans(),
                'mensaje'   => $v->mensaje,
                'vigilante' => $nombres
            ];
        }


        return view('notificaciones.listacabecera', [

            'lista_notificaciones' => $lista_notificaciones,

        ])->render();
        return response()->json([
            'status' => true,
            'notificaciones' => $lista_notificaciones,
        ]);
    }

    public function verNotificacionVista(Notificacion $notificacion)
    {

        $niveles = auth()->user()->getNiveles();

        $roles = \App\Role::all();


        return view('notificaciones.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles,
            'notificacion_id'  => $notificacion->id
        ]);
    }

    public function verNotificacion(Request $request,Notificacion $notificacion)
    {

        $titulo = '';


        switch ($notificacion->notificable_type) {
            case 'App\NotificacionMuerteMujer':
                $class       = \App\NotificacionMuerteMujer::find($notificacion->notificable_id);
                $titulo      = 'NOTIFICACION MUERTE MUJER';
                $abreviacion = 'nmm';


                break;
            case 'App\NotificacionMuerteBebe':
                $class       = \App\NotificacionMuerteBebe::find($notificacion->notificable_id);
                $titulo      = 'NOTIFICACION MUERTE BEBE';
                $abreviacion = 'nmb';
                break;
            case 'App\NotificacionParto':
                $class       = \App\NotificacionParto::find($notificacion->notificable_id);
                $titulo      = 'NOTIFICACION PARTO';
                $abreviacion = 'np';
                break;
            case 'App\NotificacionEmbarazo':
                $class       = \App\NotificacionEmbarazo::find($notificacion->notificable_id);
                $titulo      = 'NOTIFICACION MUJER EMBARAZADA';
                $abreviacion = 'nme';
                break;


            default:
                $class = null;
        }


        if (!is_null($class)) {
            // return dd("estoy por aca",$class->toArray());
            if ($notificacion->tipo == 'web') {
                $mujer = \App\Mujer::where('idAndroid', $class->idMujerAndroid)->first();
            } else {
                $mujer = \App\Mujer::where('idAndroid', $class->idMujerAndroid)->first();
            }
            if (isset($mujer)) {

                $mujer = [
                    'nombres'          => $mujer->persona->nombres.' '.$mujer->persona->segundoNombres.' '.$mujer->persona->primerApellido.' '.$mujer->persona->segundoApellido,
                    'edad'             => $mujer->persona->edad,
                    'numero_carnet'    => $mujer->persona->numeroCarnet,
                    'direccion'        => $mujer->persona->direccion,
                    'numero_celular'   => $mujer->persona->numeroCelular,
                    'fecha_nacimiento' => $mujer->persona->fechaNacimiento->format('d/m/Y'),
                    'email'            => $mujer->persona->email,
                    'mujer_id'         => $mujer->IdMujer
                ];
            } else {

                try {
                    if ($class->fechaNacimiento && $class->fechaNacimiento != '') {
                        $fecha = Carbon::createFromFormat('d/m/Y', $class->fechaNacimiento);
                        $fecha = $fecha->format('d/m/Y');
                    } else {
                        $fecha = "";
                    }

                    //   $fecha=Carbon::
                } catch (\Exception $e) {
                    $fecha = '';
                }
                $mujer = [
                    'nombres'          => $class->nombres.' '.$class->primerApellido.' '.$class->segundoApellido,
                    'edad'             => $class->edad,
                    'numero_carnet'    => '',
                    'direccion'        => $class->direccion,
                    'numero_celular'   => $class->telefono,
                    'fecha_nacimiento' => $fecha,
                    'email'            => '',
                    'mujer_id'         => 0
                ];
            }
        }


        Auditoria::guardar(\Auth::user()->id, 41, 'notificaciones',
            'notificaciones', '', 'ver notificacion', true, $request);

        return view('notificaciones.ver', [
            'notificacion' => $notificacion,
            'notificable'  => $class,
            'titulo'       => $titulo,
            'abreviacion'  => $abreviacion,
            'mujer'        => $mujer
        ]);
    }
}
