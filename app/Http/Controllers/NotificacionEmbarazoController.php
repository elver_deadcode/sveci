<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;

class NotificacionEmbarazoController extends Controller
{
    public function index()
    {
        return view('notificacionEmbarazo.index');
    }

    public function listar(Request $request)
    {

        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;

        switch ($nivel) {
            case 'nacional':
                $notificaciones = \App\NotificacionEmbarazo::orderBy('created_at', 'DESC')->get();
                break;

            case 'depto':

                $codigos_establecimientos = array_values(\App\Departamento::obtenerEstablecimientos($idnivel)->toArray());
                $notificaciones           = \App\NotificacionEmbarazo::whereIn('codEstablecimiento',
                    $codigos_establecimientos)->orderBy('created_at', 'DESC')->get();

            case 'muni':
                $codigos_establecimientos = \App\Municipio::obtenerCodigosEstablecimientosPorMunicipio($idnivel);
                $notificaciones           = \App\NotificacionEmbarazo::whereIn('codEstablecimiento',
                    $codigos_establecimientos)->orderBy('created_at', 'DESC')->get();

                break;
            case 'establ':
                $notificaciones = \App\NotificacionEmbarazo::where('codEstablecimiento',
                    $idnivel)->orderBy('created_at', 'DES')->get();
                break;

        }

        return \DataTables::of($notificaciones)
            ->addColumn('vigilante', function ($notificacion) {

                return $notificacion->vigilante->nombre_completo;

            })
            ->addColumn('establecimiento', function ($notificacion) {

                return $notificacion->obtenerEstablecimiento()->nomestabl;

            })
            ->addColumn('municipio', function ($notificacion) {

                return $notificacion->obtenerMunicipio()->mnc_nombre;

            })
            ->addColumn('departamento', function ($notificacion) {

                return $notificacion->obtenerDepartamento()->dpt_nombre;

            })
            ->addColumn('fecharegistro', function ($notificacion) {

                return $notificacion->created_at->format('d/m/Y H:i:s');

            })
            ->addColumn('fechaenvio', function ($notificacion) {

                return $notificacion->fechaRegistro->format('d/m/Y H:i:s');

            })
            ->addColumn('acciones', function ($notificacion) {
                return view('notificacionEmbarazo.acciones', compact('notificacion'))->render();
            })
            ->rawColumns(['acciones'])
            ->make(true);
    }

    public function guardarDetalle(Request $request)
    {
        try {
            $notificacion  = \App\NotificacionEmbarazo::find($request->id_notificacion);
            $plan_de_parto = 'false';

            // return dd($request->all());
            $controles_prenatales = 0;
            $comentarios          = '';
            if (isset($request->comentarios)) {
                $comentarios = $request->comentarios;
            }
            if (isset($request->plan_parto)) {
                $plan_de_parto = $request->plan_parto;
            }
            if (!is_null($request->numero_controles)) {
                $controles_prenatales = $request->numero_controles;


            }
            if ($request->edad <> $notificacion->edad) {
                $notificacion->edad = $request->edad;
                $notificacion->save();
            }

            if (is_null($notificacion->detalle)) {


                $notificacion->detalle()->create([
                    'numeroControlesPrenatal' => $controles_prenatales,
                    'IdNotificacionEmbarazo'  => $request->id_notificacion,
                    'planPartoLlenado'        => $plan_de_parto,
                    'comentarios'             => $comentarios

                ]);
                Auditoria::guardar(\Auth::user()->id, 44, 'notificaciones',
                    'notificaciones', 'notificaciones embarazo', 'boton guardar indicadores', true, $request);

            } else {
                $notificacion->detalle->update([
                    'numeroControlesPrenatal' => $controles_prenatales,
                    'IdNotificacionEmbarazo'  => $request->id_notificacion,
                    'planPartoLlenado'        => $plan_de_parto,
                    'comentarios'             => $comentarios

                ]);
                Auditoria::guardar(\Auth::user()->id, 44, 'notificaciones',
                    'notificaciones', 'notificaciones embarazo', 'boton guardar indicadores', true, $request);

            }

            if ($request->tipo == 'cerrar') {
                $notificacion->cerrado       = true;
                $notificacion->fecha_cerrado = date('Y-m-d');
                $notificacion->save();
                Auditoria::guardar(\Auth::user()->id, 45, 'notificaciones',
                    'notificaciones', 'notificaciones embarazo', 'boton cerrar notificacion ', true, $request);

            }

            return response()->json([
                'status' => true
            ], 200);
        } catch (\Exception $e) {

            return dd($e);
            return response()->json([
                'status' => false
            ], 500);
        }

    }
}
