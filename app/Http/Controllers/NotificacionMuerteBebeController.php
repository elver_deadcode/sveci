<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\NotificacionMuerteBebe;
use Illuminate\Http\Request;

class NotificacionMuerteBebeController extends Controller
{
    public function guardarDetalle(Request $request)
    {
        try {
            //   return dd($request->all());

            $notificacion        = NotificacionMuerteBebe::find($request->id_notificacion);
            $autopsia            = false;
            $planaccion          = false;
            $fecha_nacimiento    = null;
            $fecha_fallecimiento = null;
            $comentarios         = '';

            if (isset($request->autopsia)) {
                $autopsia = true;
            }
            if (isset($request->planaccion)) {
                $planaccion = true;
            }
            if (isset($request->comentarios)) {
                $comentarios = $request->comentarios;
            }
            if ($request->nacido == "true") {
                $fecha_nacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                    $request->fechaNacimiento)->format('Y-m-d');
                $fecha_fallecimiento = \Carbon\Carbon::createFromFormat('d/m/Y',
                    $request->fechafallecimiento)->format('Y-m-d');
            }

            if (is_null($notificacion->detalle)) {
                $notificacion->detalle()->create([
                    "fechanacimiento"          => $fecha_nacimiento,
                    "fechafallecimiento"       => $fecha_fallecimiento,
                    "nacido"                   => $request->nacido,
                    "tieneAutopsiaVerbal"      => $autopsia,
                    "tienePlanAccion"          => $planaccion,
                    'user_id'                  => auth()->user()->id,
                    'comentarios'              => $comentarios,
                    "IdNotificacionMuerteBebe" => $request->id_notificacion

                ]);
                Auditoria::guardar(\Auth::user()->id, 42, 'notificaciones',
                    'notificaciones', 'notificaciones muerte bebe', 'boton guardar indicadores', true, $request);

            } else {
                $notificacion->detalle->update([
                    "fechanacimiento"     => $fecha_nacimiento,
                    "fechafallecimiento"  => $fecha_fallecimiento,
                    "nacido"              => $request->nacido,
                    "tienePlanAccion"     => $planaccion,
                    "tieneAutopsiaVerbal" => $autopsia,
                    'comentarios'         => $comentarios,
                    'user_id'             => auth()->user()->id,

                ]);
                Auditoria::guardar(\Auth::user()->id, 42, 'notificaciones',
                    'notificaciones', 'notificaciones muerte bebe', 'boton guardar indicadores', true, $request);

            }

            if ($request->tipo == 'cerrar') {
                $notificacion->cerrado       = true;
                $notificacion->fecha_cerrado = date('Y-m-d');
                $notificacion->save();
                Auditoria::guardar(\Auth::user()->id, 43, 'notificaciones',
                    'notificaciones', 'notificaciones muerte bebe', 'cerrar y guardar indicadores', true, $request);

            }

            return response()->json([
                'status' => true
            ], 200);
        } catch (\Exception $e) {

            return dd($e);
            return response()->json([
                'status' => false
            ], 500);
        }

    }
}
