<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\NotificacionMuerteMujer;
use Illuminate\Http\Request;

class NotificacionMuerteMujerController extends Controller
{


    public function guardarDetalle(Request $request)
    {
        // return dd($request->all());
        try {
            $notificacion         = \App\NotificacionMuerteMujer::find($request->id_notificacion);
            $ficha_epidemiologica = false;
            $plan_accion          = false;
            $caso_atendido        = false;
            $fechaFallecimiento   = date('Y-m-d');
            $comentarios          = "";
            if (isset($request->ficha)) {
                $ficha_epidemiologica = $request->ficha;
            }
            if (isset($request->planaccion)) {
                $plan_accion = true;
            }
            if (isset($request->casofueatendido)) {
                $caso_atendido = true;
            }

            if (isset($request->comentarios)) {
                $comentarios = $request->comentarios;
            }

            if (!is_null($request->fechafallecimiento)) {
                $fechaFallecimiento = \Carbon\Carbon::createFromFormat('d/m/Y',
                    $request->fechafallecimiento)->format('Y-m-d');

            } else {
                $fechaFallecimiento = null;
            }
            $notificacion->edad = $request->edad;
            $notificacion->save();

            if (is_null($notificacion->detalle)) {


                $notificacion->detalle()->create([
                    "muerteDurante"             => $request->muertedurante,
                    "fechaFallecimiento"        => $fechaFallecimiento,
                    "tieneFicha"                => $ficha_epidemiologica,
                    "tienePlanAccion"           => $plan_accion,
                    "casoFueAtendido"           => $caso_atendido,
                    "comentarios"               => $comentarios,
                    "IdNotificacionMuerteMujer" => $request->id_notificacion,
                    'user_id'                   => auth()->user()->id


                ]);
                Auditoria::guardar(\Auth::user()->id, 46, 'notificaciones',
                    'notificaciones', 'notificaciones muerte mujer', 'boton guardar indicadores', true, $request);

            } else {
                //   return dd($ficha_epidemiologica);
                $notificacion->detalle->update([
                    "muerteDurante"      => $request->muertedurante,
                    "fechaFallecimiento" => $fechaFallecimiento,
                    "tieneFicha"         => $ficha_epidemiologica,
                    "tienePlanAccion"    => $plan_accion,
                    "casoFueAtendido"    => $caso_atendido,
                    "comentarios"        => $comentarios,
                    'user_id'            => auth()->user()->id

                ]);
                Auditoria::guardar(\Auth::user()->id, 46, 'notificaciones',
                    'notificaciones', 'notificaciones muerte mujer', 'boton guardar indicadores', true, $request);

            }


            if ($request->tipo == 'cerrar') {
                $notificacion->cerrado       = true;
                $notificacion->fecha_cerrado = date('Y-m-d');
                $notificacion->save();
                Auditoria::guardar(\Auth::user()->id, 47, 'notificaciones',
                    'notificaciones', 'notificaciones muerte mujer', 'boton  cerrar notificacion', true, $request);

            }

            return response()->json([
                'status' => true
            ], 200);
        } catch (\Exception $e) {

            return dd($e);
            return response()->json([
                'status' => false
            ], 500);
        }

    }
}
