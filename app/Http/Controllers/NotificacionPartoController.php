<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\NotificacionParto;
use Illuminate\Http\Request;

class NotificacionPartoController extends Controller
{
    public function guardarDetalle(Request $request)
    {
        try {
            //   return dd($request->all());

            $notificacion = NotificacionParto::find($request->id_notificacion);
            $comentarios  = '';
            if (isset($request->comentarios)) {
                $comentarios = $request->comentarios;
            }

            if (is_null($notificacion->detalle)) {


                $notificacion->detalle()->create([
                    'IdNotificacionParto' => $request->id_notificacion,
                    'partera'             => $request->partera,
                    'comentarios'         => $comentarios
                ]);
                Auditoria::guardar(\Auth::user()->id, 48, 'notificaciones',
                    'notificaciones', 'notificaciones parto', 'boton guardar indicadores', true, $request);

            } else {
                $notificacion->detalle->update([
                    'partera'     => $request->partera,
                    'comentarios' => $comentarios


                ]);
                Auditoria::guardar(\Auth::user()->id, 48, 'notificaciones',
                    'notificaciones', 'notificaciones parto', 'boton guardar indicadores', true, $request);

            }


            $notificacion->atendidoNombre = $request->atendidoNombre;

            if ($request->tipo == 'cerrar') {
                $notificacion->cerrado       = true;
                $notificacion->fecha_cerrado = date('Y-m-d');
            }
            $notificacion->save();
            Auditoria::guardar(\Auth::user()->id, 49, 'notificaciones',
                'notificaciones', 'notificaciones parto', 'boton cerrar notificacion', true, $request);


            return response()->json([
                'status' => true
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'status' => false
            ], 500);
        }
    }
}
