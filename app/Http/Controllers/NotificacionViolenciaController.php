<?php

namespace App\Http\Controllers;

use App\NotificacionViolencia;
use Illuminate\Http\Request;

class NotificacionViolenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NotificacionViolencia  $notificacionViolencia
     * @return \Illuminate\Http\Response
     */
    public function show(NotificacionViolencia $notificacionViolencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NotificacionViolencia  $notificacionViolencia
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificacionViolencia $notificacionViolencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NotificacionViolencia  $notificacionViolencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificacionViolencia $notificacionViolencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NotificacionViolencia  $notificacionViolencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotificacionViolencia $notificacionViolencia)
    {
        //
    }
}
