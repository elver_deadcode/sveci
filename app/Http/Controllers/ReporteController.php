<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\NotificacionTeleconsulta;
use Illuminate\Http\Request;
use App\Red;
use App\Municipio;
use App\NotificacionEmbarazo;
use App\Departamento;
use App\Establecimiento;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\NotificacionParto;
use App\NotificacionMuerteBebe;
use App\NotificacionMuerteMujer;
use App\Vigilante;

class ReporteController extends Controller
{
    public function index()
    {
        return view('reportes.index');
    }


    public function listaVigilantesComunitarios(Request $request)
    {

        $vigilantes = [];
        $niveles    = auth()->user()->getNiveles();
        $nivel      = auth()->user()->nivel;
        $idnivel    = auth()->user()->idnivel;
        $roles      = \App\Role::all();

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 15, 'reportes',
                'reportes nominales', 'lista de vigilantes comunitario', 'boton buscar', true, $request);

            $vigilantes = Vigilante::listaVigilantes($request);
            // return dd($vigilantes);
            return \DataTables::of($vigilantes)
                ->addColumn(
                    'nomestabl',
                    function ($vigilante) {
                        return $vigilante->persona->establecimiento->nomestabl;
                    }
                )
                ->addColumn(
                    'mnc_nombre',
                    function ($vigilante) {
                        return $vigilante->persona->municipio->mnc_nombre;
                    }
                )
                ->addColumn(
                    'are_nombre',
                    function ($vigilante) {
                        return $vigilante->persona->red->are_nombre;
                    }
                )
                ->addColumn(
                    'dpt_nombre',
                    function ($vigilante) {
                        return $vigilante->persona->departamento->dpt_nombre;
                    }
                )
                ->addColumn(
                    'edad',
                    function ($vigilante) {
                        return $vigilante->persona->edad;
                    }
                )
                ->rawColumns(['nomestabl', 'mnc_nombre', 'are_nombre', 'dpt_nombre', 'edad'])
                ->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 14, 'reportes',
            'reportes nominales', 'lista de vigilantes comunitario', '', true, $request);

        return view('reportes.listavigilantes.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function listaMujeres(Request $request)
    {
        $vigilantes = [];
        $niveles    = auth()->user()->getNiveles();
        $nivel      = auth()->user()->nivel;
        $idnivel    = auth()->user()->idnivel;
        $roles      = \App\Role::all();

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 17, 'reportes',
                'reportes nominales', 'lista de mujeres de 10 a  59 anios', 'boton buscar', true, $request);


            $mujeres = \App\Mujer::listaMujeres($request, $nivel, $idnivel);

            return \DataTables::of($mujeres)
                ->addColumn(
                    'nomestabl',
                    function ($mujer) {
                        return $mujer->persona->establecimiento->nomestabl;
                    }
                )
                ->addColumn(
                    'mnc_nombre',
                    function ($mujer) {
                        return $mujer->persona->municipio->mnc_nombre;
                    }
                )
                ->addColumn(
                    'are_nombre',
                    function ($mujer) {
                        return $mujer->persona->red->are_nombre;
                    }
                )
                ->addColumn(
                    'dpt_nombre',
                    function ($mujer) {
                        return $mujer->persona->departamento->dpt_nombre;
                    }
                )
                ->addColumn(
                    'edad',
                    function ($mujer) {
                        return $mujer->persona->edad;
                    }
                )
                ->rawColumns(['nomestabl', 'mnc_nombre', 'are_nombre', 'dpt_nombre', 'edad'])
                ->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 16, 'reportes',
            'reportes nominales', 'lista de mujeres de 10 a  59 anios', '', true, $request);

        return view('reportes.registro_mujeres.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function notificacionesEmbarazo(Request $request)
    {
        $notificaciones = [];
        $niveles        = auth()->user()->getNiveles();
        $nivel          = auth()->user()->nivel;
        $idnivel        = auth()->user()->idnivel;
        $roles          = \App\Role::all();


        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 19, 'reportes',
                'reportes nominales', 'lista de notificaciones de embarazo', 'boton buscar', true, $request);

            $notificaciones = \App\Notificacion::listarNotificaciones($request, $nivel, $idnivel, "embarazo");

            return \DataTables::of($notificaciones)
                ->addColumn(
                    'nombres_vigilante',
                    function ($noti) {
                        return $noti->vigilante->persona->nombre_completo;
                    }
                )
                ->addColumn(
                    'carnet',
                    function ($noti) {
                        return $noti->vigilante->persona->nombre_completo;
                    }
                )
                ->rawColumns(['nombres_vigilante'])
                ->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 18, 'reportes',
            'reportes nominales', 'lista de notificaciones de embarazo', '', true, $request);

        return view('reportes.notificaciones_embarazo.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function notificacionesMuerteMujer(Request $request)
    {
        $notificaciones = [];
        $niveles        = auth()->user()->getNiveles();
        $nivel          = auth()->user()->nivel;
        $idnivel        = auth()->user()->idnivel;
        $roles          = \App\Role::all();

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 21, 'reportes',
                'reportes nominales', 'lista de notificaciones de muerte mujer', 'boton buscar', true, $request);

            $notificaciones = \App\Notificacion::listarNotificaciones($request, $nivel, $idnivel, "muerte_mujer");

            return \DataTables::of($notificaciones)->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 20, 'reportes',
            'reportes nominales', 'lista de notificaciones de muerte de mujer', '', true, $request);

        return view('reportes.notificaciones_muertemujer.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function notificacionesMuerteBebe(Request $request)
    {
        $notificaciones = [];
        $niveles        = auth()->user()->getNiveles();
        $nivel          = auth()->user()->nivel;
        $idnivel        = auth()->user()->idnivel;
        $roles          = \App\Role::all();

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 23, 'reportes',
                'reportes nominales', 'lista de notificaciones de muerte bebe', 'boton buscar', true, $request);

            $notificaciones = \App\Notificacion::listarNotificaciones($request, $nivel, $idnivel, "muerte_bebe");

            return \DataTables::of($notificaciones)->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 22, 'reportes',
            'reportes nominales', 'lista de notificaciones de muerte bebe', '', true, $request);

        return view('reportes.notificaciones_muertebebe.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function notificacionesParto(Request $request)
    {
        $notificaciones = [];
        $niveles        = auth()->user()->getNiveles();
        $nivel          = auth()->user()->nivel;
        $idnivel        = auth()->user()->idnivel;
        $roles          = \App\Role::all();

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 25, 'reportes',
                'reportes nominales', 'lista de notificaciones de parto', 'boton buscar', true, $request);


            $notificaciones = \App\Notificacion::listarNotificaciones($request, $nivel, $idnivel, "parto");

            return \DataTables::of($notificaciones)->make(true);
        }
        Auditoria::guardar(\Auth::user()->id, 24, 'reportes',
            'reportes nominales', 'lista de notificaciones de parto', '', true, $request);

        return view('reportes.notificaciones_parto.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles
        ]);
    }

    public function obtenerLocalizacion($request)
    {

        $localizacion  = [];
        $localizacion2 = [];
        $mostrarpor_2  = '';
        $mostrarpor    = '';
        $periodos      = [];
        if ($request->periodo == 'anio') {
            if (isset($request->todosmeses)) {

                $fecha_ini  = Carbon::createFromFormat('Y-m', $request->anio.'-1')->startOfMonth()->format('Y-m-d');
                $fecha_fin  = Carbon::createFromFormat('Y-m', $request->anio.'-12')->endOfMonth()->format('Y-m-d');
                $periodos[] = [$fecha_ini, $fecha_fin];
            } else {
                $meses = $request->multiselect;
                foreach ($meses as $mes) {
                    $fecha_ini  = Carbon::createFromFormat('Y-m',
                        $request->anio.'-'.$mes)->startOfMonth()->format('Y-m-d');
                    $fecha_fin  = Carbon::createFromFormat('Y-m',
                        $request->anio.'-'.$mes)->endOfMonth()->format('Y-m-d');
                    $periodos[] = [$fecha_ini, $fecha_fin];
                }
            }
        }

        if ($request->periodo == 'periodo') {
            $fecha_ini  = Carbon::createFromFormat('d/m/Y', $request->fechaini)->format('Y-m-d');
            $fecha_fin  = Carbon::createFromFormat('d/m/Y', $request->fechafin)->format('Y-m-d');
            $periodos[] = [$fecha_ini, $fecha_fin];
        }
        switch ($request->nivel) {
            case 'nacional':

                if (isset($request->mostrarpor)) {
                    $mostrarpor = $request->mostrarpor;
                    switch ($request->mostrarpor) {
                        case 'dpto':
                            $localizacion = \App\Departamento::select('dpt_codigo', 'dpt_nombre')
                                ->orderBy('dpt_nombre', 'ASC')->get();
                            break;
                        case 'red':
                            $localizacion = Red::select('are_codigo', 'are_nombre')
                                ->orderBy('are_nombre', 'ASC')->get();
                            break;
                        case 'municipal':
                            $localizacion = Municipio::select('mnc_codigo', 'mnc_nombre')
                                ->orderBy('mnc_nombre', 'ASC')->get();
                            break;
                    }
                }
                $o               = new \stdClass();
                $o->codigo       = '0';
                $o->nombre       = 'nacional';
                $localizacion2[] = $o;
                $mostrarpor_2    = 'nacional';
                // return dd($datos);
                break;

            case 'depto':
                if (isset($request->mostrarpor)) {
                    $mostrarpor = $request->mostrarpor;
                    switch ($request->mostrarpor) {

                        case 'red':
                            $localizacion = Red::select('are_codigo', 'are_nombre')
                                ->where('dpt_codigo', $request->dpto)
                                ->orderBy('are_nombre', 'ASC')->get();
                            break;
                        case 'municipal':
                            $dpto         = Departamento::where('dpt_codigo', $request->dpto)->first();
                            $localizacion = $dpto->obtenerMunicipios();
                            break;
                        case 'establecimiento':
                            $localizacion = Departamento::obtenerEstablecimientos($request->dpto);
                            break;
                    }
                }
                $localizacion2[] = Departamento::where('dpt_codigo', $request->dpto)->first();
                $mostrarpor_2    = 'dpto';
                break;

            case 'red':

                if (isset($request->mostrarpor)) {
                    $mostrarpor = $request->mostrarpor;
                    switch ($request->mostrarpor) {

                        case 'municipal':
                            $localizacion = Red::obtenerMunicipiosPorRed($request->red);
                            break;
                        case 'establecimiento':
                            $localizacion = Red::obtenerEstablecimientosPorRed($request->red);
                            break;
                    }
                }
                $localizacion2[] = Red::where('are_codigo', $request->red)->first();
                $mostrarpor_2    = 'red';
                break;

            case 'muni':
                $mostrarpor = 'establecimiento';
                $codigos    = \App\Municipio::obtenerCodigosEstablecimientosPorMunicipio($request->municipio);
                foreach ($codigos as $codigo) {

                    $localizacion[] = Establecimiento::obtenerUltimoEstablecimiento($codigo['codestabl']);
                }
                $localizacion2[] = Municipio::where('mnc_codigo', $request->municipio)->first();
                $mostrarpor_2    = 'municipal';

                break;

            case 'establ':
                $mostrarpor      = 'establecimiento';
                $establecimiento = Establecimiento::obtenerUltimoEstablecimiento($request->establecimiento);
                $localizacion[]  = $establecimiento;
                $localizacion2[] = $establecimiento;
                $mostrarpor_2    = 'establecimiento';
                //$datos = NotificacionEmbarazo::reporteMujeresEmbarazadas($localizacion, $mostrarpor, $fecha_ini, $fecha_fin);

                break;
        }     //fin de switch  //fin de switch
        return [
            'localizacion'  => $localizacion,
            'localizacion2' => $localizacion2,
            'periodos'      => $periodos,
            'mostrarpor_2'  => $mostrarpor_2,
            'mostrarpor'    => $mostrarpor,

        ];
    }

    public function mujeresEmbarazadas(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 4, 'reportes',
            'reportes consolidados', 'mujeres embarazadas identificadas por vijilante comunitario', '', true, $request);

        $datos     = [];
        $historial = [];
        $niveles   = \Auth::user()->getNiveles();
        $roles     = \App\Role::all();


        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 5, 'reportes', 'reportes consolidados',
                'mujeres embarazadas identificadas por vijilante comunitario', 'boton consultar', true, $request);

            $localizaciones = [];
            if (isset($request->nivel)) {
                $prenatales_15_19_cpn       = [];
                $prenatales_15_19_scpn      = [];
                $prenatales_menores_15_cpn  = [];
                $prenatales_menores_15_scpn = [];
                $prenatales_mayores_19_cpn  = [];
                $prenatales_mayores_19_scpn = [];
                $planparto_15_19_cpp        = [];
                $planparto_15_19_scpp       = [];
                $planparto_menores_15_cpp   = [];
                $planparto_menores_15_spp   = [];
                $planparto_mayores_19_cpp   = [];
                $planparto_mayores_19_spp   = [];
                $total                      = [];
                $localizaciones             = $this->obtenerLocalizacion($request);

                $localizacion = $localizaciones['localizacion'];
                //$localizacion2 = $localizaciones['localizacion2'];
                $mostrarpor = $localizaciones['mostrarpor'];
                //$mostrarpor_2 = $localizaciones['mostrarpor_2'];
                $periodos = $localizaciones['periodos'];

                $datos = NotificacionEmbarazo::reporteMujeresEmbarazadas($localizacion, $mostrarpor, $periodos);

                // $mes1 = NotificacionEmbarazo::reporteMujeresEmbarazadas($localizacion2, $mostrarpor_2, $fechaanterior_ini_1->subMonth(1)->format('Y-m-d'),  $fechaanterior_fin_1->subMonth(1)->endOfMonth()->format('Y-m-d'));

                // $mes2 = NotificacionEmbarazo::reporteMujeresEmbarazadas($localizacion2, $mostrarpor_2, $fechaanterior_ini_2->subMonth(2)->format('Y-m-d'),  $fechaanterior_fin_2->subMonth(2)->endOfMonth()->format('Y-m-d'));
                // $mes3 = NotificacionEmbarazo::reporteMujeresEmbarazadas($localizacion2, $mostrarpor_2, $fechaanterior_ini_3->subMonth(3)->format('Y-m-d'),  $fechaanterior_fin_3->subMonth(3)->endOfMonth()->format('Y-m-d'));

                // if (count($mes1) > 0) {
                //   $array = Collection::make($mes1);
                //   $prenatales_15_19_cpn[] = $array->sum('prenatales_15_19_cpn');
                //   $prenatales_15_19_scpn[] = $array->sum('prenatales_15_19_scpn');
                //   $prenatales_menores_15_cpn[] = $array->sum('prenatales_menores_15_cpn');
                //   $prenatales_menores_15_scpn[] = $array->sum('prenatales_menores_15_scpn');
                //   $prenatales_mayores_19_cpn[] = $array->sum('prenatales_mayores_19_cpn');
                //   $prenatales_mayores_19_scpn[] = $array->sum('prenatales_mayores_19_scpn');
                //   $planparto_15_19_cpp[] = $array->sum('planparto_15_19_cpp');
                //   $planparto_15_19_scpp[] = $array->sum('planparto_15_19_scpp');
                //   $planparto_menores_15_cpp[] = $array->sum('planparto_menores_15_cpp');
                //   $planparto_menores_15_spp[] = $array->sum('planparto_menores_15_spp');
                //   $planparto_mayores_19_cpp[] = $array->sum('planparto_mayores_19_cpp');
                //   $planparto_mayores_19_spp[] = $array->sum('planparto_mayores_19_spp');
                //   $total[] = $array->sum('total');

                //   $historial['periodo'][] = $fechaanterior_ini_1->format('M/Y');
                // }
                // if (count($mes2) > 0) {
                //   $array = Collection::make($mes2);
                //   $prenatales_15_19_cpn[] = $array->sum('prenatales_15_19_cpn');
                //   $prenatales_15_19_scpn[] = $array->sum('prenatales_15_19_scpn');
                //   $prenatales_menores_15_cpn[] = $array->sum('prenatales_menores_15_cpn');
                //   $prenatales_menores_15_scpn[] = $array->sum('prenatales_menores_15_scpn');
                //   $prenatales_mayores_19_cpn[] = $array->sum('prenatales_mayores_19_cpn');
                //   $prenatales_mayores_19_scpn[] = $array->sum('prenatales_mayores_19_scpn');
                //   $planparto_15_19_cpp[] = $array->sum('planparto_15_19_cpp');
                //   $planparto_15_19_scpp[] = $array->sum('planparto_15_19_scpp');
                //   $planparto_menores_15_cpp[] = $array->sum('planparto_menores_15_cpp');
                //   $planparto_menores_15_spp[] = $array->sum('planparto_menores_15_spp');
                //   $planparto_mayores_19_cpp[] = $array->sum('planparto_mayores_19_cpp');
                //   $planparto_mayores_19_spp[] = $array->sum('planparto_mayores_19_spp');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_2->format('M/Y');
                // }
                // if (count($mes3) > 0) {
                //   $array = Collection::make($mes2);
                //   $prenatales_15_19_cpn[] = $array->sum('prenatales_15_19_cpn');
                //   $prenatales_15_19_scpn[] = $array->sum('prenatales_15_19_scpn');
                //   $prenatales_menores_15_cpn[] = $array->sum('prenatales_menores_15_cpn');
                //   $prenatales_menores_15_scpn[] = $array->sum('prenatales_menores_15_scpn');
                //   $prenatales_mayores_19_cpn[] = $array->sum('prenatales_mayores_19_cpn');
                //   $prenatales_mayores_19_scpn[] = $array->sum('prenatales_mayores_19_scpn');
                //   $planparto_15_19_cpp[] = $array->sum('planparto_15_19_cpp');
                //   $planparto_15_19_scpp[] = $array->sum('planparto_15_19_scpp');
                //   $planparto_menores_15_cpp[] = $array->sum('planparto_menores_15_cpp');
                //   $planparto_menores_15_spp[] = $array->sum('planparto_menores_15_spp');
                //   $planparto_mayores_19_cpp[] = $array->sum('planparto_mayores_19_cpp');
                //   $planparto_mayores_19_spp[] = $array->sum('planparto_mayores_19_spp');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_3->format('M/Y');
                // }
                if (count($datos) > 0) {
                    $array                        = Collection::make($datos);
                    $prenatales_15_19_cpn[]       = $array->sum('prenatales_15_19_cpn');
                    $prenatales_15_19_scpn[]      = $array->sum('prenatales_15_19_scpn');
                    $prenatales_menores_15_cpn[]  = $array->sum('prenatales_menores_15_cpn');
                    $prenatales_menores_15_scpn[] = $array->sum('prenatales_menores_15_scpn');
                    $prenatales_mayores_19_cpn[]  = $array->sum('prenatales_mayores_19_cpn');
                    $prenatales_mayores_19_scpn[] = $array->sum('prenatales_mayores_19_scpn');
                    $planparto_15_19_cpp[]        = $array->sum('planparto_15_19_cpp');
                    $planparto_15_19_scpp[]       = $array->sum('planparto_15_19_scpp');
                    $planparto_menores_15_cpp[]   = $array->sum('planparto_menores_15_cpp');
                    $planparto_menores_15_spp[]   = $array->sum('planparto_menores_15_spp');
                    $planparto_mayores_19_cpp[]   = $array->sum('planparto_mayores_19_cpp');
                    $planparto_mayores_19_spp[]   = $array->sum('planparto_mayores_19_spp');
                    $total[]                      = $array->sum('total');

                    $historial['periodo'][] = 'Totales';
                }
                $historial['datos'] = [
                    ['titulo' => 'Nro Mujeres 15 a   19 años con al menos 1 CPN', 'valor' => $prenatales_15_19_cpn],
                    ['titulo' => 'Nro Mujeres 15 a   19 años Sin CPN', 'valor' => $prenatales_15_19_scpn],
                    ['titulo' => 'Nro Mujeres < 15 años   con al menos 1 CPN', 'valor' => $prenatales_menores_15_cpn],
                    ['titulo' => 'Nro Mujeres < 15 años   Sin CPN', 'valor' => $prenatales_menores_15_scpn],
                    ['titulo' => 'Nro Mujeres > 19 años   con al menos 1 CPN', 'valor' => $prenatales_mayores_19_cpn],
                    ['titulo' => 'Nro Mujeres > 19 años   Sin CPN', 'valor' => $prenatales_mayores_19_scpn],
                    ['titulo' => 'Nro Mujeres  15 a 19 años  con plan de parto', 'valor' => $planparto_15_19_cpp],
                    ['titulo' => 'Nro Mujeres  15 a 19 años  sin plan de parto', 'valor' => $planparto_15_19_scpp],
                    [
                        'titulo' => 'Nro Mujeres  menores 15 años  con plan de parto',
                        'valor'  => $planparto_menores_15_cpp
                    ],
                    [
                        'titulo' => 'Nro Mujeres  menores 15 años  sin plan de parto',
                        'valor'  => $planparto_menores_15_spp
                    ],
                    [
                        'titulo' => 'Nro Mujeres  mayores 19 años con plan de parto',
                        'valor'  => $planparto_mayores_19_cpp
                    ],
                    [
                        'titulo' => 'Nro Mujeres  mayores 19 años sin plan de parto',
                        'valor'  => $planparto_mayores_19_spp
                    ],
                    ['titulo' => 'Total', 'valor' => $total],
                ];
            }


            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }
        return view('reportes.mujeres-embarazadas', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles

        ]);
    }

    public function mujeresParto(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 6, 'reportes', 'reportes consolidados',
            'parto de mujeres  identificadas por vijilante comunitario', '', true, $request);

        $datos     = [];
        $historial = [];
        $niveles   = \Auth::user()->getNiveles();
        $roles     = \App\Role::all();
        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 7, 'reportes', 'reportes consolidados',
                'parto de mujeres  identificadas por vijilante comunitario', ' boton consultar ', true, $request);

            $localizaciones = [];
            if ($request->nivel) {

                $datos          = [];
                $localizaciones = $this->obtenerLocalizacion($request);
                $localizacion   = $localizaciones['localizacion'];
                $localizacion2  = $localizaciones['localizacion2'];
                $mostrarpor     = $localizaciones['mostrarpor'];
                $mostrarpor_2   = $localizaciones['mostrarpor_2'];
                // $fecha_ini = $localizaciones['fecha_ini'];
                // $fecha_fin = $localizaciones['fecha_fin'];
                // $fechaanterior_ini_1 = $localizaciones['fechaanterior_ini_1'];
                // $fechaanterior_fin_1 = $localizaciones['fechaanterior_fin_1'];
                // $fechaanterior_ini_2 = $localizaciones['fechaanterior_ini_2'];
                // $fechaanterior_fin_2 = $localizaciones['fechaanterior_fin_2'];
                // $fechaanterior_ini_3 = $localizaciones['fechaanterior_ini_3'];
                // $fechaanterior_fin_3 = $localizaciones['fechaanterior_fin_3'];
                $capacitada = [];
                $empirica   = [];
                $total      = [];
                $periodos   = $localizaciones['periodos'];
                $datos      = NotificacionParto::reporteConsolidadoParto($localizacion, $mostrarpor, $periodos);

                // $mes1 = NotificacionParto::reporteConsolidadoParto($localizacion2, $mostrarpor_2, $fechaanterior_ini_1->subMonth(1)->format('Y-m-d'),  $fechaanterior_fin_1->subMonth(1)->endOfMonth()->format('Y-m-d'));
                // $mes2 = NotificacionParto::reporteConsolidadoParto($localizacion2, $mostrarpor_2, $fechaanterior_ini_2->subMonth(2)->format('Y-m-d'),  $fechaanterior_fin_2->subMonth(2)->endOfMonth()->format('Y-m-d'));
                // $mes3 = NotificacionParto::reporteConsolidadoParto($localizacion2, $mostrarpor_2, $fechaanterior_ini_3->subMonth(3)->format('Y-m-d'),  $fechaanterior_fin_3->subMonth(3)->endOfMonth()->format('Y-m-d'));
                // if (count($mes1) > 0) {
                //   $array = Collection::make($mes1);
                //   $capacitada[] = $array->sum('capacitada');
                //   $empirica[] = $array->sum('empirica');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_1->format('M/Y');
                // }
                // if (count($mes2) > 0) {
                //   $array = Collection::make($mes2);
                //   $capacitada[] = $array->sum('capacitada');
                //   $empirica[] = $array->sum('empirica');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_2->format('M/Y');
                // }
                // if (count($mes3) > 0) {
                //   $array = Collection::make($mes3);
                //   $capacitada[] = $array->sum('capacitada');
                //   $empirica[] = $array->sum('empirica');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_3->format('M/Y');
                // }
                if (count($datos) > 0) {
                    $array                  = Collection::make($datos);
                    $capacitada[]           = $array->sum('capacitada');
                    $empirica[]             = $array->sum('empirica');
                    $total[]                = $array->sum('total');
                    $historial['periodo'][] = 'Totales';
                }
                $historial['datos'] = [
                    ['titulo' => 'Nro. Atendidos por Partera capacitada', 'valor' => $capacitada],
                    ['titulo' => 'Nro. Atendidos por Partera empirica', 'valor' => $empirica],
                    ['titulo' => 'Total', 'valor' => $total],
                ];
            }

            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }

        return view('reportes.mujeres-parto', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles


        ]);
    }

    public function muertesVigilante(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 8, 'reportes',
            'reportes consolidados', 'muertes fetales y neonatales identificadas por vijilante comunitario', '', true,
            $request);

        $datos     = [];
        $niveles   = \Auth::user()->getNiveles();
        $nivel     = auth()->user()->nivel;
        $idnivel   = auth()->user()->idnivel;
        $historial = [];
        $roles     = \App\Role::all();


        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 9, 'reportes',
                'reportes consolidados', 'muertes fetales y neonatales identificadas por vijilante comunitario',
                'boton consultar', true, $request);
            if ($request->nivel) {
                // return dd($request->nivel);
                $datos          = [];
                $localizaciones = $this->obtenerLocalizacion($request);
                $localizacion   = $localizaciones['localizacion'];
                $localizacion2  = $localizaciones['localizacion2'];
                $mostrarpor     = $localizaciones['mostrarpor'];
                $mostrarpor_2   = $localizaciones['mostrarpor_2'];
                $periodos       = $localizaciones['periodos'];
                // $fecha_ini = $localizaciones['fecha_ini'];
                // $fecha_fin = $localizaciones['fecha_fin'];
                // $fechaanterior_ini_1 = $localizaciones['fechaanterior_ini_1'];
                // $fechaanterior_fin_1 = $localizaciones['fechaanterior_fin_1'];
                // $fechaanterior_ini_2 = $localizaciones['fechaanterior_ini_2'];
                // $fechaanterior_fin_2 = $localizaciones['fechaanterior_fin_2'];
                // $fechaanterior_ini_3 = $localizaciones['fechaanterior_ini_3'];
                // $fechaanterior_fin_3 = $localizaciones['fechaanterior_fin_3'];
                $muertesfetales_uv   = [];
                $muertesfetales_uvpa = [];
                $muertes7dias_uv     = [];
                $muertes7dias_uvpa   = [];
                $muertes28dias_uv    = [];
                $muertes28dias_uvpa  = [];
                $muertesfetales      = [];
                $muertes7dias        = [];
                $muertes28dias       = [];
                $total               = [];
                $datos               = NotificacionMuerteBebe::reporteConsolidadoMuertesPorVigilante($localizacion,
                    $mostrarpor, $periodos);
                // $mes1 = NotificacionMuerteBebe::reporteConsolidadoMuertesPorVigilante($localizacion2, $mostrarpor_2, $fechaanterior_ini_1->subMonth(1)->format('Y-m-d'),  $fechaanterior_fin_1->subMonth(1)->endOfMonth()->format('Y-m-d'));
                // $mes2 = NotificacionMuerteBebe::reporteConsolidadoMuertesPorVigilante($localizacion2, $mostrarpor_2, $fechaanterior_ini_3->subMonth(2)->format('Y-m-d'),  $fechaanterior_fin_3->subMonth(2)->endOfMonth()->format('Y-m-d'));
                // $mes3 = NotificacionMuerteBebe::reporteConsolidadoMuertesPorVigilante($localizacion2, $mostrarpor_2, $fechaanterior_ini_3->subMonth(3)->format('Y-m-d'),  $fechaanterior_fin_3->subMonth(3)->endOfMonth()->format('Y-m-d'));
                // if (count($mes1) > 0) {
                //   $array = Collection::make($mes1);
                //   $muertesfetales_uv[] = $array->sum('muertesfetales_uv');
                //   $muertesfetales_uvpa[] = $array->sum('muertesfetales_uvpa');
                //   $muertes7dias_uv[] = $array->sum('muertes7dias_uv');
                //   $muertes7dias_uvpa[] = $array->sum('muertes7dias_uvpa');
                //   $muertes28dias_uv[] = $array->sum('muertes28dias_uv');
                //   $muertes28dias_uvpa[] = $array->sum('muertes28dias_uvpa');
                //   $muertesfetales[] = $array->sum('muertesfetales');
                //   $muertes7dias[] = $array->sum('muertes7dias');
                //   $muertes28dias[] = $array->sum('muertes28dias');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_1->format('M/Y');
                // }
                // if (count($mes2) > 0) {
                //   $array = Collection::make($mes2);
                //   $muertesfetales_uv[] = $array->sum('muertesfetales_uv');
                //   $muertesfetales_uvpa[] = $array->sum('muertesfetales_uvpa');
                //   $muertes7dias_uv[] = $array->sum('muertes7dias_uv');
                //   $muertes7dias_uvpa[] = $array->sum('muertes7dias_uvpa');
                //   $muertes28dias_uv[] = $array->sum('muertes28dias_uv');
                //   $muertes28dias_uvpa[] = $array->sum('muertes28dias_uvpa');
                //   $muertesfetales[] = $array->sum('muertesfetales');
                //   $muertes7dias[] = $array->sum('muertes7dias');
                //   $muertes28dias[] = $array->sum('muertes28dias');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_2->format('M/Y');
                // }
                // if (count($mes3) > 0) {
                //   $array = Collection::make($mes3);
                //   $muertesfetales_uv[] = $array->sum('muertesfetales_uv');
                //   $muertesfetales_uvpa[] = $array->sum('muertesfetales_uvpa');
                //   $muertes7dias_uv[] = $array->sum('muertes7dias_uv');
                //   $muertes7dias_uvpa[] = $array->sum('muertes7dias_uvpa');
                //   $muertes28dias_uv[] = $array->sum('muertes28dias_uv');
                //   $muertes28dias_uvpa[] = $array->sum('muertes28dias_uvpa');
                //   $muertesfetales[] = $array->sum('muertesfetales');
                //   $muertes7dias[] = $array->sum('muertes7dias');
                //   $muertes28dias[] = $array->sum('muertes28dias');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_3->format('M/Y');
                // }
                if (count($datos) > 0) {
                    $array                  = Collection::make($datos);
                    $muertesfetales_uv[]    = $array->sum('muertesfetales_uv');
                    $muertesfetales_uvpa[]  = $array->sum('muertesfetales_uvpa');
                    $muertes7dias_uv[]      = $array->sum('muertes7dias_uv');
                    $muertes7dias_uvpa[]    = $array->sum('muertes7dias_uvpa');
                    $muertes28dias_uv[]     = $array->sum('muertes28dias_uv');
                    $muertes28dias_uvpa[]   = $array->sum('muertes28dias_uvpa');
                    $muertesfetales[]       = $array->sum('muertesfetales');
                    $muertes7dias[]         = $array->sum('muertes7dias');
                    $muertes28dias[]        = $array->sum('muertes28dias');
                    $total[]                = $array->sum('total');
                    $historial['periodo'][] = "Totales";
                }
                $historial['datos'] = [
                    ['titulo' => 'Nro. muertes fetales(nacidos muertos con AV)', 'valor' => $muertesfetales_uv],
                    ['titulo' => 'Nro. muertes fetales(nacidos muertos AV+PA)', 'valor' => $muertesfetales_uvpa],
                    ['titulo' => 'Nro. muertes < 7 dias con (AV)', 'valor' => $muertes7dias_uv],
                    ['titulo' => 'Nro. muertes < 7 dias con (AV+PA)', 'valor' => $muertes7dias_uvpa],
                    ['titulo' => 'Nro. muertes de 7 a 28  dias con (AV)', 'valor' => $muertes28dias_uv],
                    ['titulo' => 'Nro. muertes de 7 a 28  dias con (AV+PA)', 'valor' => $muertes28dias_uvpa],
                    ['titulo' => 'Nro. muertes fetales(nacidos muertos sin  AV)', 'valor' => $muertesfetales],
                    ['titulo' => 'Nro. muertes  < 7 dias (sin  AV)', 'valor' => $muertes7dias],
                    ['titulo' => 'Nro. muertes   7 a 28  dias (sin  AV)', 'valor' => $muertes28dias],
                    ['titulo' => 'Total', 'valor' => $total],
                ];
            }
            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }

        return view('reportes.muertes-vigilante', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles

        ]);
    }

    public function muertesAutopsia(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 10, 'reportes',
            'reportes consolidados', 'muertes de mujeres identificadas por vijilante comunitario', '', true, $request);

        $datos     = [];
        $niveles   = \Auth::user()->getNiveles();
        $nivel     = auth()->user()->nivel;
        $idnivel   = auth()->user()->idnivel;
        $historial = [];

        $roles = \App\Role::all();


        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 11, 'reportes',
                'reportes consolidados', 'muertes de mujeres identificadas por vijilante comunitario',
                'boton consultar', true, $request);
            if ($request->nivel) {
                $datos          = [];
                $localizaciones = $this->obtenerLocalizacion($request);
                $localizacion   = $localizaciones['localizacion'];
                $localizacion2  = $localizaciones['localizacion2'];
                $mostrarpor     = $localizaciones['mostrarpor'];
                $mostrarpor_2   = $localizaciones['mostrarpor_2'];
                $periodos       = $localizaciones['periodos'];

                // $fecha_ini = $localizaciones['fecha_ini'];
                // $fecha_fin = $localizaciones['fecha_fin'];
                // $fechaanterior_ini_1 = $localizaciones['fechaanterior_ini_1'];
                // $fechaanterior_fin_1 = $localizaciones['fechaanterior_fin_1'];
                // $fechaanterior_ini_2 = $localizaciones['fechaanterior_ini_2'];
                // $fechaanterior_fin_2 = $localizaciones['fechaanterior_fin_2'];
                // $fechaanterior_ini_3 = $localizaciones['fechaanterior_ini_3'];
                // $fechaanterior_fin_3 = $localizaciones['fechaanterior_fin_3'];
                $gav               = [];
                $gavpa             = [];
                $g1av              = [];
                $g1avpa            = [];
                $ninguno           = [];
                $casos_atendidos   = [];
                $casos_registrados = [];
                $total             = [];
                $datos             = NotificacionMuerteMujer::reporteConsolidadoMuerteMujer($localizacion, $mostrarpor,
                    $periodos);
                // $mes1 = NotificacionMuerteMujer::reporteConsolidadoMuerteMujer($localizacion2, $mostrarpor_2, $fechaanterior_ini_1->subMonth(1)->format('Y-m-d'),  $fechaanterior_fin_1->subMonth(1)->endOfMonth()->format('Y-m-d'));
                // $mes2 = NotificacionMuerteMujer::reporteConsolidadoMuerteMujer($localizacion2, $mostrarpor_2, $fechaanterior_ini_2->subMonth(2)->format('Y-m-d'),  $fechaanterior_fin_2->subMonth(2)->endOfMonth()->format('Y-m-d'));
                // $mes3 = NotificacionMuerteMujer::reporteConsolidadoMuerteMujer($localizacion2, $mostrarpor_2, $fechaanterior_ini_3->subMonth(3)->format('Y-m-d'),  $fechaanterior_fin_3->subMonth(3)->endOfMonth()->format('Y-m-d'));

                // if (count($mes1) > 0) {
                //   $array = Collection::make($mes1);
                //   $gav[] = $array->sum('gav');
                //   $gavpa[] = $array->sum('gavpa');
                //   $g1av[] = $array->sum('g1av');
                //   $g1avpa[] = $array->sum('g1avpa');
                //   $ninguno[] = $array->sum('ninguno');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_1->format('M/Y');
                // }
                // if (count($mes2) > 0) {
                //   $array = Collection::make($mes2);
                //   $gav[] = $array->sum('gav');
                //   $gavpa[] = $array->sum('gavpa');
                //   $g1av[] = $array->sum('g1av');
                //   $g1avpa[] = $array->sum('g1avpa');
                //   $ninguno[] = $array->sum('ninguno');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_2->format('M/Y');
                // }
                // if (count($mes3) > 0) {
                //   $array = Collection::make($mes3);
                //   $gav[] = $array->sum('gav');
                //   $gavpa[] = $array->sum('gavpa');
                //   $g1av[] = $array->sum('g1av');
                //   $g1avpa[] = $array->sum('g1avpa');
                //   $ninguno[] = $array->sum('ninguno');
                //   $total[] = $array->sum('total');
                //   $historial['periodo'][] = $fechaanterior_ini_3->format('M/Y');
                // }
                if (count($datos) > 0) {
                    $array                  = Collection::make($datos);
                    $gav[]                  = $array->sum('gav');
                    $gavpa[]                = $array->sum('gavpa');
                    $g1av[]                 = $array->sum('g1av');
                    $g1avpa[]               = $array->sum('g1avpa');
                    $ninguno[]              = $array->sum('ninguno');
                    $casos_atendidos[]      = $array->sum('casos_atendidos');
                    $casos_registrados[]    = $array->sum('casos_registrados');
                    $total[]                = $array->sum('total');
                    $historial['periodo'][] = "Totales";
                }
                $historial['datos'] = [
                    [
                        'titulo' => 'Muertes maternas directas (metodologia RAMOS) de mujeres de 10 a 59 años que inicialmente no se sabian si eran maternas o no (AV)',
                        'valor'  => $gav
                    ],
                    [
                        'titulo' => 'Muertes maternas directas (metodologia RAMOS) de mujeres de 10 a 59 años que inicialmente no se sabian si eran maternas o no (AV+PA)',
                        'valor'  => $gavpa
                    ],
                    [
                        'titulo' => 'Muertes maternas directas (metodologia RAMOS) de mujeres de 10 a 59 años que ya se conocian que estaba embarazada(AV)',
                        'valor'  => $g1av
                    ],
                    [
                        'titulo' => 'Muertes maternas directas (metodologia RAMOS) de mujeres de 10 a 59 años que ya se conocian que estaba embarazada (AV+PA)',
                        'valor'  => $g1avpa
                    ],
                    ['titulo' => 'Fallecio por otras causas con (AV)', 'valor' => $ninguno],
                    [
                        'titulo' => 'Cuenta con el dictamen del comite departamental de muerta materna',
                        'valor'  => $casos_atendidos
                    ],
                    ['titulo' => 'Numero de muertes de mujeres de 10 a 59 años', 'valor' => $casos_registrados],
                    ['titulo' => 'Total', 'valor' => $total],
                ];
            }

            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }

        return view('reportes.muertes-autopsia', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles

        ]);
    }

    public function consolidadoVigilante(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 12, 'reportes',
            'reportes consolidados', 'vigilantes comunitario', '', true, $request);

        $datos            = [];
        $niveles          = \Auth::user()->getNiveles();
        $nivel            = '';
        $deptos           = [];
        $municipios       = [];
        $redes            = [];
        $establecimientos = [];
        $comunidades      = [];
        $historial        = [];
        $niveles          = \Auth::user()->getNiveles();
        $nivel            = auth()->user()->nivel;
        $idnivel          = auth()->user()->idnivel;

        $roles  = \App\Role::all();
        $nivel2 = '';

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 13, 'reportes',
                'reportes consolidados', 'vigilantes comunitario', 'boton consultar', true, $request);
            $localizaciones = [];

            if (isset($request->nivel)) {
                $vigilantes_si_activados      = [];
                $vigilantes_no_activados      = [];
                $vigilantes_baja              = [];
                $vigilantes_notifican_android = [];
                $vigilantes_notifican_papel   = [];
                $vigilantes_no_notifican      = [];
                $total                        = [];
                $localizaciones               = $this->obtenerLocalizacion($request);

                $localizacion  = $localizaciones['localizacion'];
                $localizacion2 = $localizaciones['localizacion2'];
                $mostrarpor    = $localizaciones['mostrarpor'];
                $mostrarpor_2  = $localizaciones['mostrarpor_2'];
                $periodos      = $localizaciones['periodos'];

                $datos = Vigilante::reporteConsolidadoVigilantes($localizacion, $mostrarpor, $periodos,
                    $request->meses);

                if (count($datos) > 0) {
                    $array                          = Collection::make($datos);
                    $vigilantes_si_activados[]      = $array->sum('vigilantes_si_activados');
                    $vigilantes_no_activados[]      = $array->sum('vigilantes_no_activados');
                    $vigilantes_baja[]              = $array->sum('vigilantes_baja');
                    $vigilantes_notifican_android[] = $array->sum('vigilantes_notifican_android');
                    $vigilantes_notifican_papel[]   = $array->sum('vigilantes_notifican_papel');
                    $vigilantes_no_notifican[]      = $array->sum('vigilantes_no_notifican');
                    $total[]                        = $array->sum('total');
                    $historial['periodo'][]         = "Totales";
                }
                $historial['datos'] = [
                    ['titulo' => 'Nro vigilantes que SI activaron   su celular ', 'valor' => $vigilantes_si_activados],
                    ['titulo' => 'Nro vigilantes que NO activaron   su celular', 'valor' => $vigilantes_no_activados],
                    ['titulo' => 'Nro Vigilantes dados Baja', 'valor' => $vigilantes_baja],
                    [
                        'titulo' => 'Nro Vigilantes Activados que SI  envian notificaciones Android',
                        'valor'  => $vigilantes_notifican_android
                    ],
                    [
                        'titulo' => 'Nro Vigilantes Activados que SI  envian notificaciones Papel ',
                        'valor'  => $vigilantes_notifican_papel
                    ],
                    [
                        'titulo' => 'Nro Vigilantes Activados que NO  envian notificaciones',
                        'valor'  => $vigilantes_no_notifican
                    ],
                    // ['titulo' => 'Total', 'valor' => $total],
                ];
            }
            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }

        return view('reportes.consolidado-vigilantes', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles


        ]);
    }

    public function consolidadoTeleconsulta(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 12, 'reportes',
                           'reportes teleconsultas', ' consolidado teleconsultas', '', true, $request);

        $datos            = [];
        $niveles          = \Auth::user()->getNiveles();
        $nivel            = '';
        $deptos           = [];
        $municipios       = [];
        $redes            = [];
        $establecimientos = [];
        $comunidades      = [];
        $historial        = [];
        $niveles          = \Auth::user()->getNiveles();
        $nivel            = auth()->user()->nivel;
        $idnivel          = auth()->user()->idnivel;

        $roles  = \App\Role::all();
        $nivel2 = '';

        if ($request->ajax()) {
            Auditoria::guardar(\Auth::user()->id, 13, 'reportes',
                               'reportes consolidados', 'notificaciones teleconsultas', 'boton consultar', true, $request);
            $localizaciones = [];

            if (isset($request->nivel)) {
                $confirmadas     = [];
                $atendidas     = [];
                $canceladas            = [];
                $total                        = [];
                $localizaciones               = $this->obtenerLocalizacion($request);

                $localizacion  = $localizaciones['localizacion'];
                $localizacion2 = $localizaciones['localizacion2'];
                $mostrarpor    = $localizaciones['mostrarpor'];
                $mostrarpor_2  = $localizaciones['mostrarpor_2'];
                $periodos      = $localizaciones['periodos'];

                $datos = NotificacionTeleconsulta::reporteConsolidado($localizacion, $mostrarpor, $periodos);

                if (count($datos) > 0) {
                    $array                          = Collection::make($datos);
                    $atendidas[]      = $array->sum('atendidas');
                    $confirmadas[]      = $array->sum('confirmadas');
                    $canceladas[]              = $array->sum('canceladas');
                    $total[]                        = $array->sum('total');
                    $historial['periodo'][]         = "Totales";
                }
                $historial['datos'] = [
                    ['titulo' => 'Nro Teleconsultas Confirmadas  ', 'valor' => $confirmadas],
                    ['titulo' => 'Nro Teleconsultas   Atendidas', 'valor' => $atendidas],
                    ['titulo' => 'Nro Teleconsultas Canceladas', 'valor' => $canceladas],

                ];
            }
            return \DataTables::of($datos)->with('historial', $historial)->make(true);
        }

        return view('reportes.consolidado-teleconsultas', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'roles'            => $roles


        ]);
    }

    public function indicadoresProceso(Request $request)
    {
        $datos            = [];
        $niveles          = \Auth::user()->getNiveles();
        $nivel            = '';
        $deptos           = [];
        $municipios       = [];
        $redes            = [];
        $establecimientos = [];
        $comunidades      = [];

        $datos   = [];
        $niveles = \Auth::user()->getNiveles();
        $nivel   = auth()->user()->nivel;
        $idnivel = auth()->user()->idnivel;

        $roles = \App\Role::all();


        if ($request->ajax()) {

            if (!$request->nivel) {
                $datos = [];
            } else {
                switch ($request->nivel) {

                    case 'nacional':
                        $localizacion = \App\Departamento::select('dpt_codigo', 'dpt_nombre')
                            ->orderBy('dpt_nombre', 'ASC')->get();

                        foreach ($localizacion as $r => $v) {
                            $resultados_rango      = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('dpto',
                                $v->dpt_codigo, [15, 19], 1);
                            $resultados_menores    = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('dpto',
                                $v->dpt_codigo, [0, 15], 1);
                            $resultados_plan_parto = \App\NotificacionEmbarazo::obtenerNumeroMujeresConPlanParto('dpto',
                                $v->dpt_codigo, [0, 15], true);


                            $datos[] = [
                                'localizacion'         => $v->dpt_nombre,
                                'nmrango'              => $resultados_rango["contador"],
                                'porcentaje_rango'     => $resultados_rango["porcentaje"],
                                'nmmenores'            => $resultados_menores['contador'],
                                'porcentaje_menores'   => $resultados_menores['porcentaje'],
                                'planparto'            => $resultados_plan_parto['contador'],
                                'porcentaje_planparto' => $resultados_plan_parto['porcentaje']

                            ];
                        }


                        break;

                    case 'depto':

                        $dpto = \App\Departamento::where('dpt_codigo', $request->dpto)->first();


                        $localizacion = $dpto->obtenerMunicipios();
                        foreach ($localizacion as $r => $v) {
                            $resultados_rango      = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('municipal',
                                $v->mnc_codigo, [15, 19], 1);
                            $resultados_menores    = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('municipal',
                                $v->mnc_codigo, [0, 15], 1);
                            $resultados_plan_parto = \App\NotificacionEmbarazo::obtenerNumeroMujeresConPlanParto('municipal',
                                $v->mnc_codigo, [0, 15], true);
                            $datos[]               = [
                                'localizacion'         => $v->mnc_nombre,
                                'nmrango'              => $resultados_rango["contador"],
                                'porcentaje_rango'     => $resultados_rango["porcentaje"],
                                'nmmenores'            => $resultados_menores['contador'],
                                'porcentaje_menores'   => $resultados_menores['porcentaje'],
                                'planparto'            => $resultados_plan_parto['contador'],
                                'porcentaje_planparto' => $resultados_plan_parto['porcentaje']


                            ];
                        }

                        break;

                    case 'red':
                        $localizacion = \App\Red::obtenerEstablecimientosPorRed($request->red);
                        foreach ($localizacion as $r => $v) {

                            $establecimiento = \App\Establecimiento::where('codestabl', $v->codestabl)->first();


                            if (!is_null($establecimiento)) {
                                $resultados_rango      = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                                    $establecimiento->codestabl, [15, 19], 1);
                                $resultados_menores    = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                                    $establecimiento->codestabl, [0, 15], 1);
                                $resultados_plan_parto = \App\NotificacionEmbarazo::obtenerNumeroMujeresConPlanParto('establecimiento',
                                    $establecimiento->codestabl, [0, 15], true);
                                $datos[]               = [
                                    'localizacion'         => $establecimiento->nomestabl,
                                    'nmrango'              => $resultados_rango["contador"],
                                    'porcentaje_rango'     => $resultados_rango["porcentaje"],
                                    'nmmenores'            => $resultados_menores['contador'],
                                    'porcentaje_menores'   => $resultados_menores['porcentaje'],
                                    'planparto'            => $resultados_plan_parto['contador'],
                                    'porcentaje_planparto' => $resultados_plan_parto['porcentaje']

                                ];
                            }
                        }
                        break;

                    case 'muni':
                        $localizacion = \App\Municipio::obtenerCodigosEstablecimientosPorMunicipio($request->municipio);
                        foreach ($localizacion as $r => $v) {

                            $establecimiento = \App\Establecimiento::where('codestabl', $v['codestabl'])->first();


                            $resultados_rango      = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                                $establecimiento->codestabl, [15, 19], 1);
                            $resultados_menores    = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                                $establecimiento->codestabl, [0, 15], 1);
                            $resultados_plan_parto = \App\NotificacionEmbarazo::obtenerNumeroMujeresConPlanParto('establecimiento',
                                $establecimiento->codestabl, [0, 15], true);
                            if (!is_null($establecimiento)) {
                                $datos[] = [
                                    'localizacion'         => $establecimiento->nomestabl,
                                    'nmrango'              => $resultados_rango["contador"],
                                    'porcentaje_rango'     => $resultados_rango["porcentaje"],
                                    'nmmenores'            => $resultados_menores['contador'],
                                    'porcentaje_menores'   => $resultados_menores['porcentaje'],
                                    'planparto'            => $resultados_plan_parto['contador'],
                                    'porcentaje_planparto' => $resultados_plan_parto['porcentaje']

                                ];
                            }
                        }

                        break;

                    case 'establ':
                        $establecimiento       = \App\Establecimiento::where('codestabl',
                            $request->establecimiento)->first();
                        $resultados_rango      = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                            $establecimiento->codestabl, [15, 19], 1);
                        $resultados_menores    = \App\NotificacionEmbarazo::obtenerNumeroControlesPrenatalesPorEdad('establecimiento',
                            $establecimiento->codestabl, [0, 15], 1);
                        $resultados_plan_parto = \App\NotificacionEmbarazo::obtenerNumeroMujeresConPlanParto('establecimiento',
                            $establecimiento->codestabl, [0, 15], true);
                        $datos[]               = [
                            'localizacion'         => $establecimiento->nomestabl,
                            'nmrango'              => $resultados_rango["contador"],
                            'porcentaje_rango'     => $resultados_rango["porcentaje"],
                            'nmmenores'            => $resultados_menores['contador'],
                            'porcentaje_menores'   => $resultados_menores['porcentaje'],
                            'planparto'            => $resultados_plan_parto['contador'],
                            'porcentaje_planparto' => $resultados_plan_parto['porcentaje']

                        ];
                        break;
                }
            }

            return \DataTables::of($datos)->make(true);
        }
        // return dd($nivel);
        return view('reportes.indicadores-proceso', [
            'datos'            => $datos,
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],


        ]);
    }
}
