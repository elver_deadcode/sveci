<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    public function index(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 50, 'roles y permisos',
            'roles y permisos', '', '', true, $request);

        return view('roles.index');
    }

    public function listar()
    {
        $datos = \App\Role::where('name', '!=', 'superadmin')->orderBy('name', 'ASC')->get();
        return \DataTables::of($datos)->make(true);
    }


    public function editar(Request $request, Role $role)
    {

        // return dd($role);
        $permisos = Permission::orderBy('modulo', 'ASC')->get();
        Auditoria::guardar(\Auth::user()->id, 53, 'roles y permisos',
            'roles y permisos', 'edicion  de roles y permisos', '', true, $request);


        return view('roles.addedit', [
            'permisos' => $permisos,
            'role'     => $role
        ]);

    }

    public function nuevo(Request $request)
    {

        $permisos = Permission::orderBy('modulo', 'ASC')->get();
        Auditoria::guardar(\Auth::user()->id, 51, 'roles y permisos',
            'roles y permisos', 'registro de roles y permisos', '', true, $request);

        return view('roles.addedit', [
            'permisos' => $permisos
        ]);
    }

    public function borrar(Role $role)
    {
        if (count($role->users) > 0) {
            session()->flash('warning',
                'El rol Tiene usuarios relacionados y no se puede Borrar. Desvincule los usuarios a este rol ante de eliminarlo');
            return redirect('roles');
        }
        try {


            DB::beginTransaction();
            $role->permissions()->sync([]);
            $role->delete();
            session()->flash('success', 'El rol se elimino corretamente');

            DB::commit();


        } catch (\Exception $e) {
            DB::rollBack();
            // return dd($e);
            session()->flash('error', 'Ocurrio un error en el servidor');

        }
        return redirect('roles');
    }

    public function guardar(Request $request)
    {
        // return dd($request->all());
        try {
            if (isset($request->id)) {
                $rol = Role::find($request->id);
            } else {

                $rol = new Role();
            }
            $rol->name         = trim(strtoupper($request->nombre));
            $rol->display_name = trim(strtoupper($request->nombre)); // optional
            $rol->description  = trim(strtoupper($request->nombre)); // optional
            $rol->save();

            if (isset($request->id)) {
                $rol->permissions()->sync($request->permiso);
                Auditoria::guardar(\Auth::user()->id, 54, 'roles y permisos',
                    'roles y permisos', 'registro de roles y permisos', '', true, $request);

            } else {
                Auditoria::guardar(\Auth::user()->id, 52, 'roles y permisos',
                    'roles y permisos', 'registro de roles y permisos', '', true, $request);

                $rol->permissions()->attach($request->permiso);
            }
            //  $rol->permissions()->attach($request->permiso);
            session()->flash('success', 'El registro :  '.$rol->name.' se guardo correctamnete');

        } catch (Exception $e) {
            return dd($e);
            session()->flash('error', 'Ocurrio un error al guardar el rol');

        }
        return redirect('roles');

    }
}
