<?php

namespace App\Http\Controllers;

use App\Auditoria;
use App\ContraseniaUsuario;
use App\ParametrosSeguridad;
use App\Vigilante;
use Illuminate\Http\Request;
use App\User;
use App\Departamento;
use App\Municipio;
use App\Establecimiento;
use App\Persona;

class UserController extends Controller
{

    public function crearVigilante(User $user){
        try{
        $persona=$user->persona;
        $codigo    = \App\Vigilante::generarCodigo($persona->numeroCarnet);
        $vigilante = new \App\Vigilante();
        $vigilante->codigo   = $codigo;
        $vigilante->estado   = true;
        $vigilante->activado = false;
        $vigilante->IdUsuario = auth()->user()->id;
        $vigilante->IdPersona = $persona->IdPersona;
        $vigilante->save();

        if ($vigilante->persona->email != '') {
            try {
                if(env('APP_ENV','dev')=='produccion')
                    \Mail::to($vigilante->persona->email)->send(new \App\Mail\codigoVigilante($vigilante));
            } catch (\Exception $e) {
            }
        }
        session()->flash('success',
                         'Los datos del  Vigilante '.$vigilante->persona->nombre_completo.', se guardo correctamente');

        return redirect('vigilantes/'.$vigilante->IdVigilante.'/ver-perfil');

    } catch (\Exception $e) {
        return dd($e);

}


}
    public function index(Request $request)
    {
        Auditoria::guardar(\Auth::user()->id, 55, 'usuarios',
            'usaurios', 'lista de usuarios', '', true, $request);


        $niveles = auth()->user()->getNiveles();


        return view('usuarios.index', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos']
        ]);
    }

    public function editar(Request $request, User $user)
    {
        $niveles          = auth()->user()->getNiveles();
        $roles            = \App\Role::all();
        $nacional         = 0;
        $dpt_codigo       = 0;
        $mnc_codigo       = 0;
        $are_codigo       = 0;
        $codestabl        = 0;
        $establecimientos = $niveles['establecimientos'];
        switch ($user->nivel) {

            case 'depto':
                $dpt_codigo = $user->idnivel;
                break;
            case 'muni':
                $muni       = Municipio::where('mnc_codigo', $user->idnivel)->first();
                $dpt_codigo = $muni->departamento()->dpt_codigo;
                $mnc_codigo = $muni->mnc_codigo;
                break;
            case 'red':
                $red = \App\Red::where('are_codigo', $user->idnivel)->first();
                //  return dd($red);
                $are_codigo = $red->are_codigo;
                $dpt_codigo = $red->depto->dpt_codigo;
                break;

            case 'establ':
                $esta = Establecimiento::where('codestabl', $user->idnivel)->orderBy('idgestion', 'DESC');

                $codestabl  = $esta->first()->codestabl;
                $mnc_codigo = $esta->first()->municipio->mnc_codigo;
                $dpt_codigo = $esta->first()->municipio->departamento()->dpt_codigo;
                // $establecimientos = $esta->municipio->establecimientos;
                $establecimientos = $esta->get();
                break;
        }
        Auditoria::guardar(\Auth::user()->id, 58, 'usuarios',
            'usuarios', 'lista de usuarios', '', true, $request);
        $parametro_seguridad = $this->getParametroSeguridad();

        return view("usuarios.create_edit", [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $establecimientos,
            'roles'            => $roles,
            'redes'            => $niveles['redes'],
            'user'             => $user,
            'nacional'         => $nacional,
            'dpt_codigo'       => $dpt_codigo,
            'mnc_codigo'       => $mnc_codigo,
            'are_codigo'       => $are_codigo,
            'codestabl'        => $codestabl,
            'parametro_seguridad'=>$parametro_seguridad

        ]);
    }

    public function editarApi(Request $request, User $user)
    {

        Auditoria::guardar(\Auth::user()->id, 58, 'usuarios',
            'usuarios', 'lista de usuarios', '', true, $request);
        $parametro_seguridad = $this->getParametroSeguridad();
        $roles            = \App\Role::all();

        return view("usuarios.user_api", [
            'roles'            => $roles,
            'user'             => $user,
            'parametro_seguridad'=>$parametro_seguridad

        ]);
    }


    public function editarPerfil(Request $request, User $user)
    {
        $niveles          = auth()->user()->getNiveles();
        $roles            = \App\Role::all();
        $nacional         = 0;
        $dpt_codigo       = 0;
        $mnc_codigo       = 0;
        $are_codigo       = 0;
        $codestabl        = 0;
        $establecimientos = $niveles['establecimientos'];
        switch ($user->nivel) {

            case 'depto':
                $dpt_codigo = $user->idnivel;
                break;
            case 'muni':
                $muni       = Municipio::where('mnc_codigo', $user->idnivel)->first();
                $dpt_codigo = $muni->departamento()->dpt_codigo;
                $mnc_codigo = $muni->mnc_codigo;
                break;
            case 'red':
                $red = \App\Red::where('are_codigo', $user->idnivel)->first();
                //  return dd($red);
                $are_codigo = $red->are_codigo;
                $dpt_codigo = $red->depto->dpt_codigo;
                break;

            case 'establ':
                $esta = Establecimiento::where('codestabl', $user->idnivel)->orderBy('idgestion', 'DESC');

                $codestabl  = $esta->first()->codestabl;
                $mnc_codigo = $esta->first()->municipio->mnc_codigo;
                $dpt_codigo = $esta->first()->municipio->departamento()->dpt_codigo;
                // $establecimientos = $esta->municipio->establecimientos;
                $establecimientos = $esta->get();
                break;
        }
        Auditoria::guardar(\Auth::user()->id, 62, 'usuarios',
            'usuarios', 'editar mi usuario', 'editar mi usuario', true, $request);

        $parametro_seguridad = $this->getParametroSeguridad();

        return view("usuarios.edit_perfil", [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $establecimientos,
            'roles'            => $roles,
            'redes'            => $niveles['redes'],
            'user'             => $user,
            'nacional'         => $nacional,
            'dpt_codigo'       => $dpt_codigo,
            'mnc_codigo'       => $mnc_codigo,
            'are_codigo'       => $are_codigo,
            'codestabl'        => $codestabl,
            'parametro_seguridad'=>$parametro_seguridad

        ]);
    }

    private function getParametroSeguridad()
    {
        $parametro_seguridad = ParametrosSeguridad::first();
        if (!isset($parametro_seguridad)) {
            $parametro_seguridad = ['patron' => '', 'mensaje' => ''];
        } else {
            $parametro_seguridad = $parametro_seguridad->getPatronCalidadPassword();
        }
        return $parametro_seguridad;
    }

    public function nuevo()
    {
        $niveles = auth()->user()->getNiveles();
        $roles   = \App\Role::all();

        $parametro_seguridad = $this->getParametroSeguridad();
        return view("usuarios.create_edit", [
            'dptos'               => $niveles['dptos'],
            'municipios'          => $niveles['municipios'],
            'establecimientos'    => $niveles['establecimientos'],
            'redes'               => $niveles['redes'],
            'roles'               => $roles,
            'parametro_seguridad' => $parametro_seguridad

        ]);
    }

    public function nuevoApi()
    {
        $niveles = auth()->user()->getNiveles();
        $roles   = \App\Role::all();

        $parametro_seguridad = $this->getParametroSeguridad();
        return view("usuarios.user_api", [
            'roles'               => $roles,
            'parametro_seguridad' => $parametro_seguridad

        ]);
    }

    public function listar()
    {
        $usuarios = [];
        $user     = auth()->user();
        if ($user->nivel == 'nacional') {
            $usuarios = User::with('persona')->orderBy('username')->get();

        } elseif ($user->nivel == 'red') {

            $red = \App\Red::find($user->idnivel);

            $usuarios_red = User::with('persona')->where('nivel', 'red')->where('idnivel', $user->idnivel)->get();

            $establecimientos = Establecimiento::select('codestabl')->distinct()->where('codarea',
                $red->are_codigo)->get()->map->only(['codestabl'])->toArray();

            $usuarios_establecimientos = User::with('persona')->where('nivel', 'establ')->whereIn('idnivel',
                array_values($establecimientos))->get();
            $usuarios                  = $usuarios_red->merge($usuarios_establecimientos);
        } elseif ($user->nivel == 'depto') {

            $depto                     = Departamento::where('dpt_codigo', $user->idnivel)->first();
            $municipios                = $depto->obtenerMunicipios()->map(function ($muni) {
                return $muni->only(['mnc_codigo']);
            })->toArray();
            $establecimientos          = Establecimiento::select('codestabl')->distinct()->whereIn('codmunicip',
                array_values($municipios))->get()->map->only(['codestabl'])->toArray();
            $usuarios_departamento     = User::with('persona')->where('nivel', 'depto')->where('idnivel',
                $user->idnivel)->get();
            $usuarios_municipios       = User::with('persona')->where('nivel', 'muni')->whereIn('idnivel',
                array_values($municipios))->get();
            $usuarios_establecimientos = User::with('persona')->where('nivel', 'establ')->whereIn('idnivel',
                array_values($establecimientos))->get();
            $usuarios                  = $usuarios_departamento->merge($usuarios_municipios->merge($usuarios_establecimientos));
        } elseif ($user->nivel == 'muni') {

            $muni = Municipio::where('mnc_codigo', $user->idnivel)->first();

            $establecimientos = $muni->establecimientos()->select('codestabl')
                ->distinct()->get()->map->only(['codestabl'])->toArray();

            $usuarios_municipios       = User::with('persona')->where('nivel', 'muni')->where('idnivel',
                $muni->mnc_codigo)->get();
            $usuarios_establecimientos = User::with('persona')->where('nivel', 'establ')->whereIn('idnivel',
                array_values($establecimientos))->get();

            $usuarios = $usuarios_municipios->merge($usuarios_establecimientos);
        } elseif ($user->nivel == 'establ') {


            $usuarios = User::with('persona')->where('nivel', 'establ')->where('idnivel', $user->idnivel)->get();
        }

        return \DataTables::of($usuarios)->addColumn('localizacion', function ($usuario) {

            return $usuario->obtenerNivelUsuario();
        })
            ->addColumn('acciones', function ($usuario) {
                return view('usuarios.includes.acciones', compact('usuario'))->render();
            })
            ->rawColumns(['acciones'])
            ->make(true);
    }

    public function darBaja(Request $request)
    {

        try {
            $status = true;
            $user   = User::find($request->id);
            if ($user->estado == true) {

                $user->estado = false;
                Auditoria::guardar(\Auth::user()->id, 61, 'usuarios',
                    'usuarios', 'lista de usuarios', 'boton dar de baja', true, $request);

            } else {

                $user->estado = true;
                Auditoria::guardar(\Auth::user()->id, 60, 'usuarios',
                    'usuarios', 'lista de usuarios', 'boton dar alta', true, $request);

            }
            $user->save();
        } catch (\Exception $e) {
            $status = false;
        }

        return response()->json([
            'status' => $status
        ]);
    }
    public function guardarApi(Request $request)
    {
//        dd($request->all());
        try {
            \DB::beginTransaction();
            $username = User::generarNombreUsuario($request->nombres, $request->primer_apellido,
                $request->segundo_apellido);


            if (isset($request->usuario_id)) {
                $u           = User::find($request->usuario_id);
                $persona     = $u->persona;
                $query_email = Persona::where('email', $request->email)->where('IdPersona', '!=',
                    $persona->IdPersona)->first();

                if (!is_null($query_email)) {
                    session()->flash('warning',
                        ' El email: '.$request->email.' ya se encuentra registrado en el sistema');

                    return back()->withInput($request->input());
                }
                if ($persona->verificado == false) {

                    $persona->nombres         = strtoupper($request->nombres);
                    $persona->primerApellido  = strtoupper($request->primer_apellido);
                    $persona->segundoApellido = strtoupper($request->segundo_apellido);
                    $query_carnet             = Persona::where('numeroCarnet',
                        $request->numero_carnet)->where('IdPersona', '!=', $persona->IdPersona)->first();

                    if (!is_null($query_carnet)) {
                        session()->flash('warning',
                            ' Este numero de carnet: '.$request->numero_carnet.' ya se encuentra registrado');
                        return back()->withInput($request->input());
                    }

                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOLIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;

                    $persona->celular = $request->numero_celular;
                    $persona->email = $request->email;
//                    dd($persona);
                    $persona->save();
                } else {

                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    $persona->sexo    = $request->sexo;

                    $persona->save();
                }
                Auditoria::guardar(\Auth::user()->id, 59, 'usuarios',
                    'usuarios', 'edicion de usuarios', 'boton guardar datos', true, $request);

            } else {
                $u           = new \App\User();
                $u->username = $username;
                $persona     = Persona::where('IdPersona', $request->persona_id)->first();
                $query_email = Persona::where('email', $request->email)->first();
                if (!is_null($query_email)) {
                    session()->flash('warning',
                        ' El email: '.$request->email.' ya se encuentra registrado en el sistema');

                    return back()->withInput($request->input());
                }

                if (is_null($persona)) {


                    $persona                     = new Persona();
                    $persona->nombres            = strtoupper($request->nombres);
                    $persona->primerApellido     = strtoupper($request->primer_apellido);
                    $persona->segundoApellido    = strtoupper($request->segundo_apellido);
                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOlIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;

                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    if ($request->adscrito_id > 0 && !is_null($request->adscrito_id)) {
                        $persona->verificado = true;
                    } else {
                        $persona->verificado = false;
                    }
                    $persona->save();
                } else {
                    $query = User::where('IdPersona', $request->persona_id)->first();
                    if (!is_null($query)) {
                        session()->flash('warning',
                            ' Esta Persona con Numero de carnet: '.$request->numero_carnet.' ya se encuentra registrada como Usuario');

                        return back()->withInput($request->input());
                    } else {

                        $persona->celular = $request->numero_celular;
                        $persona->email   = $request->email;
                        $persona->save();
                    }
                }
                Auditoria::guardar(\Auth::user()->id, 57, 'usuarios',
                    'usuarios', 'registrar usuario', 'boton guardar datos', true, $request);

            }


            if (isset($request->usuario_id)) {
                if( isset($request->editpassword)) {
                    $u->password = bcrypt($request->password);
                }
            }
            else{
                $u->password = bcrypt($request->password);

            }
            $idnivel = 0;
            $nivel = 'api';
            $u->nivel     = $nivel;
            $u->idnivel   = $idnivel;
            $u->estado    = true;
            $u->IdPersona = $persona->IdPersona;
            $u->save();

            if(isset($request->password)) {
                $validar_contrasenia = $passwords = ContraseniaUsuario::validarUsuarioContrasenia($u,
                    $request->password);
                if (!$validar_contrasenia) {

                    throw new \Exception(' La contraseña ya se utilizo anteriormente. intente con otra contraseña');
                } else {
                    try {
                        ContraseniaUsuario::insert([
                            [
                                'user_id'       => $u->id,
                                'FechaDeCambio' => date('Y-m-d'),
                                'Contrasenia'   => $u->password
                            ]
                        ]);
                    } catch (\Exception $e) {

                    }
                }
            }

            if (isset($request->usuario_id)) {
                $u->roles()->sync([$request->role]);
            } else {
                $u->roles()->attach([$request->role]);
            }

            session()->flash('success', 'Los datos del usuario '.$u->nombre_completo.' se guardaron correctamente');
            \DB::commit();

            return view('usuarios.registro_completo', [
                'user'     => $u,
                'password' => $request->password
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
                dd($e);
//            session()->flash('error', 'El Usuario  no se pudo registrar,'.$e->getMessage());
//            return redirect('/usuarios');
        }


    }
    public function guardar(Request $request)
    {
        try {
            \DB::beginTransaction();
            $username = User::generarNombreUsuario($request->nombres, $request->primer_apellido,
                $request->segundo_apellido);


            if (isset($request->usuario_id)) {
                $u           = User::find($request->usuario_id);
                $persona     = $u->persona;
                $query_email = Persona::where('email', $request->email)->where('IdPersona', '!=',
                    $persona->IdPersona)->first();

                if (!is_null($query_email)) {
                    session()->flash('warning',
                        ' El email: '.$request->email.' ya se encuentra registrado en el sistema');

                    return back()->withInput($request->input());
                }
                if ($persona->verificado == false) {

                    $persona->nombres         = strtoupper($request->nombres);
                    $persona->primerApellido  = strtoupper($request->primer_apellido);
                    $persona->segundoApellido = strtoupper($request->segundo_apellido);
                    $query_carnet             = Persona::where('numeroCarnet',
                        $request->numero_carnet)->where('IdPersona', '!=', $persona->IdPersona)->first();

                    if (!is_null($query_carnet)) {
                        session()->flash('warning',
                            ' Este numero de carnet: '.$request->numero_carnet.' ya se encuentra registrado');
                        return back()->withInput($request->input());
                    }

                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOLIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;
                    $persona->codEstablecimiento = $request->establecimiento_id==""?0:$request->establecimiento_id;
                    $persona->mnc_codigo         = $request->municipio_id==""?0:$request->municipio_id;
                    $persona->dpt_codigo         = $request->departamento_id==""?0:$request->departamento_id;
                    $persona->are_codigo         = $request->red_id==""?0:$request->red_id;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;

                    $persona->email = $request->email;
                    $persona->save();
                } else {

                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    $persona->sexo    = $request->sexo;

                    $persona->save();
                }
                Auditoria::guardar(\Auth::user()->id, 59, 'usuarios',
                    'usuarios', 'edicion de usuarios', 'boton guardar datos', true, $request);

            } else {
                $u           = new \App\User();
                $u->username = $username;
                $persona     = Persona::where('IdPersona', $request->persona_id)->first();
                $query_email = Persona::where('email', $request->email)->first();
                if (!is_null($query_email)) {
                    session()->flash('warning',
                        ' El email: '.$request->email.' ya se encuentra registrado en el sistema');

                    return back()->withInput($request->input());
                }

                if (is_null($persona)) {


                    $persona                     = new Persona();
                    $persona->nombres            = strtoupper($request->nombres);
                    $persona->primerApellido     = strtoupper($request->primer_apellido);
                    $persona->segundoApellido    = strtoupper($request->segundo_apellido);
                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOlIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;
                    $persona->codEstablecimiento = $request->establecimiento_id;
                    $persona->mnc_codigo         = $request->municipio_id;
                    $persona->dpt_codigo         = $request->departamento_id;
                    $persona->are_codigo         = $request->red_id;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    if ($request->adscrito_id > 0 && !is_null($request->adscrito_id)) {
                        $persona->verificado = true;
                    } else {
                        $persona->verificado = false;
                    }
                    $persona->save();
                } else {
                    $query = User::where('IdPersona', $request->persona_id)->first();
                    if (!is_null($query)) {
                        session()->flash('warning',
                            ' Esta Persona con Numero de carnet: '.$request->numero_carnet.' ya se encuentra registrada como Usuario');

                        return back()->withInput($request->input());
                    } else {
                        $persona->codEstablecimiento = $request->establecimiento_id;
                        $persona->mnc_codigo         = $request->municipio_id;
                        $persona->dpt_codigo         = $request->departamento_id;
                        $persona->are_codigo         = $request->red_id;
                        // $persona->IdComunidad=$request->comunidad;
                        $persona->celular = $request->numero_celular;
                        $persona->email   = $request->email;
                        $persona->save();
                    }
                }
                Auditoria::guardar(\Auth::user()->id, 57, 'usuarios',
                    'usuarios', 'registrar usuario', 'boton guardar datos', true, $request);

            }


            $idnivel = 0;

            if ($request->nivel == 'depto') {
                $idnivel = $request->dpto;
                if ($request->dpto == '' || $request->dpto == 0) {
                    throw new Exception("El codigo del dpto no es valido");
                }
            } else {
                if ($request->nivel == 'muni') {
                    if (!isset($request->municipio) || $request->municipio == '' || $request->municipio == 0) {
                        throw new Exception("El codigo del dpto no es valido");
                    }
                    $idnivel = $request->municipio;
                } else {
                    if ($request->nivel == 'red') {
                        if (!isset($request->red) || $request->red == '' || $request->red == 0) {
                            throw new Exception("El codigo de la red  no es valido");
                        }
                        $idnivel = $request->red;
                    } else {
                        if ($request->nivel == 'establ') {
                            if (!isset($request->establecimiento) || $request->establecimiento == '' || $request->establecimiento == 0) {
                                throw new Exception("El codigo del establecimiento no es valido");
                            }
                            $idnivel = $request->establecimiento;
                        }
                    }
                }
            }



            if (isset($request->usuario_id)) {
                if( isset($request->editpassword)) {
                    $u->password = bcrypt($request->password);
                }
            }
            else{
                $u->password = bcrypt($request->password);

            }

            $u->nivel     = $request->nivel;
            $u->idnivel   = $idnivel;
            $u->estado    = true;
            $u->IdPersona = $persona->IdPersona;
            $u->save();
//                Actualiza los datos de localizacion como ser establecimiento,municipio, departamento de una persona
            $persona->establecerLocalizacion($u->nivel,$u->idnivel);

                if(isset($request->password)) {
                    $validar_contrasenia = $passwords = ContraseniaUsuario::validarUsuarioContrasenia($u,
                        $request->password);
                    if (!$validar_contrasenia) {

                        throw new \Exception(' La contraseña ya se utilizo anteriormente. intente con otra contraseña');
                    } else {
                        try {
                            ContraseniaUsuario::insert([
                                [
                                    'user_id'       => $u->id,
                                    'FechaDeCambio' => date('Y-m-d'),
                                    'Contrasenia'   => $u->password
                                ]
                            ]);
                        } catch (\Exception $e) {

                        }
                    }
                }

            if (isset($request->usuario_id)) {
                $u->roles()->sync([$request->role]);
            } else {
                $u->roles()->attach([$request->role]);
            }

            session()->flash('success', 'Los datos del usuario '.$u->nombre_completo.' se guardaron correctamente');
            \DB::commit();

            return view('usuarios.registro_completo', [
                'user'     => $u,
                'password' => $request->password
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            dd($e);
            session()->flash('error', 'El Usuario  no se pudo registrar,'.$e->getMessage());
            return redirect('/usuarios');
        }


    }


    public function guardarPerfil(Request $request)
    {
        // return dd($request->all());

        try {
            \DB::beginTransaction();


            if (isset($request->usuario_id)) {
                $u           = User::find($request->usuario_id);
                $persona     = $u->persona;
                $query_email = Persona::where('email', $request->email)->where('IdPersona', '!=',
                    $persona->IdPersona)->first();

                if (!is_null($query_email)) {
                    session()->flash('warning',
                        ' El email: '.$request->email.' ya se encuentra registrado en el sistema');

                    return back()->withInput($request->input());
                }
                if ($persona->verificado == false) {

                    $persona->nombres         = strtoupper($request->nombres);
                    $persona->primerApellido  = strtoupper($request->primer_apellido);
                    $persona->segundoApellido = strtoupper($request->segundo_apellido);
                    $query_carnet             = Persona::where('numeroCarnet',
                        $request->numero_carnet)->where('IdPersona', '!=', $persona->IdPersona)->first();

                    if (!is_null($query_carnet)) {
                        session()->flash('warning',
                            ' Este numero de carnet: '.$request->numero_carnet.' ya se encuentra registrado');
                        return back()->withInput($request->input());
                    }

                    $persona->numeroCarnet    = strtoupper($request->numero_carnet);
                    $persona->complemento     = strtoupper($request->complemento);
                    $persona->nacionalidad    = 'BOLIVIANA';
                    $persona->direccion       = $request->direccion;
                    $persona->fechaNacimiento = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo            = $request->sexo;
                    // $persona->codEstablecimiento = $request->establecimiento_id;
                    // $persona->mnc_codigo = $request->municipio_id;
                    // $persona->dpt_codigo = $request->departamento_id;
                    // $persona->are_codigo = $request->red_id;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;

                    $persona->email = $request->email;
                    $persona->save();
                } else {
                    // $persona->nombres = strtoupper($request->nombres);
                    // $persona->primerApellido = strtoupper($request->primer_apellido);
                    // $persona->segundoApellido = strtoupper($request->segundo_apellido);
                    // $query_carnet = Persona::where('numeroCarnet', $request->numero_carnet)->where('IdPersona', '!=', $persona->IdPersona)->first();

                    // if (!is_null($query_carnet)) {
                    //     session()->flash('warning', ' Este numero de carnet: ' . $request->numero_carnet . ' ya se encuentra registrado');
                    //     return back()->withInput($request->input());
                    // }

                    // $persona->numeroCarnet = strtoupper($request->numero_carnet);
                    // $persona->complemento = strtoupper($request->complemento);
                    // $persona->nacionalidad = 'BOLIVIANA';
                    // $persona->direccion = $request->direccion;
                    // $persona->fechaNacimiento = \Carbon\Carbon::createFromFormat('d/m/Y', $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    $persona->sexo    = $request->sexo;

                    $persona->save();
                }
            }


            if (isset($request->usuario_id) && isset($request->editpassword)) {
                $u->password = bcrypt($request->password);
            }

            $u->save();
            $validar_contrasenia=$passwords= ContraseniaUsuario::validarUsuarioContrasenia($u,$request->password);
            if(!$validar_contrasenia){

//                throw new \Exception(' La contraseña ya se utilizo anteriormente. intente con otra contraseña');
                \DB::rollback();
                session()->flash('error', 'La contraseña ya se utilizo anteriormente. intente con otra contraseña');
                return redirect('usuarios');
            }
            else{
//                dd($u->password);
                try {
                    ContraseniaUsuario::insert([
                        [
                            'user_id'       => $u->id,
                            'FechaDeCambio' => date('Y-m-d'),
                            'Contrasenia'   => $u->password
                        ]
                    ]);
                }
                catch (\Exception $e){

                }
            }

            Auditoria::guardar(\Auth::user()->id, 63, 'usuarios',
                'usuarios', 'editar mi usuario', 'boton guardar datos', true, $request);


            session()->flash('success', 'El Usuario '.$u->nombre_completo.' se registro correctamente');
            \DB::commit();

            return view('usuarios.registro_completo', [
                'user'     => $u,
                'password' => $request->password
            ]);
        } catch (Exceptopn $e) {
            \DB::rollback();
            session()->flash('error', 'El Usuario  no se pudo registrar,'.$e->getMessage());
            return redirect('usuarios');
        }


    }
}
