<?php

namespace App\Http\Controllers;

use App\Auditoria;
use Illuminate\Http\Request;
use App\Vigilante;
use App\User;
use App\Departamento;
use App\Municipio;
use App\Establecimiento;
use App\Persona;
use App\Api;

class VigilanteController extends Controller
{

    public function __construct()
    {


    }


    public function index(Request $request)
    {

        Auditoria::guardar(\Auth::user()->id, 26, 'vigilantes',
            'vigilantes', 'lista de vigilantes', '', true, $request);

        return view('vigilantes.index');
    }

    public function listar()
    {


    }

    public function crearVigilante(Request $request)
    {


        $niveles = auth()->user()->getNiveles();
        Auditoria::guardar(\Auth::user()->id, 33, 'vigilantes',
            'registro nuevo vigilante', '', '', true, $request);

        return view('vigilantes.crear_edit', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $niveles['establecimientos'],
            'redes'            => $niveles['redes'],
            'comunidades'      => $niveles['comunidades']
        ]);

    }

    public function editarVigilante(Request $request, Vigilante $vigilante)
    {
        $niveles = auth()->user()->getNiveles();


        $user             = auth()->user();
        $nacional         = 0;
        $dpt_codigo       = 0;
        $mnc_codigo       = 0;
        $are_codigo       = 0;
        $codestabl        = 0;
        $establecimientos = $niveles['establecimientos'];
        //   return dd ($establecimientos);

        $comunidades = [];
        switch ($user->nivel) {

            case 'depto':
                $dpt_codigo = $user->idnivel;
                break;
            case 'muni':
                $muni       = Municipio::where('cod_muni', $user->idnivel)->first();
                $dpt_codigo = $muni->departamento->dpt_codigo;
                $mnc_codigo = $muni->mnc_codigo;
                break;
            case 'red':
                $red = \App\Red::where('are_codigo', $user->idnivel)->first();
                //  return dd($red);
                $are_codigo = $red->are_codigo;
                $dpt_codigo = $red->depto->dpt_codigo;
                break;

            case 'establ':
                $esta = Establecimiento::where('codestabl', $user->idnivel)->orderBy('idgestion', 'DESC')->first();


                $codestabl          = $esta->codestabl;
                $mnc_codigo         = $esta->municipio->mnc_codigo;
                $dpt_codigo         = $esta->municipio->departamento()->dpt_codigo;
                $establecimientos[] = $esta;
                $muni               = $esta->municipio;
                $comunidades        = $muni->comunidades()->orderBy('nomComunidad', 'ASC')->get();

                //  $comunidades=
                break;
        }
        $establecimientos = Municipio::where('mnc_codigo', $vigilante->persona->mnc_codigo)->first()
            ->establecimientos;

        Auditoria::guardar(\Auth::user()->id, 29, 'vigilantes',
            'vigilantes', 'lista de vigilantes', 'editar vigilante', true, $request);


        return view('vigilantes.crear_edit', [
            'dptos'            => $niveles['dptos'],
            'municipios'       => $niveles['municipios'],
            'establecimientos' => $establecimientos,
            'vigilante'        => $vigilante,
            'comunidades'      => $comunidades
        ]);
    }


    public function guardar(Request $request)
    {


        try {

            $establecimiento = \App\Establecimiento::find($request->establecimiento);
            \DB::beginTransaction();
            if (isset($request->vigilante_id)) {
                $vigilante = \App\Vigilante::find($request->vigilante_id);

                $persona = $vigilante->persona;
                if ($persona->verficado == false) {

                    $persona->nombres            = strtoupper($request->nombres);
                    $persona->primerApellido     = strtoupper($request->primer_apellido);
                    $persona->segundoApellido    = strtoupper($request->segundo_apellido);
                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOlIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;
                    $persona->codEstablecimiento = $request->establecimiento;
                    $persona->mnc_codigo         = $establecimiento->codmunicip;
                    $persona->dpt_codigo         = $establecimiento->municipio->departamento()->dpt_codigo;
                    $persona->are_codigo         = $establecimiento->codarea;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;

                    $persona->save();

                } else {

                    $persona->codEstablecimiento = $request->establecimiento;
                    $persona->mnc_codigo         = $establecimiento->codmunicip;
                    $persona->dpt_codigo         = $establecimiento->municipio->departamento()->dpt_codigo;
                    $persona->are_codigo         = $establecimiento->codarea;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    $persona->save();
                }

                Auditoria::guardar(\Auth::user()->id, 30, 'vigilantes',
                    'lista de vigilantes', 'editar vigilante', 'boton guardar', true, $request);

            } else {
                $codigo    = \App\Vigilante::generarCodigo($request->numero_carnet);
                $vigilante = new \App\Vigilante();
                $persona   = Persona::where('IdPersona', $request->persona_id)->first();
                if (is_null($persona)) {

                    $persona                     = new Persona();
                    $persona->nombres            = strtoupper($request->nombres);
                    $persona->primerApellido     = strtoupper($request->primer_apellido);
                    $persona->segundoApellido    = strtoupper($request->segundo_apellido);
                    $persona->numeroCarnet       = strtoupper($request->numero_carnet);
                    $persona->complemento        = strtoupper($request->complemento);
                    $persona->nacionalidad       = 'BOlIVIANA';
                    $persona->direccion          = $request->direccion;
                    $persona->fechaNacimiento    = \Carbon\Carbon::createFromFormat('d/m/Y',
                        $request->fecha_nacimiento)->format('Y-m-d');
                    $persona->sexo               = $request->sexo;
                    $persona->codEstablecimiento = $request->establecimiento;
                    $persona->mnc_codigo         = $establecimiento->codmunicip;
                    $persona->dpt_codigo         = $establecimiento->municipio->departamento()->dpt_codigo;
                    $persona->are_codigo         = $establecimiento->codarea;
                    // $persona->IdComunidad=$request->comunidad;
                    $persona->celular = $request->numero_celular;
                    $persona->email   = $request->email;
                    if ($request->adscrito_id > 0 && !is_null($request->adscrito_id)) {
                        $persona->verificado = true;
                    } else {
                        $persona->verificado = false;
                    }
                    $persona->save();
                } else {
                    $query = Vigilante::where('IdPersona', $request->persona_id)->first();
                    if (!is_null($query)) {
                        session()->flash('warning',
                            ' Esta Persona con Numero de carnet: '.$request->numero_carnet.' ya se encuentra registrada como vigilante');

                        return back()->withInput($request->input());
                    } else {
                        $persona->codEstablecimiento = $request->establecimiento;
                        $persona->mnc_codigo         = $establecimiento->codmunicip;
                        $persona->dpt_codigo         = $establecimiento->municipio->departamento()->dpt_codigo;
                        $persona->are_codigo         = $establecimiento->codarea;
                        // $persona->IdComunidad=$request->comunidad;
                        $persona->celular = $request->numero_celular;
                        $persona->email   = $request->email;
                        $persona->save();

                    }

                }

                Auditoria::guardar(\Auth::user()->id, 34, 'vigilantes',
                    'registo vigilante', '', 'guardar  vigilante', true, $request);

            }


            if (!isset($request->vigilante_id)) {
                $vigilante->codigo   = $codigo;
                $vigilante->estado   = true;
                $vigilante->activado = false;

            }

            $vigilante->IdUsuario = auth()->user()->id;
            $vigilante->IdPersona = $persona->IdPersona;

            $vigilante->save();

            if ($vigilante->persona->email != '') {
                try {
                    if(env('APP_ENV','dev')=='produccion')
                    \Mail::to($vigilante->persona->email)->send(new \App\Mail\codigoVigilante($vigilante));
                } catch (\Exception $e) {
                }
            }
            \DB::commit();
            session()->flash('success',
                'Los datos del  Vigilante '.$vigilante->nombre_completo.', se guardo correctamente');

            return redirect('vigilantes/'.$vigilante->IdVigilante.'/ver-perfil');

        } catch (\Exception $e) {
            \DB::rollBack();
            return dd($e);


            // return dd($e);
            // session()->flash('error', 'Ocurrio un error en el servidor al guardar registrar el vigilante');

            //    return redirect()->back();
        }

    }

    public function cambiarEstado(Request $request)
    {
        try {
            $vigilante = Vigilante::find($request->id);
            $status    = true;
            if ($vigilante->estado) {
                Auditoria::guardar(\Auth::user()->id, 31, 'vigilantes',
                    'lista de vigilantes', '', 'boton dar de baja', true, $request);

                $vigilante->estado = false;
            } else {
                Auditoria::guardar(\Auth::user()->id, 32, 'vigilantes',
                    'lista de vigilantes', '', 'boton dar de Alta', true, $request);

                $vigilante->estado = true;
            }
            $vigilante->save();
        } catch (\Exception $e) {
            $status = false;
        }
        return response()->json([
            'status' => $status
        ]);

    }


    public function listarVigilantes()
    {

        $nivel                    = auth()->user()->nivel;
        $idnivel                  = auth()->user()->idnivel;
        $codigos_establecimientos = [];


        switch ($nivel) {
            case 'nacional':
                $vigilantes               = Vigilante::with('persona')->get();
                $codigos_establecimientos = [];
                break;

            case 'depto':
                $codigos_establecimientos = array_values(\App\Departamento::obtenerEstablecimientos($idnivel)->map->only(['codestabl'])->toArray());
                break;

            case 'red':
                $codigos_establecimientos = array_values(\App\Red::obtenerEstablecimientosPorRed($idnivel)->map->only(['codestabl'])->toArray());
                break;

            case 'muni':
                $codigos_establecimientos = \App\Municipio::obtenerCodigosEstablecimientosPorMunicipio($idnivel);
                break;
            case 'establ':
                $codigos_establecimientos = [$idnivel];
                break;

        }
        if (count($codigos_establecimientos) > 0) {

            $vigilantes = Vigilante::ConsultaVigilantesPorEstablecimiento($codigos_establecimientos);
        }


        return \DataTables::of($vigilantes)
            ->addColumn('establecimiento', function ($vigilante) {

                return $vigilante->persona->obtenerEstablecimiento()->nomestabl;

            })
            ->addColumn('fechanacimiento', function ($vigilante) {

                return $vigilante->persona->fechaNacimiento->format('d/m/Y');

            })
            ->addColumn('edad', function ($vigilante) {

                return $vigilante->persona->edad;

            })
            ->addColumn('genero', function ($vigilante) {

                return $vigilante->persona->genero;

            })
            ->addColumn('municipio', function ($vigilante) {

                return $vigilante->persona->obtenerMunicipio()->mnc_nombre;

            })
            ->addColumn('red', function ($vigilante) {

                return $vigilante->persona->obtenerRed()->are_nombre;

            })
            ->addColumn('departamento', function ($vigilante) {

                return $vigilante->persona->obtenerDepartamento()->dpt_nombre;

            })
            ->addColumn('acciones', function ($vigilante) {
                return view('vigilantes.acciones', compact('vigilante'))->render();
            })
            ->rawColumns([
                'acciones',
                'edad',
                'departamento',
                'red',
                'municipio',
                'genero',
                'fechanacimiento',
                'establecimiento'
            ])
            ->make(true);

    }

    public function verPerfil(Request $request, Vigilante $vigilante)
    {

        $config['center']      = 'Clifton, Karachi';
        $config['zoom']        = '14';
        $config['map_height']  = '250px';
        $config['scrollwheel'] = false;

        Auditoria::guardar(\Auth::user()->id, 28, 'vigilantes',
            'vigilantes', 'lista de vigilantes', 'ver perfil', true, $request);

        return view('vigilantes.verPerfil', [
            'vigilante' => $vigilante,

        ]);

    }

    public function buscarPersona(Request $request)
    {


        $api              = new Api();
        $numcarnet        = isset($request->num_carnet) ? $request->num_carnet : '0';
        $fecha_nacimiento = isset($request->fecha_nacimiento) ? $request->fecha_nacimiento : '';
        $complemento      = isset($request->complemento) ? $request->complemento : '';

        if ($numcarnet > 0) {
            $buscar_persona = Persona::buscarPersona($numcarnet, $fecha_nacimiento, $complemento);

            if (!is_null($buscar_persona)) {
                return response()->json([
                    'success' => true,
                    'data'    => $buscar_persona,
                ]);

            }

            $buscar_sus = $api->buscarCarnetSus($numcarnet, $fecha_nacimiento, $complemento);
            if ($buscar_sus['status'] == 200) {
                return response()->json([
                    'success' => true,
                    'data'    => $buscar_sus['response']

                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'data'    => $buscar_sus['message'],
                    'status'  => 500
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'data'    => 'datos invalidos',
                'status'  => 500
            ]);

        }

    }
}
