<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Http\Request;


class Mujer extends Model
{
  protected $table = 'mujeres';
  protected $primaryKey = 'IdMujer';
  // protected $appends = [
  //     'edad',

  // ];
  protected $fillable = [

    "IdVigilante",
    "longitud",
    "latitud",
    "estado",
    "idAndroid",
    "IdUsuario",
    "IdPersona",

  ];
  protected $dates = ['fechaNacimiento'];


  public function notificable()
  {
    return $this->morphMany(Notificacion::class, 'notificable');
  }

  public function persona()
  {
    return $this->belongsTo(Persona::class, 'IdPersona', 'IdPersona');
  }
  public static function MujeresPorEstablecimiento($codigos_establecimientos)
  {
    $mujeres = self::join('personas as p', 'p.IdPersona', '=', 'mujeres.IdPersona')
      ->whereIn('p.codEstablecimiento', $codigos_establecimientos)
      ->orderBy('p.created_at', 'DESC')
      ->select('mujeres.*')
      ->with('persona')
      ->get();
    return $mujeres;
  }


  public function vigilante()
  {
    return $this->belongsTo(Vigilante::class, 'IdVigilante', 'IdVigilante');
  }



  public static function listaMujeres(Request $request)
  {
    $datos = self::join('personas as p', 'p.IdPersona', 'mujeres.IdPersona')
      ->leftjoin('comunidades as c', 'c.IdComunidad', '=', 'p.IdComunidad');
    switch ($request->nivel) {
      case 'nacional':

        break;
      case 'depto':

        $datos->where('p.dpt_codigo', $request->dpto);
        break;
      case 'muni':
        $datos->where('p.mnc_codigo', $request->municipio);
        break;
      case 'red':
        $datos->where('p.are_codigo', $request->red);
        break;
      case 'establ':
        $datos->where('p.are_codigo', $request->establecimiento);
        break;
    }
    return $datos->get();
  }
}