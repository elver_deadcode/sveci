<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
/**
 * @property string mnc_codigo
 * @property string prv_codigo
 * @property string mnc_nombre
 * @property string mnc_poblacion
 * @property string anio_creacion

 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="Municipio",
 *     @OA\Property(property="mnc_codigo", type="string",example="41503"),
 *     @OA\Property(property="prv_codigo", type="string",example="415"),
 *     @OA\Property(property="mnc_nombre", type="string",example="CARANGAS"),
 *     @OA\Property(property="mnc_poblacion", type="string",example="0"),
 *
 * ),
 * @OA\Schema(
 *     schema="MunicipioConDEpartamentoEstablecimientos",
 *     @OA\Property(property="mnc_codigo", type="string",example="41503"),
 *     @OA\Property(property="prv_codigo", type="string",example="415"),
 *     @OA\Property(property="mnc_nombre", type="string",example="CARANGAS"),
 *     @OA\Property(property="provincia", ref="#/components/schemas/ProvinciaDepartamento"),
 *     @OA\Property(property="establecimientos", type="array",
 *      @OA\Items(ref="#/components/schemas/Establecimiento"),
 * ),
 *
 * ),
 */
class Municipio extends Model
{
    protected $table = 'ctg_municipios';
    protected $connection = 'snis';

    public $timestamps = false;
    protected $primaryKey = "mnc_codigo";
    public $keyType = 'string';

    protected $fillable = [
        "mnc_codigo",
        "prv_codigo",
        "mnc_nombre",
        "mnc_poblacion",
        "anio_creacion",

    ];

    public static function obtenerEstablecimientosPorMunicipio($codigo_municipio)
    {
        $result = Establecimiento::where('codmunicip', $codigo_municipio)->get();
        $result = $result->sortByDesc('idgestion')->groupby('idgestion')->first();
        return $result;

    }

    public static function obtenerCodigosEstablecimientosPorMunicipio($codigo_municipio)
    {
        $establecimientos = Establecimiento::where('codmunicip',
            $codigo_municipio)->select('codestabl')->distinct()->get()->map->only(['codestabl'])->toArray();
        return array_values($establecimientos);
    }

    public function getCodigoAttribute()
    {
        return $this->mnc_codigo;
    }

    public function getNombreAttribute()
    {
        return $this->mnc_nombre;
    }

    public function departamento()
    {
        $dpto = Departamento::join('ctg_provincia as p', 'p.coddepto', '=', 'ctg_departamentos.dpt_codigo')
            ->join('ctg_municipios as m', 'm.prv_codigo', '=', 'p.codprovi')
            ->where('m.mnc_codigo', $this->mnc_codigo)
            ->first();
        return $dpto;
    }

    public function provincia()
    {
        return $this->belongsTo(Provincia::class, 'prv_codigo');
    }

    public function establecimientos()
    {
        return $this->hasMany(Establecimiento::class, 'codmunicip', 'mnc_codigo');
    }

    public function establecimientos2()
    {
        return Establecimiento::Raw('select DISTINCT * from tbl_estabgest WHERE codmunicip='.$this->mnc_codigo.' ORDER BY idgestion desc')->get();
    }

    public function comunidades()
    {
        return $this->hasMany(Comunidad::class, 'codMunicipio', 'mnc_codigo');
    }
}
