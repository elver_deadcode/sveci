<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Notificacion extends Model
{
        protected $table = 'notificaciones';

        protected $fillable = [
                "notificable_type",
                "notificable_id",
                "mensaje",
                "codEstablecimiento",
                "tipo",
                "prioridad",
                "mnc_codgo",
                "dpt_codigo",
                "vistoEstablecimiento",
                "vistoMunicipio",
                "vistoDepartamento",
                "vistoNacional",
                "are_codigo",
                "vistoRed",
                "user_id",
                "idVigilante",
                "idComunidad"


        ];
        protected $casts = [
                'created_at' => 'date:d-m-Y H:i:s',
        ];

        public function notificable()
        {
                return $this->morphTo();
        }
        public function vigilante()
        {
                return $this->belongsTo(Vigilante::class, 'idVigilante', 'IdVigilante');
        }

        public function user()
        {
                return $this->belongsTo(User::class, 'user_id', 'id');
        }
        public function municipio()
        {
                return $this->belongsTo(Municipio::class, 'mnc_codgo', 'mnc_codigo');
        }
        public function establecimiento()
        {
                return $this->belongsTo(Establecimiento::class, 'codEstablecimiento', 'codestabl');
        }

        public function red()
        {
                return $this->belongsTo(Red::class, 'are_codigo', 'are_codigo');
        }

        public function departamento()
        {
                return $this->belongsTo(Departamento::class, 'dpt_codigo', 'dpt_codigo');
        }

        public function obtenerRelacion()
        {
                $class = null;
                switch ($this->notificable_type) {
                        case 'App\NotificacionMuerteMujer':
                                $class = NotificacionMuerteMujer::find($this->notificable_id);
                                break;
                        case 'App\NotificacionMuerteBebe':
                                $class = NotificacionMuerteBebe::find($this->notificable_id);
                                break;
                        case 'App\NotificacionParto':
                                $class = NotificacionParto::find($this->notificable_id);
                                break;
                        case 'App\NotificacionEmbarazo':
                                $class = NotificacionEmbarazo::find($this->notificable_id);
                                break;
                        case 'App\Vigilante':
                                $class = Vigilante::find($this->notificable_id);
                                break;
                        case 'App\Mujer':

                                $class = Mujer::find($this->notificable_id);
                                break;

                        default:
                                $class = null;
                }
                return $class;
        }

        public function obtenerAbreviacionNotificacion()
        {
                $abreviacion = "";
                switch ($this->notificable_type) {
                        case 'App\NotificacionMuerteMujer':
                                $abreviacion = "NMM";
                                break;
                        case 'App\NotificacionMuerteBebe':
                                $abreviacion = "NMB";
                                break;
                        case 'App\NotificacionParto':
                                $abreviacion = "NP";
                                break;
                        case 'App\NotificacionEmbarazo':
                                $abreviacion = "NME";
                                break;
                        case 'App\NotificacionTeleconsulta':
                                $abreviacion = "NT";
                                break;

                        default:
                                $abreviacion = "s/n";
                }
                return  $abreviacion;
        }

        public function obtenerEstablecimiento()
        {
                $establecimiento = null;
                if (isset($this->codEstablecimiento) || $this->codEstablecimiento != '') {

                        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)
                                ->orderBy('idgestion')->first();
                }
                return $establecimiento;
        }

        public function obtenerMunicipio()
        {
                $municipio = null;
                if (isset($this->mnc_codgo) || $this->mnc_codgo != '') {

                        $municipio = Municipio::where('mnc_codigo', $this->mnc_codgo)
                                ->first();
                }
                return $municipio;
        }
        public function obtenerDepartamento()
        {
                $departamento = null;
                if (isset($this->dpt_codigo) || $this->dpt_codigo != '') {

                        $departamento = Departamento::where('dpt_codigo', $this->dpt_codigo)
                                ->first();
                }
                return $departamento;
        }

        public static function listarNotificaciones(Request $request, $nivel, $idnivel, $tipo)
        {
                $periodos=[];
                if ($request->periodo == 'anio') {
                        if (isset($request->todosmeses)) {

                          $fecha_ini = Carbon::createFromFormat('Y-m', $request->anio . '-1')->startOfMonth()->format('Y-m-d');
                          $fecha_fin = Carbon::createFromFormat('Y-m', $request->anio . '-12')->endOfMonth()->format('Y-m-d');
                          $periodos[] = [$fecha_ini, $fecha_fin];
                        } else {
                          $meses = $request->multiselect;
                          foreach ($meses  as $mes) {
                            $fecha_ini = Carbon::createFromFormat('Y-m', $request->anio . '-' . $mes)->startOfMonth()->format('Y-m-d');
                            $fecha_fin = Carbon::createFromFormat('Y-m', $request->anio . '-' . $mes)->endOfMonth()->format('Y-m-d');
                            $periodos[] = [$fecha_ini, $fecha_fin];
                          }
                        }
                      }

                      if ($request->periodo == 'periodo') {
                        $fecha_ini = Carbon::createFromFormat('d/m/Y', $request->fechaini)->format('Y-m-d');
                        $fecha_fin = Carbon::createFromFormat('d/m/Y', $request->fechafin)->format('Y-m-d');
                        $periodos[] = [$fecha_ini, $fecha_fin];
                      }



                $tipo_notificacion = "";
                switch ($tipo) {
                        case 'embarazo':
                                $tipo_notificacion = "App\NotificacionEmbarazo";
                                break;
                        case 'muerte_mujer':
                                $tipo_notificacion = "App\NotificacionMuerteMujer";
                                break;
                        case 'muerte_bebe':
                                $tipo_notificacion = "App\NotificacionMuerteBebe";
                                break;
                        case 'parto':
                                $tipo_notificacion = "App\NotificacionParto";
                                break;
                        case 'todas':
                                $tipo_notificacion = "";
                                break;
                }

                $datos = [];
                // $fecha_inicio = $request->fecha_inicio;
                // $fecha_fin = $request->fecha_fin;
                // if (!is_null($fecha_inicio)) {
                //         $fecha_inicio = \Carbon\Carbon::createFromFormat('d/m/Y', $fecha_inicio)->format('Y-m-d');
                // }
                // if (!is_null($fecha_fin)) {
                //         $fecha_fin = \Carbon\Carbon::createFromFormat('d/m/Y', $fecha_fin)->format('Y-m-d');
                // }

                if ($nivel) {
                        switch ($nivel) {
                                case 'nacional':

                                        if (isset($request->nivel)) {
                                                switch ($request->nivel) {
                                                        case 'nacional':

                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                        case 'depto':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('dpt_codigo', $request->dpto)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);

                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('dpt_codigo', $request->dpto)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                        case 'muni':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $request->municipio)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $request->municipio)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;

                                                        case 'red':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $request->red)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $request->red)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;

                                                        case 'establ':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                }
                                        } else {


                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        }
                                        break;

                                        //FIN DE USUARIO NACIONAL

                                case 'depto':
                                        if (isset($request->nivel)) {
                                                switch ($request->nivel) {

                                                        case 'depto':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('dpt_codigo', $idnivel)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('dpt_codigo', $idnivel)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                        case 'muni':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $request->municipio)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $request->municipio)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;

                                                        case 'red':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $request->red)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $request->red)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;

                                                        case 'establ':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }

                                                                break;
                                                }
                                        } else {
                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('dpt_codigo', $idnivel)
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        }
                                        break;

                                case 'muni':
                                        if (isset($request->nivel)) {
                                                switch ($request->nivel) {
                                                        case 'muni':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $idnivel)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('mnc_codgo', $idnivel)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }

                                                                break;


                                                        case 'establ':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                }
                                        } else {
                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('mnc_codgo', $idnivel)
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        }
                                        break;

                                case 'red':
                                        if (isset($request->nivel)) {
                                                switch ($request->nivel) {
                                                        case 'red':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $idnivel)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('are_codigo', $idnivel)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }

                                                                break;

                                                        case 'establ':
                                                                if (count($periodos)>0) {

                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                // ->whereDate('created_at', '>=', $fecha_inicio)
                                                                                // ->whereDate('created_at', '<=', $fecha_fin)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                } else {
                                                                        $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                                                ->where('codEstablecimiento', $request->establecimiento)
                                                                                ->where('notificable_type', $tipo_notificacion);
                                                                                // ->orderBy('created_at', 'DESC')
                                                                                // ->get();
                                                                }
                                                                break;
                                                }
                                        } else {
                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('are_codigo', $idnivel)
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        }
                                        break;

                                case 'establ':
                                        if (count($periodos)>0) {

                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('codEstablecimiento', $idnivel)
                                                        // ->whereDate('created_at', '>=', $fecha_inicio)
                                                        // ->whereDate('created_at', '<=', $fecha_fin)
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        } else {
                                                $datos = Notificacion::with(['notificable.mujer.persona', 'notificable.vigilante.persona', 'notificable.detalle', 'user', 'municipio', 'red', 'establecimiento', 'departamento'])
                                                        ->where('codEstablecimiento', $idnivel)
                                                        ->where('notificable_type', $tipo_notificacion);
                                                        // ->orderBy('created_at', 'DESC')
                                                        // ->get();
                                        }

                                        break;
                        }
                }
                foreach ($periodos as $p) {
                        $datos->whereBetween('created_at', [$p[0], $p[1]])
                        ->orderBy('created_at', 'DESC')->get();
                      }

                return $datos;
        }
}
