<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionEmbarazoDetalle extends Model
{
    protected $table='notificacionEmbarazoDetalle';
    protected $primaryKey='IdNotificacionEmbarazoDetalle';
    protected $fillable=[
        'numeroControlesPrenatal',
        'IdNotificacionEmbarazo',
        'planPartoLlenado',
        'user_id',
        'comentarios'


    ];

    public function notificacion(){
        return $this->BelongsTo(NotificacionEmbarazo::class,'IdNotificacionEmbarazo','IdNotificacionEmbarazo');
    }

    
}
