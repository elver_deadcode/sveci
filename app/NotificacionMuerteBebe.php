<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionMuerteBebe extends Model
{
    protected $table = 'notificacionMuerteBebe';
    protected $primaryKey = "IdNotificacionMuerteBebe";
    protected $fillable = [

        "IdAndroid",
        "idMujerAndroid",
        "codVigilante",
        "codEstablecimiento",
        "estado",
        "nombres",
        "primerApellido",
        "segundoApellido",
        "fechaNacimento",
        "direccion",
        "telefono",
        "latitud",
        "longitud",
        "edad",
        "nacido",
        "fechaRegistro",
        "cerrado",
        "fecha_cerrado"


    ];

    protected $dates = ["fechaRegistro", "fechaNacimiento"];
    protected $appends = [
        'apellidos_completo',
        'estaba_nacido',

    ];
    public function getApellidosCompletoAttribute()
    {
        return  $this->primerApellido . ' ' . $this->segundoApellido;
    }
    public function getEstabaNacidoAttribute()
    {
        $estaba_nacido = 0;
        if ($this->nacido) {
            $estaba_nacido = 1;
        }
        return $estaba_nacido;
    }
    public function notificable()
    {
        return $this->morphMany(Notificacion::class, 'notificable');
    }

    public function detalle()
    {
        return $this->hasOne(NotificacionMuerteBebeDetalle::class, 'IdNotificacionMuerteBebe', 'IdNotificacionMuerteBebe');
    }

    public function mujer()
    {
        return $this->belongsTo(Mujer::class, 'idMujerAndroid', 'idAndroid');
    }
    public function vigilante()
    {
        return $this->belongsTo(Vigilante::class, 'codVigilante', 'codigo');
    }

    public function obtenerEstablecimiento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->orderBy('idgestion', 'DESC')->first();
        return $establecimiento;
    }
    public function obtenerMunicipio()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        return $municipio;
    }
    public function obtenerDepartamento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        $departamento = $municipio->departamento();
        return $departamento;
    }

    public static function muertesBebes($localizacion, $idlocalizacion, $nacido, $dias, $avpa, $plan_accion = false, $periodos)
    {

        $contador = 0;

        switch ($localizacion) {
            case 'nacional':

                if (count($dias) <= 0) {


                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('nmbd.nacido', $nacido)
                        ->where('nmbd.tieneAutopsiaVerbal', $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                } else {
                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('nmbd.nacido', $nacido)
                        ->where("nmbd.tieneAutopsiaVerbal", $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion)
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) >" . $dias[0])
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) <=" . $dias[1]);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                }



                break;

            case 'dpto':

                if (count($dias) <= 0) {


                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.dpt_codigo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where('nmbd.tieneAutopsiaVerbal', $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                } else {
                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.dpt_codigo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where("nmbd.tieneAutopsiaVerbal", $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion)
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) >" . $dias[0])
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) <=" . $dias[1]);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                }



                break;
            case 'red':

                if (count($dias) <= 0) {


                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.are_codigo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where('nmbd.tieneAutopsiaVerbal', $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                } else {
                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.are_codigo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where("nmbd.tieneAutopsiaVerbal", $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion)
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) >" . $dias[0])
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) <=" . $dias[1]);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                }

                break;
            case 'municipal':
                if (count($dias) <= 0) {


                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.mnc_codgo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where('nmbd.tieneAutopsiaVerbal', $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                } else {
                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.mnc_codgo', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where("nmbd.tieneAutopsiaVerbal", $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion)
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) >" . $dias[0])
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) <=" . $dias[1]);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                }


                break;
            case 'establecimiento':
                if (count($dias) <= 0) {


                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.codEstablecimiento', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where('nmbd.tieneAutopsiaVerbal', $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                } else {
                    $contador = Notificacion::join('notificacionMuerteBebe as nmb', 'nmb.IdNotificacionMuerteBebe', '=', 'notificaciones.notificable_id')
                        ->join('notificacionMuerteBebeDetalle as nmbd', 'nmbd.IdNotificacionMuerteBebe', '=', 'nmb.IdNotificacionMuerteBebe')
                        ->where('notificaciones.notificable_type', 'App\NotificacionMuerteBebe')
                        ->where('notificaciones.codEstablecimiento', $idlocalizacion)
                        ->where('nmbd.nacido', $nacido)
                        ->where("nmbd.tieneAutopsiaVerbal", $avpa)
                        ->where('nmbd.tienePlanAccion', $plan_accion)
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) >" . $dias[0])
                        ->whereRaw("DATE_PART('day', age(fechafallecimiento , fechanacimiento)) <=" . $dias[1]);
                    // ->whereBetween('nmb.fechaRegistro', [$fecha_ini, $fecha_fin])
                    // ->count();
                }
                break;
        }
        foreach ($periodos as $p) {
            $contador->whereBetween('nmb.fechaRegistro', [$p[0], $p[1]]);
        }
        $contador = $contador->count();

        return  [
            "contador" => $contador,

        ];
    }

    public static function  reporteConsolidadoMuertesPorVigilante($localizacion, $mostrarpor, $periodos)
    {
        $datos = [];
        foreach ($localizacion as $k => $v) {
            $resultados_muertesfetales_uv = self::muertesBebes($mostrarpor, $v->codigo, false, [], true, false, $periodos);
            $resultados_muertesfetales_uvpa = self::muertesBebes($mostrarpor, $v->codigo, false, [], true, true, $periodos);
            $resultados_muertes7dias_uv = self::muertesBebes($mostrarpor, $v->codigo, true, ['0', '6'], true, false, $periodos);
            $resultados_muertes7dias_uvpa = self::muertesBebes($mostrarpor, $v->codigo, true, ['0', '6'], true, true, $periodos);
            $resultados_muertes28dias_uv = self::muertesBebes($mostrarpor, $v->codigo, true, ['7', '27'], true, false, $periodos);
            $resultados_muertes28dias_uvpa = self::muertesBebes($mostrarpor, $v->codigo, true, ['7', '27'], true, true, $periodos);
            $resultados_muertesfetales = self::muertesBebes($mostrarpor, $v->codigo, false, [], false, false, $periodos);
            $resultados_muertes7dias = self::muertesBebes($mostrarpor, $v->codigo, true, ['0', '6'], false, false, $periodos);
            $resultados_muertes28dias = self::muertesBebes($mostrarpor, $v->codigo, true, ['7', '27'], false, false, $periodos);
            $total = $resultados_muertesfetales_uv['contador'] +
                $resultados_muertesfetales_uvpa['contador'] +
                $resultados_muertes7dias_uv['contador'] +
                $resultados_muertes7dias_uvpa['contador'] +
                $resultados_muertes28dias_uv['contador'] +
                $resultados_muertes28dias_uvpa['contador'] +
                $resultados_muertesfetales['contador'] +
                $resultados_muertes7dias['contador'] +
                $resultados_muertes28dias['contador'];
            if ($total > 0) {
                $datos[] = [
                    'localizacion' => $v->nombre,
                    'muertesfetales_uv' => $resultados_muertesfetales_uv['contador'],
                    'muertesfetales_uvpa' => $resultados_muertesfetales_uvpa['contador'],
                    'muertes7dias_uv' => $resultados_muertes7dias_uv['contador'],
                    'muertes7dias_uvpa' => $resultados_muertes7dias_uvpa['contador'],
                    'muertes28dias_uv' => $resultados_muertes28dias_uv['contador'],
                    'muertes28dias_uvpa' => $resultados_muertes28dias_uvpa['contador'],
                    'muertesfetales' => $resultados_muertesfetales['contador'],
                    'muertes7dias' => $resultados_muertes7dias['contador'],
                    'muertes28dias' =>  $resultados_muertes28dias['contador'],
                    'total' => $total

                ];
            }
        }
        return $datos;
    }
}