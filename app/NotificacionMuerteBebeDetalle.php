<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionMuerteBebeDetalle extends Model
{
     protected $table='notificacionMuerteBebeDetalle';
     protected $primaryKey='IdMuerteBebeDetalle';
     protected $fillable=[

        'nacido',
        'tieneAutopsiaVerbal',
        'tienePlanAccion',
        'fechanacimiento',
        'fechafallecimiento',
        'IdNotificacionMuerteBebe',
        'user_id',
        'comentarios'

     ];
     protected $dates=[
        'fechanacimiento',
        'fechafallecimiento'
     ];
        protected $casts = [
    'fechanacimiento' => 'date:d-m-Y ',
    'fechafallecimiento' => 'date:d-m-Y ',
    ];


     public function notificacion()
     {
         return $this->belongsTo(NotificacionMuerteBebe::class, 'IdNotificacionMuerteBebe', 'IdNotificacionMuerteBebe');
     }

     public function getNacidoAttribute(){
         if($this->attributes['nacido']==true){
             return "SI";
         }
         else
         {
             return "NO";
         }
     }

     public function getTieneautopsiaverbalAttribute(){
         if($this->attrinutes['tieneAutopsiaVerbal']){
             return "SI";
         }
         else
         {
             return "NO";
         }
     }

}
