<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionMuerteMujer extends Model
{
  protected $table = 'notificacionMuerteMujer';
  protected $primaryKey = "IdNotificacionMuerteMujer";
  protected $fillable = [

    "IdAndroid",
    "idMujerAndroid",
    "codVigilante",
    "codEstablecimiento",
    "estado",
    "nombres",
    "primerApellido",
    "segundoApellido",
    "fechaNacimento",
    "direccion",
    "telefono",
    "latitud",
    "longitud",
    "edad",
    "embarazada",
    "fechaRegistro",
    "cerrado",
    "fecha_cerrado"


  ];
  protected $appends = [
    'apellidos_completo',
    'estaba_embarazada',

  ];
  protected $dates = ["fechaRegistro", "fechaNacimiento", "fecha_cerrado"];
  public function getApellidosCompletoAttribute()
  {
    return  $this->primerApellido . ' ' . $this->segundoApellido;
  }
  public function getEstabaEmbarazadaAttribute()
  {
    $estaba_embarazada = 0;
    if ($this->embarazada) {
      $estaba_embarazada = 1;
    }
    return $estaba_embarazada;
  }
  public function notificable()
  {
    return $this->morphMany(Notificacion::class, 'notificable');
  }

  public function detalle()
  {
    return $this->hasOne(NotificacionMuerteMujerDetalle::class, 'IdNotificacionMuerteMujer', 'IdNotificacionMuerteMujer');
  }

  public function mujer()
  {
    return $this->belongsTo(Mujer::class, 'idMujerAndroid', 'idAndroid');
  }
  public function vigilante()
  {
    return $this->belongsTo(Vigilante::class, 'codVigilante', 'codigo');
  }

  public function obtenerEstablecimiento()
  {
    $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->orderBy('idgestion', 'DESC')->first();
    return $establecimiento;
  }
  public function obtenerMunicipio()
  {
    $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
    $municipio = $establecimiento->municipio;
    return $municipio;
  }
  public function obtenerDepartamento()
  {
    $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
    $municipio = $establecimiento->municipio;
    $departamento = $municipio->departamento();
    return $departamento;
  }
  public static function existeNotificacionMuerteAndroid($idMujerAndroid): bool
  {
    $noti = self::where('idMujerAndroid', $idMujerAndroid)->first();
    if (is_null($noti)) {
      return false;
    }
    return true;
  }

  public  static function casosAtendidos($localizacion, $idlocalizacion, $casoatendido, $periodos)
  {
    $contador = 0;
    switch ($localizacion) {
      case 'dpto':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.dpt_codigo', $idlocalizacion)
          ->where('nmmd.casoFueAtendido', $casoatendido);
        break;
      case 'red':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.are_codigo', $idlocalizacion)
          ->where('nmmd.casoFueAtendido', $casoatendido);

        break;
      case 'municipal':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.mnc_codgo', $idlocalizacion)
          ->where('nmmd.casoFueAtendido', $casoatendido);


        break;
      case 'establecimiento':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.codEstablecimiento', $idlocalizacion)
          ->where('nmmd.casoFueAtendido', $casoatendido);

        break;
    }
    foreach ($periodos as $p) {
      $contador->whereBetween('nmm.fechaRegistro', [$p[0], $p[1]]);
    }
    $contador = $contador->count();
    return  [
      "contador" => $contador,
    ];
  }

  public  static function casosRegistrados($localizacion, $idlocalizacion, $periodos)
  {
    $contador = 0;
    switch ($localizacion) {
      case 'dpto':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
         
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.dpt_codigo', $idlocalizacion);
        break;
      case 'red':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.are_codigo', $idlocalizacion);

        break;
      case 'municipal':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.mnc_codgo', $idlocalizacion);


        break;
      case 'establecimiento':
        $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
          ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
          ->where('notificaciones.codEstablecimiento', $idlocalizacion);

        break;
    }
    foreach ($periodos as $p) {
      $contador->whereBetween('nmm.fechaRegistro', [$p[0], $p[1]]);
    }
    $contador = $contador->count();
    return  [
      "contador" => $contador,
    ];
  }

  public static function muertesMujeres($localizacion, $idlocalizacion, $edades, $durante, $tieneFicha, $tiene_plan_accion = false, $periodos)
  {
    $contador = 0;
    switch ($localizacion) {
      case 'dpto':

        if (empty($edades)) {

          $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
            ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
            ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
            ->where('notificaciones.dpt_codigo', $idlocalizacion)
            ->whereIn('nmmd.muerteDurante', $durante)
            ->where('nmmd.tieneFicha', $tieneFicha)
            ->where('nmmd.tienePlanAccion', $tiene_plan_accion);
          // ->whereBetween('nmm.fechaRegistro', [$fecha_ini, $fecha_fin])
          // ->count();
        }
        break;
      case 'red':
        if (empty($edades)) {

          $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
            ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
            ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
            ->where('notificaciones.are_codigo', $idlocalizacion)
            ->whereIn('nmmd.muerteDurante', $durante)
            ->where('nmmd.tieneFicha', $tieneFicha)
            ->where('nmmd.tienePlanAccion', $tiene_plan_accion);
          // ->whereBetween('nmm.fechaRegistro', [$fecha_ini, $fecha_fin])
          // ->count();
        }


        break;
      case 'municipal':
        if (empty($edades)) {

          $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
            ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
            ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
            ->where('notificaciones.mnc_codgo', $idlocalizacion)
            ->whereIn('nmmd.muerteDurante', $durante)
            ->where('nmmd.tieneFicha', $tieneFicha)
            ->where('nmmd.tienePlanAccion', $tiene_plan_accion);
          // ->whereBetween('nmm.fechaRegistro', [$fecha_ini, $fecha_fin])
          // ->count();
        }

        break;
      case 'establecimiento':
        if (empty($edades)) {
          //  return dd($durante,$tieneFicha,$idlocalizacion);

          $contador = Notificacion::join('notificacionMuerteMujer as nmm', 'nmm.IdNotificacionMuerteMujer', '=', 'notificaciones.notificable_id')
            ->join('notificacionMuerteMujerDetalle as nmmd', 'nmmd.IdNotificacionMuerteMujer', '=', 'nmm.IdNotificacionMuerteMujer')
            ->where('notificaciones.notificable_type', 'App\NotificacionMuerteMujer')
            ->where('notificaciones.codEstablecimiento', $idlocalizacion)
            ->whereIn('nmmd.muerteDurante', $durante)
            ->where('nmmd.tieneFicha', $tieneFicha)
            ->where('nmmd.tienePlanAccion', $tiene_plan_accion);
          // ->whereBetween('nmm.fechaRegistro', [$fecha_ini, $fecha_fin])
          // ->count();
        }
        break;
    }
    foreach ($periodos as $p) {
      $contador->whereBetween('nmm.fechaRegistro', [$p[0], $p[1]]);
    }
    $contador = $contador->count();
    return  [
      "contador" => $contador,

    ];
  }
  public static function reporteConsolidadoMuerteMujer($localizacion, $mostrarpor, $periodos)
  {
    $datos = [];
    foreach ($localizacion as $r => $v) {

      $gav = \App\NotificacionMuerteMujer::muertesMujeres($mostrarpor, $v->codigo, [], ['parto', 'embarazo', 'despues'], true, false, $periodos);
      $gavpa = \App\NotificacionMuerteMujer::muertesMujeres($mostrarpor, $v->codigo, [], ['parto', 'embarazo', 'despues'], true, true, $periodos);

      $g1av = \App\NotificacionMuerteMujer::muertesMujeres($mostrarpor, $v->codigo, [], ['parto1', 'embarazo1', 'despues1'], true, false, $periodos);
      $g1avpa = \App\NotificacionMuerteMujer::muertesMujeres($mostrarpor, $v->codigo, [], ['parto1', 'embarazo1', 'despues1'], true, true, $periodos);

      // $g2av=\App\NotificacionMuerteMujer::muertesMujeres($mostrarpor,$v->codigo,[],['parto2','embarazo2','despues2'],true,false);
      // $g2avpa=\App\NotificacionMuerteMujer::muertesMujeres($mostrarpor,$v->codigo,[],['parto2','embarazo2','despues2'],true,true);

      $ninguno = \App\NotificacionMuerteMujer::muertesMujeres($mostrarpor, $v->codigo, [], ['ninguno'], true, false, $periodos);
      $casos_atendidos = \App\NotificacionMuerteMujer::casosAtendidos($mostrarpor, $v->codigo, true, $periodos);
      $casos_registrados=self::casosRegistrados($mostrarpor, $v->codigo, $periodos);
      $total = $gav['contador'] + $gavpa['contador'] + $g1av['contador'] + $g1avpa['contador'] + $ninguno['contador']+$casos_atendidos['contador']+$casos_registrados['contador'];

      if ($total > 0) {
        $datos[] = [
          'localizacion' => $v->nombre,
          'gav' => $gav['contador'],
          'gavpa' => $gavpa['contador'],
          'g1av' => $g1av['contador'],
          'g1avpa' => $g1avpa['contador'],
          'ninguno' => $ninguno['contador'],
          'casos_atendidos' => $casos_atendidos['contador'],
          'casos_registrados'=>$casos_registrados['contador'],
          'total' => $total
        ];
      }
    }
    return $datos;
  }
}
