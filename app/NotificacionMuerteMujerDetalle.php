<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionMuerteMujerDetalle extends Model
{
     protected $table='notificacionMuerteMujerDetalle';
     protected $primaryKey='IdMuerteMujerDetalle';
     protected $fillable=[
            "muerteDurante",
            "tieneFicha",
            "IdNotificacionMuerteMujer",
            'user_id',
            "fechaFallecimiento",
            'tienePlanAccion',
            "casoFueAtendido",
            "comentarios"
     ];
     protected $dates=['fechaFallecimiento'];

           protected $casts = [
    
    'fechaFallecimiento' => 'date:d-m-Y ',
    ];


     public function notificaion()
     {
         return $this->belongsTo(NotificacionMuerteMujer::class, 'IdNotificacionMuerteMujer', 'IdNotificacionMuerteMujer');
     }
}
