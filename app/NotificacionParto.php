<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionParto extends Model
{
    protected $table = 'notificacionParto';
    protected $primaryKey = "IdNotificacionParto";
    protected $fillable = [


        "IdAndroid",
        "idMujerAndroid",
        "codVigilante",
        "codEstablecimiento",
        "estado",
        "nombres",
        "primerApellido",
        "segundoApellido",
        "fechaNacimento",
        "direccion",
        "telefono",
        "latitud",
        "longitud",
        "edad",
        "atendido",
        "fechaRegistro",
        "cerrado",
        "fecha_cerrado",
        "atendidoNombre"



    ];

    protected $dates = ["fechaRegistro", "fechaNacimiento"];
    protected $appends = [
        'apellidos_completo',


    ];
    public function getApellidosCompletoAttribute()
    {
        return  $this->primerApellido . ' ' . $this->segundoApellido;
    }

    public function notificable()
    {
        return $this->morphMany(Notificacion::class, 'notificable');
    }

    public function detalle()
    {
        return $this->hasOne(NotificacionPartoDetalle::class, 'IdNotificacionParto', 'IdNotificacionParto');
    }

    public function mujer()
    {
        return $this->belongsTo(Mujer::class, 'idMujerAndroid', 'idAndroid');
    }
    public function vigilante()
    {
        return $this->belongsTo(Vigilante::class, 'codVigilante', 'codigo');
    }

    public function obtenerEstablecimiento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->orderBy('idgestion', 'DESC')->first();
        return $establecimiento;
    }
    public function obtenerMunicipio()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        return $municipio;
    }
    public function obtenerDepartamento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        $departamento = $municipio->departamento();
        return $departamento;
    }

    public  static function obtenerPartosVigilante($localizacion, $idlocalizacion, $partera, $periodos)
    {
        switch ($localizacion) {
            case 'nacional':
                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();
                break;
            case 'dpto':

                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('notificaciones.dpt_codigo', $idlocalizacion)
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();
                break;
            case 'red':
                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('notificaciones.are_codigo', $idlocalizacion)
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();

                break;
            case 'municipal':



                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('notificaciones.mnc_codgo', $idlocalizacion)
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();

                break;
            case 'establecimiento':


                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('notificaciones.codEstablecimiento', $idlocalizacion)
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();


                break;
            default:

                $contador = Notificacion::join('notificacionParto as np', 'np.IdNotificacionParto', '=', 'notificaciones.notificable_id')
                    ->join('notificacionPartoDetalle as npd', 'npd.IdNotificacionParto', '=', 'np.IdNotificacionParto')
                    ->where('notificaciones.notificable_type', 'App\NotificacionParto')
                    ->where('npd.partera', '=', $partera);
                // ->whereBetween('np.fechaRegistro', [$fecha_ini, $fecha_fin])
                // ->count();
        }
        foreach ($periodos as $p) {
            $contador->whereBetween('np.fechaRegistro', [$p[0], $p[1]]);
        }
        $contador = $contador->count();

        return  [
            "contador" => $contador,

        ];
    }
    public static function reporteConsolidadoParto($localizacion, $mostrarpor, $periodos)
    {
        $datos = [];
        foreach ($localizacion as $r => $v) {
            $resultado_capacitada = self::obtenerPartosVigilante($mostrarpor, $v->codigo, 'capacitada', $periodos);
            $resultado_empirica = self::obtenerPartosVigilante($mostrarpor, $v->codigo, 'empirica', $periodos);
            $total = $resultado_capacitada['contador'] + $resultado_empirica['contador'];
            if ($total > 0) {
                $datos[] = [
                    'localizacion' => $v->nombre,
                    'capacitada' => $resultado_capacitada['contador'],
                    'empirica' => $resultado_empirica['contador'],
                    'total' => $total

                ];
            }
        }

        return $datos;
    }
}