<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionPartoDetalle extends Model
{
    protected $table='notificacionPartoDetalle';
    protected $primaryKey='IdPartoDetalle';
    protected $fillable=[
        "IdNotificacionParto",
        "partera",
        'user_id',
        'comentarios'

    ];

    public function notificacion()
    {
        return $this->belongsTo(NotificacionParto::class, 'IdNotificacionParto', 'IdNotificacionParto');
    }
}
