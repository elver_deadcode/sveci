<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionTeleconsulta extends Model
{
    protected $table = 'notificacionTeleconsulta';
    protected $fillable=[
        'nombresPaciente',
        'apellidosPaciente',
        'fechaNacimientoPaciente',
        'edadPaciente',
        'carnetPaciente',
        'direccionPaciente',
        'sexoPaciente',
        'nombresMedico',
        'apellidosMedico',
        'carnetMedico',
        'numeroTeleconsulta',
        'fechaTeleconsulta',
        'codEstablecimiento',
        'estadoTeleconsulta',
    ];
    protected $dates = ["fechaTeleconsulta"];


    public function notificable()
    {
        return $this->morphMany(Notificacion::class, 'notificable');
    }
    public static function obtenerEstadosTeleconsultaPorLocalizacion($localizacion, $idlocalizacion,$periodos){
        $notificaciones=Notificacion::query();
        switch ($localizacion) {
            case 'nacional':

                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionTeleconsulta');
                break;
            case 'dpto':

                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionTeleconsulta')
                                        ->where('notificaciones.dpt_codigo', $idlocalizacion);
                break;
            case 'red':


                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionTeleconsulta')
                                        ->where('notificaciones.are_codigo', $idlocalizacion);
                break;
            case 'municipal':

                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionTeleconsulta')
                                        ->where('notificaciones.mnc_codgo', $idlocalizacion);
                break;
            case 'establecimiento':

                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionTeleconsulta')
                                        ->where('notificaciones.codEstablecimiento', $idlocalizacion);
                break;
            default:
                $notificaciones->join('notificacionTeleconsulta as nt', 'nt.id', '=', 'notificaciones.notificable_id')
                                        ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo');
                break;
        }
        foreach ($periodos as $p) {
            $notificaciones->whereBetween('nt.created_at', [$p[0], $p[1]]);
        }
        $atendidas=$notificaciones->get();
        $confirmadas=$notificaciones->get();
        $canceladas=$notificaciones->get();
        $canceladas=$canceladas->where('estadoTeleconsulta','cancelada')->count();
        $atendidas=$atendidas->where('estadoTeleconsulta','atendida')->count();
        $confirmadas=$confirmadas->where('estadoTeleconsulta','confirmada')->count();
        return  [
            "confirmadas" =>$confirmadas,
            "atendidas" =>$atendidas,
            "canceladas" =>$canceladas,
        ];
    }
    public  static  function reporteConsolidado($localizacion, $mostrarpor, $periodos)
    {
        $datos = [];
        foreach ($localizacion as $r => $v) {
            $teleconsultas=self::obtenerEstadosTeleconsultaPorLocalizacion($mostrarpor, $v->codigo,$periodos);
             $total=$teleconsultas['atendidas']+$teleconsultas['confirmadas']+$teleconsultas['canceladas'];
            if ($total > 0) {
                $datos[] = [
                    'localizacion' => $v->nombre,
                    'atendidas'    => $teleconsultas['atendidas'],
                    'confirmadas'  => $teleconsultas['confirmadas'],
                    'canceladas'   => $teleconsultas['canceladas'],
                    'total'        => $total
                ];
            }
        }
        return $datos;

    }
}
