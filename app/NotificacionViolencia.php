<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionViolencia extends Model
{
    protected $table = 'notificacionViolencia';
    public $primaryKey = 'IdNotificacionViolencia';

    protected $fillable = [
        "IdAndroid",
        "codVigilante",
        "codEstablecimiento",
        "estado",
        "nombres",
        "primerApellido",
        "segundoApellido",
        "fechaNacimento",
        "direccion",
        "telefono",
        "latitud",
        "longitud",
        "edad",
        "fechaRegistro",
        "idMujerAndroid",
        "cerrado",
        "fecha_cerrado"

    ];
    protected $casts = [
        'created_at' => 'date:d-m-Y H:i:s',
        'fechaRegistro' => 'date:d-m-Y H:i:s',
    ];
    protected $appends = [
        'apellidos_completo',

    ];

    public function notificable()
    {
        return $this->morphMany(Notificacion::class, 'notificable');
    }

//    public function detalle()
//    {
//        return $this->hasOne(NotificacionEmbarazoDetalle::class, 'IdNotificacionEmbarazo', 'IdNotificacionEmbarazo');
//    }

    protected $dates = ['fechaRegistro', 'fechaNacimiento'];

    public function mujer()
    {
        return $this->belongsTo(Mujer::class, 'idMujerAndroid', 'idAndroid');
    }
    public function vigilante()
    {
        return $this->belongsTo(Vigilante::class, 'codVigilante', 'codigo');
    }

    public function obtenerEstablecimiento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->orderBy('idgestion', 'DESC')->first();
        return $establecimiento;
    }
    public function obtenerMunicipio()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        return $municipio;
    }
    public function obtenerDepartamento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio = $establecimiento->municipio;
        $departamento = $municipio->departamento();
        return $departamento;
    }

    public function getNombreCompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primerApellido . ' ' . $this->segundoApellido;
    }
    public function getApellidosCompletoAttribute()
    {
        return  $this->primerApellido . ' ' . $this->segundoApellido;
    }
    public static function validarNotificacionMismoAnioAndroid($idMujerAndroid): bool
    {
        $anio_actual = date('Y');
        $mujer = NotificacionEmbarazo::where('idMujerAndroid', $idMujerAndroid)
            ->whereYear('fechaRegistro', $anio_actual)
            ->first();
        if (is_null($mujer)) {

            return false;
        }
        return true;
    }

    public static function obtenerNumeroControlesPrenatalesPorEdad($localizacion, $idlocalizacion, $rango_edad, $num_controles, $periodos)
    {

        $signo = '>=';
        if ($num_controles <= 0) {
            $signo = '<=';
        }
        // $idlocalizacion = 4;

        switch ($localizacion) {
            case 'nacional':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', $signo, $num_controles);
                break;
            case 'dpto':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.dpt_codigo', $idlocalizacion)
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', $signo, $num_controles);
                break;
            case 'red':


                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.are_codigo', $idlocalizacion)
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', $signo, $num_controles);

                break;
            case 'municipal':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.mnc_codgo', $idlocalizacion)
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', '>', $num_controles);


                break;
            case 'establecimiento':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.codEstablecimiento', $idlocalizacion)
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', '>', $num_controles);


                break;
            default:


                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->whereBetween('ne.edad', $rango_edad)
                    ->where('ned.numeroControlesPrenatal', '>', $num_controles);


                break;
        }
        foreach ($periodos as $p) {
            $contador->whereBetween('ne.fechaRegistro', [$p[0], $p[1]]);
        }
        $contador = $contador->count();

        return  [
            "contador" => $contador,

        ];
    }
    public static function obtenerNumeroMujeresConPlanParto($localizacion, $idlocalizacion, $rango_edad, $planparto, $periodos)
    {
        switch ($localizacion) {
            case 'dpto':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.dpt_codigo', $idlocalizacion)
                    ->where('ned.planPartoLlenado', $planparto)
                    ->whereBetween('ne.edad', $rango_edad);



                break;
            case 'red':
                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.are_codigo', $idlocalizacion)
                    ->where('ned.planPartoLlenado', $planparto)
                    ->whereBetween('ne.edad', $rango_edad);

                break;
            case 'municipal':

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.mnc_codgo', $idlocalizacion)
                    ->where('ned.planPartoLlenado', $planparto)
                    ->whereBetween('ne.edad', $rango_edad);

                break;
            case 'establecimiento':


                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('notificaciones.codEstablecimiento', $idlocalizacion)
                    ->where('ned.planPartoLlenado', $planparto)
                    ->whereBetween('ne.edad', $rango_edad);


                break;
            default:

                $contador = Notificacion::join('notificacionEmbarazo as ne', 'ne.IdNotificacionEmbarazo', '=', 'notificaciones.notificable_id')
                    ->join('notificacionEmbarazoDetalle as ned', 'ned.IdNotificacionEmbarazo', '=', 'ne.IdNotificacionEmbarazo')
                    ->where('notificaciones.notificable_type', 'App\NotificacionEmbarazo')
                    ->where('ned.planPartoLlenado', $planparto)
                    ->whereBetween('ne.edad', $rango_edad);
        }


        foreach ($periodos as $p) {
            $contador->whereBetween('ne.fechaRegistro', [$p[0], $p[1]]);
        }
        $contador = $contador->count();

        return  [
            "contador" => $contador,

        ];
    }

    public  static  function reporteMujeresEmbarazadas($localizacion, $mostrarpor, $periodos)
    {
        $datos = [];
        foreach ($localizacion as $r => $v) {
            $prenatales_15_19_cpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [15, 19], 1, $periodos);


            $prenatales_15_19_scpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [15, 19], 0, $periodos);
            $prenatales_menores_15_cpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [0, 14], 1, $periodos);
            $prenatales_menores_15_scpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [0, 14], 0, $periodos);
            $prenatales_mayores_19_cpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [20, 59], 1, $periodos);
            $prenatales_mayores_19_scpn = self::obtenerNumeroControlesPrenatalesPorEdad($mostrarpor, $v->codigo, [20, 59], 0, $periodos);

            $planparto_15_19_cpp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [15, 19], true, $periodos);
            $planparto_15_19_scpp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [15, 19], false, $periodos);
            $planparto_menores_15_cpp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [0, 15], true, $periodos);
            $planparto_menores_15_spp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [0, 15], false, $periodos);
            $planparto_mayores_19_cpp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [20, 59], true, $periodos);
            $planparto_mayores_19_spp = self::obtenerNumeroMujeresConPlanParto($mostrarpor, $v->codigo, [20, 59], false, $periodos);

            $total = $prenatales_15_19_cpn['contador'] +
                +$prenatales_15_19_scpn['contador']
                + $prenatales_menores_15_cpn['contador']
                + $prenatales_menores_15_scpn['contador']
                + $prenatales_mayores_19_cpn['contador']
                + $prenatales_mayores_19_scpn['contador']
                + $planparto_15_19_cpp['contador']
                + $planparto_15_19_scpp['contador']
                + $planparto_menores_15_cpp['contador']
                + $planparto_menores_15_spp['contador']
                + $planparto_mayores_19_cpp['contador']
                + $planparto_mayores_19_spp['contador'];


            if ($total > 0) {

                $datos[] = [
                    'localizacion' => $v->nombre,
                    'prenatales_15_19_cpn' => $prenatales_15_19_cpn['contador'],
                    'prenatales_15_19_scpn' => $prenatales_15_19_scpn['contador'],
                    'prenatales_menores_15_cpn' => $prenatales_menores_15_cpn['contador'],
                    'prenatales_menores_15_scpn' => $prenatales_menores_15_scpn['contador'],
                    'prenatales_mayores_19_cpn' => $prenatales_mayores_19_cpn['contador'],
                    'prenatales_mayores_19_scpn' => $prenatales_mayores_19_scpn['contador'],
                    'planparto_15_19_cpp' => $planparto_15_19_cpp['contador'],
                    'planparto_15_19_scpp' => $planparto_15_19_scpp['contador'],
                    'planparto_menores_15_cpp' => $planparto_menores_15_cpp['contador'],
                    'planparto_menores_15_spp' => $planparto_menores_15_spp['contador'],
                    'planparto_mayores_19_cpp' => $planparto_mayores_19_cpp['contador'],
                    'planparto_mayores_19_spp' => $planparto_mayores_19_spp['contador'],
                    'total' => $total
                ];
            }
        }
        return $datos;
    }
}
