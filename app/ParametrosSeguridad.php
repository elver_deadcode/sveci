<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParametrosSeguridad extends Model
{
    protected $table = 'parametrosSeguridad';
    protected $fillable = [
        'Longitud_PWD',
        'Calidad_PWD',
        'Tiempo_PWD'
    ];
    public $timestamps = false;

    public function getPatronCalidadPassword()
    {
        $patron='';
        $mensaje='';
        $longitud=(int)$this->Longitud_PWD;
        $longitud_max=20;

        switch ($this->Calidad_PWD) {
            case 1:
                $patron="^[a-zA-Z0-9]{".$longitud.",".$longitud_max."}$";
                $mensaje="La contraseña tiene que tener como   minimo ".$this->Longitud_PWD." caracteres";
                break;
            case 2:
                $patron="^([a-zA-Z0-9]+){".$longitud.",".$longitud_max."}$";
                $mensaje="La contraseña tiene que tener letras y numeros  y como   minimo ".$this->Longitud_PWD." caracteres";
                break;
            case 3:
                $patron="^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{".$longitud.",".$longitud_max."}$";
                $mensaje="La contraseña debe tener al menos ".$this->Longitud_PWD.", al menos un dígito, al menos una minúscula, al menos una mayúscula y al menos un caracter no alfanumérico ";
                break;
        }
        return [
            'patron'=>$patron,
            'mensaje'=>$mensaje
        ];

    }
}
