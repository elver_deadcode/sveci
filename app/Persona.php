<?php

namespace App;

use App\Http\Controllers\EstablecimientoController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
/**

 * @property int IdPersona
 * @property string nombres
 * @property string primerApellido
 * @property string segundoApellido
 * @property string numeroCarnet
 * @property string complemento
 * @property string nacionalidad
 * @property string direccion
 * @property Carbon fechaNacimiento
 * @property string sexo
 * @property int adscrito_id
 * @property boolean verificado
 * @property int codEstablecimiento
 * @property int IdComunidad
 * @property int mnc_codigo
 * @property int are_codigo
 * @property int dpt_codigo
 * @property string email
 * @property string celular
 * @property Carbon $updated_at
 * @property Carbon $created_at
 *
 *
 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="PersonaEditable",
 *     @OA\Property(property="nombres", type="string"),
 *     @OA\Property(property="primerApellido", type="string"),
 *     @OA\Property(property="segundoApellido", type="string"),
 *     @OA\Property(property="numeroCarnet", type="string"),
 *     @OA\Property(property="complemento", type="string"),
 *     @OA\Property(property="nacionalidad", type="string"),
 *     @OA\Property(property="direccion", type="string"),
 *     @OA\Property(property="fechaNacimiento", type="string",format="date-time"),
 *     @OA\Property(property="sexo", type="string", enum={"M", "F"}),
 *     @OA\Property(property="adscrito_id", type="integer"),
 *     @OA\Property(property="verificado", type="integer",enum={"0", "1"}),
 *     @OA\Property(property="codEstablecimiento", type="integer"),
 *     @OA\Property(property="IdComunidad", type="integer"),
 *     @OA\Property(property="mnc_codigo", type="integer"),
 *     @OA\Property(property="are_codigo", type="integer"),
 *     @OA\Property(property="dpt_codigo", type="integer"),
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="celular", type="string"),
 *     @OA\Property(property="edad", type="integer"),
 *     @OA\Property(property="nombre_completo", type="string"),
 *     @OA\Property(property="fecha_nacimiento_android", type="string"),
 * ),
 * @OA\Schema(
 *     schema="Persona",
 *     allOf={
 *         @OA\Schema(ref="#/components/schemas/PersonaEditable"),
 *         @OA\Schema(
 *             type="object",
 *             @OA\Property(property="IdPersona", type="integer", format="id"),
 *             @OA\Property(property="created_at", type="string", format="date-time"),
 *             @OA\Property(property="updated_at", type="string", format="date-time"),
 *
 *         )
 *     },
 *     required={"nombres","primer_apellido", "numeroCarnet", "fechaNacimiento", "sexo", "adscrito_id"}
 * )
 *
 * @OA\Schema(
 *     schema="PersonaMedicoConLocalizacion",
 *     @OA\Property(property="nombres", type="string"),
 *     @OA\Property(property="primerApellido", type="string"),
 *     @OA\Property(property="segundoApellido", type="string"),
 *     @OA\Property(property="numeroCarnet", type="string"),
 *     @OA\Property(property="complemento", type="string"),
 *     @OA\Property(property="nacionalidad", type="string"),
 *     @OA\Property(property="direccion", type="string"),
 *     @OA\Property(property="fechaNacimiento", type="string",format="date-time"),
 *     @OA\Property(property="sexo", type="string", enum={"M", "F"}),
 *     @OA\Property(property="adscrito_id", type="integer"),
 *     @OA\Property(property="verificado", type="integer",enum={"0", "1"}),
 *     @OA\Property(property="codEstablecimiento", type="integer"),
 *     @OA\Property(property="IdComunidad", type="integer"),
 *     @OA\Property(property="mnc_codigo", type="integer"),
 *     @OA\Property(property="are_codigo", type="integer"),
 *     @OA\Property(property="dpt_codigo", type="integer"),
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="celular", type="string"),
 *     @OA\Property(property="edad", type="integer"),
 *     @OA\Property(property="nombre_completo", type="string"),
 *     @OA\Property(property="fecha_nacimiento_android", type="string"),
 *     @OA\Property(property="user",ref="#/components/schemas/UserEditable"),
 *     @OA\Property(property="establecimiento",ref="#/components/schemas/Establecimiento"),
 *     @OA\Property(property="departamento",ref="#/components/schemas/Departamento"),
 *     @OA\Property(property="municipio",ref="#/components/schemas/Municipio"),
 *     @OA\Property(property="red",ref="#/components/schemas/Red"),

 * ),
 * @OA\Schema(
 *     schema="PersonaUsuario",
 *     @OA\Property(property="nombres", type="string"),
 *     @OA\Property(property="primerApellido", type="string"),
 *     @OA\Property(property="segundoApellido", type="string"),
 *     @OA\Property(property="numeroCarnet", type="string"),
 *     @OA\Property(property="complemento", type="string"),
 *     @OA\Property(property="nacionalidad", type="string"),
 *     @OA\Property(property="direccion", type="string"),
 *     @OA\Property(property="fechaNacimiento", type="string",format="date-time"),
 *     @OA\Property(property="sexo", type="string", enum={"M", "F"}),
 *     @OA\Property(property="adscrito_id", type="integer"),
 *     @OA\Property(property="verificado", type="integer",enum={"0", "1"}),
 *     @OA\Property(property="codEstablecimiento", type="integer"),
 *     @OA\Property(property="IdComunidad", type="integer"),
 *     @OA\Property(property="mnc_codigo", type="integer"),
 *     @OA\Property(property="are_codigo", type="integer"),
 *     @OA\Property(property="dpt_codigo", type="integer"),
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="celular", type="string"),
 *     @OA\Property(property="edad", type="integer"),
 *     @OA\Property(property="nombre_completo", type="string"),
 *     @OA\Property(property="fecha_nacimiento_android", type="string"),
 *     @OA\Property(property="user",ref="#/components/schemas/UserEditable"),


 * ),
 *
 *  @OA\Schema(
 *     schema="BuscarPersonaRequest",
 *     @OA\Property(property="num_carnet", type="string",example="50443423"),
 *     @OA\Property(property="fecha_nacimiento", type="string",example="01/01/2000"),
 *     @OA\Property(property="complemento", type="string"),
 * ),
 */
class Persona extends Model
{
    protected $table = 'personas';
    const INCLUDE=['user','mujer','vigilante','comunidad','establecimiento','red','municipio','departamento'];


    protected $primaryKey = "IdPersona";

    protected $appends = [
        'edad',
        'nombre_completo',
        'fecha_nacimiento_android'
    ];

    protected $fillable = [
        'nombres',
        'primerApellido',
        'segundoApellido',
        'numeroCarnet',
        'complemento',
        'nacionalidad',
        'direccion',
        'fechaNacimiento',
        'sexo',
        'adscrito_id',
        'verificado',
        'codEstablecimiento',
        'IdComunidad',
        'mnc_codigo',
        'are_codigo',
        'dpt_codigo',
        'email',
        'celular'


    ];
//    protected $casts = [
//        'codEstablecimiento' => 'string',
//    ];
    protected $dates = ['fechaNacimiento'];

    public function mujer()
    {
        return $this->hasOne(Mujer::class, 'IdPersona', 'IdPersona');
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }



    public function user()
    {
        return $this->hasOne(User::class, 'IdPersona', 'IdPersona');
    }
    public function vigilante()
    {
        return $this->hasOne(User::class, 'IdPersona', 'IdPersona');
    }

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class, 'codEstablecimiento', 'codestabl');
    }

    public function comunidad()
    {
        return $this->belongsTo(Comunidad::class, 'IdComunidad', 'IdComunidad');
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'mnc_codigo', 'mnc_codigo');
    }


    public function red()
    {
        return $this->belongsTo(Red::class, 'are_codigo', 'are_codigo');
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'dpt_codigo', 'dpt_codigo');
    }

    public function obtenerEstablecimiento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->orderBy('idgestion',
            'DESC')->first();
        return $establecimiento;
    }

    public function obtenerMunicipio()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio       = $establecimiento->municipio;
        return $municipio;
    }

    public function obtenerDepartamento()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $municipio       = $establecimiento->municipio;
        $departamento    = $municipio->departamento();
        return $departamento;
    }

    public function obtenerRed()
    {
        $establecimiento = Establecimiento::where('codestabl', $this->codEstablecimiento)->first();
        $red             = Red::find($establecimiento->codarea);
        return $red;
    }

    public function getEdadAttribute()
    {

        $fecha_actual = \Carbon\Carbon::now();

        $edad_anios = $fecha_actual->diffInYears($this->fechaNacimiento);

        return $edad_anios;
    }

    public function getNombreCompletoAttribute()
    {
        return $this->nombres.' '.$this->primerApellido.' '.$this->segundoApellido;
    }

    public function getFechaNacimientoAndroidAttribute()
    {
        return $this->fechaNacimiento->format('d/m/Y');
    }
    public function getFecha_nacimientoAttribute()
    {
        return $this->fechaNacimiento->format('d/m/Y');
    }
    public function getGeneroAttribute()
    {
        if ($this->sexo == 'F') {
            return 'Femenino';
        } elseif ($this->sexo == 'M') {
            return 'Masculino';
        }
        return '';
    }

    public function getTelReferenciaAttribute()
    {
        return $this->celular;
    }

    public static function buscarPersona($numcarnet, $fechanacimiento, $complemento)
    {
        $persona = self::where('numeroCarnet', $numcarnet);
        if ($complemento != '') {
            $persona->where('complemento', $complemento);
        }


        $persona = $persona->first();
        if (is_null($persona)) {
            return null;
        } else {

            return [
                'data'    => [
                    'adscrito_id'        => $persona->adscrito_id,
                    'ci'                 => $persona->numeroCarnet,
                    'complemento'        => $persona->complemento,
                    'departamento'       => $persona->departamento->dpt_nombre ?? '',
                    'departamento_id'    => $persona->dpt_codigo,
                    'direccion'          => $persona->direccion,
                    'establecimiento'    => $persona->establecimiento->nomestabl ?? '',
                    'establecimiento_id' => $persona->codEstablecimiento,
                    'fechanacimiento'    => $persona->fechaNacimiento->format('d/m/Y'),
                    'municipio'          => $persona->municipio->mnc_nombre ?? '',
                    'municipio_id'       => $persona->mnc_codigo,
                    'nombres'            => $persona->nombres,
                    'persona_id'         => $persona->IdPersona,
                    'primerapellido'     => $persona->primerApellido,
                    'red'                => $persona->red->are_nombre ?? '',
                    'red_id'             => $persona->are_codigo,
                    'segundoapellido'    => $persona->segundoApellido,
                    'sexo'               => $persona->genero,
                    'tel_referencia'     => $persona->celular,
                    'email'              => $persona->email,
                    'id'                 => $persona->IdPersona
                ],
                'code'    => 200,
                'success' => true
            ];
        }
    }

    public static function validarNumeroCarnet($numeroCarnet, $complemento)
    {
        $result = [];
        try {
            if (is_empty($numeroCarnet)) {
                throw new Exception("Numero de carnet vacio");
            }
            $persona = self::where('numeroCarnet', $numeroCarnet)
                ->where('complemento', $complemento)
                ->first();

            if (!is_null($persona)) {
                throw new Exception("la persona con numero de carnet ".$numeroCarnet." ya se encuentra registrada en el sistema");
            }
            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false, 'msg' => $e->getMessage()];
        }
    }

    public function obtenerLocalizacion($nivel,$codigo){
        $localizacion=[];
        switch($nivel){
            case 'depto':
                $localizacion=[
                    'dpt_codigo'=>$codigo,
                    'are_codigo'=>'',
                    'mnc_codigo'=>'',
                    'codEstablecimiento'=>''
                    ];
                break;
            case 'muni':
                $muni=Municipio::where('mnc_codigo',$codigo)->first();
                if($muni==null) {
                    $localizacion = [
                        'dpt_codigo'         => $codigo,
                        'are_codigo'         => '',
                        'mnc_codigo'         => '',
                        'codEstablecimiento' => ''
                    ];
                }
                else{
                    $localizacion = [
                        'dpt_codigo'         => $muni->departamento()->dpt_codigo,
                        'are_codigo'         => '',
                        'mnc_codigo'         => $codigo,
                        'codEstablecimiento' => ''
                    ];
                }
                break;
            case 'red':
                $red=Red::where('are_codigo',$codigo)->first();
                if($red==null) {
                    $localizacion = [
                        'dpt_codigo'         => '',
                        'are_codigo'         => $codigo,
                        'mnc_codigo'         => '',
                        'codEstablecimiento' => ''
                    ];
                }
                else{
                    $localizacion = [
                        'dpt_codigo'         => $red->depto->dpt_codigo,
                        'are_codigo'         => $codigo,
                        'mnc_codigo'         => '',
                        'codEstablecimiento' => ''
                    ];
                }
                break;
            case 'establ':
                $estab=Establecimiento::where('codestabl',$codigo)->orderBy('idgestion','DESC')->get();
                if($estab==null) {
                    $localizacion = [
                        'dpt_codigo'         => '',
                        'are_codigo'         => '',
                        'mnc_codigo'         => '',
                        'codEstablecimiento' => $codigo
                    ];
                }
                else{
                        $muni=$estab[0]->municipio;
//                        dd($estab->municipio);
                    $localizacion = [
                        'dpt_codigo'         => $muni->departamento()->dpt_codigo,
                        'are_codigo'         => (int)$estab[0]->codarea,
                        'mnc_codigo'         => $muni->mnc_codigo,
                        'codEstablecimiento' => $codigo
                    ];
                }

                break;
        }

       return $localizacion;


    }
    public function establecerLocalizacion($nivel,$codigo){
        if($nivel!='nacional') {
            $localizacion             = $this->obtenerLocalizacion($nivel, $codigo);
            $this->mnc_codigo         = $localizacion['mnc_codigo']==""?0:$localizacion['mnc_codigo'];
            $this->are_codigo         = $localizacion['are_codigo']==""?0:$localizacion['are_codigo'];
            $this->dpt_codigo         = $localizacion['dpt_codigo']==""?0:$localizacion['dpt_codigo'];
            $this->codEstablecimiento = $localizacion['codEstablecimiento']==""?0:$localizacion['codEstablecimiento'];
            $this->save();
        }
    }


}
