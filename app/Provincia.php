<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @property string coddepto
 * @property string codprovi
 * @property string nomprovi


 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="Provincia",
 *     @OA\Property(property="coddepto", type="string",example="4"),
 *     @OA\Property(property="codprovi", type="string",example="415"),
 *     @OA\Property(property="nomprovi", type="string",example="MEJILLONES"),
 *
 * ),
 * @OA\Schema(
 *     schema="ProvinciaDepartamento",
 *     @OA\Property(property="coddepto", type="string",example="4"),
 *     @OA\Property(property="codprovi", type="string",example="415"),
 *     @OA\Property(property="nomprovi", type="string",example="MEJILLONES"),
 *     @OA\Property(property="departamento", ref="#/components/schemas/Departamento"),
 *
 * ),
 * @OA\Schema(
 *     schema="ProvinciaMunicipios",
 *     @OA\Property(property="coddepto", type="string",example="4"),
 *     @OA\Property(property="codprovi", type="string",example="415"),
 *     @OA\Property(property="nomprovi", type="string",example="MEJILLONES"),
 *     @OA\Property(property="municipios", type="array",
 *     @OA\Items(ref="#/components/schemas/Municipio"),
 * ),
 * ),
 */
class Provincia extends Model
{
    protected $table='ctg_provincia';
     protected $connection= 'snis';

    public $timestamps = false;
    protected $primaryKey="codprovi";
    public $keyType = 'string';

    protected $fillable=[
        "coddepto",
        "codprovi",
        "nomprovi",

    ];

    public function departamento(){
        return $this->belongsTo(Departamento::class,'coddepto');
    }
    public function municipios()
    {
        return $this->hasMany(Municipio::class, 'prv_codigo');
    }

}
