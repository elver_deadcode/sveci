<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @property string are_codigo
 * @property string dpt_codigo
 * @property string are_nombre
 * @property string marca_cpd
 * @property string are_direccion
 * @property string are_telefono
 * @property string are_fax
 * @property string are_casilla
 * @property string are_director
 * @property string are_planificacion
 * @property string are_admcpd


 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="Red",
 *     @OA\Property(property="are_codigo", type="string"),
 *     @OA\Property(property="dpt_codigo", type="string"),
 *     @OA\Property(property="are_nombre", type="string"),
 *     @OA\Property(property="marca_cpd", type="string"),
 *     @OA\Property(property="are_direccion", type="string"),
 *     @OA\Property(property="are_telefono", type="string"),
 *     @OA\Property(property="are_fax", type="string"),
 *     @OA\Property(property="are_casilla", type="string"),
 *     @OA\Property(property="are_director", type="string"),
 *     @OA\Property(property="are_planificacion", type="string"),
 *     @OA\Property(property="are_admcpd", type="string"),
 *
 * ),
 */
class Red extends Model
{
  protected $table = 'ctg_areas';
  protected $connection = 'snis';
    public $keyType = 'string';


    public $timestamps = false;
  protected $primaryKey = "are_codigo";
  protected $fillable = [
    "are_codigo",
    "dpt_codigo",
    "are_nombre",
    "marca_cpd",
    "are_direccion",
    "are_telefono",
    "are_fax",
    "are_casilla",
    "are_director",
    "are_planificacion",
    "are_admcpd",

  ];

  public function depto()
  {
    return $this->belongsTo(Departamento::class, 'dpt_codigo', 'dpt_codigo');
  }

  public static  function obtenerEstablecimientosPorRed($are_codigo)
  {
    return Establecimiento::where('codarea', $are_codigo)->select('codestabl')->distinct()->get();
  }

  public static  function obtenerMunicipiosPorRed($are_codigo)
  {
    return Municipio::join('ctg_provincia as prv', 'prv.codprovi', '=', 'ctg_municipios.prv_codigo')
      ->join('ctg_areas as area', 'area.dpt_codigo', '=', 'prv.coddepto')
      ->where('area.are_codigo', $are_codigo)
      ->get();
  }
  public function getCodigoAttribute()
  {
    return $this->are_codigo;
  }
  public function getNombreAttribute()
  {
    return $this->are_nombre;
  }
}
