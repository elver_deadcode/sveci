<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
/**

 * @property int id
 * @property string username
 * @property string password
 * @property string nivel
 * @property int idnivel
 * @property string estado
 * @property string remember_token
 * @property string IdPersona
 * @property string codigo_credencial
 * The attributes that are mass assignable.
 *
 * @OA\Schema(
 *     schema="UserEditable",
 *     @OA\Property(property="username", type="string",example="Evasquez"),
 *     @OA\Property(property="nivel", type="string",example="establ"),
 *     @OA\Property(property="idnivel", type="integer",example="23423"),
 *     @OA\Property(property="estado", type="boolean"),
 *     @OA\Property(property="IdPersona", type="integer"),
 *     @OA\Property(property="codigo_credencial", type="string"),

 * ),
 * @OA\Schema(
 *     schema="UserCredentialsResponse",
 *     @OA\Property(property="username", type="string",example="Evasquez"),
 *     @OA\Property(property="nivel", type="string",example="establ"),
 *     @OA\Property(property="idnivel", type="integer",example="123"),
 *     @OA\Property(property="estado", type="boolean"),
 *     @OA\Property(property="IdPersona", type="integer",example="1"),
 *     @OA\Property(property="codigo_credencial", type="string"),
 *     @OA\Property(property="persona",ref="#/components/schemas/Persona"),
 *     @OA\Property(property="localizacion",type="object",
 *
 *                   @OA\Property(property="tipo", type="string",example="Establecimiento"),
 *                   @OA\Property(property="informacion",ref="#/components/schemas/Establecimiento"),
 *                ),

 * ),
 *  @OA\Schema(
 *     schema="UserCredentials",
 *     @OA\Property(property="username", type="string"),
 *     @OA\Property(property="password", type="string"),
 * ),
 * @OA\Schema(
 *     schema="User",
 *     allOf={
 *         @OA\Schema(ref="#/components/schemas/UserEditable"),
 *         @OA\Schema(
 *             type="object",
 *             @OA\Property(property="IdPersona", type="integer", format="id"),
 *             @OA\Property(property="created_at", type="string", format="date-time"),
 *             @OA\Property(property="updated_at", type="string", format="date-time"),
 *
 *         )
 *     },
 *     required={"username","nivel", "id_nivel", "IdPersona"}
 * )
 */
class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "username",
        "password",
        "nivel",
        "idnivel",
        "estado",
        "IdPersona"

    ];

    /**
     * The attributecontarNotificacioness that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function persona()
    {
        return $this->belongsTo(Persona::class, 'IdPersona', 'IdPersona');
    }

    public function contarNotificaciones()
    {
        $embarazo       = 0;
        $muerte_mujer   = 0;
        $muerte_bebe    = 0;
        $parto          = 0;
        $registro_mujer = 0;
        $teleconsulta=0;
        $activacion     = 0;
        switch ($this->nivel) {
            case 'nacional':
                $embarazo     = Notificacion::where('notificable_type', 'App\NotificacionEmbarazo')
                    ->where('vistoNacional', 'false')->count();
                $muerte_mujer = Notificacion::where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->where('vistoNacional', 'false')->count();
                $muerte_bebe  = Notificacion::where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->where('vistoNacional', 'false')->count();
                $parto        = Notificacion::where('notificable_type', 'App\NotificacionParto')
                    ->where('vistoNacional', 'false')->count();
                $teleconsulta        = Notificacion::where('notificable_type', 'App\NotificacionTeleconsulta')
                                            ->where('vistoNacional', 'false')->count();
                break;
            case 'depto':
                $embarazo     = Notificacion::where('notificable_type', 'App\NotificacionEmbarazo')
                    ->where('dpt_codigo', $this->idnivel)
                    ->where('vistoDepartamento', 'false')
                    ->count();
                $muerte_mujer = Notificacion::where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->where('dpt_codigo', $this->idnivel)
                    ->where('vistoDepartamento', 'false')
                    ->count();
                $muerte_bebe  = Notificacion::where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->where('dpt_codigo', $this->idnivel)
                    ->where('vistoDepartamento', 'false')
                    ->count();
                $parto        = Notificacion::where('notificable_type', 'App\NotificacionParto')
                    ->where('dpt_codigo', $this->idnivel)
                    ->where('vistoDepartamento', 'false')
                    ->count();
                $teleconsulta        = Notificacion::where('notificable_type', 'App\NotificacionTeleconsulta')
                                            ->where('dpt_codigo', $this->idnivel)
                                            ->where('vistoDepartamento', 'false')
                                            ->count();
                break;

            case 'red':
                $embarazo     = Notificacion::where('notificable_type', 'App\NotificacionEmbarazo')
                    ->where('are_codigo', $this->idnivel)
                    ->where('vistoRed', 'false')
                    ->count();
                $muerte_mujer = Notificacion::where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->where('are_codigo', $this->idnivel)
                    ->where('vistoRed', 'false')
                    ->count();
                $muerte_bebe  = Notificacion::where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->where('are_codigo', $this->idnivel)
                    ->where('vistoRed', 'false')
                    ->count();
                $parto        = Notificacion::where('notificable_type', 'App\NotificacionParto')
                    ->where('are_codigo', $this->idnivel)
                    ->where('vistoRed', 'false')
                    ->count();
                $teleconsulta        = Notificacion::where('notificable_type', 'App\NotificacionTeleconsulta')
                                            ->where('are_codigo', $this->idnivel)
                                            ->where('vistoRed', 'false')
                                            ->count();
                break;

            case 'muni':
                $embarazo     = Notificacion::where('notificable_type', 'App\NotificacionEmbarazo')
                    ->where('mnc_codgo', $this->idnivel)
                    ->where('vistoMunicipio', 'false')
                    ->count();
                $muerte_mujer = Notificacion::where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->where('mnc_codgo', $this->idnivel)
                    ->where('vistoMunicipio', 'false')
                    ->count();
                $muerte_bebe  = Notificacion::where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->where('vistoNacional', 'false')->count();
                $parto        = Notificacion::where('notificable_type', 'App\NotificacionParto')
                    ->where('mnc_codgo', $this->idnivel)
                    ->where('vistoMunicipio', 'false')
                    ->count();
                $teleconsulta        = Notificacion::where('notificable_type', 'App\NotificacionTeleconsulta')
                                            ->where('mnc_codgo', $this->idnivel)
                                            ->where('vistoMunicipio', 'false')
                                            ->count();

                break;

            case 'establ':


                $embarazo     = Notificacion::where('notificable_type', 'App\NotificacionEmbarazo')
                    ->where('codEstablecimiento', $this->idnivel)
                    ->where('vistoEstablecimiento', 'false')
                    ->count();
                $muerte_mujer = Notificacion::where('notificable_type', 'App\NotificacionMuerteMujer')
                    ->where('codEstablecimiento', $this->idnivel)
                    ->where('vistoEstablecimiento', 'false')
                    ->count();
                $muerte_bebe  = Notificacion::where('notificable_type', 'App\NotificacionMuerteBebe')
                    ->where('codEstablecimiento', $this->idnivel)
                    ->where('vistoEstablecimiento', 'false')->count();
                $parto        = Notificacion::where('notificable_type', 'App\NotificacionParto')
                    ->where('codEstablecimiento', $this->idnivel)
                    ->where('vistoEstablecimiento', 'false')
                    ->count();
                $teleconsulta       = Notificacion::where('notificable_type', 'App\NotificacionTeleconsulta')
                                            ->where('codEstablecimiento', $this->idnivel)
                                            ->where('vistoEstablecimiento', 'false')
                                            ->count();

                break;


        }

        return [
            'num_embarazo'      => $embarazo,
            'num_muertemujer'   => $muerte_mujer,
            'num_muertebebe'    => $muerte_bebe,
            'num_registromujer' => $registro_mujer,
            'num_parto'         => $parto,
            'num_teleconsulta'  => $teleconsulta,
            'num_activacion'    => $activacion,
            'todas'             => $embarazo + $muerte_mujer + $muerte_bebe + $parto+$teleconsulta
        ];
    }

    public function contarTodasNotificaciones()
    {
        $notificaciones = 0;
        switch ($this->nivel) {
            case 'nacional':
                $notificaciones = Notificacion::where('vistoNacional', 'false')
                    ->whereIn('notificable_type', [
                        'App\NotificacionEmbarazo',
                        'App\NotificacionMuerteMujer',
                        'App\NotificacionMuerteBebe',
                        'App\NotificacionParto',
                    ])
                    ->count();

                break;
            case 'depto':
                $notificaciones = Notificacion::where('dpt_codigo', $this->idnivel)
                    ->where('vistoDepartamento', 'false')
                    ->whereIn('notificable_type', [
                        'App\NotificacionEmbarazo',
                        'App\NotificacionMuerteMujer',
                        'App\NotificacionMuerteBebe',
                        'App\NotificacionParto',
                    ])
                    ->count();

                break;

            case 'red':
                $notificaciones = Notificacion::where('are_codigo', $this->idnivel)
                    ->where('vistoRed', 'false')
                    ->whereIn('notificable_type', [
                        'App\NotificacionEmbarazo',
                        'App\NotificacionMuerteMujer',
                        'App\NotificacionMuerteBebe',
                        'App\NotificacionParto',
                    ])
                    ->count();

                break;

            case 'muni':
                $notificaciones = Notificacion::where('mnc_codgo', $this->idnivel)
                    ->where('vistoMunicipio', 'false')
                    ->whereIn('notificable_type', [
                        'App\NotificacionEmbarazo',
                        'App\NotificacionMuerteMujer',
                        'App\NotificacionMuerteBebe',
                        'App\NotificacionParto',
                    ])
                    ->count();


                break;

            case 'establ':

                $notificaciones = Notificacion::where('codEstablecimiento', $this->idnivel)
                    ->where('vistoEstablecimiento', 'false')
                    ->whereIn('notificable_type', [
                        'App\NotificacionEmbarazo',
                        'App\NotificacionMuerteMujer',
                        'App\NotificacionMuerteBebe',
                        'App\NotificacionParto',
                    ])
                    ->count();

                break;


        }

        return $notificaciones;

    }

    public static function generarNombreUsuario($nombres, $primerApellido, $segundoApellido)
    {
        $nombre_inicial = strtoupper(substr(explode(' ', ltrim($nombres))[0], 0, 1));

        $apellido  = strtolower(trim($primerApellido));
        $apellido2 = strtoupper(trim($segundoApellido));


        if ($apellido2 == '') {
            $apellido2 = strtoupper(substr($segundoApellido, 1, 1));
        }

        $username = $nombre_inicial.$apellido;

        $query = self::where('username', $username)->first();


        if (!is_null($query)) {
            $username = $username.$apellido2;
        }
        $query = self::where('username', $username)->first();
        if (!is_null($query)) {
            $username = $username.$apellido2.rand(10, 100);
        }


        return $username;
        //   return Str::random(2);
    }

    public function obtenerNivelUsuario()
    {
        $nivel = '';
        switch ($this->nivel) {
            case 'nacional':
                $nivel = "Nacional";
                break;
            case 'depto':
                $dpto  = Departamento::where('dpt_codigo', $this->idnivel)->first();
                $nivel = $dpto->dpt_nombre;
                break;
            case 'red':
                $red   = Red::where('are_codigo', $this->idnivel)->first();
                $nivel = $red->are_nombre;
                break;
            case 'muni':
                $muni  = Municipio::where('mnc_codigo', $this->idnivel)->first();
                $dpto  = $muni->departamento()->dpt_nombre;
                $nivel = $dpto.' / '.$muni->mnc_nombre;
                break;
            case 'establ':
                $establ = Establecimiento::where('codestabl', $this->idnivel)->orderBy('idgestion', 'DESC')->first();

                $muni  = $establ->municipio;
                $dpto  = $muni->departamento()->dpt_nombre;
                $nivel = $dpto.' / '.$muni->mnc_nombre.' / '.$establ->nomestabl;
                break;

        }

        return $nivel;
    }
    public function obtenerLocalizacion()
    {
        $nivel = '';
        switch ($this->nivel) {
            case 'nacional':
                $tipo='Nacional';
                $nivel = "Nacional";
                break;
            case 'depto':
                $dpto  = Departamento::where('dpt_codigo', $this->idnivel)->first();
                $tipo="Departamento";
                $nivel = $dpto;

                break;
            case 'red':
                $red   = Red::where('are_codigo', $this->idnivel)->first();
                $tipo="Red";
                $nivel = $red;
                break;
            case 'muni':
                $muni  = Municipio::where('mnc_codigo', $this->idnivel)->first();
                $tipo="Municipio";
                $nivel =$muni;
                break;
            case 'establ':
                $tipo="Establecimiento";
                $establ = Establecimiento::where('codestabl', $this->idnivel)->orderBy('idgestion', 'DESC')->first();


                $nivel =$establ;
                break;

        }

        return [
            'tipo'=>$tipo,
            'informacion'=>$nivel
        ];
    }

    public function getNiveles()
    {
        $dptos            = [];
        $municipios       = [];
        $establecimientos = [];
        $redes            = [];
        if ($this->nivel == 'nacional') {
            $dptos            = Departamento::orderBy('dpt_nombre', 'ASC')->get();
            $municipios       = Municipio::orderBy('mnc_nombre', 'ASC')->get();
            $redes            = Red::orderBy('are_codigo', 'ASC')->get();
            $establecimientos = [];
            $comunidades      = [];


        } elseif ($this->nivel == 'depto') {
            $dptos = Departamento::where('dpt_codigo', $this->idnivel)->get();


            $municipios       = $dptos[0]->obtenerMunicipios();
            $redes            = Red::where('dpt_codigo', $dptos[0]->dpt_codigo)->orderBy('dpt_codigo', 'ASC')->get();
            $establecimientos = [];
            $comunidades      = [];
        } elseif ($this->nivel == 'red') {
            $dptos = [];
            $redes = Red::where('are_codigo', $this->idnivel)->get();
            // $dptos = Departamento::where('dpt_codigo', $redes[0]->dpt_codigo)->get();
            // $municipios = $dptos[0]->obtenerMunicipios();
            $municipios = [];

            $lista_establecimientos = $redes[0]->obtenerEstablecimientosPorRed($redes[0]->are_codigo);
            $establecimientos       = [];
            $comunidades            = [];
            foreach ($lista_establecimientos as $k => $v) {
                $establecimientos[] = Establecimiento::where('codestabl', $v->codestabl)->orderBy('idgestion',
                    'DESC')->first();
            }


        } elseif ($this->nivel == 'muni') {


            $dptos       = [];
            $redes       = [];
            $municipios  = Municipio::where('mnc_codigo', $this->idnivel)->get();
            $comunidades = $municipios[0]->comunidades()->orderBy('nomComunidad', 'ASC')->get();

            $lista_establecimientos = $municipios[0]->establecimientos()->orderBy('idgestion',
                    'DESC')->get();
            $establecimientos       = [];
            foreach ($lista_establecimientos as $k => $v) {
                $establecimientos[] = Establecimiento::where('codestabl', $v->codestabl)->orderBy('idgestion',
                    'DESC')->first();
            }
        } elseif ($this->nivel == 'establ') {

            $dptos              = [];
            $municipios         = [];
            $redes              = [];
            $establ             = Establecimiento::where('codestabl', $this->idnivel)->orderBY('idgestion',
                'DESC')->first();
            $establecimientos[] = $establ;

            $comunidades = $establecimientos[0]->municipio->comunidades()->orderBy('nomComunidad', 'ASC')->get();

            // return dd($establecimientos);
        }

        return [
            'dptos'            => $dptos,
            'municipios'       => $municipios,
            'establecimientos' => $establecimientos,
            'redes'            => $redes,
            'comunidades'      => $comunidades
        ];
    }

    public function getNombreCompletoAttribute()
    {
        return $this->nombres.' '.$this->primer_apellido.' '.$this->segundo_apellido;
    }


}
