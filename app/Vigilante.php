<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class Vigilante extends Model
{
    protected $table = 'vigilantes';


    protected $primaryKey = "IdVigilante";

    protected $appends = [

        'activo',
        'estado_vigilante',

    ];

    protected $fillable = [

        "longitud",
        "latitud",
        "codigo",
        "estado",
        'IdUsuario',
        'activado',
        'IdPersona'

    ];
    public function persona()
    {
        return $this->belongsTo(Persona::class, 'IdPersona', 'IdPersona');
    }


    public function notificable()
    {
        return $this->morphMany(Notificacion::class, 'notificable');
    }


    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'IdUsuario', 'id');
    }
    public static function ConsultaVigilantesPorEstablecimiento($codigos_establecimientos)
    {
        $vigilantes = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
            ->whereIn('p.codEstablecimiento', $codigos_establecimientos)
            ->orderBy('p.primerApellido', 'DESC')
            ->select('vigilantes.*')
            ->with('persona')
            ->get();
        return $vigilantes;
    }

    public static function generarCodigo($num_carnet)
    {

        $codigo = strtoupper(\Str::random(3)) . $num_carnet;
        return substr($codigo, 0, 10);
    }


    public function getActivoAttribute()
    {
        return $this->attributes['activado'] ? 'Activado' : 'No Activado';
    }
    public function getEstadoVigilanteAttribute()
    {
        return $this->attributes['estado'] ? 'Alta' : 'De Baja';
    }




    public static function  listavigilantes($request)
    {

        $datos=[];
        if(isset($request->nivel)){
        $datos =  $datos = Vigilante::join('personas as p', 'p.IdPersona', 'vigilantes.IdPersona')
            ->leftjoin('comunidades as c', 'c.IdComunidad', '=', 'p.IdComunidad');
        switch ($request->nivel) {
            case 'nacional':

                break;
            case 'depto':

                $datos->where('p.dpt_codigo', $request->dpto);
                break;
            case 'muni':
                $datos->where('p.mnc_codigo', $request->municipio);
                break;
            case 'red':
                $datos->where('p.are_codigo', $request->red);
                break;
            case 'establ':
                $datos->where('p.codEstablecimiento', $request->establecimiento);
                break;
        }
        $datos=$datos->get();
    }
        return $datos;
    }





    public  static function consolidadoEstado($localizacion, $idlocalizacion, $estado, $activado, $periodos)
    {

        $si_activaron = 'vigilantes.fechaActivacion';
        if (!$activado) {
            $si_activaron = 'vigilantes.created_at';
        }


        $tipo_localizacion = '';
        switch ($localizacion) {
            case 'dpto':
                $tipo_localizacion = 'p.dpt_codigo';
                break;
            case 'red':
                $tipo_localizacion = 'p.are_codigo';
                break;
            case 'municipal':
                $tipo_localizacion = 'p.mnc_codigo';
                break;
            case 'establecimiento':
                $tipo_localizacion = 'p.codEstablecimiento';
                break;
        }
        if ($tipo_localizacion != '') {
            $contador = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                ->where('vigilantes.estado', $estado)
                ->where('vigilantes.activado', $activado)
                ->where($tipo_localizacion, $idlocalizacion);
            // ->whereBetween($si_activaron, [$fecha_ini, $fecha_fin])
            // ->count();
        } else {
            $contador = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                ->where('vigilantes.estado', $estado)
                ->where('vigilantes.activado', $activado);
            // ->whereBetween($si_activaron, [$fecha_ini, $fecha_fin])
            // ->count();
        }
        try {
            foreach ($periodos as $p) {
                $contador->whereBetween($si_activaron, [$p[0], $p[1]]);
            }
            $contador = $contador->count();

            return $contador;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public  static function consolidadoEstadoBaja($localizacion, $idlocalizacion, $estado)
    {

        $tipo_localizacion = '';
        switch ($localizacion) {
            case 'dpto':
                $tipo_localizacion = 'p.dpt_codigo';
                break;
            case 'red':
                $tipo_localizacion = 'p.are_codigo';
                break;
            case 'municipal':
                $tipo_localizacion = 'p.mnc_codigo';
                break;
            case 'establecimiento':
                $tipo_localizacion = 'p.codEstablecimiento';
                break;
        }
        if ($tipo_localizacion != '') {
            $contador = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                ->where('vigilantes.estado', $estado)
                ->where($tipo_localizacion, $idlocalizacion)
                ->count();
        } else {
            $contador = Vigilante::join('personas as p', 'p.IdPersona', '=', 'vigilantes.IdPersona')
                ->where('vigilantes.estado', $estado)

                ->count();
        }
        try {
            return $contador;
        } catch (\Exception $e) {
            return 0;
        }
    }


    public  static function consolidadoReportan($localizacion, $idlocalizacion, $meses, $activado, $periodos)
    {
        $fecha = $periodos[0][0];
        $fecha_ini = $periodos[0][0];
        $aux_array = $periodos[count($periodos) - 1];
        $fecha_fin = $periodos[count($periodos) - 1][count($aux_array) - 1];
        $nuevafecha = strtotime("-$meses month", strtotime($fecha));
        $nuevafecha = date('Y-m-d', $nuevafecha);
        $si_notifican = 0;
        $no_notifican_tiempo = 0;
        $no_notifican_nunca = 0;
        $cod_localizacion = '';


        switch ($localizacion) {
            case 'nacional':
                $cod_localizacion = '';
                break;
            case 'dpto':

                $cod_localizacion = 'notificaciones.dpt_codigo';
                break;
            case 'red':
                $cod_localizacion = 'notificaciones.are_codigo';
                break;
            case 'municipal':
                $cod_localizacion = 'notificaciones.mnc_codgo';
                break;
            case 'establecimiento':
                $cod_localizacion = 'notificaciones.codEstablecimiento';
                break;
        }
        if ($cod_localizacion != '') {
            $si_notifican_android = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where('tipo', 'android')
                ->where($cod_localizacion, $idlocalizacion)
                ->whereBetween('notificaciones.created_at', [$nuevafecha, $fecha_fin])
                ->count();




            $si_notifican_papel = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', 'false')
                ->where('vigilantes.estado', $activado)
                ->where('tipo', 'web')
                ->where($cod_localizacion, $idlocalizacion)
                ->whereBetween('notificaciones.created_at', [$nuevafecha, $fecha_fin])
                ->count();



            $si_notifican = $si_notifican_android + $si_notifican_papel;


            if ($si_notifican_papel > 0) {
                $si_notifican_papel = 1;
            }
            if ($si_notifican_android > 0) {
                $si_notifican_android = 1;
            }

            $no_notifican_tiempo_papel = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where($cod_localizacion, $idlocalizacion)
                ->where('tipo', 'web')
                ->where('notificaciones.created_at', '<', $nuevafecha)
                ->count();


            $no_notifican_tiempo_android = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where('tipo', 'android')
                ->where($cod_localizacion, $idlocalizacion)
                ->where('notificaciones.created_at', '<', $nuevafecha)
                ->count();

            $no_notifican_nunca_papel = Vigilante::leftjoin('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.estado', 'true')
                ->where('vigilantes.activado', 'false')
                ->where('tipo', 'web')
                ->whereBetween('vigilantes.created_at', [$fecha_ini, $fecha_fin])
                ->where($cod_localizacion, $idlocalizacion)
                ->whereNull('notificaciones.id')
                ->count();


            $no_notifican_nunca_android = Vigilante::leftjoin('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.estado', 'true')
                ->where('vigilantes.activado', 'true')
                ->where('tipo', 'android')
                ->whereBetween('vigilantes.fechaActivacion', [$fecha_ini, $fecha_fin])
                ->where($cod_localizacion, $idlocalizacion)
                ->whereNull('notificaciones.id')
                ->count();
        } else {
            $si_notifican_android = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where('tipo', 'android')
                ->whereBetween('notificaciones.created_at', [$nuevafecha, $fecha_fin])
                ->count();




            $si_notifican_papel = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', 'false')
                ->where('vigilantes.estado', $activado)
                ->where('tipo', 'web')
                ->whereBetween('notificaciones.created_at', [$nuevafecha, $fecha_fin])
                ->count();



            $si_notifican = $si_notifican_android + $si_notifican_papel;


            if ($si_notifican_papel > 0) {
                $si_notifican_papel = 1;
            }
            if ($si_notifican_android > 0) {
                $si_notifican_android = 1;
            }

            $no_notifican_tiempo_papel = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where('tipo', 'web')
                ->where('notificaciones.created_at', '<', $nuevafecha)
                ->count();


            $no_notifican_tiempo_android = Vigilante::join('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.activado', $activado)
                ->where('tipo', 'android')
                ->where('notificaciones.created_at', '<', $nuevafecha)
                ->count();

            $no_notifican_nunca_papel = Vigilante::leftjoin('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.estado', 'true')
                ->where('vigilantes.activado', 'false')
                ->where('tipo', 'web')
                ->whereBetween('vigilantes.created_at', [$fecha_ini, $fecha_fin])
                ->whereNull('notificaciones.id')
                ->count();


            $no_notifican_nunca_android = Vigilante::leftjoin('notificaciones', 'vigilantes.IdVigilante', '=', 'notificaciones.idVigilante')
                ->where('vigilantes.estado', 'true')
                ->where('vigilantes.activado', 'true')
                ->whereBetween('vigilantes.fechaActivacion', [$fecha_ini, $fecha_fin])
                ->whereNull('notificaciones.id')
                ->count();
        }
        $no_notifican = $no_notifican_tiempo_papel + $no_notifican_tiempo_android + $no_notifican_nunca_papel + $no_notifican_nunca_android;


        try {

            return [
                'notifican_android' => $si_notifican_android,
                'notifican_papel' => $si_notifican_papel,
                'no_notifican' => $no_notifican,
                'no_notifican_tiempo' => $no_notifican_tiempo_papel + $no_notifican_tiempo_android,
                'no_notifican_nunca' => $no_notifican_nunca_papel + $no_notifican_nunca_android
            ];
        } catch (\Exception $e) {
            return [
                'notifican_android' => 0,
                'notifican_papel' => 0,
                'no_notifican' => 0,
                'no_notifican_tiempo' => 0,
                'no_notifican_nunca' => 0
            ];
        }
    }

    public static function reporteConsolidadoVigilantes($localizacion, $mostrarpor, $periodos, $meses)
    {

        $datos = [];

        foreach ($localizacion as $r => $v) {
            $consulta1 = self::consolidadoReportan($mostrarpor, $v->codigo, $meses, true, $periodos);
            $vigilantes_si_activados = self::consolidadoEstado($mostrarpor, $v->codigo, true, true, $periodos);
            $vigilantes_no_activados = self::consolidadoEstado($mostrarpor, $v->codigo, true, false, $periodos);
            $vigilantes_baja = self::consolidadoEstadoBaja($mostrarpor, $v->codigo, false, $periodos);
            $vigilantes_alta = self::consolidadoEstadoBaja($mostrarpor, $v->codigo, true, $periodos);
            $total = $vigilantes_baja + $vigilantes_alta;



            if ($total > 0) {
                $datos[] = [
                    'localizacion' => $v->nombre,
                    'vigilantes_si_activados' => $vigilantes_si_activados,
                    'vigilantes_no_activados' => $vigilantes_no_activados,
                    'vigilantes_baja' => $vigilantes_baja,
                    'vigilantes_notifican_android' => $consulta1['notifican_android'],
                    'vigilantes_notifican_papel' => $consulta1['notifican_papel'],
                    'vigilantes_no_notifican' => $consulta1['no_notifican'],
                    'total' => $total


                ];
            }
        }
        return $datos;
    }
}
