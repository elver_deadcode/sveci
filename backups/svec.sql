PGDMP     &        	            x            svec    9.6.14    9.6.14 �    �
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �
           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �
           1262    50461    svec    DATABASE     b   CREATE DATABASE svec WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE svec;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �
           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12655    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �
           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    511323 	   auditoria    TABLE     3  CREATE TABLE public.auditoria (
    id integer NOT NULL,
    "Nombre_tabla" character varying(255) NOT NULL,
    "Id_tabla" character varying(255) NOT NULL,
    "Tipo_usuario" character varying(255) NOT NULL,
    "Fecha_ing" date,
    "Hora_ing" time(0) without time zone,
    "Fecha_sal" date,
    "Hora_sal" time(0) without time zone,
    "IP_terminal" character varying(255),
    "Evento" character varying(255),
    "Modulo" character varying(255),
    "Menu" character varying(255),
    "Submenu" character varying(255),
    "Item" character varying(255)
);
    DROP TABLE public.auditoria;
       public         postgres    false    3            �            1259    511321    auditoria_id_seq    SEQUENCE     y   CREATE SEQUENCE public.auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auditoria_id_seq;
       public       postgres    false    226    3            �
           0    0    auditoria_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auditoria_id_seq OWNED BY public.auditoria.id;
            public       postgres    false    225            �            1259    511107    comunidades    TABLE     d  CREATE TABLE public.comunidades (
    "IdComunidad" integer NOT NULL,
    "codLocalidad" character varying(255) NOT NULL,
    "codMunicipio" character varying(255) NOT NULL,
    "nomLocalidad" character varying(255),
    "nomComunidad" character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.comunidades;
       public         postgres    false    3            �            1259    511105    comunidades_IdComunidad_seq    SEQUENCE     �   CREATE SEQUENCE public."comunidades_IdComunidad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."comunidades_IdComunidad_seq";
       public       postgres    false    3    222            �
           0    0    comunidades_IdComunidad_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public."comunidades_IdComunidad_seq" OWNED BY public.comunidades."IdComunidad";
            public       postgres    false    221            �            1259    511350    contraseniaUsuario    TABLE       CREATE TABLE public."contraseniaUsuario" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    "FechaDeCambio" date NOT NULL,
    "Contrasenia" character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 (   DROP TABLE public."contraseniaUsuario";
       public         postgres    false    3            �            1259    511348    contraseniaUsuario_id_seq    SEQUENCE     �   CREATE SEQUENCE public."contraseniaUsuario_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public."contraseniaUsuario_id_seq";
       public       postgres    false    3    232            �
           0    0    contraseniaUsuario_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public."contraseniaUsuario_id_seq" OWNED BY public."contraseniaUsuario".id;
            public       postgres    false    231            �            1259    511334    evento    TABLE     k   CREATE TABLE public.evento (
    "Id_evento" integer NOT NULL,
    "Descripcion" character varying(255)
);
    DROP TABLE public.evento;
       public         postgres    false    3            �            1259    511332    evento_Id_evento_seq    SEQUENCE        CREATE SEQUENCE public."evento_Id_evento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."evento_Id_evento_seq";
       public       postgres    false    3    228            �
           0    0    evento_Id_evento_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."evento_Id_evento_seq" OWNED BY public.evento."Id_evento";
            public       postgres    false    227            �            1259    511072    gmaps_geocache    TABLE       CREATE TABLE public.gmaps_geocache (
    id integer NOT NULL,
    address text NOT NULL,
    latitude character varying(255) NOT NULL,
    longitude character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 "   DROP TABLE public.gmaps_geocache;
       public         postgres    false    3            �            1259    511070    gmaps_geocache_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.gmaps_geocache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.gmaps_geocache_id_seq;
       public       postgres    false    220    3            �
           0    0    gmaps_geocache_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.gmaps_geocache_id_seq OWNED BY public.gmaps_geocache.id;
            public       postgres    false    219            �            1259    510837 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false    3            �            1259    510835    migrations_id_seq    SEQUENCE     z   CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    3    186            �
           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    185            �            1259    510937    mujeres    TABLE     �  CREATE TABLE public.mujeres (
    "IdMujer" integer NOT NULL,
    "IdVigilante" integer,
    longitud character varying(250),
    latitud character varying(250),
    estado boolean NOT NULL,
    "idAndroid" character varying(250),
    "IdUsuario" integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "IdPersona" integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.mujeres;
       public         postgres    false    3            �            1259    510935    mujeres_IdMujer_seq    SEQUENCE     ~   CREATE SEQUENCE public."mujeres_IdMujer_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."mujeres_IdMujer_seq";
       public       postgres    false    200    3            �
           0    0    mujeres_IdMujer_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public."mujeres_IdMujer_seq" OWNED BY public.mujeres."IdMujer";
            public       postgres    false    199            �            1259    510948    notificacionEmbarazo    TABLE     �  CREATE TABLE public."notificacionEmbarazo" (
    "IdNotificacionEmbarazo" integer NOT NULL,
    "IdAndroid" character varying(255) NOT NULL,
    "codVigilante" character varying(255) NOT NULL,
    "codEstablecimiento" character varying(255) NOT NULL,
    estado character varying(20),
    nombres character varying(150),
    "primerApellido" character varying(250),
    "segundoApellido" character varying(250),
    "fechaNacimento" date,
    direccion character varying(250),
    telefono character varying(255),
    latitud character varying(255),
    longitud character varying(255),
    edad integer,
    "semanasEmbarazo" integer,
    "fechaRegistro" timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "idMujerAndroid" character varying(255) NOT NULL,
    cerrado boolean DEFAULT false NOT NULL,
    fecha_cerrado date
);
 *   DROP TABLE public."notificacionEmbarazo";
       public         postgres    false    3            �            1259    511035    notificacionEmbarazoDetalle    TABLE     �  CREATE TABLE public."notificacionEmbarazoDetalle" (
    "IdNotificacionEmbarazoDetalle" integer NOT NULL,
    "numeroControlesPrenatal" integer DEFAULT 0 NOT NULL,
    "IdNotificacionEmbarazo" integer NOT NULL,
    "planPartoLlenado" boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    user_id integer DEFAULT 0 NOT NULL,
    comentarios character varying(500)
);
 1   DROP TABLE public."notificacionEmbarazoDetalle";
       public         postgres    false    3            �            1259    511033 =   notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 V   DROP SEQUENCE public."notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq";
       public       postgres    false    3    212            �
           0    0 =   notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq" OWNED BY public."notificacionEmbarazoDetalle"."IdNotificacionEmbarazoDetalle";
            public       postgres    false    211            �            1259    510946 /   notificacionEmbarazo_IdNotificacionEmbarazo_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionEmbarazo_IdNotificacionEmbarazo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 H   DROP SEQUENCE public."notificacionEmbarazo_IdNotificacionEmbarazo_seq";
       public       postgres    false    3    202            �
           0    0 /   notificacionEmbarazo_IdNotificacionEmbarazo_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionEmbarazo_IdNotificacionEmbarazo_seq" OWNED BY public."notificacionEmbarazo"."IdNotificacionEmbarazo";
            public       postgres    false    201            �            1259    510970    notificacionMuerteBebe    TABLE     �  CREATE TABLE public."notificacionMuerteBebe" (
    "IdNotificacionMuerteBebe" integer NOT NULL,
    "IdAndroid" character varying(255) NOT NULL,
    "idMujerAndroid" character varying(255) NOT NULL,
    "codVigilante" character varying(255) NOT NULL,
    "codEstablecimiento" character varying(255) NOT NULL,
    estado character varying(20),
    nombres character varying(150),
    "primerApellido" character varying(250),
    "segundoApellido" character varying(250),
    "fechaNacimento" date,
    direccion character varying(250),
    telefono character varying(255),
    latitud character varying(255),
    longitud character varying(255),
    edad integer,
    nacido boolean,
    "fechaRegistro" timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    cerrado boolean DEFAULT false NOT NULL,
    fecha_cerrado date
);
 ,   DROP TABLE public."notificacionMuerteBebe";
       public         postgres    false    3            �            1259    511054    notificacionMuerteBebeDetalle    TABLE       CREATE TABLE public."notificacionMuerteBebeDetalle" (
    "IdMuerteBebeDetalle" integer NOT NULL,
    nacido boolean DEFAULT false NOT NULL,
    "tieneAutopsiaVerbal" boolean DEFAULT false NOT NULL,
    "IdNotificacionMuerteBebe" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    user_id integer DEFAULT 0 NOT NULL,
    fechanacimiento date,
    fechafallecimiento date,
    "tienePlanAccion" boolean DEFAULT false NOT NULL,
    comentarios character varying(500)
);
 3   DROP TABLE public."notificacionMuerteBebeDetalle";
       public         postgres    false    3            �            1259    511052 5   notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 N   DROP SEQUENCE public."notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq";
       public       postgres    false    3    216            �
           0    0 5   notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq" OWNED BY public."notificacionMuerteBebeDetalle"."IdMuerteBebeDetalle";
            public       postgres    false    215            �            1259    510968 3   notificacionMuerteBebe_IdNotificacionMuerteBebe_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionMuerteBebe_IdNotificacionMuerteBebe_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 L   DROP SEQUENCE public."notificacionMuerteBebe_IdNotificacionMuerteBebe_seq";
       public       postgres    false    3    206            �
           0    0 3   notificacionMuerteBebe_IdNotificacionMuerteBebe_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionMuerteBebe_IdNotificacionMuerteBebe_seq" OWNED BY public."notificacionMuerteBebe"."IdNotificacionMuerteBebe";
            public       postgres    false    205            �            1259    510959    notificacionMuerteMujer    TABLE     �  CREATE TABLE public."notificacionMuerteMujer" (
    "IdNotificacionMuerteMujer" integer NOT NULL,
    "IdAndroid" character varying(255) NOT NULL,
    "idMujerAndroid" character varying(255) NOT NULL,
    "codVigilante" character varying(255) NOT NULL,
    "codEstablecimiento" character varying(255) NOT NULL,
    estado character varying(20),
    nombres character varying(150),
    "primerApellido" character varying(250),
    "segundoApellido" character varying(250),
    "fechaNacimento" date,
    direccion character varying(250),
    telefono character varying(255),
    latitud character varying(255),
    longitud character varying(255),
    edad integer,
    embarazada boolean,
    "fechaRegistro" timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    cerrado boolean DEFAULT false NOT NULL,
    fecha_cerrado date
);
 -   DROP TABLE public."notificacionMuerteMujer";
       public         postgres    false    3            �            1259    511045    notificacionMuerteMujerDetalle    TABLE     Q  CREATE TABLE public."notificacionMuerteMujerDetalle" (
    "IdMuerteMujerDetalle" integer NOT NULL,
    "muerteDurante" character varying(255) NOT NULL,
    "diasMuerte" integer,
    "tieneFicha" boolean DEFAULT false NOT NULL,
    "IdNotificacionMuerteMujer" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    user_id integer DEFAULT 0 NOT NULL,
    "fechaFallecimiento" date,
    "tienePlanAccion" boolean DEFAULT false NOT NULL,
    "casoFueAtendido" boolean DEFAULT false NOT NULL,
    comentarios character varying(500)
);
 4   DROP TABLE public."notificacionMuerteMujerDetalle";
       public         postgres    false    3            �            1259    511043 7   notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 P   DROP SEQUENCE public."notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq";
       public       postgres    false    3    214            �
           0    0 7   notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq" OWNED BY public."notificacionMuerteMujerDetalle"."IdMuerteMujerDetalle";
            public       postgres    false    213            �            1259    510957 5   notificacionMuerteMujer_IdNotificacionMuerteMujer_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionMuerteMujer_IdNotificacionMuerteMujer_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 N   DROP SEQUENCE public."notificacionMuerteMujer_IdNotificacionMuerteMujer_seq";
       public       postgres    false    204    3            �
           0    0 5   notificacionMuerteMujer_IdNotificacionMuerteMujer_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionMuerteMujer_IdNotificacionMuerteMujer_seq" OWNED BY public."notificacionMuerteMujer"."IdNotificacionMuerteMujer";
            public       postgres    false    203            �            1259    510981    notificacionParto    TABLE     �  CREATE TABLE public."notificacionParto" (
    "IdNotificacionParto" integer NOT NULL,
    "IdAndroid" character varying(255) NOT NULL,
    "idMujerAndroid" character varying(255) NOT NULL,
    "codVigilante" character varying(255) NOT NULL,
    "codEstablecimiento" character varying(255) NOT NULL,
    estado character varying(20),
    nombres character varying(150),
    "primerApellido" character varying(250),
    "segundoApellido" character varying(250),
    "fechaNacimento" date,
    direccion character varying(250),
    telefono character varying(255),
    latitud character varying(255),
    longitud character varying(255),
    edad integer,
    atendido character varying(255),
    "fechaRegistro" timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    cerrado boolean DEFAULT false NOT NULL,
    "atendidoNombre" character varying(255) DEFAULT ''::character varying NOT NULL,
    fecha_cerrado date
);
 '   DROP TABLE public."notificacionParto";
       public         postgres    false    3            �            1259    511064    notificacionPartoDetalle    TABLE     X  CREATE TABLE public."notificacionPartoDetalle" (
    "IdPartoDetalle" integer NOT NULL,
    "IdNotificacionParto" integer NOT NULL,
    partera character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    user_id integer DEFAULT 0 NOT NULL,
    comentarios character varying(500)
);
 .   DROP TABLE public."notificacionPartoDetalle";
       public         postgres    false    3            �            1259    511062 +   notificacionPartoDetalle_IdPartoDetalle_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionPartoDetalle_IdPartoDetalle_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public."notificacionPartoDetalle_IdPartoDetalle_seq";
       public       postgres    false    3    218            �
           0    0 +   notificacionPartoDetalle_IdPartoDetalle_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."notificacionPartoDetalle_IdPartoDetalle_seq" OWNED BY public."notificacionPartoDetalle"."IdPartoDetalle";
            public       postgres    false    217            �            1259    510979 )   notificacionParto_IdNotificacionParto_seq    SEQUENCE     �   CREATE SEQUENCE public."notificacionParto_IdNotificacionParto_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 B   DROP SEQUENCE public."notificacionParto_IdNotificacionParto_seq";
       public       postgres    false    3    208            �
           0    0 )   notificacionParto_IdNotificacionParto_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public."notificacionParto_IdNotificacionParto_seq" OWNED BY public."notificacionParto"."IdNotificacionParto";
            public       postgres    false    207            �            1259    510992    notificaciones    TABLE     S  CREATE TABLE public.notificaciones (
    id integer NOT NULL,
    notificable_type character varying(255) NOT NULL,
    notificable_id integer NOT NULL,
    mensaje character varying(255) NOT NULL,
    "codEstablecimiento" integer NOT NULL,
    tipo character varying(255) NOT NULL,
    prioridad character varying(10) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    mnc_codgo integer,
    dpt_codigo integer,
    "vistoEstablecimiento" boolean DEFAULT false,
    "vistoMunicipio" boolean DEFAULT false,
    "vistoDepartamento" boolean DEFAULT false,
    "vistoNacional" boolean DEFAULT false,
    are_codigo integer DEFAULT 0,
    "vistoRed" boolean DEFAULT false NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    "idVigilante" integer DEFAULT 0,
    "idComunidad" integer DEFAULT 0
);
 "   DROP TABLE public.notificaciones;
       public         postgres    false    3            �            1259    510990    notificaciones_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.notificaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.notificaciones_id_seq;
       public       postgres    false    210    3            �
           0    0    notificaciones_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.notificaciones_id_seq OWNED BY public.notificaciones.id;
            public       postgres    false    209            �            1259    511342    parametrosSeguridad    TABLE     �   CREATE TABLE public."parametrosSeguridad" (
    id integer NOT NULL,
    "Longitud_PWD" integer,
    "Calidad_PWD" integer,
    "Tiempo_PWD" integer
);
 )   DROP TABLE public."parametrosSeguridad";
       public         postgres    false    3            �            1259    511340    parametrosSeguridad_id_seq    SEQUENCE     �   CREATE SEQUENCE public."parametrosSeguridad_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."parametrosSeguridad_id_seq";
       public       postgres    false    230    3            �
           0    0    parametrosSeguridad_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."parametrosSeguridad_id_seq" OWNED BY public."parametrosSeguridad".id;
            public       postgres    false    229            �            1259    510856    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false    3            �            1259    510909    permission_role    TABLE     j   CREATE TABLE public.permission_role (
    permission_id integer NOT NULL,
    role_id integer NOT NULL
);
 #   DROP TABLE public.permission_role;
       public         postgres    false    3            �            1259    510899    permission_user    TABLE     �   CREATE TABLE public.permission_user (
    permission_id integer NOT NULL,
    user_id integer NOT NULL,
    user_type character varying(255) NOT NULL
);
 #   DROP TABLE public.permission_user;
       public         postgres    false    3            �            1259    510878    permissions    TABLE     8  CREATE TABLE public.permissions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    modulo character varying(50)
);
    DROP TABLE public.permissions;
       public         postgres    false    3            �            1259    510876    permissions_id_seq    SEQUENCE     {   CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.permissions_id_seq;
       public       postgres    false    3    193            �
           0    0    permissions_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;
            public       postgres    false    192            �            1259    511364    personal_access_tokens    TABLE     �  CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 *   DROP TABLE public.personal_access_tokens;
       public         postgres    false    3            �            1259    511362    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.personal_access_tokens_id_seq;
       public       postgres    false    234    3            �
           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;
            public       postgres    false    233            �            1259    511224    personas    TABLE     `  CREATE TABLE public.personas (
    "IdPersona" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nombres character varying(100) NOT NULL,
    "primerApellido" character varying(100) NOT NULL,
    "segundoApellido" character varying(100),
    "numeroCarnet" character varying(10) NOT NULL,
    complemento character varying(10),
    nacionalidad character varying(20),
    direccion character varying(250),
    "fechaNacimiento" date NOT NULL,
    sexo character(1) NOT NULL,
    adscrito_id integer DEFAULT 0 NOT NULL,
    verificado boolean,
    "codEstablecimiento" integer DEFAULT 0,
    "IdComunidad" integer DEFAULT 0,
    mnc_codigo integer DEFAULT 0,
    are_codigo integer DEFAULT 0,
    dpt_codigo integer DEFAULT 0,
    email character varying(255),
    celular character varying(255)
);
    DROP TABLE public.personas;
       public         postgres    false    3            �            1259    511222    personas_IdPersona_seq    SEQUENCE     �   CREATE SEQUENCE public."personas_IdPersona_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."personas_IdPersona_seq";
       public       postgres    false    224    3            �
           0    0    personas_IdPersona_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."personas_IdPersona_seq" OWNED BY public.personas."IdPersona";
            public       postgres    false    223            �            1259    510889 	   role_user    TABLE     �   CREATE TABLE public.role_user (
    role_id integer NOT NULL,
    user_id integer NOT NULL,
    user_type character varying(255) NOT NULL
);
    DROP TABLE public.role_user;
       public         postgres    false    3            �            1259    510865    roles    TABLE       CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.roles;
       public         postgres    false    3            �            1259    510863    roles_id_seq    SEQUENCE     u   CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public       postgres    false    3    191            �
           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
            public       postgres    false    190            �            1259    510845    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    nivel character varying(255) NOT NULL,
    idnivel integer,
    estado boolean NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "IdPersona" integer DEFAULT 0 NOT NULL,
    codigo_credencial character varying(7)
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    510843    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    188    3            �
           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    187            �            1259    510926 
   vigilantes    TABLE     �  CREATE TABLE public.vigilantes (
    "IdVigilante" integer NOT NULL,
    longitud character varying(250),
    latitud character varying(250),
    codigo character varying(10) NOT NULL,
    estado boolean NOT NULL,
    activado boolean NOT NULL,
    "fechaActivacion" date,
    "IdUsuario" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "IdPersona" integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.vigilantes;
       public         postgres    false    3            �            1259    510924    vigilantes_IdVigilante_seq    SEQUENCE     �   CREATE SEQUENCE public."vigilantes_IdVigilante_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."vigilantes_IdVigilante_seq";
       public       postgres    false    3    198            �
           0    0    vigilantes_IdVigilante_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."vigilantes_IdVigilante_seq" OWNED BY public.vigilantes."IdVigilante";
            public       postgres    false    197            �	           2604    511326    auditoria id    DEFAULT     l   ALTER TABLE ONLY public.auditoria ALTER COLUMN id SET DEFAULT nextval('public.auditoria_id_seq'::regclass);
 ;   ALTER TABLE public.auditoria ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    225    226    226            �	           2604    511110    comunidades IdComunidad    DEFAULT     �   ALTER TABLE ONLY public.comunidades ALTER COLUMN "IdComunidad" SET DEFAULT nextval('public."comunidades_IdComunidad_seq"'::regclass);
 H   ALTER TABLE public.comunidades ALTER COLUMN "IdComunidad" DROP DEFAULT;
       public       postgres    false    222    221    222            �	           2604    511353    contraseniaUsuario id    DEFAULT     �   ALTER TABLE ONLY public."contraseniaUsuario" ALTER COLUMN id SET DEFAULT nextval('public."contraseniaUsuario_id_seq"'::regclass);
 F   ALTER TABLE public."contraseniaUsuario" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    232    232            �	           2604    511337    evento Id_evento    DEFAULT     x   ALTER TABLE ONLY public.evento ALTER COLUMN "Id_evento" SET DEFAULT nextval('public."evento_Id_evento_seq"'::regclass);
 A   ALTER TABLE public.evento ALTER COLUMN "Id_evento" DROP DEFAULT;
       public       postgres    false    227    228    228            �	           2604    511075    gmaps_geocache id    DEFAULT     v   ALTER TABLE ONLY public.gmaps_geocache ALTER COLUMN id SET DEFAULT nextval('public.gmaps_geocache_id_seq'::regclass);
 @   ALTER TABLE public.gmaps_geocache ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    220    219    220            �	           2604    510840    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    185    186            �	           2604    510940    mujeres IdMujer    DEFAULT     v   ALTER TABLE ONLY public.mujeres ALTER COLUMN "IdMujer" SET DEFAULT nextval('public."mujeres_IdMujer_seq"'::regclass);
 @   ALTER TABLE public.mujeres ALTER COLUMN "IdMujer" DROP DEFAULT;
       public       postgres    false    199    200    200            �	           2604    510951 +   notificacionEmbarazo IdNotificacionEmbarazo    DEFAULT     �   ALTER TABLE ONLY public."notificacionEmbarazo" ALTER COLUMN "IdNotificacionEmbarazo" SET DEFAULT nextval('public."notificacionEmbarazo_IdNotificacionEmbarazo_seq"'::regclass);
 ^   ALTER TABLE public."notificacionEmbarazo" ALTER COLUMN "IdNotificacionEmbarazo" DROP DEFAULT;
       public       postgres    false    201    202    202            �	           2604    511038 9   notificacionEmbarazoDetalle IdNotificacionEmbarazoDetalle    DEFAULT     �   ALTER TABLE ONLY public."notificacionEmbarazoDetalle" ALTER COLUMN "IdNotificacionEmbarazoDetalle" SET DEFAULT nextval('public."notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq"'::regclass);
 l   ALTER TABLE public."notificacionEmbarazoDetalle" ALTER COLUMN "IdNotificacionEmbarazoDetalle" DROP DEFAULT;
       public       postgres    false    212    211    212            �	           2604    510973 /   notificacionMuerteBebe IdNotificacionMuerteBebe    DEFAULT     �   ALTER TABLE ONLY public."notificacionMuerteBebe" ALTER COLUMN "IdNotificacionMuerteBebe" SET DEFAULT nextval('public."notificacionMuerteBebe_IdNotificacionMuerteBebe_seq"'::regclass);
 b   ALTER TABLE public."notificacionMuerteBebe" ALTER COLUMN "IdNotificacionMuerteBebe" DROP DEFAULT;
       public       postgres    false    205    206    206            �	           2604    511057 1   notificacionMuerteBebeDetalle IdMuerteBebeDetalle    DEFAULT     �   ALTER TABLE ONLY public."notificacionMuerteBebeDetalle" ALTER COLUMN "IdMuerteBebeDetalle" SET DEFAULT nextval('public."notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq"'::regclass);
 d   ALTER TABLE public."notificacionMuerteBebeDetalle" ALTER COLUMN "IdMuerteBebeDetalle" DROP DEFAULT;
       public       postgres    false    215    216    216            �	           2604    510962 1   notificacionMuerteMujer IdNotificacionMuerteMujer    DEFAULT     �   ALTER TABLE ONLY public."notificacionMuerteMujer" ALTER COLUMN "IdNotificacionMuerteMujer" SET DEFAULT nextval('public."notificacionMuerteMujer_IdNotificacionMuerteMujer_seq"'::regclass);
 d   ALTER TABLE public."notificacionMuerteMujer" ALTER COLUMN "IdNotificacionMuerteMujer" DROP DEFAULT;
       public       postgres    false    204    203    204            �	           2604    511048 3   notificacionMuerteMujerDetalle IdMuerteMujerDetalle    DEFAULT     �   ALTER TABLE ONLY public."notificacionMuerteMujerDetalle" ALTER COLUMN "IdMuerteMujerDetalle" SET DEFAULT nextval('public."notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq"'::regclass);
 f   ALTER TABLE public."notificacionMuerteMujerDetalle" ALTER COLUMN "IdMuerteMujerDetalle" DROP DEFAULT;
       public       postgres    false    213    214    214            �	           2604    510984 %   notificacionParto IdNotificacionParto    DEFAULT     �   ALTER TABLE ONLY public."notificacionParto" ALTER COLUMN "IdNotificacionParto" SET DEFAULT nextval('public."notificacionParto_IdNotificacionParto_seq"'::regclass);
 X   ALTER TABLE public."notificacionParto" ALTER COLUMN "IdNotificacionParto" DROP DEFAULT;
       public       postgres    false    208    207    208            �	           2604    511067 '   notificacionPartoDetalle IdPartoDetalle    DEFAULT     �   ALTER TABLE ONLY public."notificacionPartoDetalle" ALTER COLUMN "IdPartoDetalle" SET DEFAULT nextval('public."notificacionPartoDetalle_IdPartoDetalle_seq"'::regclass);
 Z   ALTER TABLE public."notificacionPartoDetalle" ALTER COLUMN "IdPartoDetalle" DROP DEFAULT;
       public       postgres    false    218    217    218            �	           2604    510995    notificaciones id    DEFAULT     v   ALTER TABLE ONLY public.notificaciones ALTER COLUMN id SET DEFAULT nextval('public.notificaciones_id_seq'::regclass);
 @   ALTER TABLE public.notificaciones ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    209    210    210            �	           2604    511345    parametrosSeguridad id    DEFAULT     �   ALTER TABLE ONLY public."parametrosSeguridad" ALTER COLUMN id SET DEFAULT nextval('public."parametrosSeguridad_id_seq"'::regclass);
 G   ALTER TABLE public."parametrosSeguridad" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    230    229    230            �	           2604    510881    permissions id    DEFAULT     p   ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);
 =   ALTER TABLE public.permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    192    193    193            �	           2604    511367    personal_access_tokens id    DEFAULT     �   ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);
 H   ALTER TABLE public.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    233    234    234            �	           2604    511227    personas IdPersona    DEFAULT     |   ALTER TABLE ONLY public.personas ALTER COLUMN "IdPersona" SET DEFAULT nextval('public."personas_IdPersona_seq"'::regclass);
 C   ALTER TABLE public.personas ALTER COLUMN "IdPersona" DROP DEFAULT;
       public       postgres    false    223    224    224            �	           2604    510868    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    191    190    191            �	           2604    510848    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    188    187    188            �	           2604    510929    vigilantes IdVigilante    DEFAULT     �   ALTER TABLE ONLY public.vigilantes ALTER COLUMN "IdVigilante" SET DEFAULT nextval('public."vigilantes_IdVigilante_seq"'::regclass);
 G   ALTER TABLE public.vigilantes ALTER COLUMN "IdVigilante" DROP DEFAULT;
       public       postgres    false    197    198    198            �
          0    511323 	   auditoria 
   TABLE DATA               �   COPY public.auditoria (id, "Nombre_tabla", "Id_tabla", "Tipo_usuario", "Fecha_ing", "Hora_ing", "Fecha_sal", "Hora_sal", "IP_terminal", "Evento", "Modulo", "Menu", "Submenu", "Item") FROM stdin;
    public       postgres    false    226   !'      �
           0    0    auditoria_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.auditoria_id_seq', 228, true);
            public       postgres    false    225            �
          0    511107    comunidades 
   TABLE DATA               �   COPY public.comunidades ("IdComunidad", "codLocalidad", "codMunicipio", "nomLocalidad", "nomComunidad", created_at, updated_at) FROM stdin;
    public       postgres    false    222   ,0      �
           0    0    comunidades_IdComunidad_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."comunidades_IdComunidad_seq"', 1, false);
            public       postgres    false    221            �
          0    511350    contraseniaUsuario 
   TABLE DATA               s   COPY public."contraseniaUsuario" (id, user_id, "FechaDeCambio", "Contrasenia", created_at, updated_at) FROM stdin;
    public       postgres    false    232   I0      �
           0    0    contraseniaUsuario_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public."contraseniaUsuario_id_seq"', 11, true);
            public       postgres    false    231            �
          0    511334    evento 
   TABLE DATA               <   COPY public.evento ("Id_evento", "Descripcion") FROM stdin;
    public       postgres    false    228   �2      �
           0    0    evento_Id_evento_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."evento_Id_evento_seq"', 71, true);
            public       postgres    false    227            �
          0    511072    gmaps_geocache 
   TABLE DATA               b   COPY public.gmaps_geocache (id, address, latitude, longitude, created_at, updated_at) FROM stdin;
    public       postgres    false    220   �5      �
           0    0    gmaps_geocache_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.gmaps_geocache_id_seq', 1, false);
            public       postgres    false    219            �
          0    510837 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    186   �5      �
           0    0    migrations_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.migrations_id_seq', 77, true);
            public       postgres    false    185            �
          0    510937    mujeres 
   TABLE DATA               �   COPY public.mujeres ("IdMujer", "IdVigilante", longitud, latitud, estado, "idAndroid", "IdUsuario", created_at, updated_at, "IdPersona") FROM stdin;
    public       postgres    false    200   �:      �
           0    0    mujeres_IdMujer_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."mujeres_IdMujer_seq"', 4, true);
            public       postgres    false    199            �
          0    510948    notificacionEmbarazo 
   TABLE DATA               Y  COPY public."notificacionEmbarazo" ("IdNotificacionEmbarazo", "IdAndroid", "codVigilante", "codEstablecimiento", estado, nombres, "primerApellido", "segundoApellido", "fechaNacimento", direccion, telefono, latitud, longitud, edad, "semanasEmbarazo", "fechaRegistro", created_at, updated_at, "idMujerAndroid", cerrado, fecha_cerrado) FROM stdin;
    public       postgres    false    202   �;      �
          0    511035    notificacionEmbarazoDetalle 
   TABLE DATA               �   COPY public."notificacionEmbarazoDetalle" ("IdNotificacionEmbarazoDetalle", "numeroControlesPrenatal", "IdNotificacionEmbarazo", "planPartoLlenado", created_at, updated_at, user_id, comentarios) FROM stdin;
    public       postgres    false    212   �;      �
           0    0 =   notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq    SEQUENCE SET     n   SELECT pg_catalog.setval('public."notificacionEmbarazoDetalle_IdNotificacionEmbarazoDetalle_seq"', 1, false);
            public       postgres    false    211            �
           0    0 /   notificacionEmbarazo_IdNotificacionEmbarazo_seq    SEQUENCE SET     `   SELECT pg_catalog.setval('public."notificacionEmbarazo_IdNotificacionEmbarazo_seq"', 1, false);
            public       postgres    false    201            �
          0    510970    notificacionMuerteBebe 
   TABLE DATA               R  COPY public."notificacionMuerteBebe" ("IdNotificacionMuerteBebe", "IdAndroid", "idMujerAndroid", "codVigilante", "codEstablecimiento", estado, nombres, "primerApellido", "segundoApellido", "fechaNacimento", direccion, telefono, latitud, longitud, edad, nacido, "fechaRegistro", created_at, updated_at, cerrado, fecha_cerrado) FROM stdin;
    public       postgres    false    206   �;      �
          0    511054    notificacionMuerteBebeDetalle 
   TABLE DATA               �   COPY public."notificacionMuerteBebeDetalle" ("IdMuerteBebeDetalle", nacido, "tieneAutopsiaVerbal", "IdNotificacionMuerteBebe", created_at, updated_at, user_id, fechanacimiento, fechafallecimiento, "tienePlanAccion", comentarios) FROM stdin;
    public       postgres    false    216   <      �
           0    0 5   notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq    SEQUENCE SET     f   SELECT pg_catalog.setval('public."notificacionMuerteBebeDetalle_IdMuerteBebeDetalle_seq"', 1, false);
            public       postgres    false    215            �
           0    0 3   notificacionMuerteBebe_IdNotificacionMuerteBebe_seq    SEQUENCE SET     d   SELECT pg_catalog.setval('public."notificacionMuerteBebe_IdNotificacionMuerteBebe_seq"', 1, false);
            public       postgres    false    205            �
          0    510959    notificacionMuerteMujer 
   TABLE DATA               X  COPY public."notificacionMuerteMujer" ("IdNotificacionMuerteMujer", "IdAndroid", "idMujerAndroid", "codVigilante", "codEstablecimiento", estado, nombres, "primerApellido", "segundoApellido", "fechaNacimento", direccion, telefono, latitud, longitud, edad, embarazada, "fechaRegistro", created_at, updated_at, cerrado, fecha_cerrado) FROM stdin;
    public       postgres    false    204   7<      �
          0    511045    notificacionMuerteMujerDetalle 
   TABLE DATA                 COPY public."notificacionMuerteMujerDetalle" ("IdMuerteMujerDetalle", "muerteDurante", "diasMuerte", "tieneFicha", "IdNotificacionMuerteMujer", created_at, updated_at, user_id, "fechaFallecimiento", "tienePlanAccion", "casoFueAtendido", comentarios) FROM stdin;
    public       postgres    false    214   T<      �
           0    0 7   notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public."notificacionMuerteMujerDetalle_IdMuerteMujerDetalle_seq"', 1, false);
            public       postgres    false    213            �
           0    0 5   notificacionMuerteMujer_IdNotificacionMuerteMujer_seq    SEQUENCE SET     f   SELECT pg_catalog.setval('public."notificacionMuerteMujer_IdNotificacionMuerteMujer_seq"', 1, false);
            public       postgres    false    203            �
          0    510981    notificacionParto 
   TABLE DATA               \  COPY public."notificacionParto" ("IdNotificacionParto", "IdAndroid", "idMujerAndroid", "codVigilante", "codEstablecimiento", estado, nombres, "primerApellido", "segundoApellido", "fechaNacimento", direccion, telefono, latitud, longitud, edad, atendido, "fechaRegistro", created_at, updated_at, cerrado, "atendidoNombre", fecha_cerrado) FROM stdin;
    public       postgres    false    208   q<      �
          0    511064    notificacionPartoDetalle 
   TABLE DATA               �   COPY public."notificacionPartoDetalle" ("IdPartoDetalle", "IdNotificacionParto", partera, created_at, updated_at, user_id, comentarios) FROM stdin;
    public       postgres    false    218   �<      �
           0    0 +   notificacionPartoDetalle_IdPartoDetalle_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public."notificacionPartoDetalle_IdPartoDetalle_seq"', 1, false);
            public       postgres    false    217            �
           0    0 )   notificacionParto_IdNotificacionParto_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public."notificacionParto_IdNotificacionParto_seq"', 1, false);
            public       postgres    false    207            �
          0    510992    notificaciones 
   TABLE DATA               <  COPY public.notificaciones (id, notificable_type, notificable_id, mensaje, "codEstablecimiento", tipo, prioridad, created_at, updated_at, mnc_codgo, dpt_codigo, "vistoEstablecimiento", "vistoMunicipio", "vistoDepartamento", "vistoNacional", are_codigo, "vistoRed", user_id, "idVigilante", "idComunidad") FROM stdin;
    public       postgres    false    210   �<      �
           0    0    notificaciones_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.notificaciones_id_seq', 1, false);
            public       postgres    false    209            �
          0    511342    parametrosSeguridad 
   TABLE DATA               `   COPY public."parametrosSeguridad" (id, "Longitud_PWD", "Calidad_PWD", "Tiempo_PWD") FROM stdin;
    public       postgres    false    230   �<      �
           0    0    parametrosSeguridad_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public."parametrosSeguridad_id_seq"', 1, true);
            public       postgres    false    229            �
          0    510856    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    189   �<      �
          0    510909    permission_role 
   TABLE DATA               A   COPY public.permission_role (permission_id, role_id) FROM stdin;
    public       postgres    false    196   =      �
          0    510899    permission_user 
   TABLE DATA               L   COPY public.permission_user (permission_id, user_id, user_type) FROM stdin;
    public       postgres    false    195   Q=      �
          0    510878    permissions 
   TABLE DATA               j   COPY public.permissions (id, name, display_name, description, created_at, updated_at, modulo) FROM stdin;
    public       postgres    false    193   n=      �
           0    0    permissions_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.permissions_id_seq', 17, true);
            public       postgres    false    192            �
          0    511364    personal_access_tokens 
   TABLE DATA               �   COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
    public       postgres    false    234   	?      �
           0    0    personal_access_tokens_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 10, true);
            public       postgres    false    233            �
          0    511224    personas 
   TABLE DATA               6  COPY public.personas ("IdPersona", created_at, updated_at, nombres, "primerApellido", "segundoApellido", "numeroCarnet", complemento, nacionalidad, direccion, "fechaNacimiento", sexo, adscrito_id, verificado, "codEstablecimiento", "IdComunidad", mnc_codigo, are_codigo, dpt_codigo, email, celular) FROM stdin;
    public       postgres    false    224   fA      �
           0    0    personas_IdPersona_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."personas_IdPersona_seq"', 21, true);
            public       postgres    false    223            �
          0    510889 	   role_user 
   TABLE DATA               @   COPY public.role_user (role_id, user_id, user_type) FROM stdin;
    public       postgres    false    194   �D      �
          0    510865    roles 
   TABLE DATA               \   COPY public.roles (id, name, display_name, description, created_at, updated_at) FROM stdin;
    public       postgres    false    191   �D      �
           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 1, true);
            public       postgres    false    190            �
          0    510845    users 
   TABLE DATA               �   COPY public.users (id, username, password, nivel, idnivel, estado, remember_token, created_at, updated_at, "IdPersona", codigo_credencial) FROM stdin;
    public       postgres    false    188   3E      �
           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 17, true);
            public       postgres    false    187            �
          0    510926 
   vigilantes 
   TABLE DATA               �   COPY public.vigilantes ("IdVigilante", longitud, latitud, codigo, estado, activado, "fechaActivacion", "IdUsuario", created_at, updated_at, "IdPersona") FROM stdin;
    public       postgres    false    198   *H      �
           0    0    vigilantes_IdVigilante_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."vigilantes_IdVigilante_seq"', 1, false);
            public       postgres    false    197            
           2606    511331    auditoria auditoria_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auditoria DROP CONSTRAINT auditoria_pkey;
       public         postgres    false    226    226            
           2606    511115    comunidades comunidades_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.comunidades
    ADD CONSTRAINT comunidades_pkey PRIMARY KEY ("IdComunidad");
 F   ALTER TABLE ONLY public.comunidades DROP CONSTRAINT comunidades_pkey;
       public         postgres    false    222    222            
           2606    511355 *   contraseniaUsuario contraseniaUsuario_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public."contraseniaUsuario"
    ADD CONSTRAINT "contraseniaUsuario_pkey" PRIMARY KEY (id);
 X   ALTER TABLE ONLY public."contraseniaUsuario" DROP CONSTRAINT "contraseniaUsuario_pkey";
       public         postgres    false    232    232            

           2606    511339    evento evento_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY ("Id_evento");
 <   ALTER TABLE ONLY public.evento DROP CONSTRAINT evento_pkey;
       public         postgres    false    228    228             
           2606    511080 "   gmaps_geocache gmaps_geocache_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.gmaps_geocache
    ADD CONSTRAINT gmaps_geocache_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.gmaps_geocache DROP CONSTRAINT gmaps_geocache_pkey;
       public         postgres    false    220    220            �	           2606    511289 +   notificacionMuerteBebe idandroid_mmb_unique 
   CONSTRAINT     o   ALTER TABLE ONLY public."notificacionMuerteBebe"
    ADD CONSTRAINT idandroid_mmb_unique UNIQUE ("IdAndroid");
 W   ALTER TABLE ONLY public."notificacionMuerteBebe" DROP CONSTRAINT idandroid_mmb_unique;
       public         postgres    false    206    206            �	           2606    511283 )   notificacionEmbarazo idandroid_nme_unique 
   CONSTRAINT     m   ALTER TABLE ONLY public."notificacionEmbarazo"
    ADD CONSTRAINT idandroid_nme_unique UNIQUE ("IdAndroid");
 U   ALTER TABLE ONLY public."notificacionEmbarazo" DROP CONSTRAINT idandroid_nme_unique;
       public         postgres    false    202    202            �	           2606    511287 ,   notificacionMuerteMujer idandroid_nmm_unique 
   CONSTRAINT     p   ALTER TABLE ONLY public."notificacionMuerteMujer"
    ADD CONSTRAINT idandroid_nmm_unique UNIQUE ("IdAndroid");
 X   ALTER TABLE ONLY public."notificacionMuerteMujer" DROP CONSTRAINT idandroid_nmm_unique;
       public         postgres    false    204    204            �	           2606    511291 %   notificacionParto idandroid_np_unique 
   CONSTRAINT     i   ALTER TABLE ONLY public."notificacionParto"
    ADD CONSTRAINT idandroid_np_unique UNIQUE ("IdAndroid");
 Q   ALTER TABLE ONLY public."notificacionParto" DROP CONSTRAINT idandroid_np_unique;
       public         postgres    false    208    208            �	           2606    510842    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    186    186            �	           2606    511285     mujeres mujeres_idandroid_unique 
   CONSTRAINT     b   ALTER TABLE ONLY public.mujeres
    ADD CONSTRAINT mujeres_idandroid_unique UNIQUE ("idAndroid");
 J   ALTER TABLE ONLY public.mujeres DROP CONSTRAINT mujeres_idandroid_unique;
       public         postgres    false    200    200            �	           2606    510945    mujeres mujeres_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.mujeres
    ADD CONSTRAINT mujeres_pkey PRIMARY KEY ("IdMujer");
 >   ALTER TABLE ONLY public.mujeres DROP CONSTRAINT mujeres_pkey;
       public         postgres    false    200    200            �	           2606    511042 <   notificacionEmbarazoDetalle notificacionEmbarazoDetalle_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionEmbarazoDetalle"
    ADD CONSTRAINT "notificacionEmbarazoDetalle_pkey" PRIMARY KEY ("IdNotificacionEmbarazoDetalle");
 j   ALTER TABLE ONLY public."notificacionEmbarazoDetalle" DROP CONSTRAINT "notificacionEmbarazoDetalle_pkey";
       public         postgres    false    212    212            �	           2606    510956 .   notificacionEmbarazo notificacionEmbarazo_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionEmbarazo"
    ADD CONSTRAINT "notificacionEmbarazo_pkey" PRIMARY KEY ("IdNotificacionEmbarazo");
 \   ALTER TABLE ONLY public."notificacionEmbarazo" DROP CONSTRAINT "notificacionEmbarazo_pkey";
       public         postgres    false    202    202            �	           2606    511061 @   notificacionMuerteBebeDetalle notificacionMuerteBebeDetalle_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionMuerteBebeDetalle"
    ADD CONSTRAINT "notificacionMuerteBebeDetalle_pkey" PRIMARY KEY ("IdMuerteBebeDetalle");
 n   ALTER TABLE ONLY public."notificacionMuerteBebeDetalle" DROP CONSTRAINT "notificacionMuerteBebeDetalle_pkey";
       public         postgres    false    216    216            �	           2606    510978 2   notificacionMuerteBebe notificacionMuerteBebe_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionMuerteBebe"
    ADD CONSTRAINT "notificacionMuerteBebe_pkey" PRIMARY KEY ("IdNotificacionMuerteBebe");
 `   ALTER TABLE ONLY public."notificacionMuerteBebe" DROP CONSTRAINT "notificacionMuerteBebe_pkey";
       public         postgres    false    206    206            �	           2606    511051 B   notificacionMuerteMujerDetalle notificacionMuerteMujerDetalle_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionMuerteMujerDetalle"
    ADD CONSTRAINT "notificacionMuerteMujerDetalle_pkey" PRIMARY KEY ("IdMuerteMujerDetalle");
 p   ALTER TABLE ONLY public."notificacionMuerteMujerDetalle" DROP CONSTRAINT "notificacionMuerteMujerDetalle_pkey";
       public         postgres    false    214    214            �	           2606    510967 4   notificacionMuerteMujer notificacionMuerteMujer_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionMuerteMujer"
    ADD CONSTRAINT "notificacionMuerteMujer_pkey" PRIMARY KEY ("IdNotificacionMuerteMujer");
 b   ALTER TABLE ONLY public."notificacionMuerteMujer" DROP CONSTRAINT "notificacionMuerteMujer_pkey";
       public         postgres    false    204    204            �	           2606    511069 6   notificacionPartoDetalle notificacionPartoDetalle_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."notificacionPartoDetalle"
    ADD CONSTRAINT "notificacionPartoDetalle_pkey" PRIMARY KEY ("IdPartoDetalle");
 d   ALTER TABLE ONLY public."notificacionPartoDetalle" DROP CONSTRAINT "notificacionPartoDetalle_pkey";
       public         postgres    false    218    218            �	           2606    510989 (   notificacionParto notificacionParto_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public."notificacionParto"
    ADD CONSTRAINT "notificacionParto_pkey" PRIMARY KEY ("IdNotificacionParto");
 V   ALTER TABLE ONLY public."notificacionParto" DROP CONSTRAINT "notificacionParto_pkey";
       public         postgres    false    208    208            �	           2606    511000 "   notificaciones notificaciones_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.notificaciones
    ADD CONSTRAINT notificaciones_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.notificaciones DROP CONSTRAINT notificaciones_pkey;
       public         postgres    false    210    210            
           2606    511347 ,   parametrosSeguridad parametrosSeguridad_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public."parametrosSeguridad"
    ADD CONSTRAINT "parametrosSeguridad_pkey" PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public."parametrosSeguridad" DROP CONSTRAINT "parametrosSeguridad_pkey";
       public         postgres    false    230    230            �	           2606    510923 $   permission_role permission_role_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);
 N   ALTER TABLE ONLY public.permission_role DROP CONSTRAINT permission_role_pkey;
       public         postgres    false    196    196    196            �	           2606    510908 $   permission_user permission_user_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.permission_user
    ADD CONSTRAINT permission_user_pkey PRIMARY KEY (user_id, permission_id, user_type);
 N   ALTER TABLE ONLY public.permission_user DROP CONSTRAINT permission_user_pkey;
       public         postgres    false    195    195    195    195            �	           2606    510888 #   permissions permissions_name_unique 
   CONSTRAINT     ^   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_unique UNIQUE (name);
 M   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_name_unique;
       public         postgres    false    193    193            �	           2606    510886    permissions permissions_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_pkey;
       public         postgres    false    193    193            
           2606    511372 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       public         postgres    false    234    234            
           2606    511375 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     v   ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 d   ALTER TABLE ONLY public.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       public         postgres    false    234    234            
           2606    511234 %   personas personas_numerocarnet_unique 
   CONSTRAINT     j   ALTER TABLE ONLY public.personas
    ADD CONSTRAINT personas_numerocarnet_unique UNIQUE ("numeroCarnet");
 O   ALTER TABLE ONLY public.personas DROP CONSTRAINT personas_numerocarnet_unique;
       public         postgres    false    224    224            
           2606    511232    personas personas_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.personas
    ADD CONSTRAINT personas_pkey PRIMARY KEY ("IdPersona");
 @   ALTER TABLE ONLY public.personas DROP CONSTRAINT personas_pkey;
       public         postgres    false    224    224            �	           2606    510898    role_user role_user_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (user_id, role_id, user_type);
 B   ALTER TABLE ONLY public.role_user DROP CONSTRAINT role_user_pkey;
       public         postgres    false    194    194    194    194            �	           2606    510875    roles roles_name_unique 
   CONSTRAINT     R   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);
 A   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_name_unique;
       public         postgres    false    191    191            �	           2606    510873    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         postgres    false    191    191            �	           2606    510853    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    188    188            �	           2606    510855    users users_username_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);
 E   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_unique;
       public         postgres    false    188    188            �	           2606    510934    vigilantes vigilantes_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.vigilantes
    ADD CONSTRAINT vigilantes_pkey PRIMARY KEY ("IdVigilante");
 D   ALTER TABLE ONLY public.vigilantes DROP CONSTRAINT vigilantes_pkey;
       public         postgres    false    198    198            �	           1259    511293    are_codigo_idx    INDEX     O   CREATE INDEX are_codigo_idx ON public.notificaciones USING btree (are_codigo);
 "   DROP INDEX public.are_codigo_idx;
       public         postgres    false    210            �	           1259    511297    codvigilante_idx    INDEX     ]   CREATE INDEX codvigilante_idx ON public."notificacionEmbarazo" USING btree ("codVigilante");
 $   DROP INDEX public.codvigilante_idx;
       public         postgres    false    202            �	           1259    511301    codvigilante_nmb_idx    INDEX     c   CREATE INDEX codvigilante_nmb_idx ON public."notificacionMuerteBebe" USING btree ("codVigilante");
 (   DROP INDEX public.codvigilante_nmb_idx;
       public         postgres    false    206            �	           1259    511299    codvigilante_nmm_idx    INDEX     d   CREATE INDEX codvigilante_nmm_idx ON public."notificacionMuerteMujer" USING btree ("codVigilante");
 (   DROP INDEX public.codvigilante_nmm_idx;
       public         postgres    false    204            �	           1259    511303    codvigilante_np_idx    INDEX     ]   CREATE INDEX codvigilante_np_idx ON public."notificacionParto" USING btree ("codVigilante");
 '   DROP INDEX public.codvigilante_np_idx;
       public         postgres    false    208            �	           1259    511294    dpt_codigo_idx    INDEX     O   CREATE INDEX dpt_codigo_idx ON public.notificaciones USING btree (dpt_codigo);
 "   DROP INDEX public.dpt_codigo_idx;
       public         postgres    false    210            �	           1259    511296    idandroid_idx    INDEX     W   CREATE INDEX idandroid_idx ON public."notificacionEmbarazo" USING btree ("IdAndroid");
 !   DROP INDEX public.idandroid_idx;
       public         postgres    false    202            �	           1259    511300    idandroid_nmb_idx    INDEX     ]   CREATE INDEX idandroid_nmb_idx ON public."notificacionMuerteBebe" USING btree ("IdAndroid");
 %   DROP INDEX public.idandroid_nmb_idx;
       public         postgres    false    206            �	           1259    511298    idandroid_nmm_idx    INDEX     ^   CREATE INDEX idandroid_nmm_idx ON public."notificacionMuerteMujer" USING btree ("IdAndroid");
 %   DROP INDEX public.idandroid_nmm_idx;
       public         postgres    false    204            �	           1259    511302    idandroid_np_idx    INDEX     W   CREATE INDEX idandroid_np_idx ON public."notificacionParto" USING btree ("IdAndroid");
 $   DROP INDEX public.idandroid_np_idx;
       public         postgres    false    208            �	           1259    511292    mnc_codigo_idx    INDEX     N   CREATE INDEX mnc_codigo_idx ON public.notificaciones USING btree (mnc_codgo);
 "   DROP INDEX public.mnc_codigo_idx;
       public         postgres    false    210            �	           1259    511295    notificable_type_idx    INDEX     [   CREATE INDEX notificable_type_idx ON public.notificaciones USING btree (notificable_type);
 (   DROP INDEX public.notificable_type_idx;
       public         postgres    false    210            �	           1259    511173 ?   notificaciones_codestablecimiento_mnc_codgo_dpt_codigo_are_codi    INDEX     �   CREATE INDEX notificaciones_codestablecimiento_mnc_codgo_dpt_codigo_are_codi ON public.notificaciones USING btree ("codEstablecimiento", mnc_codgo, dpt_codigo, are_codigo, notificable_id);
 S   DROP INDEX public.notificaciones_codestablecimiento_mnc_codgo_dpt_codigo_are_codi;
       public         postgres    false    210    210    210    210    210            �	           1259    510862    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    189            
           1259    511373 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 L   DROP INDEX public.personal_access_tokens_tokenable_type_tokenable_id_index;
       public         postgres    false    234    234            
           2606    511356 5   contraseniaUsuario contraseniausuario_user_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public."contraseniaUsuario"
    ADD CONSTRAINT contraseniausuario_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;
 a   ALTER TABLE ONLY public."contraseniaUsuario" DROP CONSTRAINT contraseniausuario_user_id_foreign;
       public       postgres    false    188    2496    232            
           2606    510912 5   permission_role permission_role_permission_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.permission_role DROP CONSTRAINT permission_role_permission_id_foreign;
       public       postgres    false    193    196    2507            
           2606    510917 /   permission_role permission_role_role_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.permission_role DROP CONSTRAINT permission_role_role_id_foreign;
       public       postgres    false    191    196    2503            
           2606    510902 5   permission_user permission_user_permission_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.permission_user
    ADD CONSTRAINT permission_user_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.permission_user DROP CONSTRAINT permission_user_permission_id_foreign;
       public       postgres    false    2507    193    195            
           2606    510892 #   role_user role_user_role_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.role_user DROP CONSTRAINT role_user_role_id_foreign;
       public       postgres    false    191    194    2503            �
   �  x��\k��8�m�"��z�u�=A���d.�(�(�{��$GI#����`��L���Q�%��]v�K!�a{���d��j�T��d/�^W�׿�!��
��o��w����K�M�q�>�пJ5�XG���ۏ�yw	���u�lw����6��u�k|���
O_���J�ZP���L��G�������������rڏ�a{
!��߆��߰.�q�;^����;�-p�w~�p;����V6s�8���x��.7�῵���/e�[ҽ̍n7gA�O���f]2��X��H����������R�EI�}��/J��J��,���@B���YpI~��8{��x�a7����-�re(�+&Wǩ5e�jx��Ǖ	��qɫmN�Jvy|�����P�WJIV UT��� �~M�����!I�9H����$�����-{��kp)��Dқq�S+�$��ڮܞK�5�n��\d	&V���,%��&.�IȊ/v���J	l��WK*��ʈ�[ZFWFl���~�+�RO���h�L��QHK�����"y2�J�,�VzaX8Vzq�Ӝ�X鐑3MB{�9Nn׋���8�z)1�@r<��K�5���;D�|
�� �|Ԯ1��rGWMr�É�Z:|�$�<��M�� �������{;�9��7[xu;\O�R���L/��lo��/B���$��iA�Ð�t����;��Sm���F<5x�$+�{<[���$�.��5�*�h8�� �霼1M-8�g����������Ƭ��˂�y���3"�0�0o�����tG��Eg8i�Ѿ3�f8�����
�1ԁK�z��s|��^�=\28�W ��d;��d-��ZPf��������Ղ����dp����&2��Z��d��c($�����7Cx�%K��ԛ�N�O�5=�v�Nt����p��Я��ǥ�6��v˅��'~�*.c�j�Hi��e����a>9x�� 8Ҟ�2��\�&�Ȳw��؞&�Gcŕ>gQ"ej����
; &Ǌ�N�ml��-�󎧈�ׯr�秩z�l�����	�H�K����˓=���!}}�Eů(9�������ơ#�t�gFa3�t�g2���uQq)��U�v�9%MT\"9�X��.����닚���=��&}���|@�HS�6���5-YKM߶{�
��Kǌ��h�h\}��h���83TDo �W�u�U"K���w*J�d�˂q������ d�Y�~^�][!��xK_�<�`�,�����@R�UE����'�}�h��JA���R'��x~~p�:*�!s���0�V$ZS�<�'/
S4�E����t5)h-B�FI�s�V#xl�t1v�b�	�0Y�q���R��`/^ɴ0���Z�01U�'@�e�	�C���<U�H�	iN�HU��f��Z�O�ʃz���<d������wv�c�km�w�]�˰�N|��a(���~ F(zx��&�䛨[��]<�}3�s��J<,����a�Ņ�! ����h��� pxs�X�o�]�%���O�I1�G�c���bF�&��I'�|R�0a��"�據�^Z�Hr��AJ&��f��T<�7	R����Ub�HY��Vv��	Oi�^Z�H>Ck�ؔ����$q��;���fH�:L�bo�I�h���k�s&H�5�����6������0^���N�5�#3� ����KA*&�zI��T�B��7��$u=������&%
��%��\5M�e� E
����̘��`�5�"�z�W�G*8v��:Gk�y9/H�GW�s�؂�-xt��)�w��N�����^�.3�/HUĄ����d�y��s���d��:=2<��h��d�96Y���Y�}R�2���{����sG��qB�O:�Hɾs$��H>��TULY�n�YKMj-�8g'�z��5�Hyį���]R��1F��w\tjEdD����+�O����`2+�^���<���:���a\�٠ObOY�>�㡈�����o�}a."��ύ@�K�M_-�KRf�w���㕭h8���g��̬���.5g1�9�&%A,t6e�C�C< ��H."34�Q���`y����p4�a�t�.t�f�C�C'�����M����x �i��H`9��'D�;4"�O�1���S#X����Q�$E9�����<�E$��L9��.�ȱFZ{�CZ(.ܦ��R�1�^u�[�򕀉}���[�%�	�+>�i:��B��9��'����V������$      �
      x������ � �      �
   E  x�m�ɒ�@�5~Go�d(KY*��ʠ��BP����_/�""�'n�M�����ov��B��8��q��5[�Y��K�+c܄%�)�������L�(�����B��Y!b~�ߙ�0!h���]�ŀ�Ry�\�Ag��� ���0M�C�t�9��Q8!�[�e��fx�oիOD���Wd`���>��^%��j��v�(Bf1!�ȩ�Pi`����� ��IN�"�I�kp�Bں2��w�ixC�'.4!�l}hb��u���i1PyG涆�n���M�fkW���#z?d��n��ޜu ެj��`�&��q��A����t�����a,��[��\2���2��wy�ƾIV�z��p�gQT�jmB�J�Ë���cFr5M�J�Z?���!��N���M�?���cZmn=�R�s��Z[��ɗv����ʥ;��y�9�ÓW��J�.��g���mq��~�p�Hr,�M��\U^�48������{�P�}W�'�oT�Ǐ��{��J���U>՟�1���y47q�TJ����
�J
!�A�tM$WPM�t���r"@�E���?`6����%      �
   -  x��Wˎ�0<����3�:�GZi�{1�D�5vd ���oc�r��͞�	U�զ�H����Ą�5-�	��1'�LDK8\T3�J&�jF�ZѳT-ť��"��j{<��O
(�PQѲ#+IE|a'ơIϬ;�Z��D��n�Q���G
���T
b���#J��?�
��Bw�8��SN����l��ל��n��aMZD��:,m����mV�A%��Nkw��0��~a�Z<���;��j����k�;��.���`��ģmm�޺������r���j; ��|���_3�&EM��4.���̰���gEy�F���M�q��Ekk�4����WЂ�h����������":�Q���g�O�hX֮�h���Y����U�"����g�����(�d5��]��]p�Ю�Ò���r��J.�'��V�8�ȳ���C>JUw�8���#x�d�	���\t�⤥�G2����'f�!��Nk��V�@Fj�r�%;�\ۛ(	R(O�oD�XE���^|�;O���%�$�"���[���>�.��{-����ǷhSp&��&�]�X��]�PL�O��*s����K�?���S�o����w������
��9ژzK�������,��/�M��o����&C�υ=I`��DX3��u��
{�	��fQ��J��㕘��~*��Ŗo��ߨ1��$�E8S��,��"�I�28����>Q��*.���ǝ��w�f�@ׇ^��� 孏�,���?�N�w6�a�TR�o=cK9=J�ІM^"����!�\�P      �
      x������ � �      �
   �  x����n�6��S�.�]
�D;lu%m�<}g$���	18ߐ�Κ~c�
M��L���n�5���b��W���o��BZg�,�L���.vMG�֤�TjZSNj�oV�-��6��"�+M�h8Ѧ��0u[?�uҳ��[7���I_k���~���f\mڊJj��\���_�g��"�L��qZ݇kM{�vx����"XG�͸���O�]w,����	>}�Iq(�!�������#LI��B�T�E��w�`��Tp���_ӆ)K� ���)˒L�7X7�W{�f�h��}Nqa�`�n^_2�dx���<�'�˾��v\�pU�
���6���.$�T&����z��j�A���Ւ��iJFR����u��q�c��j!8�68�2��Yf,>��W�����j�>���<�\A����OQ������/��DA8g�>�a�'<��"aP��,b2>3/��N�{���H��B�0�ꋏcΫS>������`8U�_�)�D [�"����N��tYs����k!cË@���a�?t7R�>6��������D�\�'����D��s�e1�%�_�!�[B�!xC���ِ>=C����/xKf�9��+��i?�UD�`���~7�	�n���o*C��?TQ�k7[e���ˊ֡k��0BF����CI�4����3�̴{oS!h��{����jvQK�Ju29�y��o��G�����>�Z����#����_���饰u��� �}�fnonO�Ƭ�#-H�@8��D�����{�M��`�@�x	;Y^��� �0��A�L��c@_�ѯ�a�w,O�(M��$��Ya���-�2I�c���N�5ԲR+�d��c�F�_:�Q4K)"�o�-�h]�[Z��\4@���,��5���#0�<���M��tsv���a�q`����VR!�*�%�К4�������Ց�wIP��e�2M�m�8~0�8�$�0�3ɐ���:;���Ȝ�^��?�C���������rN٭�pU�-�/��z"sߗ�K�k�06�F�P9J^BTR�:��!�"�_9�H�����*��M]�YsQu���2S_2�\Ċ�'q8rE.ڊ�]�<[l�Ϛ<Ƹ�׷f�4�sݮV8�cZN�v<�m����_�9
'V�FΎd�*ڻԓ*���8(U<~i�����R���W#*R��$v��!(���OjU2B(�i�Wo;∱-�q�H���[9�v��� 3�m�eo�������K�      �
   �   x�m��m!���P�6���6`SK.�@��~F�"ewG���������c���&�
2���%`F�,�aa�Jl��K��6�$O� ��8�p'+>(f8��9�X����}�6�&z��I��@���yYt\�;��\]�	�x��I�m�dO~��6ʊNv�P�kR]ݗ�>�ț�������>?RJ?�Qf      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x�3�4�4�463�����       �
      x������ � �      �
   5   x�ʹ ! ���\����;XE�C՚u6mٶc��$+y�Ln�������
.      �
      x������ � �      �
   �  x����r� ��������Ӻ�����Ug���Ё��_�@��Q!;��|w^B�H��1W9�L���
�eQ0
f� BA4�y�j�<� %,k;Vuc�^m�;t����?!'|��?�愢BD�Eo�ؤ#E
$=3�I��+�
w�ݽ�+�<'6���r�gyK�CM�IHiP���²mد�af:�o!3����J��`���jc�AK��3ٜ����+�XOT�������|1.2���0��ϲ\����J��a 0��,J���W���f���!Bj�]����Pvd��ag�b�թ~�J�5���o������]��fGoa��L1�^����x���͐i&�Z�����NL���+����Fn�M��c��&̪8�;�ڴW�%}>A:�JN      �
   M  x�}��n1��ݧ�R"��oGAK�DE(|��"����3?B(D����e:�ظ�{|������7����O�~��m��P�T�bDm���҄}����_޹� �2`�5�Ϸon�l�6�;�w�7���Ap��|b#��-�衔0sgWWV��ŜTc�.}M�X����L��d���H� �Npn> ��H�N#�j�a r`�NI�������ʯ)�H�N��rN�mڪ�,��,Qi�����PG+���M�]��1�ұUQ��-��@9�v=�ڳ 5�eEkըD�Ć��"��MS��4�Z���e�s����kG~k{=ieN�is�s:3ć���[�A�2�Q��Y��[�4g��yV�!>��3m�+�pǦ>���&�i��K��>FKb7����E��
b��+ �����LAr��Pg�0"����v�Cnj�qh)!�(U^���;�����ٺ��$��������Ӗ�J��e�F*�k�����8��"�Q0�����$�x�8���za���8��i{zw�E{G��hQ^ы-�y�6K�,L��-�h������1{�mWY2�r���o�}��^      �
   ,  x�u��n�@Ư�����`�:Uڲ����f��A͊m���{@ia�D<������(��
{W���*�c����(���4��˿�)��.$���ݦq��F�o�M	JF'������vg���8�G�'��;�����E�dm���*E��*
�,\�G�IzA�-J}A9�bo��#�S�</�y_p9�ū�W�À�y�p��`����p�8Ewq��,�{����M��3��� LYiL�qF��P\(�O��|��B/f�%a6��^1��^$�ig�D��ӌe�vO_o���U�_i�P�e� 9	�3�j�AFy!�bRq<Y[D�7��(N���Ι���Rc��ȱ��[5�([��<��H=�0<��c�<��`
W�E�T[[��9���B5Є�鲍,u#����D6�wN�A�<	�;
?}���C3n�4
���j��g-I�UGe��d�H�b��R����1�q��X����@w�^^�9�°&��=:��	a�C�W�M�u�Ͳ|m��fC�?�x/^�W���=��_ǲ��D�+�p�upX���.�AE�dm�N4���<�bpURO
.Fz��K�-ڠ~ak]A7C[{6�b�a�m�εU��E \�X�Lg�Q
F��	o���xx3�˱ܟJ׸�q�-l-U�p���R^�8�ga9�����e0%v��YS� C-	z�˟yh�K��/۟V������~O�6�q��A�[�m�q�G(p=K:��o5�����\H��HJѽe`�`t<�M��i��T�Lӣ<_;����Ȓ      �
   7   x�3�4�t,(��	-N-�2�4B���LPx�(<3�!�FCT��h�͑�1z\\\ ,t))      �
   :   x�3�LL����tt����	rt�B����)Y[c����� ?
      �
   �  x���˒�JE�Y_у�6�/P�)������	B*(OQ��ֺQ}���tEF䊽�9j��րW|E�u��,��<]Mx��B7À�n|/��3ޟ�ц�	N���U�� �8��@P�_�ß���+�(��2�x��A���iN�&�q���{+B��B�ᰊ�[�2�}��ect����;	���r��b@�����v�4 @=\���ό8�R���QOe���b��"i.�l�⵰ZY�.ڗ�����������3��J�
�ZyJP���/���"���nN�C���0>D�j#-�Mu͍/�{��fk�������@D�6�(Hle�� �)�� ��
Yr�7�׵�`3�L�1����fa�􄱻���V3�QMnS9����%�>���7$*TTH���O	�_<��ס�A7�Y�<o�6do+�șt�9�ݏܝ}]�c�<�eֽ������N+��� �s)o�-�&=8=_8��";��Ns�u��ܚ�u���eJCb��f�~xE�{/��I������x4�,���ͲT=�`�Ʊ�9�eJ�[���F�'qqT�E�>����@0鴉P�`��u�,���cZǇK��+��tK���q.��F��vd��8�V3׫'��8�=2o�B��r����C�A����!y�G(���� �a;?I�?���*�᚛���C����l�v��|�n�Ėg��З�@��X3���Ӆ��,`I!b;{�]��������jQ      �
      x������ � �     