<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mujer;
use Faker\Generator as Faker;

$factory->define(Mujer::class, function (Faker $faker) {
   $persona=factory(\App\Persona::class)->create();
   $user=\App\User::all()->random(1)->first();
   return [
        "IdVigilante"=>0,
        "longitud"=>$faker->longitude,
        "latitud"=>$faker->latitude,
        "estado"=>true,
        "idAndroid"=>$faker->uuid,
        "IdUsuario"=>$user->getKey(),
        "IdPersona"=>$persona->getKey(),
    ];
});
