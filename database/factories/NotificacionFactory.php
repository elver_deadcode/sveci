<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Notificacion;
use Faker\Generator as Faker;

$factory->define(
    Notificacion::class,
    function (Faker $faker) {

        $establecimiento  = \App\Establecimiento::all()->random(1)->first();
        $municipio        = $establecimiento->municipio;
        $depto            = $municipio->departamento();

        $tipo        = $faker->randomElement(
            ['embarazo', 'muertemujer', 'teleconsulta']
        );
        $notificable = null;
        switch ($tipo) {
            case 'embarazo':
                $notificable = factory(
                    \App\NotificacionEmbarazo::class
                )->create(
                    [
                        'codEstablecimiento' => $establecimiento->codestabl
                    ]
                );
                break;
            case     'muertemujer':
                $notificable = factory(
                    \App\NotificacionMuerteMujer::class
                )->create(
                    [
                        'codEstablecimiento' => $establecimiento->codestabl
                    ]
                );
                break;
            case 'teleconsulta':
                $notificable = factory(
                    \App\NotificacionTeleconsulta::class
                )->create(
                    [
                        'codEstablecimiento' => $establecimiento->codestabl
                    ]
                );

                break;
        }


        return [
            'notificable_type'     => $notificable->getMorphClass(),
            'notificable_id'       => $notificable->getKey(),
            'mensaje'              => "Se registro una notificacion de {$tipo} en el sistema",
            'codEstablecimiento'   => $establecimiento->codestabl,
            'tipo'                 => 'teleconsultas',
            'prioridad'            => 'normal',
            'mnc_codgo'            => $municipio->mnc_codigo,
            'dpt_codigo'           => $depto->dpt_codigo,
            'vistoEstablecimiento' => 'f',
            'vistoMunicipio'       => 'f',
            'vistoDepartamento'    => 'f',
            'vistoNacional'        => 'f',
            'are_codigo'           => $establecimiento->codarea,
            'vistoRed'             => 'f',
            'user_id'              => 0,
            'idVigilante'          => 0,
            'idComunidad'          => 0

        ];
    }
);
