<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\NotificacionMuerteMujer;
use Faker\Generator as Faker;

$factory->define(NotificacionMuerteMujer::class, function (Faker $faker) {
    $vigilante=\App\Vigilante::all()->random(1)->first();
    if(null==$vigilante){
        $vigilante=factory(\App\Vigilante::class)->create();
    }
    $cerrado=$faker->randomElement(['true','false']);
    $fechaCerrado=null;
    if($cerrado=='true'){

        $fechaCerrado=$faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now')->format('Y-m-d');
    }
    $fecha_nac=$faker->dateTimeBetween($startDate = '-55 years', $endDate = 'now')
                     ->format('Y-m-d');

    return [
        "IdAndroid"=>$faker->uuid,
        "idMujerAndroid"=>$faker->uuid,
        "codVigilante"=>$vigilante->codigo,
        "codEstablecimiento"=>$vigilante->persona->codEstablecimiento,
        "estado"=>'enviado',
        "nombres"=>$faker->name,
        "primerApellido"=>$faker->lastName,
        "segundoApellido"=>$faker->lastName,
        "fechaNacimento"=>$fecha_nac,
        "direccion"=>$faker->address,
        "telefono"=>$faker->phoneNumber,
        "latitud"=>$faker->latitude,
        "longitud"=>$faker->longitude,
        "edad"=>$faker->randomNumber(2),
        "embarazada"=>$faker->randomElement(['true','false']),
        "fechaRegistro"=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        "cerrado"=>$cerrado,
        "fecha_cerrado"=>$fechaCerrado
    ];
});
