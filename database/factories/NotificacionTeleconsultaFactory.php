<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\NotificacionTeleconsulta;
use Faker\Generator as Faker;

$factory->define(NotificacionTeleconsulta::class, function (Faker $faker) {
       return [
            'nombresPaciente'=>$faker->name,
            'apellidosPaciente'=>$faker->lastName,
            'fechaNacimientoPaciente'=>$faker->date('Y-m-d'),
            'edadPaciente'=>$faker->numberBetween(10,59),
            'carnetPaciente'=>$faker->randomNumber(7),
            'direccionPaciente'=>$faker->address,
            'sexoPaciente'=>$faker->randomElement(['M','H']),
            'nombresMedico'=>$faker->name,
            'apellidosMedico'=>$faker->lastName,
            'carnetMedico'=>$faker->randomNumber(7),
            'numeroTeleconsulta'=>$faker->randomNumber(7),
            'fechaTeleconsulta'=>$faker->date('Y-m-d H:i:s'),
            'codEstablecimiento'=>$faker->countryCode,
            'estadoTeleconsulta'=>$faker->randomElement(['confirmada','cancelada','atendida'])

       ];
});
