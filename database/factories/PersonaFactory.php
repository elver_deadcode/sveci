<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
        $verificado=random_int(0, 1);
        if($verificado==1){
            $adscrito_id=$faker->randomNumber(6);
        }
        $adscrito_id=0;
    $establecimiento=\App\Establecimiento::all()->random(1)->first();
    $municipio=$establecimiento->municipio;
    $depto=$municipio->departamento();
    $randomletter=$faker->randomLetter;
    $randomNumber=$faker->randomNumber(1);
    $complemento=$randomletter.$randomNumber;

    return [
        'nombres'=>$faker->name,
        'primerApellido'=>$faker->lastName,
        'segundoApellido'=>$faker->lastName,
        'numeroCarnet'=>$faker->randomNumber(7),
        'complemento'=>$complemento,
        'nacionalidad'=>'BOLIVIANA',
        'direccion'=>$faker->streetAddress,
        'fechaNacimiento'=>$faker->dateTimeBetween($startDate = '-55 years', $endDate = 'now'),
        'sexo'=>$faker->randomElement(['M','H']),
        'adscrito_id'=>$adscrito_id,
        'verificado'=>$verificado,
        'codEstablecimiento'=>$establecimiento->codestabl,
        'IdComunidad'=>0,
        'mnc_codigo'=>$municipio->mnc_codigo,
        'are_codigo'=>$establecimiento->codarea,
        'dpt_codigo'=>$depto->dpt_codigo,
        'email'=>$faker->email,
        'celular'=>$faker->numberBetween(69600816,72476178),
    ];
});
