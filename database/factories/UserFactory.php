<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
//$nivel=$faker->randomElement(['nacional','depto','red','muni','establ']);
$nivel=$faker->randomElement(['establ']);
switch($nivel){
    case 'nacional':
        $idnivel=0;
        $persona=factory(\App\Persona::class)->create();
        break;
    case 'depto':
        $depto= \App\Departamento::all()->random(1)->first();
        $idnivel=  $depto->dpt_codigo;
        $municipio=$depto->obtenerMunicipios()->random(1)->first();
        $area=\App\Red::where('dpt_codigo',$idnivel)->get()->random(1)->first();
        $establecimiento=\App\Establecimiento::where('codarea',$area->are_codigo)->get()->random(1)->first();
        $persona=factory(\App\Persona::class)->create([
                                                           'codEstablecimiento'=>$establecimiento->codestabl,
                                                            'IdComunidad'=>0,
                                                            'mnc_codigo'=>$municipio->mnc_codigo,
                                                            'are_codigo'=>$area->are_codigo,
                                                            'dpt_codigo'=>$idnivel
                                                      ]);

        break;
    case   'red':
        $red= \App\Red::all()->random(1)->first();
        $idnivel=  $red->are_codigo;
        $depto= \App\Departamento::where('dpt_codigo',$red->dpt_codigo)->get()->random(1)->first();
        $municipio=$depto->obtenerMunicipios()->random(1)->first();
        $establecimiento=\App\Establecimiento::where('codarea',$red->are_codigo)->get()->random(1)->first();
        $persona=factory(\App\Persona::class)->create([
                                                          'codEstablecimiento'=>$establecimiento->codestabl,
                                                          'IdComunidad'=>0,
                                                          'mnc_codigo'=>$municipio->mnc_codigo,
                                                          'are_codigo'=>$red->are_codigo,
                                                          'dpt_codigo'=>$depto->dpt_codigo
                                                      ]);

        break;
    case   'muni':
        $municipio= \App\Municipio::all()->random(1)->first();
        $idnivel=  $municipio->mnc_codigo;
        $establecimiento=\App\Establecimiento::where('codmunicip',$municipio->mnc_codigo)->get()->random(1)->first();
        $area=\App\Red::where('are_codigo',$establecimiento->codarea)->get()->random(1)->first();
        $depto= \App\Departamento::where('dpt_codigo',$area->dpt_codigo)->get()->random(1)->first();
        $persona=factory(\App\Persona::class)->create([
                                                          'codEstablecimiento'=>$establecimiento->codestabl,
                                                          'IdComunidad'=>0,
                                                          'mnc_codigo'=>$municipio->mnc_codigo,
                                                          'are_codigo'=>$area->are_codigo,
                                                          'dpt_codigo'=>$depto->dpt_codigo
                                                      ]);

        break;
    case   'establ':
        $establecimiento=\App\Establecimiento::all()->random(1)->first();
        $idnivel=   $establecimiento->codestabl;

        $municipio= \App\Municipio::where('mnc_codigo',$establecimiento->codmunicip)->get()->random(1)->first();
        $area=\App\Red::where('are_codigo',$establecimiento->codarea)->get()->random(1)->first();
        $depto= \App\Departamento::where('dpt_codigo',$area->dpt_codigo)->get()->random(1)->first();
        $persona=factory(\App\Persona::class)->create([
                                                          'codEstablecimiento'=>$establecimiento->codestabl,
                                                          'IdComunidad'=>0,
                                                          'mnc_codigo'=>$municipio->mnc_codigo,
                                                          'are_codigo'=>$area->are_codigo,
                                                          'dpt_codigo'=>$depto->dpt_codigo
                                                      ]);

        break;




}
    return [
        'username' => $faker->userName,
        'password' => bcrypt('Admin123'),
        'nivel'=>$nivel,
        'idnivel'=>$idnivel,
        'estado' => $faker->randomElement([0,1]),
        'IdPersona' => $persona->getKey(),
        'codigo_credencial' => '',
    ];
});
