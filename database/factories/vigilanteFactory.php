<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vigilante;
use Faker\Generator as Faker;

$factory->define(\App\Vigilante::class, function (Faker $faker) {
    $user=factory(\App\User::class)->create();
    $persona=null;
    switch($user->nivel){
        case 'nacional':
            $persona=factory(\App\Persona::class)->create();
            break;
        case 'depto':
            $red=\App\Red::where('dpt_codigo',$user->idnivel)->get()->random(1)->first();
            $establecimiento=\App\Establecimiento::where('codarea',$red->getKey())->get()->random(1)->first();
            $municipio=\App\Municipio::where('mnc_codigo',$establecimiento->codmunicip)->get()->random(1)->first();
            $persona=factory(\App\Persona::class)->create([
                                                              'codEstablecimiento'=>$establecimiento->codestabl,
                                                              'IdComunidad'=>0,
                                                              'mnc_codigo'=>$municipio->mnc_codigo,
                                                              'are_codigo'=>$establecimiento->codarea,
                                                              'dpt_codigo'=>$user->idnivel,

                                                          ]);
             break;
        case 'red':
            $establecimiento=\App\Establecimiento::where('codarea',$user->idnivel)->get()->random(1)->first();
            $municipio=\App\Municipio::where('mnc_codigo',$establecimiento->codmunicip)->get()->random(1)->first();
            $red=\App\Red::where('are_codigo',$user->idnivel)->get()->first();
            $persona=factory(\App\Persona::class)->create([
                                                              'codEstablecimiento'=>$establecimiento->codestabl,
                                                              'IdComunidad'=>0,
                                                              'mnc_codigo'=>$municipio->mnc_codigo,
                                                              'are_codigo'=>$user->idnivel,
                                                              'dpt_codigo'=>$red->dpt_codigo,

                                                          ]);
            break;
        case 'muni':

            $establecimiento=\App\Establecimiento::where('mnc_codigo',$user->idnivel)->get()->random(1)->first();
            $red=\App\Red::where('are_codigo',$establecimiento->codarea)->get()->first();
            $persona=factory(\App\Persona::class)->create([
                                                              'codEstablecimiento'=>$establecimiento->codestabl,
                                                              'IdComunidad'=>0,
                                                              'mnc_codigo'=>$user->idnivel,
                                                              'are_codigo'=>$red->are_codigo,
                                                              'dpt_codigo'=>$red->dpt_codigo,

                                                          ]);

            break;
        case 'establ':
            $establecimiento=\App\Establecimiento::where('codestabl',$user->idnivel)->orderBy('idgestion','DESC')->get()->first();
            $red=\App\Red::where('are_codigo',$establecimiento->codarea)->get()->first();
            $persona=factory(\App\Persona::class)->create([
                                                              'codEstablecimiento'=>$establecimiento->codestabl,
                                                              'IdComunidad'=>0,
                                                              'mnc_codigo'=>$establecimiento->codmunicip,
                                                              'are_codigo'=>$red->are_codigo,
                                                              'dpt_codigo'=>$red->dpt_codigo,

                                                          ]);
            break;
}
    return [
        "longitud"=>$faker->longitude,
        "latitud"=>$faker->latitude,
        "codigo"=>$faker->ean8,
        "estado"=>true,
        'IdUsuario'=>$user->getKey(),
        'activado'=> true,
        'IdPersona'=>$persona->getKey()

    ];
});
