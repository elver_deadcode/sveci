<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVigilantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vigilantes', function (Blueprint $table) {
            $table->increments('IdVigilante');
            $table->string('codEstablecimiento',6);
            $table->string('nombres',50);
            $table->string('primerApellido',100);
            $table->string('segundoApellido',100)->nullable();
            $table->string('numeroCarnet',10)->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('numeroCelular',11);
            $table->string('email',250)->nullable();
            $table->date('fechaNacimiento');
            $table->string('longitud',250)->nullable();
            $table->string('latitud',250)->nullable();
            $table->string('codigo',10);
            $table->string('foto',250)->nullable();
            $table->boolean('estado');
            $table->boolean('activado');
            $table->date('fechaActivacion')->nullable();
            $table->char('sexo',1);
            $table->integer('IdUsuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vigilantes');
    }
}
