<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMujeresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mujeres', function (Blueprint $table) {
            $table->increments('IdMujer');
            $table->string('nombres',50);
            $table->string('segundoNombres',50);
            $table->string('primerApellido',100);
            $table->integer('IdVigilante')->nullable();
            $table->string('codEstablecimiento',6);
            $table->string('segundoApellido',100)->nullable();
            $table->string('numeroCarnet',10)->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('numeroCelular',11)->nullable();
            $table->string('email',250)->nullable();
            $table->date('fechaNacimiento');
            $table->string('longitud',250)->nullable();
            $table->string('latitud',250)->nullable();
            $table->string('foto',250)->nullable();
            $table->boolean('estado');
            $table->string('idAndroid',250)->nullable();
            $table->integer('IdUsuario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mujeres');
    }
}
