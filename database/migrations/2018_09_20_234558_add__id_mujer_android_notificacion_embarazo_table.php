<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdMujerAndroidNotificacionEmbarazoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
             $table->string('idMujerAndroid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
            $table->dropColumn('idMujerAndroid');
        });
    }
}
