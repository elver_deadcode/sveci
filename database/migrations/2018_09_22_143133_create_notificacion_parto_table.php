<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionPartoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionParto', function (Blueprint $table) {
            $table->increments('IdNotificacionParto');
            $table->string('IdAndroid');
            $table->string('idMujerAndroid');
            $table->string('codVigilante');
            $table->string('codEstablecimiento');
            $table->string('estado',20)->nullable();
            $table->string('nombres',150)->nullable();
            $table->string('primerApellido',250)->nullable();
            $table->string('segundoApellido',250)->nullable();
            $table->date('fechaNacimento')->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('telefono')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->integer('edad')->nullable();
            $table->string('atendido')->nullable();
            $table->timestamp('fechaRegistro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionParto');
    }
}
