<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionEmbarazoDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionEmbarazoDetalle', function (Blueprint $table) {
            $table->increments('IdNotificacionEmbarazoDetalle');
            $table->integer('numeroControlesPrenatal')->default(0);
            $table->integer('IdNotificacionEmbarazo');
            $table->boolean('planPartoLlenado')->default('false');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionEmbarazoDetalle');
    }
}
