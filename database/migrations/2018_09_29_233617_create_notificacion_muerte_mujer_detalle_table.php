<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionMuerteMujerDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->increments('IdMuerteMujerDetalle');
            $table->string('muerteDurante');
            $table->integer('diasMuerte')->nullable();
            $table->boolean('tieneFicha')->default('false');
            $table->integer('IdNotificacionMuerteMujer');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionMuerteMujerDetalle');
    }
}
