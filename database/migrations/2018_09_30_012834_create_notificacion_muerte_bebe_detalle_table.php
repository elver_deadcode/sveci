<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionMuerteBebeDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionMuerteBebeDetalle', function (Blueprint $table) {
            $table->increments('IdMuerteBebeDetalle');
            $table->boolean('nacido')->default('false');
            $table->integer('diasVida')->nullable();
            $table->integer('mesesVida')->nullable();
            $table->boolean('tieneAutopsiaVerbal')->default('false');
            $table->integer('IdNotificacionMuerteBebe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionMuerteBebeDetalle');
    }
}
