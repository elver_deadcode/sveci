<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDiasMuerteDetallemuertemujer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('notificacionMuerteMujerDetalle', function ($table) {
            // $table->dropColumn('diasMuerte');
        });

        Schema::table('notificacionMuerteMujerDetalle', function ($table) {
            $table->date('fechaFallecimiento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->dropColumn('fechaFallecimiento');
        });
    }
}