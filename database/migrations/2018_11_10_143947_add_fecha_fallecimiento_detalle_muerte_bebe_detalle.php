<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaFallecimientoDetalleMuerteBebeDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('notificacionMuerteBebeDetalle', function($table)
        {
            $table->dropColumn('diasVida');
            $table->dropColumn('mesesVida');
        });
        Schema::table('notificacionMuerteBebeDetalle', function (Blueprint $table) {
            $table->date('fechanacimiento')->nullable();
            $table->date('fechafallecimiento')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteBebeDetalle', function (Blueprint $table) {
           $table->dropColumn('fechafallecimiento');
        });
    }
}
