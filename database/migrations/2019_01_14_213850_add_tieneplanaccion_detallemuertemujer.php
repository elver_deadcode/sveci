<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTieneplanaccionDetallemuertemujer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->boolean('tienePlanAccion')->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->dropColumn('tienePlanAccion');
        });
    }
}
