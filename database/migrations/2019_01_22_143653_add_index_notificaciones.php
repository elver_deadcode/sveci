<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexNotificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
        $table->index(['codEstablecimiento','mnc_codgo','dpt_codigo','are_codigo','notificable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
           $table->dropIndex(['codEstablecimiento','mnc_codgo','dpt_codigo','are_codigo','notificable_id']);
        });
    }
}
