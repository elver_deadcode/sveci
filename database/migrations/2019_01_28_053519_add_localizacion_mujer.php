<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalizacionMujer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mujeres', function (Blueprint $table) {
            $table->integer('mnc_codigo')->nullable()->default('0');
            $table->integer('are_codigo')->nullable()->default('0');
            $table->integer('dpt_codigo')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mujeres', function (Blueprint $table) {
           $table->dropColumn('mnc_codigo');
           $table->dropColumn('are_codigo');
           $table->dropColumn('dpt_codigo');
        });
    }
}
