<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCerradoColumnsNotificacionMuerteBebe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            $table->boolean('cerrado')->default('false');
            $table->date('fecha_cerrado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            $table->dropColumn('cerrado');
            $table->dropColumn('fecha_cerrado');
        });
    }
}
