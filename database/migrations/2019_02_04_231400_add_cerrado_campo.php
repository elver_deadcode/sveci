<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCerradoCampo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            $table->boolean('cerrado')->default('false');
            $table->string('atendidoNombre')->default('');
            $table->date('fecha_cerrado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            $table->dropColumn('cerrado');
            $table->dropColumn('atendidoNombre');
            $table->dropColumn('fecha_cerrado');
        });
    }
}
