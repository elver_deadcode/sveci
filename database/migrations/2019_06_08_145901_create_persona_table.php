<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('IdPersona');
            $table->timestamps();
            $table->string('nombres',100);
            $table->string('primerApellido',100);
            $table->string('segundoApellido',100)->nullable();
            $table->string('numeroCarnet',10)->unique();
            $table->string('complemento',10)->nullable();
            $table->string('nacionalidad',20)->nullable();
            $table->string('direccion',250)->nullable();
            $table->date('fechaNacimiento');
            $table->char('sexo',1);
            $table->boolean('esverificadosegip',1);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
