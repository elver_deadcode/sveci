<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPersonaLocalizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->integer('codEstablecimiento')->default(0)->nullable();
            $table->integer('IdComunidad')->default(0)->nullable();
            $table->integer('mnc_codigo')->default(0)->nullable();
            $table->integer('are_codigo')->default(0)->nullable();
            $table->integer('dpt_codigo')->default(0)->nullable();
            
            $table->string('email')->nullable();
            $table->string('celular')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropColumn('codEstablecimiento');
            $table->dropColumn('IdComunidad');
            $table->dropColumn('mnc_codigo');
            $table->dropColumn('are_codigo');
            $table->dropColumn('dpt_codigo');
            $table->dropColumn('email');
            $table->dropColumn('celular');
        });
    }
}
