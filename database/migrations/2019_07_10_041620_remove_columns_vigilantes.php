<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsVigilantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vigilantes', function (Blueprint $table) {
            $table->dropColumn('nombres');
            $table->dropColumn('primerApellido');
            $table->dropColumn('segundoApellido');
            $table->dropColumn('numeroCarnet');
            $table->dropColumn('direccion');
            $table->dropColumn('fechaNacimiento');
            $table->dropColumn('numeroCelular');
            $table->dropColumn('email');
            $table->dropColumn('sexo');
            $table->dropColumn('foto');
            $table->dropColumn('codEstablecimiento');
            $table->dropColumn('IdComunidad');
            $table->dropColumn('mnc_codigo');
            $table->dropColumn('are_codigo');
            $table->dropColumn('dpt_codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vigilantes', function (Blueprint $table) {
            $table->string('nombres',50)->nullable();
            $table->string('primerApellido',100)->nullable();
            $table->string('segundoApellido',100)->nullable();
            $table->string('numeroCarnet',10)->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('numeroCelular',11)->nullable();
            $table->string('email',250)->nullable();
            $table->date('fechaNacimiento')->nullable();
            $table->char('sexo',1)->nullable();
            $table->string('foto',250)->nullable();
            $table->string('codEstablecimiento','6')->nullable();
            $table->integer('IdComunidad')->default(0)->nullable();
            $table->integer('mnc_codigo')->default(0)->nullable();
            $table->integer('are_codigo')->default(0)->nullable();
            $table->integer('dpt_codigo')->default(0)->nullable();
        });
    }
}
