<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nombres');
            $table->dropColumn('primer_apellido');
            $table->dropColumn('segundo_apellido');
            $table->dropColumn('numero_carnet');
            $table->dropColumn('email');
            $table->dropColumn('numero_celular');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nombres',255)->nullable();
            $table->string('primer_apellido',255)->nullable();
            $table->string('segundo_apellido',255)->nullable();
            $table->string('numero_carnet',255)->nullable();
            $table->string('email',255)->nullable();
            $table->string('numero_celular',255)->nullable();
        });
    }
}
