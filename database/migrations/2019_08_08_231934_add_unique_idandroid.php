<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdandroid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
            DB::statement('ALTER TABLE "notificacionEmbarazo" ADD CONSTRAINT IdAndroid_nme_unique UNIQUE ("IdAndroid");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
            $table->dropUnique('idandroid_nme_unique');
        });
    }
}