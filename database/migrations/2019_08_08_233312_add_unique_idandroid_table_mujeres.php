<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdandroidTableMujeres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mujeres', function (Blueprint $table) {
            // $table->unique('idAndroid');
            DB::statement('ALTER TABLE mujeres ADD CONSTRAINT mujeres_idandroid_unique UNIQUE ("idAndroid");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mujeres', function (Blueprint $table) {
            $table->dropUnique('mujeres_idandroid_unique');
        });
    }
}