<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdandroidNotificacionmuertemujer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteMujer', function (Blueprint $table) {
            DB::statement('ALTER TABLE "notificacionMuerteMujer" ADD CONSTRAINT IdAndroid_nmm_unique UNIQUE ("IdAndroid");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteMujer', function (Blueprint $table) {
            $table->dropUnique('idandroid_nmm_unique');
        });
    }
}