<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdandroidNotificacionmuertebebe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            // $table->unique('IdAndroid');
            DB::statement('ALTER TABLE "notificacionMuerteBebe" ADD CONSTRAINT IdAndroid_mmb_unique UNIQUE ("IdAndroid");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            $table->dropUnique('idandroid_mmb_unique');
        });
    }
}