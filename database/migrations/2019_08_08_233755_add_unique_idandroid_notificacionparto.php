<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdandroidNotificacionparto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            DB::statement('ALTER TABLE "notificacionParto" ADD CONSTRAINT IdAndroid_np_unique UNIQUE ("IdAndroid");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            $table->dropUnique('idandroid_np_unique');
        });
    }
}