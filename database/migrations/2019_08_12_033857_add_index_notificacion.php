<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexNotificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
            DB::statement('CREATE INDEX mnc_codigo_idx ON notificaciones(mnc_codgo);');
            DB::statement('CREATE INDEX are_codigo_idx ON notificaciones(are_codigo);');
            DB::statement('CREATE INDEX dpt_codigo_idx ON notificaciones(dpt_codigo);');
            DB::statement('CREATE INDEX notificable_type_idx ON notificaciones(notificable_type);');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
            $table->dropIndex('mnc_codigo_idx');
            $table->dropIndex('are_codigo_idx');
            $table->dropIndex('dpt_codigo_idx');
            $table->dropIndex('notificable_type_idx');
        });
    }
}