<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexNotiembarazo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
            DB::statement('CREATE INDEX IdAndroid_idx ON "notificacionEmbarazo"("IdAndroid");');
            DB::statement('CREATE INDEX codVigilante_idx ON "notificacionEmbarazo"("codVigilante");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionEmbarazo', function (Blueprint $table) {
            $table->dropIndex('idandroid_idx');
            $table->dropIndex('codvigilante_idx');
        });
    }
}