<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexMuertemujer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteMujer', function (Blueprint $table) {
            DB::statement('CREATE INDEX idandroid_nmm_idx ON "notificacionMuerteMujer"("IdAndroid");');
            DB::statement('CREATE INDEX codvigilante_nmm_idx ON "notificacionMuerteMujer"("codVigilante");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteMujer', function (Blueprint $table) {
            $table->dropIndex('idandroid_nmm_idx');
            $table->dropIndex('codvigilante_nmm_idx');
        });
    }
}