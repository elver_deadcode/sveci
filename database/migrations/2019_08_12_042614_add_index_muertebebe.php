<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexMuertebebe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            DB::statement('CREATE INDEX idandroid_nmb_idx ON "notificacionMuerteBebe"("IdAndroid");');
            DB::statement('CREATE INDEX codvigilante_nmb_idx ON "notificacionMuerteBebe"("codVigilante");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteBebe', function (Blueprint $table) {
            $table->dropIndex('idandroid_nmb_idx');
            $table->dropIndex('codvigilante_nmb_idx');
        });
    }
}