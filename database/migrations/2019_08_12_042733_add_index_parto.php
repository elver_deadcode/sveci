<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexParto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            DB::statement('CREATE INDEX idandroid_np_idx ON "notificacionParto"("IdAndroid");');
            DB::statement('CREATE INDEX codvigilante_np_idx ON "notificacionParto"("codVigilante");');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionParto', function (Blueprint $table) {
            $table->dropIndex('idandroid_np_idx');
            $table->dropIndex('codvigilante_np_idx');
        });
    }
}