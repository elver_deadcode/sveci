<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCasoAtendido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->boolean('casoFueAtendido')->default('false');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->dropColumn('casoFueAtendido');
        });
    }
}
