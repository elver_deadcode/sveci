<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentariosNotificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
            $table->string('comentarios',500)->nullable();
        });
        Schema::table('notificacionMuerteBebeDetalle', function (Blueprint $table) {
            $table->string('comentarios',500)->nullable();
        });
        Schema::table('notificacionPartoDetalle', function (Blueprint $table) {
            $table->string('comentarios',500)->nullable();
        });
        Schema::table('notificacionEmbarazoDetalle', function (Blueprint $table) {
            $table->string('comentarios',500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       

        Schema::table('notificacionMuerteMujerDetalle', function (Blueprint $table) {
          $table->dropColumn('comentarios');
        });
        Schema::table('notificacionMuerteBebeDetalle', function (Blueprint $table) {
          $table->dropColumn('comentarios');
        });
        Schema::table('notificacionPartoDetalle', function (Blueprint $table) {
          $table->dropColumn('comentarios');
        });
        Schema::table('notificacionEmbarazoDetalle', function (Blueprint $table) {
          $table->dropColumn('comentarios');
        });
    }
}
