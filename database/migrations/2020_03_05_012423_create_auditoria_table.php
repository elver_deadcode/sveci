<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditoria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nombre_tabla');
            $table->string('Id_tabla');
            $table->string('Tipo_usuario');
            $table->date('Fecha_ing')->nullable();;
            $table->time('Hora_ing')->nullable();
            $table->date('Fecha_sal')->nullable();;
            $table->time('Hora_sal')->nullable();
            $table->string('IP_terminal')->nullable();
            $table->string('Evento')->nullable();
            $table->string('Modulo')->nullable();
            $table->string('Menu')->nullable();
            $table->string('Submenu')->nullable();
            $table->string('Item')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditoria', function (Blueprint $table) {
            Schema::dropIfExists('auditoria');


        });
    }
}
