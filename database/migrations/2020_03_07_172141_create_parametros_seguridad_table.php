<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametrosSeguridadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametrosSeguridad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Longitud_PWD')->nullable();
            $table->integer('Calidad_PWD')->nullable();
            $table->integer('Tiempo_PWD')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametrosSeguridad');
    }
}
