<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableNotificacionTeleconsulta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionTeleconsulta', function (Blueprint $table) {
            $table->bigIncrements('id');

           $table->string('nombresPaciente',255)->nullable();
           $table->string('apellidosPaciente',255)->nullable();
           $table->date('fechaNacimientoPaciente')->nullable();
           $table->integer('edadPaciente')->default(0);
           $table->string('carnetPaciente')->nullable();
           $table->string('direccionPaciente')->nullable();
           $table->string('sexoPaciente',5)->nullable();
           $table->string('nombresMedico',255)->nullable();
           $table->string('apellidosMedico',255)->nullable();
           $table->string('carnetMedico',20)->default('n\n');

           $table->string('numeroTeleconsulta');
           $table->dateTime('fechaTeleconsulta');
           $table->string('codEstablecimiento');
           $table->enum('estadoTeleconsulta',['confirmada','cancelada','atendida']);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionTeleconsulta');
    }
}
