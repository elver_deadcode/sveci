<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionViolencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacionViolencia', function (Blueprint $table) {
            $table->increments('IdNotificacionViolencia');
            $table->string('IdAndroid');
            $table->string('codVigilante');
            $table->string('codEstablecimiento');
            $table->string('estado',20)->nullable();
            $table->string('nombres',150)->nullable();
            $table->string('primerApellido',250)->nullable();
            $table->string('segundoApellido',250)->nullable();
            $table->date('fechaNacimento')->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('telefono')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->integer('edad')->nullable();
            $table->timestamp('fechaRegistro');
            $table->integer('idMujerAndroid')->nullable();
            $table->boolean('cerrado')->default(false);
            $table->date('fecha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacionViolencia');
    }
}
