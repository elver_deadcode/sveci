<?php

use Illuminate\Database\Seeder;
use App\Persona;
use Illuminate\Foundation\Auth\User;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $p = new Persona();
        $p->nombres = 'elver';
        $p->primerApellido = 'vasquez';
        $p->segundoApellido = 'segales';
        $p->numeroCarnet = '5066349';
        $p->complemento = '';
        $p->nacionalidad = 'BOLIVIANA';
        $p->direccion = '';
        $p->fechaNacimiento = '1994-07-21';
        $p->sexo = 'M';
        $p->adscrito_id = 0;
        $p->verificado = true;
        $p->codEstablecimiento = 0;
        $p->IdComunidad = 0;
        $p->mnc_codigo = 0;
        $p->are_codigo = 0;
        $p->dpt_codigo = 0;
        $p->email = 'admin@gmail.com';
        $p->celular = '72476178';
        $p->save();
        $u = new User();
        $u->username = "Evasquez";
        $u->password = bcrypt('Admin123');
        $u->nivel = 'nacional';
        $u->idnivel = '0';
        $u->estado = 't';
        $u->IdPersona = $p->IdPersona;
        $u->save();

        $rol = new Role();
        $rol->name = "admin";
        $rol->display_name = "ADMINISTRADOR";
        $rol->description = "ADMINISTRADOR";
        $rol->save();

        $permiso = new Permission();
        $permiso->name = 'crear_usuarios';
        $permiso->display_name = 'crear_usuarios';
        $permiso->description = 'crear usaurios';
        $permiso->modulo = 'usuarios';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        DB::table('role_user')->insert([
            'role_id' => $rol->id,
            'user_id' => $u->id,
            'user_type' => 'App\User'
        ]);
        $permiso = new Permission();
        $permiso->name = 'editar_usuarios';
        $permiso->display_name = 'editar usaurios';
        $permiso->description = 'EDITAR USUAIOS';
        $permiso->modulo = 'usuarios';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'baja_usuarios';
        $permiso->display_name = 'dar de baja usuarios';
        $permiso->description = 'dar de baja usuarios';
        $permiso->modulo = 'usuarios';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'crear_rol';
        $permiso->display_name = 'registrar roles de usuario';
        $permiso->description = 'registrar roles de usuario';
        $permiso->modulo = 'roles';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        $permiso = new Permission();
        $permiso->name = 'editar_rol';
        $permiso->display_name = 'Editar roles de usuario';
        $permiso->description = 'Editar roles de usuario';
        $permiso->modulo = 'roles';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'borrar_rol';
        $permiso->display_name = 'Borrar rol de usuario';
        $permiso->description = 'Borrar rol de usuario';
        $permiso->modulo = 'roles';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'reportes_consolidados';
        $permiso->display_name = 'Ver reportes Consolidados';
        $permiso->description = 'Ver reportes Consolidados';
        $permiso->modulo = 'reportes';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        $permiso = new Permission();
        $permiso->name = 'reportes_nominales';
        $permiso->display_name = 'Ver Reportes Nominales';
        $permiso->description = 'Ver Reportes Nominales';
        $permiso->modulo = 'reportes';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'crear_notificaciones';
        $permiso->display_name = 'Crear Notificaciones';
        $permiso->description = 'Crear Notificaciones';
        $permiso->modulo = 'notificaciones';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        $permiso = new Permission();
        $permiso->name = 'anular_notificacion';
        $permiso->display_name = 'Anular Notificaciones';
        $permiso->description = 'Anular Notificaciones';
        $permiso->modulo = 'notificaciones';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'registrar_indicadores';
        $permiso->display_name = 'Registrar Indicadores';
        $permiso->description = 'Registrar Indicadores';
        $permiso->modulo = 'notificaciones';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);


        $permiso = new Permission();
        $permiso->name = 'crear_vigilantes';
        $permiso->display_name = 'Crear Vigilantes comunitarios';
        $permiso->description = 'Crear Vigilantes comunitarios';
        $permiso->modulo = 'vigilantes';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        $permiso = new Permission();
        $permiso->name = 'editar_vigilantes';
        $permiso->display_name = 'Editar Vigilantes comunitarios';
        $permiso->description = 'Editar Vigilantes comunitarios';
        $permiso->modulo = 'vigilantes';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'baja_vigilantes';
        $permiso->display_name = 'Dar de baja Vigilantes comunitarios';
        $permiso->description = 'Dar de baja Vigilantes comunitarios';
        $permiso->modulo = 'vigilantes';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'crear_mujeres';
        $permiso->display_name = 'Crear mujeres ';
        $permiso->description = 'Crear mujeres ';
        $permiso->modulo = 'mujeres';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        $permiso = new Permission();
        $permiso->name = 'editar_mujeres';
        $permiso->display_name = 'Editar mujeres ';
        $permiso->description = 'Editar mujeres ';
        $permiso->modulo = 'mujeres';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);
        $permiso = new Permission();
        $permiso->name = 'baja_mujeres';
        $permiso->display_name = 'Dar de baja mujeres ';
        $permiso->description = 'Dar de baja mujeres ';
        $permiso->modulo = 'mujeres';
        $permiso->save();
        $rol->permissions()->attach($permiso->id);

        \App\ParametrosSeguridad::insert([
            [
                'Longitud_PWD' => 7,
                'Calidad_PWD' => 2,
                'Tiempo_PWD' => 365
            ]
        ]);

        \App\Evento::insert(
            [

                ['Descripcion' => 'ingreso al sistema'],
                ['Descripcion' => 'ingreso a la pantalla de inicio del sistema'],
                ['Descripcion' => 'ingreso reporte consolidado parto de mujeres indentificadas por vigilante comunitario'],
                ['Descripcion' => 'ingreso reporte consolidado muertes fetales y neonatales indentificadas por vigilante comunitario'],
                ['Descripcion' => 'click en el boton consultar del  reporte ingreso reporte consolidado muertes fetales y neonatales indentificadas por vigilante comunitario'],
                ['Descripcion' => 'click en el boton consultar del  reporte consolidado parto de mujeres indentificadas por vigilante comunitario'],
                ['Descripcion' => 'click en el boton consultar del reporte consolidado mujeres embarazadas'],
                ['Descripcion' => 'ingreso al reporte consolidado mujeres embarazadas'],
                ['Descripcion' => 'ingreso a la pantalla de manual de usuario'],
                ['Descripcion' => 'ingreso reporte consolidado muertes mujeres  indentificadas por vigilante comunitario'],
                ['Descripcion' => 'click en el boton consultar del  reporte ingreso reporte consolidado muertes mujeres indentificadas por vigilante comunitario'],
                ['Descripcion' => 'ingreso reporte consolidado  vigilantes comunitarios'],
                ['Descripcion' => 'click en el boton consultar del  reporte ingreso reporte consolidado vigilantes comunitarios'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  vigilantes comunitarios'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de vigilantes comunitarios'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  de mujeres de 10 a 59 anios'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de mujeres de 10 a 59 anios'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  de notificaciones de embarazo'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de notificaciones de embarazo'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  de notificaciones de muerte mujer'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de notificaciones de muerte mujer'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  de notificaciones de muerte bebe'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de notificaciones de muerte bebe'],
                ['Descripcion' => 'ingreso reporte nomninal  lista  de notificaciones de parto'],
                ['Descripcion' => 'click en el boton Buscar del reporte nominal lista  de notificaciones de parto'],
                ['Descripcion' => 'ingreso pantalla lista de vigilantes'],
                ['Descripcion' => 'click boton buscar  en lista de vigilantes'],
                ['Descripcion' => 'ingreso a  la pantalla perfil de un vigilante'],
                ['Descripcion' => 'ingreso a la pantalla de editar vigilante'],
                ['Descripcion' => 'guardar cambios  en editar vigilante'],
                ['Descripcion' => 'dar de baja a un vigilante'],
                ['Descripcion' => 'dar de alta a un vigilante'],
                ['Descripcion' => 'ingreso a la pantalla  formulario registro vigilante comunitario'],
                ['Descripcion' => 'guardar resgistro de un nuevo vigilante comunitario'],
                ['Descripcion' => 'ingreso pantalla lista de  mujeres'],
                ['Descripcion' => 'ingreso a la pantalla de editar mujer'],
                ['Descripcion' => 'guardar cambios  en editar mujer'],
                ['Descripcion' => 'ingreso a la pantalla  formulario registro de una mujer'],
                ['Descripcion' => 'guardar resgistro de un nueva mujer'],
                ['Descripcion' => 'ingreso a la pantalla de notificaciones'],
                ['Descripcion' => 'abrir ventana para registrar indicadores de indicadores'],
                ['Descripcion' => 'guardar datos de indicador muerte bebe'],
                ['Descripcion' => 'guardar datos de indicador y cerrar notificacion muerte bebe'],
                ['Descripcion' => 'guardar datos de indicador notificacion embarazo'],
                ['Descripcion' => 'guardar datos  de indicador  y cerrar la notificacion embarazo'],
                ['Descripcion' => 'guardar datos de indicador notificacion  muerte mujer'],
                ['Descripcion' => 'guardar datos  de indicador  y cerrar la notificacion  muerte mujer'],
                ['Descripcion' => 'guardar datos de indicador notificacion  parto'],
                ['Descripcion' => 'guardar datos  de indicador  y cerrar la notificacion  parto'],
                ['Descripcion' => 'ingreso a la pantalla de roles de usuarios'],
                ['Descripcion' => 'ingreso a la pantalla de registro de roles de usuarios'],
                ['Descripcion' => 'guardar datos  de nuevo rol de usuario'],
                ['Descripcion' => 'ingreso a la pantalla de edicion de  rol de usuarios'],
                ['Descripcion' => 'guardar cambios  en editar rol de usuario'],
                ['Descripcion' => 'ingreso a la pantalla de lista  usuarios'],
                ['Descripcion' => 'ingreso a la pantalla de registro de usuarios'],
                ['Descripcion' => 'guardar datos  de nuevo usuario'],
                ['Descripcion' => 'ingreso a la pantalla de edicion de usuario'],
                ['Descripcion' => 'guardar cambios  en editar usuario'],
                ['Descripcion' => 'dar de alta usuario'],
                ['Descripcion' => 'dar de baja a un usuario'],
                ['Descripcion' => 'ingreso a la pantalla editar mi perfil de usuario'],
                ['Descripcion' => 'guardar cambios en mi perfil de usuario'],
                ['Descripcion' => 'Salir del sistema'],
                ['Descripcion' => 'El vigilante activo su celular'],
                ['Descripcion' => 'El vigilante envio notificacion Embarazo'],
                ['Descripcion' => 'El vigilante envio notificacion Muerte de Mujer'],
                ['Descripcion' => 'El vigilante envio notificacion Muerte de Bebe'],
                ['Descripcion' => 'El vigilante envio notificacion  de Parto'],
                ['Descripcion' => 'El vigilante sincronizo su telefono'],
                ['Descripcion' => 'El vigilante registro a una mujer']

            ]
        );
    }
}
