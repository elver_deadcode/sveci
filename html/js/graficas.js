
var fuente='FUENTE SVECI/SNIS VE'

function graficarBarra(titulo,subtitulo,titulo_y,data,container){
        
     
            Highcharts.chart(container, {
                        
                chart: {
                    type: 'column'
                },
                title: {
                    text: titulo
                },
                subtitle: {
                    text: subtitulo
                },
                xAxis: {
                    type: "category"
                },
                yAxis: {
                    title: {
                        text: titulo_y
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                "series": [
                    {
                        "name": "Browsers",
                        "colorByPoint": true,
                        "data": data
                    }
                ],
            
            });
     
    }

    function crearTitulo(titulo){
        let resultado=`<span style="font-size:12px;">${titulo}</span>`
        return resultado;
    }

    function graficarBarraHorizontal(titulo,subtitulo,titulo_y,data,container){

        Highcharts.chart(container, {
  chart: {
    type: 'bar',
    marginLeft: 150
  },
  title: {
    text: titulo
  },
  subtitle: {
    text: subtitulo
  },
  xAxis: {
    type: 'category',
    title: {
      text: 'Localizacion'
    },
    min: 0,
    max: 5,
    scrollbar: {
      enabled: true
    },
    tickLength: 50
  },
  yAxis: {
    min: 0,
    max: 100,
    title: {
      text: 'Porcentaje',
      align: 'high'
    }
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    }
  },
  legend: {
    enabled: false
  },
  credits: {
    enabled: true
  },
  series: [{
    name: '%',
    data: data
  }]
});
 
}         

    function graficarTorta(titulo,data,container){
            Highcharts.chart(container, {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: titulo
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Porcentaje',
                    data: data
                }]
            });
    }