var fuente = "FUENTE SVECI/SNIS VE";

function graficarBarra(titulo, subtitulo, titulo_y, data, container) {
  Highcharts.chart(container, {
    chart: {
      type: "column"
    },
    title: {
      text: titulo
    },
    subtitle: {
      text: subtitulo
    },
    xAxis: {
      type: "category"
    },
    yAxis: {
      title: {
        text: titulo_y
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.1f}%"
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
      {
        name: "Browsers",
        colorByPoint: true,
        data: data
      }
    ]
  });
}
function graficarBarra2(titulo, subtitulo, titulo_y, datos, container) {
  // datos = [
  //   { name: "oruro", y: 1 },
  //   { name: "cbba", y: 2 },
  //   { name: "santa", y: 3 },
  //   { name: "santa", y: 3 },
  //   { name: "santa", y: 4 },
  //   { name: "santa", y: 6 },
  //   { name: "santa", y: 1 },
  //   { name: "santa", y: 2 },
  //   { name: "santa", y: 3 },
  //   { name: "santa", y: 12 },
  //   { name: "santa", y: 3 },
  //   { name: "santa", y: 4 },
  //   { name: "santa", y: 5 },
  //   { name: "santa", y: 6 },
  //   { name: "santa", y: 9 },
  //   { name: "santa", y: 1 },
  //   { name: "santa", y: 20 },
  //   { name: "santa", y: 13 },
  //   { name: "santa", y: 14 },
  //   { name: "santa", y: 15 },
  //   { name: "santa", y: 33 }
  // ];
  datos.sort((a, b) => (a.y < b.y ? 1 : -1));
  Highcharts.chart(container, {
    chart: {
      type: "bar",
      backgroundColor: "#F0F0F0"
    },
    title: {
      text: titulo
    },
    subtitle: {
      text: subtitulo
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      enabled: false
    },
    xAxis: {
      offset: -10,
      lineWidth: 0,
      tickWidth: 20,
      tickLength: 20,
      tickInterval: 1,
      tickColor: "#9C9C9C",
      labels: {
        x: -3,
        style: {
          color: "#FFFFFF"
        },
        formatter: function() {
          var returningString = this.value.toString();

          if (returningString.length === 1) {
            returningString = "0" + returningString;
          }

          return returningString;
        }
      }
    },
    yAxis: {
      endOnTick: false,
      gridLineWidth: 0,
      plotLines: [
        {
          value: 0,
          width: 20,
          color: "#EBEBEB",
          zIndex: 1
        }
      ],
      title: {
        text: ""
      },
      labels: {
        enabled: false
      }
    },
    plotOptions: {
      series: {
        pointStart: 1,
        borderWidth: 0,
        threshold: 0.5,
        stacking: "normal",
        dataLabels: {
          enabled: true,
          style: {
            textOutline: false
          }
        }
      }
    },
    series: [
      {
        index: 1,
        color: "#3264a8",
        dataLabels: {
          align: "left",
          formatter: function() {
            return this.key;
          }
        },
        data: datos
      },
      {
        index: 0,
        color: "#000000",
        dataLabels: {
          align: "center",
          formatter: function() {
            // console.log(this);
            return "+" + this.point.value + "%";
          }
        },
        data: []
      }
    ]
  });
}
function graficarColumna(
  titulo,
  subtitulo,
  type,
  stacking,
  datos,
  container,
  categorias
) {
  Highcharts.chart(container, {
    chart: {
      // type: "column"
      type: type
    },
    title: {
      text: titulo
    },
    xAxis: {
      //  categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
      categories: categorias,
      crosshair: true
    },
    yAxis: {
      min: 0,
      // tickInterval: 1,
      title: {
        text: subtitulo
      }
    },
    tooltip: {
      pointFormat:
        // '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
      shared: true
    },
    plotOptions: {
      column: {
        // stacking: "percent"
        stacking: 'normal'
      }
    },
    series:datos
  });
}
function crearTitulo(titulo) {
  let resultado = `<span style="font-size:12px;">${titulo}</span>`;
  return resultado;
}

function graficarBarraHorizontal(titulo, subtitulo, titulo_y, data, container) {
  Highcharts.chart(container, {
    chart: {
      type: "bar",
      marginLeft: 150
    },
    title: {
      text: titulo
    },
    subtitle: {
      text: subtitulo
    },
    xAxis: {
      type: "category",
      title: {
        text: "Localizacion"
      },
      min: 0,
      max: 5,
      scrollbar: {
        enabled: true
      },
      tickLength: 50
    },
    yAxis: {
      min: 0,
      max: 100,
      title: {
        text: "Porcentaje",
        align: "high"
      }
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      enabled: false
    },
    credits: {
      enabled: true
    },
    series: [
      {
        name: "%",
        data: data
      }
    ]
  });
}

function graficarTorta(titulo, data, container) {
  Highcharts.chart(container, {
    chart: {
      type: "pie",
      options3d: {
        enabled: true,
        alpha: 45,
        beta: 0
      }
    },
    title: {
      text: titulo
    },
    tooltip: {
      pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        depth: 35,
        dataLabels: {
          enabled: true,
          format: "{point.name}"
        }
      }
    },
    series: [
      {
        type: "pie",
        name: "Porcentaje",
        data: data
      }
    ]
  });
}
function dibujarHistorial(historial) {
  $(".div-historial").empty();
  if (historial.periodo && historial.periodo.length > 0) {
    let tabla = "";

    tabla += `<table class="table table-striped" style="font-size:10px">
    <thead>
        <tr>
            <th class="col-md-3 bg-primary" align="center">INDICADOR</th>`;
    historial.periodo.forEach(function(v, k) {
      tabla += `<th>${v}</th>`;
    });
    tabla += `
        </tr>
    </thead>
    <tbody>`;
    historial.datos.forEach(function(v, k) {
      tabla += `<tr>
            <td class="bg-primary">${v.titulo}</td>`;
      v.valor.forEach(function(v2, k2) {
        tabla += `<td align="left">${v2}</td>`;
      });
      tabla += `
        </tr>`;
    });
    tabla += `</tbody>
</table>`;

    $(".div-historial").html(tabla);
  }
}

function graficaBarraVertical(titulo,
  subtitulo,
  type,
  stacking,
  datos,
  container,
  categorias){
 
  

  Highcharts.chart(container, {
    chart: {
        type: 'column'
    },
    title: {
        text: titulo
    },
    subtitle: {
        text: subtitulo
    },
    xAxis: {
        categories:categorias,
        crosshair: true
    },
    yAxis: {
        min: 0,
        tickInterval:1,
        title: {
            text: 'Numero de Notificaciones1'
        }

    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:f} Notificaciones</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: datos,
    
});
}
