
function dos_decimales(cadena){
    var expresion=/^\d+(\.\d{0,2})?$/;
    var resultado=expresion.test(cadena);
    return resultado;
}


$(document).ready(function() {



    $('form').bind("keypress", function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

});

$('body').on("keypress keyup blur",'.solo-enteros',function (e) {
    //this.value = this.value.replace(/[^0-9\.]/g,'');
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }

});
$('body').on("keypress keyup blur",'.numeroscondecimales',function (event) {
    //this.value = this.value.replace(/[^0-9\.]/g,'');
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));

    if (($(this).val().indexOf('.') != -1) && (event.which == 13 || event.which > 57)) {
        event.preventDefault();
    }

});

$(".tres-decimales").keypress(function (e) {
    this.value = parseFloat(this.value.replace(/,/g, ""))
        .toFixed(2)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});


