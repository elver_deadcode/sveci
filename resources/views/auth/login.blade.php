@extends('layouts.login')
@section('content')
<div class="container">
  <div id="login" class="signin-card " style="background-color: #1c4385">
    <div class="logo-image">
      <img src="{{asset('img/logo.png')}}" alt="Logo" title="Logo" width="150">
    </div>
    <h1 class="display1" style="color:#FFF">SVEC - SMN</h1>
    <p class="subhead" style="color:#FFF">V1.1</p>
    <form action="{{ route('login') }}" method="POST" class="" role="form" aria-label="{{ __('Login') }}">
      @csrf
      <div id="form-login-username" class="form-group">
        <input id="username" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }} " name="username"
          value="{{ old('username') }}" required autofocus />
        <span class="form-highlight"></span>
        <span class="form-bar"></span>
        <label for="username" class="float-label">Usuario</label>
      </div>
      @if ($errors->has('username'))
      <span class=" text-danger invalid-feedback" role="alert">
        <strong>{{ $errors->first('username') }}</strong>
      </span>
      @endif
      <div id="form-login-password" class="form-group">
        <input id="password" type="password"
          class="input100 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
        <span class="form-highlight"></span>
        <span class="form-bar"></span>
        <label for="password" class="float-label">Contraseña</label>
      </div>
      <div id="form-login-remember" class="form-group row">
        <div class="col-md-7">
          <a class="" href="{{url('login/recuperar')}}" style="color:#FFF; font-size:1.1em"><strong>Olvide mis
              credenciales</strong></a>
        </div>
      </div>
      <div>
        <button class=" btn btn-block btn-info ripple-effect" type="submit" name="Submit" alt="sign in">Ingresar al
          sistema</button>
      </div>

  </form>
</div>
</div>
@stop
