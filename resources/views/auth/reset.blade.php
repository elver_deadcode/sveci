@extends('layouts.login')
@section('estilos')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@stop
@section('content')

<div class="container">
        <div id="login" class="signin-card " style="background-color: #1c4385">
          <div class="logo-image">
          {{-- <img src="{{asset('img/logo.png')}}" alt="Logo" title="Logo" width="150"> --}}
          </div>
          <h1 class="display1" style="color:#FFF">SVEC - SMN</h1>
          <br>
          <p class="subhead" style="color:#FFF">CAMBIAR MI CONTRASEÑA</p>
          <form action="{{ route('login') }}" method="POST" class="form-credenciales" role="form" aria-label="{{ __('Login') }}">
                @csrf
            <div id="form-login-username" class="form-group">
              <input  id="username" class="form-control  " 
              name="carnet" value="" maxlength="9" required autofocus/>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="username" class="float-label">Numero de  carnet</label>
            </div>
       
            <div id="form-login-password" class="form-group">
                    <input id="password" 
                    type="test" 
                    
                    pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"
                     class="datepicker input100 form-control" 
                    name="fecha_nacimiento" required>
                   
                    <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="password" class="float-label">Fecha Nacimiento (dia/mes/año)</label>
            </div>
          
            <div>
              <button class="btn-enviar btn btn-block btn-info ripple-effect" 
              type="button"  alt="sign in">Enviar datos</button>  
              </div>
          </form>

          <form action="" method="POST" class=" hide form-codigo" role="form" aria-label="{{ __('Login') }}">
            @csrf
            <div class="form-group">

              <label for=""><h5 style="color:#FFF" class="texto"> </h5></label>
            </div>
            <br>
            <div id="form-login-username" class="form-group">
          <input  id="username" class="form-control  " 
          name="codigo" value="" maxlength="5" required autofocus/>
          <span class="form-highlight"></span>
          <span class="form-bar"></span>
          <label for="username" class="float-label"> ESCRIBA EL CODIGO</label>
        </div>
   
      
      
        <div>
          <button class="btn-enviar-codigo btn btn-block btn-info ripple-effect" 
          type="button"  alt="sign in">Enviar datos</button>  
          </div>
      </form>


    <form action="{{url('login/guardar-credenciales')}}" method="POST" class="hide form-password" role="form" aria-label="{{ __('Login') }}">
          @csrf
          <input type="hidden" name="id" class="id-user">
          <div class="form-group">
            <h6 style="color:#FFF">SU CODIGO ES EL CORRECTO! INGRESE SU NUEVA CONTRASEÑA</h6>
            <ul class="list-unstyled list-user" style="color:#FFF">
              

            </ul>
          

          </div>
         
          <div id="form-login-username" class="form-group">
         <input type="text" name="password"   required   pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"  class="password form-control" >

        <span class="form-highlight"></span>
        <span class="form-bar"></span>
        <label for="password" class="float-label"> INGRESE NUEVA CONTRASEÑA</label>
        <p style="color:aliceblue; font-size:12px">Las contraseñas deben tener como minimo 7 caracteres, al  menos una mayuscula y al menos un numero.</p>
      </div>
 
    
    
      <div>
        <button class="btn-enviar-password btn btn-block btn-info ripple-effect" 
        type="button"  alt="sign in">GUARDAR CAMBIOS</button>  
        </div>
    </form>
    <br>
  <a href="{{url('login')}}" class="btn btn-block btn-default">REGRESAR</a>



        
            </div>
     </div>
            <script src="{{asset('js/app.js')}}"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="{{asset('js/jquery.blockUI.js')}}"></script>

            <script>
              var pattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;
            $(document).ready(function(){
              $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Anterior',
 nextText: 'Siguiente >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);


 $('.datepicker').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: '1950:2050',
            dateFormat: 'dd/mm/yy',
            
            
            });
                $('.btn-enviar').click(function(){
                    
                    let data=$('.form-credenciales').serialize();
                   $.ajax({
                       url:'{{url("login/generar-codigo")}}',
                       data:data,
                       type:'POST',
                       dataType:'json',
                       beforeSend:function(){
                        $.blockUI({ css: { 
            border: '2px solid #336699', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .6, 
            color: '#fff' 
        } ,message: '<h4><img  width="50px"src="{{asset('img/icono.png')}}" /><strong> Enviando datos</strong></h4>'}); 

                       },
                       complete:function(){
                        $.unblockUI();
                       },
                       success:function(response){
                         if(response.success){
                           if(response.status==200){
                            
                             $('.texto').html(response.msg)
                             $('.form-credenciales').addClass('hide')
                             $('.form-codigo').removeClass('hide')
                           }
                           else if(response.status==400){
                             alert(response.msg)
                             return false;
                           }
                         }

                       }

                   })
                })


                $('.btn-enviar-codigo').click(function(){
                    
                    let data=$('.form-codigo').serialize();
                   $.ajax({
                       url:'{{url("login/validar-codigo")}}',
                       data:data,
                       type:'POST',
                       dataType:'json',
                       beforeSend:function(){
                        $.blockUI({ css: { 
            border: '2px solid #336699', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .6, 
            color: '#fff' 
        } ,message: '<h4><img  width="50px"src="{{asset('img/icono.png')}}" /><strong> Validando codigo</strong></h4>'}); 
                       },
                       complete:function(){
                        $.unblockUI();
                       },
                       success:function(response){
                         if(response.success){
                           if(response.status==200){
                             let lista=`
                             <li><strong>Nombres: </strong>${response.user.persona.nombre_completo}</li>
              <li><strong>Num carnet: </strong>${response.user.persona.numeroCarnet}</li>
              <li><strong>Usuario: </strong>${response.user.username}</li>
                             `
                            
                            $('.list-user').html(lista)
                            $('.id-user').val(response.user.id)
                            $('.form-codigo').addClass('hide')
                            $('.form-password').removeClass('hide')
                           }
                           else if(response.status==400){
                             alert('El codigo  que ingreso no es corecto.')
                             return false;
                           }
                         }
                         

                       }

                   })
                })

                $('.btn-enviar-password').click(function(){
                  let pass=$('.password').val()
                  if (!pass.match(pattern) || pass=='') {
                      alert ('Contraseña no  valida. debe tener como minimo 7 caracteres, al  menos una mayuscula y al menos un numero')
                      return false;

                   }
                  if(confirm('Contraseña valida!!. Se cambiara su contraseña. esta seguro?')){
                    $('.form-password').submit();

                  }  
                    
               
                })

            


            })
            </script>
            @stop