@extends('layouts.error')

@section('code')
No autorizado
@stop
@section('mensaje')
 Actualmente estas dado de baja del sistema. ponte en contacto con un administrador del sistema.
@stop
@section('link')
 <form action="{{url('logout')}}" method="POST">
@csrf
<button  type="submit" class="btn btn-primary">Salir del sistema</button>
</form>
@stop