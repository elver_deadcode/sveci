@extends('layouts.error')

@section('code')
No autorizado
@stop
@section('mensaje')
  No tienes los permisos para ver el contenido de esta pagina. comunicate con el administrador. <br>
  <center>

      <a class="btn btn-lg btn-success" href="{{url('/home')}}">REGRESAR</a>
  </center>
@stop
