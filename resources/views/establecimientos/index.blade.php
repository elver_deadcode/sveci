@extends('layouts.app',[
    'active_registros'=>'active',
    'active_establecimiento'=>'active',
    'title'=>'Municipios'
])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Registros</a> <span class="bread-slash">/</span>
    </li>
  
    <li><span class="bread-blod">Establecimientos</span>
    </li>
</ul>
@stop
@section('content')


<div class="table-responsive">
        <table style="font-size: 10px" class="table datatable">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>ESTABLECIMIENTO</th>
                    <th>MUNICIPIO</th>
                    <th>DEPARTAMENTO</th>
                </tr>
            </thead>

        </table>
        </div>
 @section('scripts')
        <script>
        $(document).ready(function () {
            $('.datatable').DataTable({
             dom: "Bfrltip", 
        ajax: {
            url: "{{url('registros/establecimientos/listar')}}",
            type: 'GET'
              },
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'establecimiento', name: 'municipio'},
            {data: 'municipio.municipio', name: 'provincia_id'},
            {data: 'municipio.departamento.departamento', name: 'departamento_id'}
           
        ]
                
            });
        });
        </script>
 @stop
@stop