@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hola!!!</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                                @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
        
                            <p><strong>{{auth()->user()->persona->nombre_completo}}</strong></p>   
                          <h4>Bienvenido al sistema SVEC - SMN </h4><br>
                          <p>Sistema de Vigilancia Epidemiologica Comunitaria de Salud Materna y Neonatal</p>         
        
                        </div>

                        <div class="col-md-6">

                        <img src="{{asset('img/logo_grande.png')}}" alt="">
                        </div>
                    </div>
                  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
