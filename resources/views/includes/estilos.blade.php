<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- favicon
		============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="./img/favicon.ico" />
<!-- Google Fonts
		============================================ -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/app.css')}}" />

<link rel="stylesheet" href="{{asset('css/jquery.dataTables.yadcf.css')}}" />
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
@yield('styles')
<!-- Bootstrap CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" /> --}}
<!-- Bootstrap CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" /> --}}
<!-- adminpro icon CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/adminpro-custon-icon.css')}}" /> --}}
<!-- meanmenu icon CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}" /> --}}
<!-- mCustomScrollbar CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/jquery.mCustomScrollbar.min.css')}}" /> --}}
<!-- animate CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/animate.css')}}" /> --}}
<!-- normalize CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/normalize.css')}}" /> --}}
<!-- form CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/form.css')}}" /> --}}
<!-- switcher CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/switcher/color-switcher.css')}}" /> --}}
<!-- style CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/style.css')}}" /> --}}
<!-- responsive CSS
		============================================ -->
{{--
<link rel="stylesheet" href="{{asset('css/responsive.css')}}" /> --}} {{--
<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.18/css/jquery.dataTables.min.css')}}" /> --}} {{--
<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.18/css/dataTables.bootstrap4.css')}}" /> --}}
<!-- modernizr JS
		============================================ -->
{{--
<script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script> --}}
<!-- Color Css Files
		============================================ -->
<!-- <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-one.css" title="color-one" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-two.css" title="color-two" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="css/switcher/color-three.css" title="color-three" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-four.css" title="color-four" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-five.css" title="color-five" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="css/switcher/color-six.css" title="color-six" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-seven.css" title="color-seven" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-eight.css" title="color-eight" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-nine.css" title="color-nine" media="screen" />
	<link rel="alternate stylesheet" type="text/css" href="./css/switcher/color-ten.css" title="color-ten" media="screen" /> -->


	<style>
     .ui-datepicker {
		 width:400px;
	 }
	 div.ui-datepicker{
 font-size:10px;
}

		.yadcf-filter {
			width: 70px;
		}

.form-control:required{
	border:#429ef4 1px solid;
}
.form-control{
	border:#999 1px solid;
}


.bg-primary {background-color:#456;}

.badge-danger{
    background-color: rgb(244, 66, 75); color: aliceblue;
}




 .icon-nme{
     background-color: #ff80ab;
     padding: 0;
	 text-align: center; font-size: 16px;

 }
 .icon-nmm{
     background-color: #212121;
     padding: 0;
	 text-align: center; font-size: 16px;
 }



 .icon-nmb{
     background-color: #7b1fa2;
     padding: 0;
	 text-align: center;
	 font-size: 16px;
 }
  .icon-np{
     background-color: #f4511e;
     padding: 0;
	 text-align: center; font-size: 16px;
 }
	 .icon-nt{
		 background-color: #336699;
		 padding: 0;
		 text-align: center; font-size: 16px;
	 }
 @media (min-width: 1281px) {



}

/*
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {



}

/*
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {



}

/*
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {

	.titulo-logo{
 font-size: 8px;
}

}

/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {

	.titulo-logo{
 font-size: 8px;
}

}

/*
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {

.titulo-logo{
 font-size: 8px;
}

}



	</style>
