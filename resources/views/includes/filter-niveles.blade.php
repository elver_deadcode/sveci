<div class=" col-md-2 form-group">
    <label for="">Departamentos</label>
    <select class="departamento form-control"  name="dpto" id="">
        <option value="">Seleccione</option>
    @foreach($dptos as $k=>$dpto)
    <option value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
    @endforeach
    </select>
    
</div>

<div class="col-md-3 form-group">
    <label for="">Municipio</label>
    <select  class="municipio form-control"  name="municipio" id="">
        <option dptoid="0" value="">Seleccione</option>
    @foreach($municipios as $k=>$muni)
    <option dptoid={{$muni->provincia->departamento->dpt_codigo}} value="{{$muni->mnc_codigo}}">{{$muni->mnc_nombre}}</option>
    @endforeach
    </select>

</div>
<div class=" col-md-3 form-group">
    <label for="">Establecimientos</label>
    <select  class="establecimiento form-control" name="establecimiento" id="">
        <option value="">Seleccione</option>
    @foreach($establecimientos as $k=>$e)
    <option  value="{{$e->codestabl}}">{{$e->codestabl}} {{$e->self()->nomestabl}}</option>
    @endforeach
    </select>

</div>
