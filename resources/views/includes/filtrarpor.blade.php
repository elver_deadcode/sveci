@if(Auth::user()->nivel=='nacional'||Auth::user()->nivel=='depto'||Auth::user()->nivel=='red')
<div class="col-md-2 col-xs-6 col-sm-6 div-filtrarpor">
    <label for="">Mostrar Por</label>
    <select name="mostrarpor" class="mostrarpor form-control" id="">
        @switch(Auth::user()->nivel)
        @case('nacional')
        <optgroup class="nacional_filtro " label="Mostrar por">
            <option value="dpto">Departamentos</option>
            <option value="red">Redes</option>
            <option value="municipal">Municipios</option>
        </optgroup>
        <optgroup class="dpto_filtro hide" label="Mostrar por ">
            <option value="red">Redes</option>
            <option value="municipal">Municipios</option>
            <option value="establecimiento">Establecimiento</option>
        </optgroup>
        <optgroup class="red_filtro hide" label="Mostrar por">
            <option value="municipal">Municipios</option>
            <option value="establecimiento">Establecimiento</option>
        </optgroup>
        @break
        @case('depto')
        <optgroup class="dpto_filtro" label="Mostrar por">
            <option value="red">Redes</option>
            <option value="municipal">Municipios</option>
            <option value="establecimiento">Establecimiento</option>
        </optgroup>
        <optgroup class="red_filtro hide" label="Mostrar por">
            <option value="municipal">Municipios</option>
            <option value="establecimiento">Establecimiento</option>
        </optgroup>
        @break
        @case('red')
        <optgroup class="red_filtro" label="Mostrar por">
            <option value="municipal">Municipios</option>
            <option value="establecimiento">Establecimiento</option>
        </optgroup>
        @break

        @endswitch

    </select>
</div>
@endif