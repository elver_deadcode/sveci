<style>
    .checkbox input[type="checkbox"] {
        opacity: 1;
    }

    .dropdown-menu>.active>a:hover,
    .dropdown-menu>.active>a:focus {
        color: black;
        background-color: antiquewhite;
    }

    .btn-default {
        border-color: #999 !important;
    }

    .btn {
        padding: 8px 12px !important;
    }
</style>
<div class=" row col-md-12 ">
    @if(!isset($sinubicacion))
    @include('includes.filtrarpor')
    @endif
    <div class="col-md-2 col-xs-6">
        <label for="">Filtro de tiempo</label>
        <select class="form-control filtrotiempo" name="periodo">
            <option value="anio">Por año</option>
            <option value="periodo">Por periodo</option>
        </select>
    </div>
    <div class="div-porperiodo col-md-4 row hidden">

        <div class="form-group col-md-6 col-xs-6">
            <label>Fecha inicial</label>
            <input required type="text" readonly name="fechaini" class="form-control datepicker" placeholder="d/m/y" />
        </div>

        <div class="form-group col-md-6 col-xs-6">
            <label>Fecha final</label>
            <input required type="text" readonly name="fechafin" id="zip" class="form-control datepicker"
                placeholder="d/m/Y" />
        </div>

    </div>
    <div class="div-poranio ">
        <div class="col-md-2 col-xs-6">
            <label for="">Meses</label><br>
            <select name="meses" style="font-size:10px" class="form-control" id="multiselect" multiple="multiple">
                <option value="1">ENERO</option>
                <option value="2">FEBRERO</option>
                <option value="3">MARZO</option>
                <option value="4">ABRIL</option>
                <option value="5">MAYO</option>
                <option value="6">JUNIO</option>
                <option value="7">JULIO</option>
                <option value="8">AGOSTO</option>
                <option value="9">SEPTIEMBRE</option>
                <option value="10">OCTUBRE</option>
                <option value="11">NOVIEMBRE</option>
                <option value="12">DICIEMBRE</option>
            </select>
        </div>

        <div class="col-md-2 col-xs-6">
            <label for="">Año</label>
            <select class="form-control " name="anio">
                @for($i=2019;$i<=2100;$i++) <option  @if(date('Y')==$i) selected @endif value="{{$i}}">{{$i}}</option>
                    @endfor
            </select>
        </div>
    </div>
    @isset($reportevigilantes)
    <div class="col-md-2" style="margin-top:-8px;">

        <label for="" style="font-size:10px">Num de meses que se considera Vigilante inactivo</label>


        <input type="text" class="form-control solo-enteros" value="2" name="meses">
    </div>
    @endisset
    <div class="col-md-2">
        <br>
        <button style="margin-top:5px;" type="button" class="btn-consultar btn btn-block btn-primary"> <i
                class="fa fa-search"></i>
            CONSULTAR</button>
    </div>

</div>
@push('custom-scripts')
<script>
    $(document).ready(function(){
$('.filtrotiempo').change(function(){
if($(this).val()=='anio'){
$('.div-poranio').show()
$('.div-porperiodo').hide()
}
if($(this).val()=='periodo'){
$('.div-poranio').hide()
$('.div-porperiodo').show()
$('.div-porperiodo').removeClass('hidden')
}
})


$('#multiselect').multiselect({

includeSelectAllOption: true,
allSelectedText: 'Todo el año ',
selectAllText:'Todo el año',
selectAllValue:'todos',
selectAllName: 'todosmeses',
checkboxName: function(option) {
return 'multiselect[]';
},



nonSelectedText: 'Meses seleccionados ',
onChange: function(option, checked, select) {
let column = option.context.value

saveColumns(checked,column)
hideColumn(column)


}
})
.multiselect('selectAll', false)
.multiselect('updateButtonText');
})
</script>
@endpush
