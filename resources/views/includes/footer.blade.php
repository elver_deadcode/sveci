<div class="footer-copyright-area bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copy-right">
                    <p>Copyright &#169; {{date('M Y')}} <a href="">{{ config('app.name') }}</a>
                        Todo los
                        derechos Reservados.</p>
                </div>
            </div>
        </div>
    </div>
</div>