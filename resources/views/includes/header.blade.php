<div class="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div class="admin-logo" >
                    <a href="{{url('/home')}}">
                        <img src="{{asset('img/logo.png')}}" alt="logo" width="40" />
                        <span  class="titulo-logo" style="color:aliceblue;font-size:1.6em; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif">SVEC-SMN</span>
                        </a>
                </div>
            </div>

            <div class="col-lg-10 col-md-9 col-sm-6 col-xs-6">
                <div class="header-right-info">
                    <ul class=" listamaestra nav navbar-nav mai-top-nav header-right-menu">


                        <li>
                            <a href="javascript:void(0)">

                                <span style="font-size: 10px">{{\Auth::user()->obtenerNivelUsuario()}}</span>
                            </a>
                        </li>
                        <li id="menu-noti" class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-bell-o" aria-hidden="true"></i><span class="badge badge-danger badge-no-visto">3</span></a>
                           {{-- <span style="font-size: 10px">{{\Auth::user()->obtenerNivelUsuario()}}</span> --}}
                        </li>
                        <li class="nav-item">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                    <span class="adminpro-icon adminpro-user-rounded header-riht-inf" ></span>
                                    <div class="admin-name">
                                        {{\Auth::user()->username}} <br>
                                    </div>
                                    <span class="fa fa-angle-down"></span>
                                </a>
                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">

                                <li>
                                <form action="{{ url('logout') }}" id="form-logout" method="POST">

                                    @csrf
                                        <a href="#" class="send-form-logout"><span class="fa fa-close"></span>Salir</a>

                                    </form>

                                </li>
                                <li>
                                        <a href="{{ url('usuarios/'.Auth::user()->id.'/editarperfil') }}" class="send-form-logout"><span class="fa fa-edit"></span>Editar mi Usuario</a>

                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
