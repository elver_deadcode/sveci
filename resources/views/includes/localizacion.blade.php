<fieldset class="form-group col-xs-12">
  <div class="">
    <div class="">
      @if(auth()->user()->nivel=="nacional")
      <div class="d-flex col-md-1 col-xs-4 col-sm-4">
        <label for="">Nacional</label>
        <input type="radio" @if(auth()->user()->nivel=="nacional") ) checked @endif id="cb-nacional" value="nacional"
        name="nivel">
      </div>
      @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto")
      <div class="flex-box col-md-2 col-xs-4 col-sm-4">
        <label for="">Departamental</label><br>
        <input type="radio" @if(auth()->user()->nivel=="depto") checked @endif id="cb-depto" value="depto" name="nivel">
      </div>

      @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" || auth()->user()->nivel=="red")
      <div class="col-md-1 col-xs-4 col-sm-4">
        <label for="">Red</label><br>
        <input type="radio" id="cb-red" value="red" name="nivel">
      </div>
      @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" || auth()->user()->nivel=="muni" )
      <div class="col-md-1 col-xs-4 col-sm-4">
        <label for="">Municipal</label>
        <input type="radio" id="cb-muni" value="muni" name="nivel">
      </div>
      @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" || auth()->user()->nivel=="red" ||
      auth()->user()->nivel=="muni"
      || auth()->user()->nivel=="establ" )
      <div class="col-md-2 col-xs-4 col-sm-4">
        <label for="">Establecimiento</label>
        <input type="radio" @if(isset($user) && $user->nivel=='establ') checked @endif
        @if(\Auth::user()->nivel=='establ')checked
        @endif id="cb-esta" value="establ" name="nivel">
      </div>


      @endif


    </div>
    <div class="col-sm-12">

      <div @if(isset($user) && $user->nivel=='depto') style="display:block" @else style="display:none" @endif
        class="col-md-2 div-dptos">
        <label for="">Departamentos</label>
        <select class="departamento form-control" name="dpto" id="">
          <option value="">Seleccione</option>
          @foreach($dptos as $k=>$dpto)
          <option value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
          @endforeach
        </select>
      </div>

      <div @if(isset($user) && $user->nivel=='red') style="display:block" @else style="display:none" @endif
        class="col-md-2 div-red">
        <label for="">Redes</label>
        <select class="red form-control" name="red" id="">
          <option value="">Seleccione</option>
          @foreach($redes as $k=>$red)
          <option dptoid="{{$red->dpt_codigo}}" value="{{$red->are_codigo}}">{{$red->depto->dpt_nombre}} -
            {{$red->are_nombre}}</option>
          @endforeach
        </select>
      </div>
      <div @if(isset($user) && $user->nivel=='muni') style="display:block" @else style="display:none" @endif
        class="col-md-3 form-group div-municipios">
        <label for="">Municipio</label>
        <select class="municipio form-control" name="municipio" id="">
          <option dptoid="0" value="">Seleccione</option>
          @foreach($municipios as $k=>$muni)
          <option dptoid={{$muni->provincia->departamento->dpt_codigo}} value="{{$muni->mnc_codigo}}">
            {{$muni->mnc_nombre}}</option>
          @endforeach
        </select>

      </div>
      <div @if(isset($user) && $user->nivel=='establ') style="display:block" @else style="display:none" @endif
        @if(\Auth::user()->nivel!='establ')
        style="display:none"@endif class="col-md-3 form-group div-estable">
        <label for="">Establecimientos</label> @if(\Auth::user()->nivel=='establ')
        <input type="text" readonly class="form-control" value="{{$establecimientos[0]->nomestabl}}">
        <input type="hidden" class="form-control" name="establecimiento" value="{{$establecimientos[0]->codestabl}}">
        @else
        <select class="establecimiento form-control" name="establecimiento" id="">
          <option value="">Seleccione</option>

        </select> @endif



      </div>
    </div>
  </div>
</fieldset>

@push('custom-scripts')
<script>
  function mostrarFiltroPor(tipo){
    $('.mostrarpor optgroup').each(function(k,v){
      $(v).addClass('hide')
      if($(v).hasClass(tipo+'_filtro')){
        
        $(v).removeClass('hide')
      }
      else{
        $(v).addClass('hide')
      }
    })
    $('.mostrarpor').find('.'+tipo+'_filtro  option:first').prop("selected", "selected");
  }
  function validarLocalizacion(){
    
    if($('#cb-depto').is(':checked'))
    {
        @if(auth()->user()->nivel=="nacional" )

        if($('.departamento').val()==''){
            $.alert({ title: 'Alerta!', content: 'Debe seleccionar un Departamento.', });
            return false;
        }
        @endif
       
    }
    if($('#cb-red').is(':checked'))
    {
        @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" )
        if($('.red').val()==''){
            $.alert({ title: 'Alerta!', content: 'Debe seleccionar una  Red.', });
            return false;
        }
        @endif
    }
     if($('#cb-muni').is(':checked'))
    {
        @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" || auth()->user()->nivel=="red")
        if($('.municipio').val()==''){
            $.alert({ title: 'Alerta!', content: 'Debe seleccionar un Municipio.', });
            return false;
        }
        @endif
    }

      if($('#cb-esta').is(':checked'))
    {
        @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" || auth()->user()->nivel=="muni")
        if($('.establecimiento').val()==''){
            $.alert({ title: 'Alerta!', content: 'Debe seleccionar un Establecimiento de salud.', });
            return false;
        }
        @endif
    }
 }
       $('.btn-buscar').click(function(){
            var data=$('.form-buscar').serializeArray();
          $('.lista-noti').empty();
            buscar=data;
            addFila('todas')
        })

        $('#cb-nacional').click(function(){
          $('.div-filtrarpor').show();
mostrarFiltroPor('nacional')
            if($(this).is(':checked')){
            $('.div-dptos').hide();
            $('.div-municipios').hide();
            $('.div-estable').hide();
            $('.div-red').hide();
            }
          })

              $('#cb-depto').click(function(){
                $('.div-filtrarpor').show();
               mostrarFiltroPor('dpto')
            if($(this).is(':checked')){
            $('.div-dptos').show();
            $('.div-red').hide();
            $('.div-municipios').hide();
            $('.div-estable').hide();
            }
          })

             $('#cb-red').click(function(){
            $('.div-filtrarpor').show();
               mostrarFiltroPor('red')
            if($(this).is(':checked')){
              @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" )
            $('.div-dptos').show();
            $('.div-red').show();
            @else
            $('.div-red').hide();

            @endif

            $('.div-municipios').hide();
            $('.div-estable').hide();
            }
          })

             $('#cb-muni').click(function(){
               $('.div-filtrarpor').hide();
            if($(this).is(':checked')){
            @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto"  )
            $('.div-dptos').show();
            $('.div-municipios').show();
            @else
            $('.div-municipios').hide();
            @endif
            $('.div-estable').hide();
            $('.div-red').hide();
            }
          })

             $('#cb-esta').click(function(){
$('.div-filtrarpor').hide();
            if($(this).is(':checked')){

              @if(auth()->user()->nivel=="nacional")
            $('.div-dptos').show();
            $('.div-municipios').show();
            $('.div-estable').show();
            @endif
            @if(auth()->user()->nivel=="depto")
              $('.div-municipios').show();
            $('.div-estable').show();
            @endif
              @if(auth()->user()->nivel=="red")
              $('.div-red').show();
            $('.div-estable').show();
            $('.red').val('');
            @endif

             @if(auth()->user()->nivel=="muni")
             $('.div-municipios').show();
            $('.div-estable').show();
            @endif

            }
          })

      $('.departamento').change(function(){
    //   alert('asd')
      var _dpto= $(this).val();
     $('.municipio').val('');
      $(".municipio option").each(function(i,o) {
       
            if ($(o).attr("dptoid") ===_dpto) {
                $(o).show()
            }
            else{
                $(o).hide()
            }
       });
     $('.red').val('');
      $(".red option").each(function(i,o) {
       
            if ($(o).attr("dptoid") ===_dpto) {
                $(o).show()
            }
            else{
                $(o).hide()
            }
       });

            
        })

  $('.municipio').change(function(){
         var id=$(this).val();
         var option="";
     $.ajax({
       url:"{{url('ajax/establecimientos-municipio')}}",
       type:'GET',
       dataType:'json',
       data:{idmuni:id},
       success:function(res){
        $('.establecimiento').find('option').not(':first').remove();
          
        $('.establecimiento').append(res.options);

       },
     })

  })

    $('.red').change(function(){
         var id=$(this).val();
         var option="";
     $.ajax({
       url:"{{url('ajax/establecimientos-red')}}",
       type:'GET',
       dataType:'json',
       data:{idred:id},
       success:function(res){
        $('.establecimiento').find('option').not(':first').remove();
          
        $('.establecimiento').append(res.options);

       },
     })

  })
    
</script>
@endpush