<style>

.navbar-default {
    -webkit-box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    -moz-box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    background-color: #2c5784;
    color
    margin-bottom: 0;
    border: none;
}


.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
    color: #FFF;
    background-color: transparent;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:after {
   border-top:solid 2px #0cd4d2;

}

.navbar-default .dropdown-menu {
    padding: 0;
    font-size: 14px;
    background-color: #ffffff;
    color: #878c94;
    border: none;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    -ms-border-radius: 0;
    border-radius: 0;
    -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
    -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
    -ms-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
    box-shadow: 0 1px 5px rgba(0, 0, 0, 0.08);
    -webkit-transition: all 0.1s ease-in;
    -moz-transition: all 0.1s ease-in;
    -ms-transition: all 0.1s ease-in;
    -o-transition: all 0.1s ease-in;
    transition: all 0.1s ease-in;
}

.dropdown-menu > li > a {
    display: block;
    padding: 6px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #222;
    white-space: nowrap;

}
@media (min-width: 481px) and (max-width: 767px) {

    .dropdown-menu > li > a {
    display: block;
    padding: 6px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #FFF !important;
    white-space: nowrap;

}
}

/*
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {

    .dropdown-menu > li > a {
    display: block;
    padding: 6px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #FFF !important;
    white-space: nowrap;

}

}
.dropdown-menu > li > a:hover{
     color: #ffff;
     background-color: #336699;

}

/***********************
     megaDropMenu
////////////////////////////*/

.navbar-default .container{
    position:relative;
}
.navbar-default .navbar-collapse li.dropdown.megaDropMenu {
    position: static;
}

.navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu{
    width: 100%;
}
.navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li:first-child{
    padding: 20px 0px 5px 15px;
    font-size: 16px;
    text-transform: uppercase;
    /* text-align: center; */
    color: #336699;
}
.navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li a{
    display:block;color:#878c94;font-size: 14px;text-decoration:none;padding:8px 15px;
}
.navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu li ul li a:hover{
    color: #ffff;
     background-color: #336699;
    text-decoration: none;
   /* background-color: rgba(0, 0, 0, 0.02); */
}

.navbar-default .navbar-collapse li.dropdown.megaDropMenu .dropdown-menu img{
    display: block;
    max-width: 100%;
    padding: 20px;
}
.navbar-default .navbar-nav > li > a { color: #FFF !important; }
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
    color: #555;
    background-color: rgba(0, 0, 0, 0.3) ;
   border-top:solid 3px #0cd4d2;

}


</style>
<nav class="navbar navbar-main navbar-default" role="navigation" style="opacity: 1;">
    <div class="container">
        <!-- Brand and toggle -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-1">

                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
        </div>

        <!-- Collect the nav links,  -->
        <div class="collapse navbar-collapse navbar-1" style="margin-top: 0px;">
            <ul class="nav navbar-nav">
                <li><a href="{{url('home')}}" ><i class=" fa fa-home"></i> Inicio</a></li>
            <li><a href="{{url('manual')}}" ><i class=" fa fa-book"></i> Manual del usuario</a></li>

            @permission( 'reportes_consolidados','reportes_nominales')

                <li class="dropdown megaDropMenu {{$active_tab_reportes ?? ''}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class=" fa fa-pie-chart"></i> Reportes <i class="fa fa-angle-down ml-5"></i></a>
                    <ul class="dropdown-menu row">
                            @permission( 'reportes_consolidados')
                        <li class="col-sm-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li><strong class="text-primary">REPORTES CONSOLIDADOS</strong></li>
                               <li><a href="{{url('reportes/mujeres-embarazadas')}}">1. MUJERES EMBARAZADAS IDENTIFICADAS  POR VIGILANTE COMUNITARIO</a></li>
                            <li><a href="{{url('reportes/mujeres-parto')}}">2. PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO</a></li>
                            <li><a href="{{url('reportes/muertes-vigilante')}}">3. MUERTES FETALES Y NEONATALES IDENTIFICADAS POR VIGILANTE COMUNITARIO</a></li>
                            <li><a href="{{url('reportes/muertes-autopsia')}}">4. MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO</a></li>
                            <li><a href="{{url('reportes/consolidado-vigilantes')}}">5. VIGILANTES COMUNITARIOS </a></li>
                            <li><a href="{{url('reportes/consolidado-teleconsultas')}}">6. NOTIFICACIONES DE TELECONSULTAS </a></li>
                            </ul>
                        </li>
                        @endpermission
                        @permission( 'reportes_nominales')
                        <li class="col-sm-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li class="text-danger"><strong class="text-primary">REPORTES NOMINALES</strong></li>
                                <li><a href="{{url('reportes/listavigilantes-comunitarios')}}">1. LISTA DE  VIGILANTES COMUNITARIOS</a></li>
                                <li><a href="{{url('reportes/lista-mujeres')}}">2. LISTA DE  MUJERES DE 10 A 59 AÑOS</a></li>
                                <li><a href="{{url('reportes/notificacion-embarazo')}}">3. NOTIFICACIONES EMBARAZO </a></li>
                                <li><a href="{{url('reportes/notificacion-muertemujer')}}">4. NOTIFICACIONES MUERTE MUJER </a></li>
                                <li><a href="{{url('reportes/notificacion-muertebebe')}}">5. NOTIFICACIONES MUERTE BEBE </a></li>
                                <li><a href="{{url('reportes/notificacion-parto')}}">6. NOTIFICACIONES PARTO </a></li>
                            </ul>
                        </li>
                        @endpermission

                    </ul>
                </li>
                @endpermission
                @permission( 'editar_vigilantes','crear_vigilantes')
                <li class="dropdown {{$active_tab_vigilantes ?? ''}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class=" fa fa-eye"></i>Vigilantes <i class="fa fa-angle-down ml-5"></i></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="{{url('vigilantes')}}">LISTA DE VIGILANTES</a></li>
                        @permission( 'crear_vigilantes')
                        <li><a href="{{url('vigilantes/crear')}}">REGISTRAR NUEVO VIGILANTE</a></li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission( 'crear_mujeres','editar_mujeres')
                <li class="dropdown {{$active_tab_mujeres ?? ''}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class=" fa fa-female"></i> Mujeres <i class="fa fa-angle-down ml-5"></i></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="{{url('mujeres')}}">LISTA DE MUJERES</a></li>
                        @permission( 'crear_mujeres')
                    <li><a href="{{url('mujeres/create')}}">REGISTRAR UNA MUJER</a></li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission( 'crear_notificaciones','registrar_indicadores','anular_notificacion')
                <li class="{{$active_tab_notificaciones ?? ''}}"><a href="{{url('notificaciones')}}"><i class=" fa fa-bell"></i> Notificaciones</a></li>
                @endpermission


                @permission( 'crear_rol','editar_rol','borrar_rol')
                 <li class="dropdown {{$active_tab_roles ?? ''}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class=" fa fa-lock"></i> Roles y Permisos <i class="fa fa-angle-down ml-5"></i></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="{{url('roles')}}">LISTA DE ROLES</a></li>
                        @permission( 'crear_rol')
                        <li><a href="{{'roles/nuevo'}}">REGISTRAR NUEVO ROL</a></li>
                        @endpermission
                    </ul>
                 </li>
                @endpermission

                @permission( 'crear_usuarios','editar_usuarios','baja_usuarios')
            <li class="dropdown {{$active_tab_usuarios ?? ''}}">
                <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><i class="fa fa-users"></i> Usuarios <i class="fa fa-angle-down ml-5"></i></a>
                <ul class="dropdown-menu dropdown-menu-left">
                    <li><a href="{{url('usuarios/')}}">LISTA DE USUARIOS</a></li>
                    @permission( 'crear_usuarios')
                    <li><a href="{{url('usuarios/nuevo')}}">REGISTRAR USUARIO</a></li>
                    @endpermission
                    <li><a href="{{url('usuarios/nuevoapi')}}">REGISTRAR USUARIO API</a></li>

                </ul>
            </li>
            @endpermission
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

