<div class="col-md-4 col-xs-6 col-sm-6">
    <label for="">Mes</label>
    <select class="mes form-control" name="mes">
        <option @if(date('m')=="1" ) selected @endif value="1">ENERO</option>
        <option @if(date('m')=="2" ) selected @endif value="2">FEBRERO</option>
        <option @if(date('m')=="3" ) selected @endif value="3">MARZO</option>
        <option @if(date('m')=="4" ) selected @endif value="4">ABRIL</option>
        <option @if(date('m')=="5" ) selected @endif value="5">MAYO</option>
        <option @if(date('m')=="6" ) selected @endif value="6">JUNIO</option>
        <option @if(date('m')=="7" ) selected @endif value="7">JULIO</option>
        <option @if(date('m')=="8" ) selected @endif value="8">AGOSTO</option>
        <option @if(date('m')=="9" ) selected @endif value="9">SEPTIEMBRE</option>
        <option @if(date('m')=="10" ) selected @endif value="10">OCTUBRE</option>
        <option @if(date('m')=="11" ) selected @endif value="11">NOVIEMBRE</option>
        <option @if(date('m')=="12" ) selected @endif value="12">DICIEMBRE</option>
    </select>
</div>
<div class="col-md-4 col-xs-6 col-sm-6">
    <label for="">Año</label>
    <select class="anio form-control" name="anio">
        @for($i=2019;$i<=2050;$i++) <option @if(date('Y')==$i ) selected @endif value="{{$i}}">{{$i}}</option>
            @endfor
    </select>
</div>