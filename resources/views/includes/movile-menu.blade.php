<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <li><a data-toggle="collapse" data-target="#Charts" href="{{url('/')}}">Inicio <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                           
                            </li>
                            <li><a data-toggle="collapse" data-target="#demo" href="#">Vigilantes <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                <ul id="demo" class="collapse dropdown-header-top">
                                <li><a href="{{url('vigilantes')}}">Lista de vigilantes</a>
                                    </li>
                                    <li><a href="{{'vigilantes/crear'}}">Crear nuevo vigilante</a>
                                    </li>
                                 
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#others" href="#">Mujeres <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                <ul id="others" class="collapse dropdown-header-top">
                                    <li><a href="{{url('mujeres')}}">Lista de Mujeres</a>
                                    </li>
                                    <li><a href="{{url('mujeres/crear')}}">Registrar a una mujer</a>
                                    </li>
                                   
                                </ul>
                            </li>
                        <li><a  data-target="#Miscellaneousmob" href="{{url('notificaciones')}}">Notificaciones <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                            
                            </li>
                        <li><a  data-target="#Chartsmob" href="{{url('reportes')}}">Reportes <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                               
                            </li>
                            <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Roles y Permisos <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                <ul id="Tablesmob" class="collapse dropdown-header-top">
                                    <li><a href="{{url('/roles')}}">Lista de Roles</a>
                                    </li>
                                <li><a href="{{url('roles/nuevo')}}">Crear un rol de usuario</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Usuarios <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                <ul id="Tablesmob" class="collapse dropdown-header-top">
                                    <li><a href="{{url('usuarios')}}">Lista de usuarios</a>
                                    </li>
                                    <li><a href="{{url('usuarios/nuevo')}}">Registrar a un nuevo usuario</a>
                                    </li>
                                   
                                </ul>
                            </li>

                          
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>