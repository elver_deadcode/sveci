
@if(session()->has('success'))
<script>
Lobibox.notify('success', {
             title:'Exito',
             position: 'top right',
                    size: 'mini',
                    msg: "{{session('success')}}",
                    sound: true,
                });
</script>
    @elseif(session()->has('info'))
    <script>
   Lobibox.notify('info', {
             title:'Informacion',
             position: 'top right',
                    size: 'mini',
                    msg: "{{session('info')}}",
                    sound: true,
                });
    </script>
@elseif(session()->has('warning'))
    <script>
   Lobibox.notify('warning', {
             title:'Alerta',
             position: 'top right',
                    size: 'mini',
                    msg: "{{session('warning')}}",
                    sound: true,
                });
    </script>
    
@elseif(session()->has('error'))
    <script>
    Lobibox.notify('error', {
             title:'Error',
             position: 'top right',
                    size: 'mini',
                    msg: "{{session('error')}}",
                    sound: true,
                });
    </script>

@elseif(session()->has('default'))
<script>
    Lobibox.notify('defaul', {
             title:'Mensaje',
             position: 'top right',
                    size: 'mini',
                    msg: "{{session('default')}}",
                    sound: true,
                });
</script>
@endif



