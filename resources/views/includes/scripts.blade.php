<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/push.js')}}"></script>
<script src="{{asset('serviceworker.js')}}"></script>
<script src="{{asset('js/socket.io.dev.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.blockUI.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>


function blocUI(mensaje){
    $.blockUI({ css: {
            border: '2px solid #336699',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .6,
            color: '#fff'
        } ,message: '<h4><img  width="50px"src="{{asset('img/icono.png')}}" /><strong> '+mensaje+'</strong></h4>'});

}


$(function(){
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Anterior',
 nextText: 'Siguiente >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);


 $('.datepicker').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: '1950:2050',
            dateFormat: 'dd/mm/yy',


            });


  @if (session()->has('success'))
    Lobibox.notify('success', {
        size: 'normal',
        sound: true,
        position: 'top left',
    rounded: true,
    delayIndicator: false,
    msg: '{{session("success")}}'
});


@endif

@if (session()->has('warning'))
    Lobibox.notify('warning', {
        size: 'normal',
        sound: true,
        position: 'top left',
    rounded: true,
    delayIndicator: false,
    msg: '{{session("warning")}}'
});


@endif

@if (session()->has('error'))
    Lobibox.notify('error', {
        size: 'normal',
        sound: true,
        position: 'top left',
    rounded: true,
    delayIndicator: false,
    msg: '{{session("error")}}'
});


@endif


    })
    @if (auth()->user())

    var socket = io.connect('http://200.87.64.130:3000');
    // var nivel


    socket.on('nueva-notificacion', function (data) {

      mostrarCabecera(data);

 mostrarNotificacion("SVECI::Nueva Notificacion",data.mensaje,data.id)
 console.log(data)
    });
      function contarNotificaciones(){
                $.ajax({
                type: "GET",
                dataType: "json",
                url: "{{url('notificaciones/contar-todas-notificaciones')}}",

                success: function (response) {

                   if(response.status){
                       var contador=response.contador;

                     $('.badge-no-visto').html(contador);



                   }
                }
            });
           }

               function mostrarCabecera(data){
         var nivel="{{\Auth::user()->nivel}}"
         var idnivel={{\Auth::user()->idnivel}}
         var no_vistos=parseInt($('.badge-no-visto').html())
         switch(nivel){
             case 'nacional':
                             no_vistos++;
                             break;
            case 'depto':
                            if(idnivel==data.dpt_codigo){
                                no_vistos++;
                            }
                            break;

             case 'muni':
                            if(idnivel==mnc_codgo){
                                no_vistos++;
                            }
                            break;
              case 'establ':
                            if(idnivel==codEstablecimiento){
                                no_vistos++;
                            }
         }
         $('.badge-no-visto').html(no_vistos);

  var audio=new Audio("{{asset('/sounds/notification.mp3')}}"); audio.play();


     }

    function mostrarNotificacion(titulo,mensaje,id){
 Push.create(titulo, {
        body: mensaje,
        icon: '{{asset("img/icono.png")}}',
        link: "{{url('notificaciones')}}",
        timeout: 10000,
        onClick: function () {
           window.focus();
            this.close();
        },
        vibrate: [200, 100, 200, 100, 200, 100, 200]
    });

    }


contarNotificaciones();

 $('#menu-noti').click(function(){

          $.ajax({
              type: "get",
              url: "{{url('notificaciones/mostrar-cabecera')}}",
              dataType: "html",
              success: function (response) {
                  $('#menu-noti').append(response)
                  $(".message-menu, .notification-menu, .comment-scrollbar, .notes-menu-scrollbar, .project-st-menu-scrollbar").mCustomScrollbar({
					autoHideScrollbar: true,
					scrollbarPosition: "outside",
					theme:"light-1"

				});


              }
          });
       })

           $('.send-form-logout').click(function(){
          $('#form-logout').submit();
        })
 @endif

</script>

{{--
<script src="{{asset('js/main.js')}}"></script> --}}


@yield('scripts')
@stack('custom-scripts')


