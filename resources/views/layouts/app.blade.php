<!doctype html>
<html class="no-js" lang="es">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>{{$title ?? 'SVEC'}}</title>
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  @include('includes.estilos')
  @laravelPWA
</head>

<body id="app">
  <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
  <!-- Header top area start-->
  @include('includes.header')
  <!-- Header top area end-->
  <!-- Main Menu area start-->

  @include('includes.menu')
  <!-- Main Menu area End-->
  <!-- Mobile Menu start -->

  {{-- @include('includes.movile-menu') --}}
  <!-- Mobile Menu end -->
  <!-- Breadcome start-->
  {{-- <div class="breadcome-area mg-t-15" style="margin-top:10px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcome-list shadow-reset">
                    <div class="breadcome-heading">
                        <h2>Contacts Form1</h2>
                    </div>
                    <ul class="breadcome-menu">
                        <li><a href="#">Home</a> <span class="bread-slash">/</span>
                        </li>
                        <li><a href="#">Graphs</a> <span class="bread-slash">/</span>
                        </li>
                        <li><span class="bread-blod">Contacts Form</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> --}}
  <div class="" style="margin: 2px 15px 2px 15px">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <div class="pull-left">

          <h4>@yield('titulo')</h4>
          {{-- <ul class="breadcome-menu">
            <li><a href="#">Home</a> <span class="bread-slash">/</span>
            </li>
            <li><a href="#">Graphs</a> <span class="bread-slash">/</span>
            </li>
            <li><span class="bread-blod">Contacts Form</span>
            </li>
        </ul> --}}
          @yield('breadcome')
        </div>
        <div class="pull-right">
          <a href="#" class="btn btn-default btn-sm"><i class="fa fa-expand"></i></a>

        </div>
      </div>
      <div class="panel-body">
        @yield('content')
      </div>
    </div>
  </div>
  <!-- Breadcome End-->
  <!-- Contact Start-->

  <!-- Contact End-->
  <!-- Footer Start-->
  @include('includes.footer')

  <!-- Footer End-->
  <!-- Color Switcher -->

  <!-- Color Switcher end -->
  <!-- jquery
		============================================ -->
  {{-- <script src="{{asset('/js/vendor/jquery-1.11.3.min.js')}}"></script> --}}
  <!-- bootstrap JS
		============================================ -->
  {{-- <script src="{{asset('js/bootstrap.min.js')}}"></script> --}}
  <!-- meanmenu JS
      ============================================ -->
  {{-- <script src="{{asset('plugins/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('js/jquery.meanmenu.js')}}"></script> --}}
  <!-- mCustomScrollbar JS
		============================================ -->
  {{-- <script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script> --}}
  <!-- sticky JS
		============================================ -->
  {{-- <script src="{{asset('js/jquery.sticky.js')}}"></script> --}}
  <!-- scrollUp JS
		============================================ -->
  {{-- <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script> --}}
  <!-- form validate JS
		============================================ -->
  {{-- <script src="{{asset('js/jquery.form.min.js')}}"></script> --}}
  {{-- <script src="{{asset('js/jquery.validate.min.js')}}"></script> --}}
  {{-- <script src="{{asset('js/form-active.js')}}"></script> --}}
  <!-- switcher JS
		============================================ -->
  {{-- <script src="{{asset('js/switcher/styleswitch.js')}}"></script> --}}
  {{-- <script src="{{asset('js/switcher/switch-active.js')}}"></script> --}}
  <!-- main JS
		============================================ -->
  {{-- <script src="{{asset('js/main.js')}}"></script> --}}
  @include('includes.scripts')

  {{-- @include('includes.notifications') --}}


</body>

</html>
