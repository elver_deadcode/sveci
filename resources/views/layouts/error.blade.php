<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

   <link rel="stylesheet" href="{{asset('css/error.css')}}" />
</head>

<body>
    <div class="text-wrapper">
        <div class="title" data-content="404">
           @yield('code')
        </div>

        <div class="subtitle">
            @yield('mensaje')
        </div>

        <div class="buttons">

            @yield('link')
            {{-- <a class="button" href="http://www.ajerez.es">Go to homepage</a> --}}
        </div>
    </div>

</body>

</html>