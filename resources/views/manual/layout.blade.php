@extends('layouts.app',[ 'active_tab_usuarios'=>'active', 'active_usuarios'=>'active', 'title'=>'Video tutoriales' ]) {{-- 
    @section('titulo')Municipios
    
    @stop --}} 
   
    @section('breadcome')
    
    <ul class="breadcome-menu">
        <li><a href="#">  </a> <span class="bread-slash"><i class="fa fa-book"></i> MANUAL DEL USUARIO</span>
        </li>
    
        <li><span class="bread-blod"></span>
        </li>
    </ul>
    
    @stop 
    @section('content')
    
    <div class="">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="panel-group" id="accordion">

                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse"   data-parent="#accordion" href="#collapseOne"><span class="fa fa-plug">
                                        </span> Ingreso y menu del sistema</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse {{$ingreso ?? ''}}">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr><td><a href="###" class="menu" index="0">Ingresar al Sistema</a></td></tr>
                                            <tr><td><a href="###" class="menu" index="1">Menu del sistema</a></td></tr>
                                            {{-- <tr><td><a href="###" class="">Salir del sistema</a></td> </tr> --}}
                                        </table>
                                    </div>
                                </div>
                            </div>
                    {{-- <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#moduloRoles"><span class="fa fa-lock">
                                </span> Roles y permisos</a>
                            </h4>
                        </div>
                        <div id="moduloRoles" class="panel-collapse collapse {{$roles ?? ''}}">
                            <div class="panel-body">
                               <table class="table">
                                 <tr><td><a href="{{url('manual/roles-listar')}}">Lista de roles del usuario</a></td></tr>
                                 <tr><td><a href="{{url('manual/roles-crear')}}">Crear rol del usuarios</a></td></tr>
                                 <tr><td><a href="{{url('manual/roles-editar')}}">Editar rol del usuario</a></td></tr>
                                 <tr><td><a href="{{url('manual/roles-borrar')}}">Borrar rol del usuario</a></td></tr>
                                </table>
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#moduloRoles"><span class="fa fa-users">
                                    </span> Usuarios</a>
                                </h4>
                            </div>
                            <div id="moduloRoles" class="panel-collapse collapse ">
                                <div class="panel-body">
                                   <table class="table">
                                     <tr><td><a href="###" class="menu" index="24">Registrar a un usuario</a></td></tr>
                                     
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#vigilantes"><span class="fa fa-eye">
                                        </span> Vigilantes comunitarios</a>
                                    </h4>
                                </div>
                                <div id="vigilantes" class="panel-collapse collapse ">
                                    <div class="panel-body">
                                       <table class="table">
                                         <tr><td><a href="###" class="menu" index="2">Registrar a un Vigilante</a></td></tr>
                                         <tr><td><a href="###" class="menu" index="3">Editar un vigilante comunitario</a></td></tr>
                                         <tr><td><a href="###" class="menu" index="4">Dar de baja a un vigilante  comunitario</a></td></tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#mujeres"><span class="fa fa-female">
                                            </span> Mujeres</a>
                                        </h4>
                                    </div>
                                    <div id="mujeres" class="panel-collapse collapse ">
                                        <div class="panel-body">
                                           <table class="table">
                                             <tr><td><a href="###" class="menu" index="5">Registrar a una mujer en el sistema</a></td></tr>
                                             <tr><td><a href="###" class="menu" index="6">Editar un registro de mujer</a></td></tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#notificaciones"><span class="fa fa-bell">
                                                </span> Notificaciones</a>
                                            </h4>
                                        </div>
                                        <div id="notificaciones" class="panel-collapse collapse ">
                                            <div class="panel-body">
                                               <table class="table">
                                                 <tr><td><a href="###" class="menu" index="7">Registro de una notificacion de embarazo</a></td></tr>
                                                 <tr><td><a href="###" class="menu" index="8">Registro de notificacion de muerte de mujer</a></td></tr>
                                                 <tr><td><a href="###" class="menu" index="9">Registro de notificacion de muerte de bebe</a></td></tr>
                                                 <tr><td><a href="###" class="menu" index="10">Registro de notificacion de parto</a></td></tr>
                                                 <tr><td><a href="###" class="menu" index="15">Como anular una notificacion</a></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#indicadores"><span class="fa fa-edit">
                                                    </span> Indicadores de notificaciones</a>
                                                </h4>
                                            </div>
                                            <div id="indicadores" class="panel-collapse collapse ">
                                                <div class="panel-body">
                                                   <table class="table">
                                                     <tr><td><a href="###" class="menu" index="11">Registro de indicadores de notificacion de embarazo</a></td></tr>
                                                     <tr><td><a href="###" class="menu" index="12">Registro de indicadores de notificaciones de muerte de mujer</a></td></tr>
                                                     <tr><td><a href="###" class="menu" index="13">Registro de indicadores de notificaciones de muerte de bebe</a></td></tr>
                                                     <tr><td><a href="###" class="menu" index="14">Registro de indicadores de notificacion de parto</a></td></tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#consolidados"><span class="fa fa-bar-chart">
                                                        </span> Reportes Consolidados</a>
                                                    </h4>
                                                </div>
                                                <div id="consolidados" class="panel-collapse collapse ">
                                                    <div class="panel-body">
                                                       <table class="table">
                                                         <tr><td><a href="###" class="menu" index="16">Reporte consolidado   de mujeres embarazadas</a></td></tr>
                                                         <tr><td><a href="###" class="menu" index="17">Reporte consolidado de notificacion de parto</a></td></tr>
                                                         <tr><td><a href="###" class="menu" index="18">Reporte consolidado de muertes fetales y neonatales</a></td></tr>
                                                         <tr><td><a href="###" class="menu" index="19">Reporte consolidado muerte de mujeres</a></td></tr>
                                                         <tr><td><a href="###" class="menu" index="20">Reporte consolidado de vigilantes comunitarios</a></td></tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#nominales"><span class="fa fa-list">
                                                            </span> Reporte Nominales</a>
                                                        </h4>
                                                    </div>
                                                    <div id="nominales" class="panel-collapse collapse ">
                                                        <div class="panel-body">
                                                           <table class="table">
                                                             <tr><td><a href="###" class="menu" index="21">Rerporte nominal de vigilantes comunitarios</a></td></tr>
                                                             <tr><td><a href="###" class="menu" index="22">Reporte nominal de mujeres</a></td></tr>
                                                             <tr><td><a href="###" class="menu" index="23">Reporte nominales de notificaciones</a></td></tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <div class="well">
                    {{-- <h1>
                        Accordion Menu With Icon</h1>
                    Admin Dashboard Accordion Menu --}}

                    @yield('content-manual')
                </div>
            </div>
        </div>
    </div>
    
      
    
    
    @section('scripts')
    <script>
            var tutoriales=[];
        $(document).ready(function () {
             tutoriales.push(
                 {
                'titulo':'Ingresar al sistema',
                'descripcion':'En este video explicaremos como ingresar al sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/s4FtL1nhlNY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Menu del sistema',
                'descripcion':'En este video explicaremos el menu del sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/ln86hYFV2is" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registrar vigilante',
                'descripcion':'Video en donde explicamos como registrar un vigilante comunitario',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/MJYahrVDMAw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Editar un vigilante comunitario',
                'descripcion':'En este video veremos como se puede cambiar la informacion de un vigilante comunitario en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/1o8iiaagCOY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Dar de baja a un vigilante  comunitario',
                'descripcion':'En este video se explica el procedimiento para dar de baja a un vigilante comunitario',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/ZbUQwUUe9OA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registrar a una mujer en el sistema',
                'descripcion':'En este video se explica como se registra a una mujer entre 10 y 59 años en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/tndmwJWg1ig" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Editar un registro de mujer',
                'descripcion':'En este video se muestra como podemos cambiar o editar la informacion de un registro de una mujer en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/fIG5pNUCiuI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },

                 {
                'titulo':'Registro de una notificacion de embarazo',
                'descripcion':'En este video explicamos como se registran las notificaciones de embarazo en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/hZJHbKEdFi8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 
                 {
                'titulo':'Registro de notificacion de muerte de mujer',
                'descripcion':'En este video se explica como se registra una notificacion de muerte de mujer',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/WlB_HpuPRU0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de notificacion de muerte de bebe',
                'descripcion':'En este video se explica como se registra una notificacion de muerte de bebe',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/7QMBQ3RAFXY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de notificacion de parto',
                'descripcion':'En este video se explica como se registra una notificacion de parto',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/QUTL9I2R_Vw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de indicadores de notificacion de embarazo',
                'descripcion':'En este video se explica como se registra los indicadores para una notificacion de embarazo',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/2-PNyLE3Jl0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de indicadores de notificaciones de muerte de mujer',
                'descripcion':'En este video se explica como registrar los indicadores para las notificaciones de muerte de mujer',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/YdweuyEtOzA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de indicadores muerte de bebe',
                'descripcion':'En este video se explica como registrar los indicadores para las notificaciones de muerte de mujer',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/J8IUosjweys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registro de indicadores de notificacion de parto',
                'descripcion':'En este video se explica como registrar los indicadores para las notificaciones de parto',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/YGfE5KZ93no" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Como anular una notificacion',
                'descripcion':'En este video se explica como se anula una notificacion que se haya registrado en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/piRHD8z-aGQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte consolidado  de notificaciones de mujeres embarazadas',
                'descripcion':'',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/51Zu47b6uUs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte consolidado de notificacion de parto',
                'descripcion':'en este video se muestra como generar un reporte consolidado de notificaciones de parto',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/oLHyWSWAqtc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte consolidado de muertes fetales y neonatales identidicadas por vigilante comunitario',
                'descripcion':'en este video se muestra como generar un reporte consolidado de notificaciones de muerte de beb',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/H-NwBbf7i7M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte consolidado muerte de mujeres identificadas por vigilante comunitario',
                'descripcion':'En este video se explica como se genera el reporte consolidado de muerte de mujres identificadas por vigilante comunitario',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/WJ5ecltw05w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte consolidado de vigilantes comunitarios',
                'descripcion':'En este video se explica como se genera el reporte consolidado de vigilantes comunitarios',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/3Sf-mE51dQU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Rerporte nominal de vigilantes comunitarios',
                'descripcion':'En este video se explica como se genera el reporte nominal de los vigilantes comunitarios registrados en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/GtC5h6-g57I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte nominal de mujeres',
                'descripcion':'En este video se explica como se genera el reporte nominal de las mujeres registrados en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/3c9DPufnL98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Reporte nominales de notificaciones',
                'descripcion':'En este video se explica como se genera el reporte nominal de las notificaciones que son registradas en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/41giO222xAw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 },
                 {
                'titulo':'Registrar de un nuevo  usuario',
                'descripcion':'En este video se explica  se registra,edita y se da de baja a un usuario en el sistema',
                'iframe':`<iframe width="90%" height="315" src="https://www.youtube.com/embed/6h3jN5xt3GU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                 }

             )
            //  console.log(tutoriales)

             $('.titulo').html(tutoriales[0].titulo)
             $('.descripcion').html(tutoriales[0].descripcion)
             $('.iframe').html(tutoriales[0].iframe)

             $('.menu').click(function(_evt){
                 _evt.preventDefault();
                 let index= $(this).attr('index')
                 $('.titulo').html(tutoriales[index].titulo)
             $('.descripcion').html(tutoriales[index].descripcion)
             $('.iframe').html(tutoriales[index].iframe)



             })


    
            });
    </script>
    
    @stop 
    @stop