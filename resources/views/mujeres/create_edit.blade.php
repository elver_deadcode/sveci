@extends('layouts.app',[ 'active_tab_mujeres'=>'active', 'active_crear_mujer'=>'active', 'title'=>'Mujeres'
]) 


@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="{{ url('vigilantes', []) }}">MUJERES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        @if(isset($mujer))
        <span class="bread-blod">EDITAR</span> @else
        <span class="bread-blod">REGISTRO DE MUJERES</span> @endif
    </li>
</ul>


@stop 
@section('content')

<div class=" container panel panel-default">
    <div class="panel-body">
        <form method="POST" action="{{ url('mujeres/guardar') }}" data-toggle="validator" class="form">
            
            
            <input type="hidden" class="adscrito_id" name="adscrito_id" value="
            @if(isset($mujer) ) 
             {{$mujer->persona->verificado}}
            
            @endif
            "
            >
            @csrf @if(isset($mujer))
            <input type="hidden" name="mujer_id" value="{{$mujer->IdMujer}}">
             @endif

            

<fieldset   style="padding:5px 5px;" >
<div class="form-group row">
        <div class="col-sm-2">
                <label for="" class=" search col-form-label">Numero de carnet<span class="text-danger"><small>*</small></span></label>
            <input type="text" name="numero_carnet"   data-error="Completa este campo" required
                class="form-control   solo-enteros numero_carnet" id="" placeholder="Numero de carnet" 
                @if(isset($mujer)) value="{{$mujer->persona->numeroCarnet}}"
                @endif
                @if(isset($mujer) && $mujer->persona->verificado) readonly
                @endif
                >
            <div class="help-block with-errors"></div>
        </div>
    <div class="col-sm-2">
            <label for="" class="search col-form-label">Fecha Nacimiento<span class="text-danger"><small>*</small></span></label>
        <input type="text" name="fecha_nacimiento" required data-validation="date" data-validation-format="yyyy-mm-dd" required data-error="selecciona una fecha de tal forma  que la edad este entre 10 y 59 años"  
        class="datepicker fecha_nacimiento form-control" required data-error="Completa la fecha de nacimiento"
            placeholder="{{date('d/m/Y')}}"
             @if(isset($mujer)) value="{{$mujer->persona->fechaNacimiento->format('d/m/Y')}}"
            @endif
            @if(isset($mujer) && $mujer->persona->verificado) readonly
            @endif
            >
        <div class="help-block with-errors"></div>
    </div>

    <div class="col-sm-2">
        <label for="" class="search col-form-label">Complemento</label>
        <input type="text" name="complemento"  
        @if(isset($mujer)) value="{{$mujer->persona->complemento}}"
        @endif
        @if(isset($mujer) && $mujer->persona->verificado) readonly
        @endif
        class="form-control complemento" id="" placeholder="complemento" 
        >
    </div>
    <div class="col-sm-3">
        <br>
            
       
        <button  type="button" class="@if(isset($mujer) && $mujer->persona->verificado) hide   @endif btn btn-primary mt-3 btn-buscar" style="margin-top:5px;"><i class=" fa fa-search "></i>Buscar</button>
        <button  type="button" class="@if(isset($mujer) && $mujer->persona->verificado) hide   @endif btn btn-default mt-3 btn-limpiar" style="margin-top:5px;"><i class=" fa fa-refresh "></i>Limpiar</button>
    </div>
    
</div>
</fieldset>


<fieldset class="@if(!isset($mujer))hide @endif fieldset"   style="padding:5px 5px; margin-top:5px;">
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Nombres <span class="text-danger" style="font-size:10px">(Obligatorio)</span><span class="text-danger"><small>*</small></label>
                <div class="col-sm-10">
                    <input type="text" @if(isset($mujer)) value="{{$mujer->persona->nombres}}" @endif name="nombres" data-error="Completa este campo"
                        required class="required form-control nombres" id="inputEmail3" placeholder="Nombres"
  
                        >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Primer apellido <span class="text-danger" style="font-size:10px">(Obligatorio)</span><span class="text-danger"><small>*</small></label>
                <div class="col-sm-10">
                    <input type="text" name="primer_apellido" @if(isset($mujer)) value="{{$mujer->persona->primerApellido}}" @endif required data-error="Completa este campo"
                        class="required form-control primer_apellido" id="" placeholder="Primer apellido">
                    <div class="help-block with-errors"></div>

                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Segundo apellido</label>
                <div class="col-sm-10">
                    <input type="text" name="segundo_apellido" class="required segundo_apellido form-control" @if(isset($mujer)) value="{{$mujer->persona->segundoApellido}}"
                        @endif id="" placeholder="Segundo apellido">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Direccion</label>
                <div class="col-sm-10">
                    <input type="text" name="direccion" class="required direccion form-control" id="" placeholder="Direccion actual" @if(isset($mujer)) value="{{$mujer->persona->direccion}}"
                        @endif>
                </div>
            </div>

       
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Numero celular</label>
                <div class="col-sm-2">
                    <input type="text" name="numero_celular" 
                        class="required form-control num_celular" id="" placeholder="Numero de celular" @if(isset($mujer)) value="{{$mujer->persona->numeroCelular}}"
                        @endif>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <div class="form-group">

                    <div class="">
                        <legend class=" col-sm-12"><label for="">Localizacion geografica</label></legend>
                
                        <div class="col-sm-12">
                            <div class="col-md-2 ">
                            </div>
                
                            <div class="col-md-2 div-dptos" @if(auth()->user()->nivel=='nacional') style="display:block" @else style="display:none" @endif>
                                <label for="">Departamento</label>
                                <select class="departamento form-control" name="dpto" id="">
                                                <option value="">Seleccione</option>
                                                @foreach($dptos as $k=>$dpto)
                                                <option @if(isset($mujer)&& $mujer->persona->obtenerDepartamento()->dpt_codigo==$dpto->dpt_codigo) selected @endif  value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
                                                @endforeach
                                             </select>
                            </div>
                            <div class="col-md-2 form-group
                                            div-municipios" @if(auth()->user()->nivel=='nacional' ||auth()->user()->nivel=='depto') style="display:block" @else style="display:none"
                                @endif >
                                <label for="">Municipio</label>
                                <select class="municipio form-control" name="municipio" id="">
                                                <option dptoid="0" value="">Seleccione</option>
                                                @foreach($municipios as $k=>$muni)
                                                <option 
                                                @if(isset($mujer)&& $mujer->persona->obtenerMunicipio()->mnc_codigo==$muni->mnc_codigo) selected @endif
                                                dptoid={{$muni->provincia->departamento->dpt_codigo}} value="{{$muni->mnc_codigo}}">{{$muni->mnc_nombre}}</option>
                                                @endforeach
                                             </select>
                
                            </div>
                            <div class="col-md-3 form-group div-estable">
                                <label for="">Establecimientos de  <span class="text-danger" style="font-size:8px">(Obligatorio)</span></label> @if(\Auth::user()->nivel=='establ')
                                <input type="text" readonly class="form-control" value="{{$establecimientos[0]->nomestabl}}">
                                <input type="hidden" class="form-control" name="establecimiento" value="{{$establecimientos[0]->codestabl}}">                @else
                                <select class="establecimiento form-control" data-error="Seleccione un Establecimiento" required name="establecimiento" id="">
                                                    <option value="">Seleccione</option>
                                                    @foreach($establecimientos as $k=>$v)
                                                   <option @if(isset($mujer)&& $mujer->persona->codEstablecimiento==$v->codestabl) selected @endif value="{{$v->codestabl}}">{{$v->nomestabl}}</option>
                                                    @endforeach
                        
                                             </select>
                
                                <div class="help-block with-errors"></div>
                                @endif
                
                            </div>
                
                            <div class="col-md-3 form-group div-estable">
                                <label for="">Comunidad</label>
                
                                <select class="comunidades form-control" data-error="Seleccione una Comunidad" name="comunidad" id="">
                                                    <option value="">Seleccione</option>
                                                    @foreach($comunidades as $k=>$v)
                                                   <option @if(isset($mujer)&& $mujer->persona->IdComunidad==$v->IdComunidad) selected @endif  value="{{$v->IdComunidad}}">{{$v->nomComunidad}}</option>
                                                    @endforeach
                        
                                             </select>
                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <button type="submit" class="btn-guardar btn  btn-block btn-primary"><i class="fa fa-save"></i> Guardar registro de mujer</button>
                        <a href="{{url('mujeres')}}" class="btn  btn-block btn-default"><i class="fa fa-times"></i> Cancelar</a>
                        </div>
                    </div>
</fieldset>

         

           
        </form>
    </div>
</div>




@section('scripts')


<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/Helpers.js')}}"></script>
<script>
    
    @if(isset($mujer) ) 
    @if($mujer->persona->verificado)
   $('.required').prop('readonly',true)
   @else
   $('.required').prop('readonly',false)
   @endif
   @else
   $('.required').prop('readonly',true)
        @endif

      $('.departamento').change(function(){
    //   alert('asd')
      var _dpto= $(this).val();
     $('.municipio').val('');
      $(".municipio option").each(function(i,o) {
        //   console.log(o)
   if ($(o).attr("dptoid") ===_dpto) {
      $(o).show()
   }
   else{
       $(o).hide()
   }
       });
            
  })


  
  function obtenerMunicipios(mnc_codigo,establecimiento_id=0){
      $.ajax({
          url:"{{url('ajax/establecimientos-municipio')}}",
          type:'GET',
          dataType:'json',
          data:{idmuni:mnc_codigo},
          beforeSend:function(){
              $('.establecimiento').attr('disabled',true)
              $('.comunidades').attr('disabled',true)
          },
          complete:function(){
               $('.establecimiento').attr('disabled',false)
               $('.comunidades').attr('disabled',false)
          },
          success:function(res){
              $('.establecimiento').find('option').not(':first').remove();
              
              $('.establecimiento').append(res.options);
              if(parseInt(establecimiento_id)>0){
                $('.establecimiento').val(establecimiento_id).trigger('change')
              }

               $('.comunidades').find('option').not(':first').remove();
              
              $('.comunidades').append(res.options_comunidades);
              
            },
        }) 
      
  }

  function llenarFormulario(datos){
       
      
        $('.search').prop('readonly',true)
        $('.nombres').val(datos.nombres)
        $('.adscrito_id').val(datos.adscrito_id)
        $('.primer_apellido').val(datos.primerapellido)
        $('.segundo_apellido').val(datos.segundoapellido)
        $('.complemento').val(datos.complemento)
        $('.direccion').val(datos.direccion)
        $('.num_celular').prop('disabled',false)
        $('.num_celular').val(datos.tel_referencia)
        $('.departamento').val(datos.departamento_id).trigger('change')
        $('.municipio').val(datos.municipio_id).trigger('change')
        obtenerMunicipios(datos.municipio_id,datos.establecimiento_id)
        
     
  }

  function limpiarRegistrar(){
      $('.required').prop('readonly',false)
      $('.nombres').focus();
  }

  $('.municipio').change(function(){
         var id=$(this).val();
         var option="";
    obtenerMunicipios(id)

  })
  $('.btn-limpiar').click(function(){
      $('.form')[0].reset()
      $('.search').prop('readonly',false)
      $('.required').prop('readonly',true)
  })

$('.btn-buscar').click(function(){
      if($('.numero_carnet').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese un numero de carnet',
                    });
                    return false;
      } 
      if($('.fecha_nacimiento').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese una Fecha de nacimiento',
                    });
                    return false;
      }
      
      let numcarnet=$('.numero_carnet').val()
      let fechanacimiento=$('.fecha_nacimiento').val()
      let complemento=$('.complemento').val()
      $.ajax({
          url:"{{url('ajax/buscar-persona-api')}}",
          type:'GET',
          dataType:'json',
          data:{
                  num_carnet:numcarnet,
                   fecha_nacimiento:fechanacimiento,
                     complemento:complemento,
            },
          beforeSend:function(){
            blocUI('Buscando...')
          },
          complete:function(){
                    $.unblockUI();

                },
          success:function(res){
              console.log(res)
              if(res.success){
                  if(res.data.success && res.data.code==200){
                    llenarFormulario(res.data.data)
                    $('.fieldset').removeClass('hide')

                  }
                  else if( res.status==500){
                    // alert(res.data);
                  }
                  else if(!res.data.success){
                    $.confirm({
            title: res.data.mensaje,
            content: 'No se encontraron datos de la persona.Desea registrar datos de la persona?',
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
                  }
              }
              else{
                $.confirm({
            title: 'No se encontraron datos de la persona',
            content: 'Desea registrar datos de la persona?',
            autoClose: 'Cancelar|8000',
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
              }

          },
          error:function(){}


      })

  })
 
            

           
     

</script>


@stop 
@stop