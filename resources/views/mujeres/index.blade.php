@extends('layouts.app',[
'active_tab_mujeres'=>'active',
'active_listar_usuarios'=>'active',
'title'=>'Mujeres'
]) {{--
@section('titulo')Municipios

@stop --}}
@section('breadcome')

<ul class="breadcome-menu">


    <li><span class="bread-blod">LISTA DE MUJERES EN EDAD FERTIL</span>
    </li>
</ul>


@stop
@section('styles')
<style>
    #yadcf-filter--datatable-1 {
        width: 30px;
    }

    #yadcf-filter--datatable-6 {
        width: 70px;
    }

    #yadcf-filter--datatable-7 {
        width: 30px;
    }
</style>
@stop
@section('content')

<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <table style="font-size: 10px" class="table datatable">
                <thead>
                    <tr>
                        <th>ACCIONES</th>
                        <th>CARNET VERIFICADO</th>
                        <th>NUM CARNET</th>
                        <th>NOMBRES</th>
                        <th>PRIMER APELLIDO</th>
                        <th>SEGUNDO APELLIDO</th>
                        <th>FECHA NACIMIENTO</th>
                        <th>EDAD</th>
                        <th>DIRECCION</th>
                        <th>VIGILANTE</th>
                        <th>ESTABLECIMIENTO</th>
                        <th>MUNICIPIO</th>
                        <th>RED</th>
                        <th>DEPARTAMENTO</th>
                        <th>FECHA DE REGISTRO</th>
                    </tr>
                </thead>

            </table>
        </div>

    </div>
</div>


@section('scripts')
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script>
    $('body').on('click', '.btn-borrar-mujer', function(_evt) {

        _evt.preventDefault();
        let url = $(this).attr('href')

        $.confirm({
            title: 'Borrar registro de mujer',
            content: 'Se borrara los datos del registro de esta persona. esta seguro?',
            buttons: {
                confirm: {
                    action: function() {
                        window.location = url;
                    },
                    btnClass: 'btn-danger',
                    text: 'Si, borrar'
                },

                cancel: {
                    text: 'Cancelar',
                    action: function() {}
                }

            }
        });

    })


    $(document).ready(function() {


        var table = $('.datatable').DataTable({
            dom: "<'row '<' ml-2 pl-2 pt-2 mb-1 col-md-2 'l><'px-0   col-md-4'i><' mx-0 col-md-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row txt-small'<'col-sm-5'i><'col-sm-7'p>>",
            ajax: {
                url: "{{url('mujeres/listar')}}",
                type: 'GET'
            },
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
                "infoFiltered": "(Filtrado de _MAX_ total Registros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            "lengthChange": true,
            "lengthMenu": [
                [50, 100, 200, 500, -1],
                [50, 100, 200, 500, "All"]
            ],
            serverSide: true,
            processing: true,
            columns: [{
                    data: 'acciones'
                },
                {
                    data: 'verificado',
                    "defaultContent": ''
                },
                {
                    data: 'persona.numeroCarnet',
                    data: 'persona.numeroCarnet',
                    "defaultContent": ''
                },
                {
                    data: 'persona.nombres',
                    data: 'persona.nombres',
                    "defaultContent": ''
                },
                {
                    data: 'persona.primerApellido',
                    data: 'persona.primerApellido',
                    "defaultContent": ''
                },
                {
                    data: 'persona.segundoApellido',
                    data: 'persona.segundoApellido',
                    "defaultContent": ''
                },
                {
                    data: 'fechanacimiento',
                    data: 'persona.fechaNacimiento',
                    "defaultContent": ''
                },
                {
                    data: 'edad',
                    "defaultContent": ''
                },
                {
                    data: 'persona.direccion',
                    data: 'persona.direccion',
                    "defaultContent": ''
                },
                {
                    data: 'vigilante',
                    "defaultContent": ''
                },
                {
                    data: 'establecimiento',
                    "defaultContent": ''
                },
                {
                    data: 'municipio',
                    "defaultContent": ''
                },
                {
                    data: 'red',
                    "defaultContent": ''
                },
                {
                    data: 'departamento',
                    "defaultContent": ''
                },
                {
                    data: 'fecharegistro',
                    "defaultContent": ''
                },


            ]

        })
        yadcf.init(table, [

            {
                column_number: 1,
                filter_type: "select",
                data: [{
                    value: 'SI',
                    label: 'SI'
                }, {
                    value: 'NO',
                    label: 'NO'
                }],
                filter_default_label: "Seleccionar"

            },

            {
                column_number: 2,
                filter_type: "text",
                filter_default_label: ""

            },
            {
                column_number: 3,
                filter_type: "text",
                filter_default_label: ""

            },
            {
                column_number: 4,
                filter_type: "text",
                filter_default_label: ""

            },

            {
                column_number: 5,

                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 7,

                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 6,
                filter_type: "date",
                date_format: "dd/mm/yyyy",
                filter_plugin_options: {
                    changeMonth: true,
                    changeYear: true
                },
                filter_default_label: ['dia/mes/anio']


            },
            {
                column_number: 9,

                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 10,

                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 11,
                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 12,
                filter_type: "text",
                filter_default_label: ""
            },
            {
                column_number: 13,
                filter_type: "text",
                filter_default_label: ""
            }



        ], {
            cumulative_filtering: true
        });


    });
</script>


@stop
@stop