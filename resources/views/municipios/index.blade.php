@extends('layouts.app',[
    'active_registros'=>'active',
    'active_municipio'=>'active',
    'title'=>'Municipios'
])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Registros</a> <span class="bread-slash">/</span>
    </li>
  
    <li><span class="bread-blod">Municipios</span>
    </li>
</ul>
@stop
@section('content')


<div class="table-responsive">
        <table style="font-size: 10px" class="table datatable">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>MUNICIPIO</th>
                    <th>PROVINCIA</th>
                    <th>DEPARTAMENTO</th>
                </tr>
            </thead>

        </table>
        </div>
 @section('scripts')
        <script>
        $(document).ready(function () {
            $('.datatable').DataTable({
             dom: "Bfrltip", 
        ajax: {
            url: "{{url('registros/municipios/listar')}}",
            type: 'GET'
              },
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'municipio', name: 'municipio'},
            {data: 'provincia.provincia', name: 'provincia_id'},
            {data: 'departamento.departamento', name: 'departamento_id'}
           
        ]
                
            });
        });
        </script>
 @stop
@stop