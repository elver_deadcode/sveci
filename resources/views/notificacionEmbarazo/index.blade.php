@extends('layouts.notificaciones') {{-- 
@section('titulo')Municipios

@stop --}} 


@section('content')

<div class="row">

 @include('includes.menu-notificaciones')
<div class=" table-responsive col-md-9">
<table style="font-size:10px" class="table datatable">
    <thead>
        <tr>
            <th>ID</th>
            <th>ESTADO</th>
            <th>NOMBRES</th>
            <th>APELLIDOS</th>
            <th>DIRECCION</th>
            <th>FECHA DE NOTIFICACION</th>
            <th>FECHA DE REGISTRO</th>
            <th>VIGILANTE</th>
            <th>ESTABLECIMIENTO</th>
            <th>MUNICIPIO</th>
            <th>DEPARTAMENTO</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
</div>
</div>



@section('scripts')
<script>

    $(document).ready(function () {

       var Table=$('.datatable').DataTable({
             dom: "Bfrltip", 
        ajax: {
            url: "{{url('notificacion-embarazo/listar')}}",
            type: 'GET'
              },
               "lengthChange": true,
               "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data: 'IdNotificacionEmbarazo'},
            {data: 'estado'},
            {data: 'nombres'},
            {data: 'primerApellido'},
            {data: 'direccion'},
            {data: 'fechaenvio'},
            {data: 'fecharegistro'},
            {data: 'vigilante'},
            {data: 'establecimiento'},
            {data: 'municipio'},
            {data: 'departamento'},
             {data:'acciones'}
          
           
        ]
                
            });
        });

</script>


@stop 
@stop