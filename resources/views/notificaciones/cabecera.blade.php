    <ul class="nav nav-tabs custon-set-tab" style="font-size:10px">
       
        <li><a data-toggle="tab" href="#embarazo">N.Embarazo</a>
        </li>
        <li><a data-toggle="tab" href="#muertemujer">N.Muerte Mujer</a>
        </li>
        <li><a data-toggle="tab" href="#muertebebe">N.Muerte Bebe</a>
        </li>
        <li><a data-toggle="tab" href="#parto">N.Parto</a>
        </li>
        <li><a data-toggle="tab" href="#mujer">Registro Mujer</a>
        </li>
        <li><a data-toggle="tab" href="#activacion">Activacion</a>
        </li>
    </ul>

    <div class="tab-content">

        <div id="embarazo" class="tab-pane fade in active">
            <div class="notes-area-wrap">
                <div class="note-heading-indicate">
                    <h2><i class="fa fa-comments-o"></i> Latest Notes</h2>
                    <p>You have 10 new message.</p>
                </div>
                <div class="notes-list-area notes-menu-scrollbar">
                    <ul class="notification-menu">

                        @if(count($embarazos)>0)
                        @foreach($embarazos as $k=>$noti)
                       <li>
                       <a href="{{url('notificaciones/$v->id/verNotificacion')}}">
                                <div class="notification-icon">
                                    <span class="adminpro-icon adminpro-checked-pro"></span>
                                </div>
                                <div class="notification-content">
                                    <span class="notification-date">{{$noti->updated_at->format('d/m/Y')}}</span>
                                    <h2></h2>
                                <p>{{$noti->mensaje}}</p>
                                </div>
                            </a>
                        </li>
                        @endforeach

                        @else
                        <h6 class="alert alert-info">No existen notificaciones</h6>
                        @endif
                    
                    </ul>
                </div>
            </div>
        </div>
        <div id="muertemujer" class="tab-pane fade">
            <div class="projects-settings-wrap">
                <div class="note-heading-indicate">
                    <h2><i class="fa fa-cube"></i> Latest projects</h2>
                    <p> You have 20 projects. 5 not completed.</p>
                </div>
                <div class="project-st-list-area project-st-menu-scrollbar">
                    <ul class="projects-st-menu-list">
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Web Development</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">1 hours ago</span>
                                    </div>
                                    <div class="projects-st-content">
                                        <p>Completion with: 28%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 28%;" class="progress-bar progress-bar-danger"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Software Development</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">2 hours ago</span>
                                    </div>
                                    <div class="projects-st-content project-rating-cl">
                                        <p>Completion with: 68%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 68%;" class="progress-bar"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Graphic Design</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">3 hours ago</span>
                                    </div>
                                    <div class="projects-st-content">
                                        <p>Completion with: 78%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 78%;" class="progress-bar"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Web Design</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">4 hours ago</span>
                                    </div>
                                    <div class="projects-st-content project-rating-cl2">
                                        <p>Completion with: 38%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 38%;" class="progress-bar progress-bar-danger"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Business Card</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">5 hours ago</span>
                                    </div>
                                    <div class="projects-st-content">
                                        <p>Completion with: 28%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 28%;" class="progress-bar progress-bar-danger"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Ecommerce Business</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">6 hours ago</span>
                                    </div>
                                    <div class="projects-st-content project-rating-cl">
                                        <p>Completion with: 68%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 68%;" class="progress-bar"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Woocommerce Plugin</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">7 hours ago</span>
                                    </div>
                                    <div class="projects-st-content">
                                        <p>Completion with: 78%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 78%;" class="progress-bar"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="project-list-flow">
                                    <div class="projects-st-heading">
                                        <h2>Wordpress Theme</h2>
                                        <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                        <span class="project-st-time">9 hours ago</span>
                                    </div>
                                    <div class="projects-st-content project-rating-cl2">
                                        <p>Completion with: 38%</p>
                                        <div class="progress progress-mini">
                                            <div style="width: 38%;" class="progress-bar progress-bar-danger"></div>
                                        </div>
                                        <p>Project end: 4:00 pm - 12.06.2014</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="Settings" class="tab-pane fade">
            <div class="setting-panel-area">
                <div class="note-heading-indicate">
                    <h2><i class="fa fa-gears"></i> Settings Panel</h2>
                    <p> You have 20 Settings. 5 not completed.</p>
                </div>
                <ul class="setting-panel-list">
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Show notifications</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example" />
                                        <label class="onoffswitch-label" for="example">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Disable Chat</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example3" />
                                        <label class="onoffswitch-label" for="example3">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Enable history</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example4" />
                                        <label class="onoffswitch-label" for="example4">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Show charts</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example7" />
                                        <label class="onoffswitch-label" for="example7">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Update everyday</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example2" />
                                        <label class="onoffswitch-label" for="example2">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Global search</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example6" />
                                        <label class="onoffswitch-label" for="example6">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox-setting-pro">
                            <div class="checkbox-title-pro">
                                <h2>Offline users</h2>
                                <div class="ts-custom-check">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example5" />
                                        <label class="onoffswitch-label" for="example5">
                                                                            <span class="onoffswitch-inner"></span>
                                                                            <span class="onoffswitch-switch"></span>
                                                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>

    </div>
