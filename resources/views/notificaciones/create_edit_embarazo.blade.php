<div id="modal-notificacion-embarazo" data-backdrop="static" data-keyboard="false" class="modal  fade" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center text-primary">REGISTRAR UNA NOTIFICACION </h>

            </div>
            <div class="modal-body ">
                <form action="" class=" form form-notificacion">
                    @csrf
                    <div class="form-group">
                        <h4 class="control-label">TIPO DE NOTIFICACION</h4>
                        <label for="default" class="btn btn-default "
                            style="background-color:#ff80ab;color:#FFF">Embarazo <input type="radio" name="tipo"
                                value="embarazo" id="default" class="noti-cb badgebox"><span
                                class="badge">&check;</span></label>
                        <label for="primary" class="btn btn-default "
                            style="background-color:#212121; color:#FFF">Muerte Mujer <input type="radio" name="tipo"
                                value="muertemujer" id="primary" class="noti-cb badgebox"><span
                                class="badge">&check;</span></label>
                        <label for="info" class="btn btn-info " style="background-color:#7b1fa2;">Muerte Bebe <input
                                type="radio" name="tipo" value="muertebebe" id="info" class="noti-cb badgebox"><span
                                class=" badge">&check;</span></label>
                        <label for="success" class="btn btn-warning " style="background-color:#f4511e;">Parto <input
                                type="radio" name="tipo" value="parto" id="success" class="noti-cb badgebox"><span
                                class=" badge">&check;</span></label>
                    </div>
                    <div class="hide form-group">
                        <div class="col-md-12">

                            <label for="" class="text-danger"><input type="checkbox" checked class="cb-buscarmujer ">
                                Buscar una
                                Mujer Registrada en el sistema</label><br>
                        </div>

                    </div>

                    <div class=" form-group  div-buscar ">
                        <div class="col-md-12">
                            <label for=""> Buscar </label><br>
                            <select class="buscar-mujer form-control" name="idmujer">
                                <option value=""></option>
                            </select>
                            <label for="" class="text-primary">Buscar por:</label>
                            <label for=""><input type="radio" id="cb-nombres" name="search-mujer"
                                    checked>Nombres</label>
                            <label for=""><input type="radio" id="cb-carnet" name="search-mujer">Carnet</label>
                        </div>
                    </div>

                    <div class=" hide div-datos">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="">Nombres de la mujer</label>
                                <input type="text" class="nombres_mujer form-control" name="nombres_mujer">
                            </div>

                            <div class="col-md-4">
                                <label for="">Primer Apellido </label>
                                <input type="text" class="primer_apellido form-control" name="primer_apellido">
                            </div>
                            <div class="col-md-4">
                                <label for="">Segundo Apellido</label>
                                <input type="text" class="form-control" name="segundo_apellido">
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="" style="">Edad(años)</label>
                                <input type="text" class="solo-enteros form-control" name="edad">
                            </div>
                            <div class="col-md-8">
                                <label for="" style="">Direccion</label>
                                <input type="text" class="form-control" name="direccion">
                            </div>

                        </div>

                    </div>

                    <div class="form-group" style="margin-top:25px;">
                        <div class="col-md-12">
                            <br>
                            <label for="" class="text-danger">Buscar un vigilante que este habilitado en el sistema
                            </label>
                            <select class="input-search-vigilante form-control cod_vigilante" name="id_vigilante">
                                <option value=""></option>
                            </select>

                            <label for="" class="text-primary">Buscar por:</label>
                            <label for=""><input type="radio" id="cb-nombres-vigilante" name="search-vigilante"
                                    checked>Nombres</label>
                            <label for=""><input type="radio" id="cb-carnet-vigilante"
                                    name="search-vigilante">Carnet</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">


                            <label for="">Fecha de la generacion Notificacion</label>
                            <input type="text" name="fecha_generacion" readonly
                                class="datepicker  fecha-generacion form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="">Fecha de registro de la notificacion</label>
                            <input type="text" name="fecha_registro" readonly value="{{date('d/m/Y')}}"
                                class="datepicker form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>

                    <div class=" row form-group">

                        <div class="col-md-4 col-xs-6 col-sm-6">

                            <button type="button" class="btn btn-primary btn-guardar-noti">Guardar Notificacion</button>
                        </div>
                        <div class="col-md-3  col-xs-6 col-sm-6">

                            <button type="button" data-dismiss="modal" class="btn btn-default">Cerrar Ventana</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>