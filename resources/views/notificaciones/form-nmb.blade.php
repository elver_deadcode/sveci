<form method="post" action="{{url('notificacion-muertebebe/guardar-detalle')}}" class="form-detalle-muertebebe form-horizontal panel  panel-default"
    style="background-color:#DDD" role="form">
    @csrf
    <input type="hidden" name="id_notificacion" value="{{$notificable->IdNotificacionMuerteBebe}}"> @isset($notificable->detalle)
    <input type="hidden" name="id_detalle" value="{{$notificable->detalle->IdMuerteBebeDetalle}}"> @endisset
    <h5 class="text-center"><strong>DATOS PARA INDICADORES</strong></h5>

    <div class="form-group">
        <label for="ejemplo_email_3" class="col-lg-4 control-label">El Bebe :</label>
        <div class="col-lg-4">
           <label for="">Nacido vivo <input type="radio" @if(isset($notificable->detalle) && $notificable->detalle->nacido=='SI') checked @else @if($notificable->nacido=='SI')  checked @endif @endif  name="nacido" value="true"  class="cb-bebevivo"></label>
           <label for="">Nacido muerto <input type="radio" @if(isset($notificable->detalle) && $notificable->detalle->nacido=='NO') checked @else @if($notificable->nacido=='NO') checked @endif @endif name="nacido" value="false"  class="cb-bebemuerto"></label>


        </div>
    </div>
    
    
    <div class="form-group div-fechasbebe @if(isset($notificable->detalle) && $notificable->detalle->nacido=='NO') hide  @endif" >
        <div class=" col-md-offset-3 col-lg-3">
                <label for="ejemplo_password_3" class=" control-label">Fecha de Nacimiento </label>
         
            <input  readonly type="text" max="@if(isset($notificable->detalle->fechanacimiento)) {{$notificable->detalle->fechanacimiento}} @endif" class="datepicker form-control fechanacimiento"
                name="fechaNacimiento" value="@if(isset($notificable->detalle->fechanacimiento)) {{$notificable->detalle->fechanacimiento->format('d/m/Y')}} @endif"/>
        </div>

        <div class="col-lg-3">
                <label for="ejemplo_password_3" class="control-label">Fecha de Fallecimiento </label>
            <input readonly type="text" max="@if(isset($notificable->detalle->fechafallecimiento)) {{$notificable->detalle->fechafallecimiento}} @endif"
              class="datepicker form-control fechafallecimiento" name="fechafallecimiento" value="@if(isset($notificable->detalle->fechafallecimiento)) {{$notificable->detalle->fechafallecimiento->format('d/m/Y')}} @endif"/>
        </div>
      
    </div>

    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Tiene Autopsia verbal</label>
        <div class="col-lg-3">
            <input type="checkbox" class="cb-av form-control" value="true" name="autopsia" @if(isset($notificable->detalle) && $notificable->detalle->tieneAutopsiaVerbal==true)
            checked @endif/>
        </div>
    </div>
    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Tiene Plan Accion</label>
        <div class="col-lg-3">
            <input type="checkbox" class="cb-pa form-control" value="true" name="planaccion" @if(isset($notificable->detalle) && $notificable->detalle->tienePlanAccion==true)
            checked @endif/>
        </div>
    </div>
    <div class="form-group ">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Comentarios
        </label>
        <div class="col-lg-6">
            <textarea  class="form-control"name="comentarios" id="" cols="30" rows="3">@if(isset($notificable->detalle->comentarios)) {{$notificable->detalle->comentarios}}
                 @endif</textarea>
        </div>
    </div>
    <div class="form-group">

            @if(!$notificable->cerrado)
            <div class="col-lg-offset-2 col-lg-4">
                    <button type="button" class=" btn-guardardetallemuertebebe btn  btn-success"><i class="fa fa-save"></i> GUARDAR DATOS INDICADOR</button>
                </div>
          
            <div class=" @if($notificable->cerrado)col-lg-offset-2 @endif col-lg-4">
                <button type="button" class="btn-cancelarMuerteBebe  btn  btn-danger"><i class="fa fa-lock"></i> CERRAR NOTIFICACION </button>
            </div>
            @else
            <h3 class="col-md-12 text-danger">La Notificacion esta cerrada ya no se pueden modificar la informacion</h4>
            @endif
       

     
    </div>
</form> 
<script>
    $('.datepicker').datepicker({
            format:'dd/mm/yyyy',
            autoclose:true,
            showDropdowns: true,
            
        });

        $('.cb-bebemuerto').click(function(){
            if($(this).is(':checked'))
            {
                $('.div-fechasbebe').addClass('hide');
                $('.datepicker').val('')
            }
           
        })

        $('.cb-bebevivo').click(function(){
           
            if($(this).is(':checked'))
            {
                $('.div-fechasbebe').removeClass('hide');
            }
           
        })


</script>