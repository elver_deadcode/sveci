<form method="post" action="{{url('notificacion-embarazo/guardar-detalle')}}" class="form-detalle-embarazo form-horizontal panel  panel-primary"
    style="background-color:#" role="form">
    @csrf
    <input type="hidden" name="id_notificacion" value="{{$notificable->IdNotificacionEmbarazo}}"> @isset($notificable->detalle)
    <input type="hidden" name="id_detalle" value="{{$notificable->detalle->IdNotificacionEmbarazoDetalle}}"> @endisset
    <br>
    <div class="form-group">
        <DIV class="col-md-12">

            <h5 class="text-center"><strong>DATOS PARA INDICADORES</strong></h5>
        </DIV>
        <label for="ejemplo_email_3" class="col-lg-4 control-label">Numero de Controles Prenatales:</label>
        <div class="col-lg-2">
            <input type="text" pattern="[0-9]{1}" style="" required  value="@isset($notificable->detalle) {{$notificable->detalle->numeroControlesPrenatal}} @endisset" name="numero_controles"
                class="form-control solo-enteros numerocontroles" id="numero_controles">
        </div>
    </div>

    <div class="form-group">
  
        <label for="ejemplo_email_3" class="col-lg-4 control-label">Edad de la mujer:</label>
        <div class="col-lg-2">
            <input type="text" pattern="[0-9]{2}" style="" required value="@isset($notificable->edad) {{$notificable->edad}} @endisset"
                name="edad"  title="solo numeros" class="solo-enteros edadmujer form-control" id="numero_controles">
        </div>
    </div>
    <div class="form-group">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Plan de Parto llenado</label>
        <div class="col-lg-1">
            <input type="checkbox" class="form-control cb-planparto" name="plan_parto" value="true" @if(isset($notificable->detalle) &&
            $notificable->detalle->planPartoLlenado==true) checked @endif />
        </div>
    </div>
    <div class="form-group ">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Comentarios
        </label>
        <div class="col-lg-6">
            <textarea class="form-control" name="comentarios" id="" cols="30" rows="3">@if(isset($notificable->detalle->comentarios)) {{$notificable->detalle->comentarios}}
                 @endif</textarea>
        </div>
    </div>
    <div class="form-group">
        @if(!$notificable->cerrado)
        <div class="col-lg-offset-2 col-lg-4">
            <button type="button" class=" btn-guardar-detalle-embarazo btn  btn-success"><i class="fa fa-save"></i> GUARDAR INDICADORES</button>
        </div>
       
        <div class=" @if($notificable->cerrado)col-lg-offset-2 @endif col-lg-4">
            <button type="button" class=" btn-cancelarEmbarazo  btn  btn-danger"><i class="fa fa-lock"></i> CERRAR NOTIFICACION </button>
        </div>
        @else
        <h3 class="col-md-12 text-danger">La Notificacion esta cerrada ya no se pueden modificar la informacion</h4>

        @endif
    </div>
</form>