<form method="post" action="{{url('notificacion-muertemujer/guardar-detalle')}}"
    class="form-detalle-muertemujer form-horizontal panel  panel-primary" style="background-color:#" role="form">
    @csrf
    <input type="hidden" name="id_notificacion" value="{{$notificable->IdNotificacionMuerteMujer}}">
    @isset($notificable->detalle)
    <input type="hidden" name="id_detalle" value="{{$notificable->detalle->IdNotificacionEmbarazoDetalle}}">
    @endisset
    <div class="form-group">
        <h5 class="text-center"><strong>DATOS PARA INDICADORES</strong></h5>
    </div>     


    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Edad de la mujer </label>
        <div class="col-lg-2">
            <input type="text" class="form-control solo-enteros edad" name="edad"
                value="@if(isset($notificable->edad)) {{$notificable->edad}} @endif" />
        </div>
    </div>

    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Tiene autopsia verbal? (AV)</label>
        <div class="col-lg-3">

            <input type="checkbox" class="form-control autopsia" value="true" name="ficha"
                @if(isset($notificable->detalle) && $notificable->detalle->tieneFicha==true) checked @endif/>
        </div>
    </div>
    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Tiene Plan de accion? (PA)</label>
        <div class="col-lg-3">
            <input type="checkbox" class="form-control planaccion" style="display:in-line" value="true"
                name="planaccion" @if(isset($notificable->detalle) && $notificable->detalle->tienePlanAccion==true)
            checked @endif/>
        </div>
    </div>
    <div class="form-group dias-fallecida">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Cuenta con el dictamen del comite departamental
            de muerta materna? (SEDES)
        </label>
        <div class="col-lg-3">
            <input type="checkbox" class="form-control casofueatendido" style="display:in-line" value="true"
                name="casofueatendido" @if(isset($notificable->detalle) && $notificable->detalle->casoFueAtendido==true)
            checked @endif/>
        </div>
    </div>

    <div class="form-group">
        <label for="ejemplo_email_3" class="col-lg-4 control-label">La persona fallecio durante <br>
        </label>
        <div class="col-lg-4">
            <select name="muertedurante" class="muertedurante form-control" id="">
                <optgroup class="text-danger"
                    label="Es muerte de mujer de 10 a 59 años (no se sabe si es muerte materna o no)">
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='embarazo')
                        selected @endif value="embarazo">Embarazo</option>
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='parto') selected
                        @endif value="parto">En el parto</option>
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='despues')
                        selected @endif value="despues">Despues del parto</option>
                    <option value="ninguno">Ninguna de las opciones</option>

                </optgroup>
                <optgroup label="Muerte de mujer y ya  se  sabia que la mujer  estaba embarazada">
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='embarazo1')
                        selected @endif value="embarazo1">Embarazo 1</option>
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='parto1') selected
                        @endif value="parto1">En el parto 1</option>
                    <option @if(isset($notificable->detalle) && $notificable->detalle->muerteDurante=='despues1')
                        selected @endif value="despues1">Despues del parto 1</option>
                </optgroup>
            </select>

        </div>
    </div> 
    <div class="form-group ">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Comentarios
        </label>
        <div class="col-lg-6">
            <textarea class="form-control" name="comentarios" id="" cols="30" rows="3">@if(isset($notificable->detalle->comentarios)) {{$notificable->detalle->comentarios}}
                 @endif</textarea>
        </div>
    </div>
    <div class="form-group">
        @if(!$notificable->cerrado)
        <div class="col-lg-offset-2 col-lg-4">
            <button type="button" class=" btn-guardardetallemuertemujer btn  btn-success"><i class="fa fa-save"></i>
                GUARDAR INDICADORES </button>
        </div>

        <div class=" @if($notificable->cerrado)col-lg-offset-2 @endif col-lg-4">
            <button type="button" class="btn-cancelarMuerteMujer  btn  btn-danger"><i class="fa fa-lock"></i> CERRAR
                NOTIFICACION </button>
        </div>
        @else
        <h3 class="col-md-12 text-danger">La Notificacion esta cerrada ya no se pueden modificar la informacion</h4>
            @endif
    </div>


</form>
<script>
    $('.datepicker').datepicker({
            format:'dd/mm/yyyy',
            autoclose:true,
            showDropdowns: true,
            
        });
</script>