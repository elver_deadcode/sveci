<form method="post" action="{{url('notificacion-parto/guardar-detalle')}}"
    class="form-detalle-parto form-horizontal panel  panel-default" style="background-color:#DDD" role="form">
    @csrf
    <h5 class="text-center"><strong>DATOS PARA INDICADORES</strong></h5>

    <input type="hidden" name="id_notificacion" value="{{$notificable->IdNotificacionParto}}">
    @isset($notificable->detalle)
    <input type="hidden" name="id_detalle" value="{{$notificable->detalle->IdPartoDetalle}}"> @endisset
    <div class="form-group">
        <label for="ejemplo_email_3" class="col-lg-4 control-label">El parto fue atendido por:</label>
        <div class="col-lg-4">
            <label for="">Partera Empirica<input type="radio" @if(isset($notificable->detalle) &&
                $notificable->detalle->partera=='empirica') checked @else @if($notificable->atendido=='Partera
                empirica') checked @endif @endif name="partera" value="empirica"></label>
            <label for="">Partera Capacitada <input type="radio" @if(isset($notificable->detalle) &&
                $notificable->detalle->partera=='capacitada') checked @else @if($notificable->partera=='Partera
                capacitada') checked
                @endif @endif name="partera" value="capacitada"></label>


        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-md-4 label-control">Nombre de la partera o familiar</label>
        <div class="col-md-6">
            <input type="text" class=" form-control atendidoNombre" name="atendidoNombre"
                @if(isset($notificable->detalle) ) value="{{$notificable->atendidoNombre}}" @endif>

        </div>
    </div>
    <div class="form-group ">
        <label for="ejemplo_password_3" class="col-lg-4 control-label">Comentarios
        </label>
        <div class="col-lg-6">
            <textarea class="form-control" name="comentarios" id="" cols="30" rows="3">@if(isset($notificable->detalle->comentarios)) {{$notificable->detalle->comentarios}}
                 @endif</textarea>
        </div>
    </div>

    <div class="form-group">


        @if(!$notificable->cerrado)
        <div class="col-lg-offset-2 col-lg-4">
            <button type="button" class=" btn-guardardetalleparto btn  btn-success"><i class="fa fa-save"></i> GUARDAR
                INDICADORES</button>
        </div>

        <div class=" @if($notificable->cerrado)col-lg-offset-2 @endif col-lg-4">
            <button type="button" class="btn-cancelarParto  btn  btn-danger"><i class="fa fa-lock"></i> CERRAR
                NOTIFICACION </button>
        </div>
        @else
        <h3 class="col-md-12 text-danger">La Notificacion esta cerrada ya no se pueden modificar la informacion</h4>
            @endif

    </div>
</form>