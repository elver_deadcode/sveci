@extends('layouts.notificaciones') {{--
@section('titulo')Municipios
@stop --}}

@section('styles')

    <style>
        .badgebox {
            opacity: 0;
        }

        .badgebox + .badge {
            /* Move the check mark away when unchecked */
            text-indent: -999999px;
            /* Makes the badge's width stay the same checked and unchecked */
            width: 27px;
        }

        .badgebox:focus + .badge {
            /* Set something to make the badge looks focused */
            /* This really depends on the application, in my case it was: */

            /* Adding a light border */
            box-shadow: inset 0px 0px 5px;
            /* Taking the difference out of the padding */
        }

        .badgebox:checked + .badge {
            /* Move the check mark back when checked */
            text-indent: 0;
        }

        body {
            overflow: hidden;
        }

        .bg-primary {
            background-color: #456;
        }

        .div-item {
        }

        .div-item-img {
            background-color: aqua;
        }

        .no-visto {
            background-color: #DDD;
        }

        .div-item-options {

            /* position: absolute;
       top:0;
       right:0;
       margin-top: 6px; */
        }

        .noti-container {
            overflow: scroll;
            height: 480px;
        }

        .list-group {
            color: #456;
        }

        .icon-embarazo {
            background-color: #ff80ab;
            padding: 0;

        }

        .letras-icon {
            font-size: 1.5em;
            color: aliceblue;
            font-weight: 500;
            text-align: center;
            font-family: 'Roboto', sans-serif;
        }

        .icon-muerte-mujer {
            background-color: #212121;
            padding: 0;
        }


        .icon-muerte-bebe {
            background-color: #7b1fa2;
            padding: 0;
        }

        .icon-parto {
            background-color: #f4511e;
            padding: 0;
        }

        .icon-mujer {
            background-color: #8BC34A;
            padding: 0;

        }
        .icon-teleconsulta {
            background-color: #336699;
            padding: 0;

        }

        optgroup[label] {
            background: #FFFFFF;
            color: crimson;
        }

    </style>
@stop
@section('content')
    @include('notificaciones.create_edit_embarazo')


    <!-- cd-panel -->
    <div class="row scrollbar " style="overflow-y: scroll;">

        {{-- <a class="Primary mg-b-10" href="#" data-toggle="modal" data-target="#modal-notificacion">Primary</a> --}}
        @include('notificaciones.menu-notificaciones')
        <button type="button" class="col-md-12 navbar-toggle" data-toggle="collapse" data-target="#form-buscar"
                style="margin-top:-25px;float:right">
            <i class=" fa fa-search text-primary"></i>
            <span class="text-primary"><strong>Buscador</strong></span>

        </button>

        <div id="spinner" style="display:none;position:absolute; top:10%; z-index:1000; left:60%;">
            <img src="{{asset('img/spiner.svg')}}" alt="">
        </div>
        <div class=" col-md-9 col-lg-9 col-xs-12 col-sm-12 " style="">

            <div id="form-buscar" class=" collapse navbar-collapse">
                <form action="" class=" form-buscar ">
                    <fieldset class="form-group">
                        <div class="">


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                @if(auth()->user()->nivel=="nacional")
                                    <div class="col-md-1 col-sm-2 col-xs-4">
                                        <label for="">Nacional</label>
                                        <input type="radio" @if(auth()->user()->nivel=="nacional") ) checked @endif
                                        id="cb-nacional" value="nacional"
                                               name="nivel">
                                    </div>
                                @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto")
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <label for="">Departamental</label><br>
                                        <input type="radio" @if(auth()->user()->nivel=="depto") checked
                                               @endif id="cb-depto"
                                               value="depto"
                                               name="nivel">
                                    </div>

                                @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" ||
                            auth()->user()->nivel=="red")
                                    <div class="col-md-1 ">
                                        <label for="">Red</label><br>
                                        <input type="radio" @if(auth()->user()->nivel=="red") checked @endif id="cb-red"
                                               value="red" name="nivel">
                                    </div>
                                @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" ||
                            auth()->user()->nivel=="muni" )
                                    <div class="col-md-1 col-sm-2 col-xs-4">
                                        <label for="">Municipal</label>
                                        <input type="radio" id="cb-muni" value="muni" name="nivel">
                                    </div>
                                @endif @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" ||
                            auth()->user()->nivel=="red" || auth()->user()->nivel=="muni"
                            || auth()->user()->nivel=="establ" )
                                    <div class="col-md-2 col-sm-3 col-xs-5">
                                        <label for="">Establecimiento</label>
                                        <input type="radio" @if(isset($user) && $user->nivel=='establ') checked @endif
                                        @if(\Auth::user()->nivel=='establ')checked
                                               @endif id="cb-esta" value="establ" name="nivel">
                                    </div>


                                @endif

                                <div class="row col-md-4">
                                    <div class="col-md-6 col-sm-3 col-xs-6">
                                        <label for="">Fecha Inicio</label>
                                        <input type="text" name="fechainicio" class="datepicker form-control">
                                    </div>
                                    <div class="col-md-6 col-sm-3 col-xs-6">
                                        <label for="">Fecha Final</label>
                                        <input type="text" name="fechafin" class="datepicker form-control">
                                    </div>

                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="col-md-2 ">
                                </div>
                                <div @if(isset($user) && $user->nivel=='depto') style="display:block" @else
                                style="display:none" @endif class="col-md-2 div-dptos">
                                    <label for="">Departamentos</label>
                                    <select class="departamento form-control" name="dpto" id="">
                                        <option value="">Seleccione</option>
                                        @foreach($dptos as $k=>$dpto)
                                            <option value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div @if(isset($user) && $user->nivel=='red') style="display:block" @else
                                style="display:none" @endif class="col-md-2 div-red">
                                    <label for="">Redes</label>
                                    <select class="red form-control" name="red" id="">
                                        <option value="">Seleccione</option>
                                        @foreach($redes as $k=>$red)
                                            <option dptoid="{{$red->dpt_codigo}}" value="{{$red->are_codigo}}">
                                                {{$red->depto->dpt_nombre}} - {{$red->are_nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div @if(isset($user) && $user->nivel=='muni') style="display:block" @else
                                style="display:none" @endif class="col-md-3 form-group div-municipios">
                                    <label for="">Municipio</label>
                                    <select class="municipio form-control" name="municipio" id="">
                                        <option dptoid="0" value="">Seleccione</option>
                                        @foreach($municipios as $k=>$muni)
                                            <option dptoid={{$muni->provincia->departamento->dpt_codigo}}
                                                    value="{{$muni->mnc_codigo}}">{{$muni->mnc_nombre}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div @if(isset($user) && $user->nivel=='establ') style="display:block" @else
                                style="display:none" @endif @if(\Auth::user()->nivel!='establ')
                                     style="display:none" @endif class="col-md-3 form-group div-estable">
                                    <label for="">Establecimientos</label> @if(\Auth::user()->nivel=='establ')
                                        <input type="text" readonly class="form-control"
                                               value="{{$establecimientos[0]->nomestabl}}">
                                        <input type="hidden" class="form-control" name="establecimiento"
                                               value="{{$establecimientos[0]->codestabl}}"> @else
                                        <select class="establecimiento form-control" name="establecimiento" id="">
                                            <option value="">Seleccione</option>

                                        </select>

                                    @endif


                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-block btn-buscar"><i
                                        class="fa fa-search"></i>
                                Buscar Notificacion
                            </button>
                        </div>
                    </fieldset>
                </form>

            </div>

            <div class="noti-container col-sm-12" style="background-color:#FFF">
                <ul class=" lista-noti list-group col-md-12 col-sm-12 col-xs-12">
                </ul>
            </div>
        </div>
    </div>

    <div id="modal-notificacion" class="modal  fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-warning" data-dismiss="modal" href="#"> <i class="fa fa-times"></i>Cerrar</a>

                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>



@endsection

@section('scripts')
    <script src="{{asset('js/jquery.easy-autocomplete.min.js')}}"></script>
    <script src="{{asset('js/forms.js')}}"></script>
    <script src="{{asset('js/helpers.js')}}"></script>
    <script>
        var tipo_mujer = 'nombres'
        var tipo_vigilante = 'nombres'


        function guardarIndicadoresEmbarazo(edad, controles, planparto, datos, url, tipo) {


            if ($(edad).val() != '') {
                if (parseInt($(edad).val()) < 10 || parseInt($(edad).val()) > 59) {
                    $.alert({
                        title: 'Alerta',
                        content: 'la edad de la mujer debe estar en el rango de 10 a 59 años',
                    });
                    return false;

                }

            } else {
                $.alert({
                    title: 'Alerta',
                    content: 'Debe ingresar la edad de la mujer',
                });
            }

            if (parseInt(controles) >= 2) {

                if (parseInt(controles) > 9) {
                    $.alert({
                        title: 'Alerta',
                        content: 'Maximo de controles prenatales es 9',
                    });
                    return false;
                }

                if (!$(planparto).is(':checked')) {

                    $.alert({
                        title: 'Alerta',
                        content: 'La mujer tiene ' + controles + ' controles prenatales debes llenar plan de parto, si es asi marca la casilla "Plan de parto Llenado"',
                    });
                    return false;
                }
            }
            datos += '&tipo=' + tipo


            $.ajax({
                type: "post",
                url: url,
                data: datos,
                dataType: "json",
                success: function (response) {

                    if (response.status) {
                        $.alert({
                            title: 'Registro Exitoso',
                            content: 'Se guardaron los datos correctamente',
                        });
                    }

                }
            });


        }

        function guardarMuerteMujerIndicador(datos, url) {

            $.ajax({
                type: "post",
                url: url,
                data: datos,
                dataType: "json",
                success: function (response) {

                    if (response.status) {
                        $.alert({
                            title: 'Registro Exitoso',
                            content: 'Se guardaron los datos correctamente',
                        });
                    }
                    console.log(response);
                }
            });

        }

        function guardarPartoIndicador(datos, url) {

            $.ajax({
                type: "post",
                url: url,
                data: datos,
                dataType: "json",
                success: function (response) {

                    if (response.status) {
                        $.alert({
                            title: 'Registro Exitoso',
                            content: 'Se guardaron los datos correctamente',
                        });
                    }
                }
            });
        }

        function guardarMuerteBebeIndicador(datos, url) {

            console.log('formulario', datos)
            $.ajax({
                type: "post",
                url: url,
                data: datos,
                dataType: "json",
                success: function (response) {

                    if (response.status) {
                        $.alert({
                            title: 'Registro Exitoso',
                            content: 'Se guardaron los datos correctamente',
                        });
                    }
                    console.log(response);
                }
            });

        }

        $(document).ready(function () {

            $('#cb-nombres').click(function () {

                if ($('#cb-nombres').is(':checked')) {
                    tipo_mujer = 'nombres'
                }

            })
            $('#cb-carnet').click(function () {

                if ($('#cb-carnet').is(':checked')) {
                    tipo_mujer = 'carnet'
                }
            })


            $('#cb-nombres-vigilante').click(function () {

                if ($('#cb-nombres-vigilante').is(':checked')) {
                    tipo_vigilante = 'nombres'
                }

            })
            $('#cb-carnet-vigilante').click(function () {

                if ($('#cb-carnet-vigilante').is(':checked')) {
                    tipo_vigilante = 'carnet'
                }

            })


            $('.buscar-mujer').select2({
                width: '300',
                placeholder: "Buscar una  mujer",
                allowClear: true,

                ajax: {
                    url: '{{url("ajax/buscar-mujer")}}',
                    dataType: 'json',
                    delay: 250,

                    type: "GET",
                    data: function (params) {
                        return {
                            phrase: params.term, // search term
                            tipo: tipo_mujer, // search term


                        };
                    },
                    processResults: function (data) {
                        var arr = []
                        $.each(data, function (index, value) {
                            console.log(index, value)
                            arr.push({
                                id: value.value,
                                text: value.label
                            })
                        })
                        return {
                            results: arr
                        };
                    },

                    cache: true
                },

                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,

            });

            $('.input-search-vigilante').select2({
                width: '300',
                placeholder: "Buscar una  vigilante",
                allowClear: true,

                ajax: {
                    url: '{{url("ajax/buscar-vigilante")}}',
                    dataType: 'json',
                    delay: 250,

                    type: "GET",
                    data: function (params) {
                        return {
                            phrase: params.term, // search term
                            tipo: tipo_vigilante, // search term


                        };
                    },
                    processResults: function (data) {
                        var arr = []
                        $.each(data, function (index, value) {

                            arr.push({
                                id: value.value,
                                text: value.label
                            })
                        })
                        return {
                            results: arr
                        };
                    },

                    cache: true
                },

                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,

            });

            $('.btn-buscar').click(function () {
                var data = $('.form-buscar').serializeArray();
                $('.lista-noti').empty();
                buscar = data;
                addFila('todas')
            })

            $('#cb-nacional').click(function () {
                if ($(this).is(':checked')) {
                    $('.div-dptos').hide();
                    $('.div-municipios').hide();
                    $('.div-estable').hide();
                }
            })

            $('#cb-depto').click(function () {
                if ($(this).is(':checked')) {
                    $('.div-dptos').show();
                    $('.div-red').hide();
                    $('.div-municipios').hide();
                    $('.div-estable').hide();
                }
            })

            $('#cb-red').click(function () {
                if ($(this).is(':checked')) {
                    @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" )
                    $('.div-dptos').show();
                    @endif
                    $('.div-red').show();
                    $('.div-municipios').hide();
                    $('.div-estable').hide();
                }
            })

            $('#cb-muni').click(function () {
                if ($(this).is(':checked')) {
                    @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto"  )
                    $('.div-dptos').show();
                    @endif
                    $('.div-municipios').show();
                    $('.div-estable').hide();
                    $('.div-red').hide();
                }
            })

            $('#cb-esta').click(function () {

                if ($(this).is(':checked')) {

                    @if(auth()->user()->nivel=="nacional")
                    $('.div-dptos').show();
                    $('.div-municipios').show();
                    $('.div-estable').show();
                    @endif
                    @if(auth()->user()->nivel=="depto")
                    $('.div-municipios').show();
                    $('.div-estable').show();
                    @endif
                    @if(auth()->user()->nivel=="red")
                    $('.div-red').show();
                    $('.div-estable').show();
                    $('.red').val('');
                    @endif

                    @if(auth()->user()->nivel=="muni")
                    $('.div-municipios').show();
                    $('.div-estable').show();
                    @endif

                }
            })

            $('.departamento').change(function () {
                //   alert('asd')
                var _dpto = $(this).val();
                $('.municipio').val('');
                $(".municipio option").each(function (i, o) {

                    if ($(o).attr("dptoid") === _dpto) {
                        $(o).show()
                    } else {
                        $(o).hide()
                    }
                });
                $('.red').val('');
                $(".red option").each(function (i, o) {

                    if ($(o).attr("dptoid") === _dpto) {
                        $(o).show()
                    } else {
                        $(o).hide()
                    }
                });


            })

            $('.municipio').change(function () {
                var id = $(this).val();
                var option = "";
                $.ajax({
                    url: "{{url('ajax/establecimientos-municipio')}}",
                    type: 'GET',
                    dataType: 'json',
                    data: {idmuni: id},
                    success: function (res) {
                        $('.establecimiento').find('option').not(':first').remove();

                        $('.establecimiento').append(res.options);

                    },
                })

            })

            $('.red').change(function () {
                var id = $(this).val();
                var option = "";
                $.ajax({
                    url: "{{url('ajax/establecimientos-red')}}",
                    type: 'GET',
                    dataType: 'json',
                    data: {idred: id},
                    success: function (res) {
                        $('.establecimiento').find('option').not(':first').remove();

                        $('.establecimiento').append(res.options);

                    },
                })

            })

            $('body').on('click', '.btn-cancelarMuerteMujer', function (_evt) {

                _evt.preventDefault();
                var datos = $('.form-detalle-muertemujer').serialize();
                var url = $('.form-detalle-muertemujer').attr('action')

                console.log('formulario', datos)
                let opcion = $(this).closest('form').find('.muertedurante');
                //  let fecha= $(this).closest('form').find('.fechafallecimiento');
                let edad = $(this).closest('form').find('.edad').val();
                let pa = $(this).closest('form').find('.planaccion');
                let av = $(this).closest('form').find('.autopsia');
                if ($(opcion).val() == '') {
                    $.alert({
                        title: 'Alerta',
                        content: 'Seleccione una opcion: En la persona fallecio durante...',
                    });
                    return false;
                }

                if (edad == "") {
                    $.alert({
                        title: 'Alerta',
                        content: 'Debe escribir una edad de la mujer',
                    });

                    return false;
                }
                if (parseInt(edad) < 10 || parseInt(edad) > 59) {
                    $.alert({
                        title: 'Alerta',
                        content: 'La edad debe estar entre 10 y 59 años.',
                    });

                    return false;
                }
                if (!$(pa).is(':checked') || !$(av).is(':checked')) {

                    $.alert({
                        title: 'Alerta',
                        content: 'Marque "Tiene autposia verbal" y  "Tiene plan de accion" para poder cerrar la notificacion',
                    });


                    return false;

                }
                datos += '&tipo=cerrar'

                $.confirm({
                    title: 'Confirmar cerrar notificacion!',
                    content: 'Si deseo cerrar la notificacion',
                    buttons: {
                        procesar: {
                            text: "si cerrar la notificacion",
                            action: function () {
                                guardarMuerteMujerIndicador(datos, url)


                            }

                        },
                        Cancelar: function () {

                        }

                    }
                });
            });

            $('body').on('click', '.btn-guardardetallemuertemujer', function (_evt) {

                _evt.preventDefault();
                var datos = $('.form-detalle-muertemujer').serialize();
                var url = $('.form-detalle-muertemujer').attr('action')
                let opcion = $(this).closest('form').find('.muertedurante');
                let edad = $(this).closest('form').find('.edad').val();
                let pa = $(this).closest('form').find('.planaccion');
                let av = $(this).closest('form').find('.autopsia');
                if ($(opcion).val() == '') {
                    $.alert({
                        title: 'Alerta',
                        content: 'Seleccione una opcion: En la persona fallecio durante...',
                    });
                    return false;
                }

                if (edad == "") {
                    $.alert({
                        title: 'Alerta',
                        content: 'Debe escribir una edad de la mujer',
                    });

                    return false;
                }
                if (parseInt(edad) < 10 || parseInt(edad) > 59) {
                    $.alert({
                        title: 'Alerta',
                        content: 'La edad debe estar entre 10 y 59 años.',
                    });

                    return false;
                }
                if ($(pa).is(':checked') && !$(av).is(':checked')) {

                    $.alert({
                        title: 'Alerta',
                        content: 'No puede marcar la casilla "Tiene plan de accion" sin haber marcado primero  la casilla "Tiene autopsia verbal"',
                    });
                    $(pa).prop('checked', false)

                    return false;

                }
                datos += '&tipo=guardar'

                guardarMuerteMujerIndicador(datos, url)

            });


            $('body').on('click', '.btn-guardardetalleparto', function (_evt) {

                _evt.preventDefault();
                var datos = $('.form-detalle-parto').serialize();
                var url = $('.form-detalle-parto').attr('action')
                datos += '&tipo=guardar'
                if ($('.atendidoNombre').val() == '') {

                    $.alert({
                        title: 'Alerta',
                        content: 'Tiene que llenar el campo. Nombre de la partera o familiar',

                    });

                    return false
                }
                guardarPartoIndicador(datos, url)

            });
            $('body').on('click', '.btn-cancelarParto', function (_evt) {

                _evt.preventDefault();
                var datos = $('.form-detalle-parto').serialize();
                var url = $('.form-detalle-parto').attr('action')
                datos += '&tipo=cerrar'
                if ($('.atendidoNombre').val() == '') {

                    $.alert({
                        title: 'Alerta',
                        content: 'Tiene que llenar el campo. Nombre de la partera o familiar',

                    });

                    return false
                }

                $.confirm({
                    title: 'Confirmar cerrar notificacion!',
                    content: 'Si deseo cerrar la notificacion',
                    buttons: {
                        procesar: {
                            text: "si cerrar la notificacion",
                            action: function () {
                                guardarPartoIndicador(datos, url)

                            }

                        },
                        Cancelar: function () {

                        }

                    }
                });


            });


            $('body').on('click', '.btn-guardar-detalle-embarazo', function (_evt) {

                _evt.preventDefault();
                let edad = $(this).closest('form').find('.edadmujer')
                let controles = $(this).closest('form').find('.numerocontroles').val()
                let planparto = $(this).closest('form').find('.cb-planparto')

                var datos = $('.form-detalle-embarazo').serialize();
                var url = $('.form-detalle-embarazo').attr('action')

                guardarIndicadoresEmbarazo(edad, controles, planparto, datos, url, 'guardar')


            });

            $('body').on('click', '.btn-cancelarEmbarazo', function (_evt) {

                _evt.preventDefault();
                let edad = $(this).closest('form').find('.edadmujer')
                let controles = $(this).closest('form').find('.numerocontroles').val()
                let planparto = $(this).closest('form').find('.cb-planparto')

                var datos = $('.form-detalle-embarazo').serialize();
                var url = $('.form-detalle-embarazo').attr('action')

                guardarIndicadoresEmbarazo(edad, controles, planparto, datos, url, 'cerrar')


            });


            $('body').on('click', '.btn-guardardetallemuertebebe', function (_evt) {

                _evt.preventDefault();

                let opcion = $(this).closest('form').find('.cb-bebevivo');
                let fecha_nacimiento = $(this).closest('form').find('.fechanacimiento');
                let fecha_fallecimiento = $(this).closest('form').find('.fechafallecimiento');

                if ($(opcion).is(':checked')) {
                    if ($(fecha_nacimiento).val() == '') {
                        $.alert({
                            title: 'Alerta',
                            content: 'Debe ingresar una fecha de Nacimiento',
                        });
                        return false;
                    }
                    if ($(fecha_fallecimiento).val() == '') {
                        $.alert({
                            title: 'Alerta',
                            content: 'Debe ingresar una fecha de Fallecimiento',
                        });
                        return false;
                    }
                }
                var datos = $('.form-detalle-muertebebe').serialize();
                var url = $('.form-detalle-muertebebe').attr('action')
                datos += '&tipo=guardar'

                guardarMuerteBebeIndicador(datos, url)


            });


            $('body').on('click', '.btn-cancelarMuerteBebe', function (_evt) {

                _evt.preventDefault();

                let opcion = $(this).closest('form').find('.cb-bebevivo');
                let fecha_nacimiento = $(this).closest('form').find('.fechanacimiento');
                let fecha_fallecimiento = $(this).closest('form').find('.fechafallecimiento');
                let pa = $(this).closest('form').find('.cb-pa');
                let av = $(this).closest('form').find('.cb-av');


                var datos = $('.form-detalle-muertebebe').serialize();
                var url = $('.form-detalle-muertebebe').attr('action')
                datos += '&tipo=cerrar'

                if ($(opcion).is(':checked')) {
                    if ($(fecha_nacimiento).val() == '') {
                        $.alert({
                            title: 'Alerta',
                            content: 'Debe ingresar una fecha de Nacimiento',
                        });
                        return false;
                    }
                    if ($(fecha_fallecimiento).val() == '') {
                        $.alert({
                            title: 'Alerta',
                            content: 'Debe ingresar una fecha de Fallecimiento',
                        });
                        return false;
                    }
                }

                if (!$(pa).is(':checked') || !$(av).is(':checked')) {

                    $.alert({
                        title: 'Alerta',
                        content: 'Marque "Tiene autposia verbal" y  "Tiene plan de accion" para poder cerrar la notificacion',
                    });


                    return false;

                }


                $.confirm({
                    title: 'Confirmar cerrar notificacion!',
                    content: 'Si deseo cerrar la notificacion',
                    buttons: {
                        procesar: {
                            text: "si cerrar la notificacion",
                            action: function () {
                                guardarMuerteBebeIndicador(datos, url)


                            }

                        },
                        Cancelar: function () {

                        }

                    }
                });


            });


            $('.btn-guardar-noti').click(function () {

                if ($('.noti-cb:checked').length <= 0) {
                    alert('Debe seleccionar una notificacion')
                    return false;
                }

                if (!$(".cb-buscarmujer").is(':checked')) {
                    if ($(".nombres_mujer").val() == '') {
                        alert('Debe ingresar el   nombre de la mujer')
                        return false;
                    }
                    if ($(".primer_apellido").val() == '') {
                        alert('Debe ingresar el primer apellido de la mujer')
                        return false;
                    }

                }

                if ($(".cod_vigilante").val() == '') {
                    alert('Debe seleccionar un vigilante')
                    return false;
                }

                if ($('.fecha-generacion').val() == '') {
                    alert('Debe seleccionar una fecha de generacion de la notificacion')
                    return false;
                }

                var data = $('.form-notificacion').serialize();

                $.ajax({
                    type: "POST",
                    url: "{{url('notificaciones/guardar')}}",
                    data: data,
                    dataType: "json",
                    before: function () {
                        $('.btn-guardar-noti').attr('disabled', true)
                    },
                    complete: function () {
                        $('.btn-guardar-noti').attr('disabled', false)
                    },
                    success: function (response) {
                        if (response.status) {
                            $('#modal-notificacion-embarazo').modal('hide')
                            abririModalVer(response.notificacion_id);
                            $('.buscar-mujer').val('').trigger('change')
                            $('.input-search-vigilante').val('').trigger('change')
                            $('.form-notificacion')[0].reset();


                        } else {
                            alert('ocurrio un error en el servidor');
                        }


                    },
                    error: function () {
                        alert('ocurrio un error en el servidor');
                    }
                });

            })


            $('.crear-embarazo').click(function () {
                $('#modal-notificacion-embarazo').modal('show')
            })


            $('.cb-buscarmujer').click(function () {
                if ($(this).is(':checked')) {
                    $('.div-buscar').removeClass('hide')
                    $('.div-datos').addClass('hide')
                } else {
                    $('.div-buscar').addClass('hide')
                    $('.div-datos').removeClass('hide')
                }
            });

            //*****************BUSCAR MUJER************////
            //         $(".input-search").click(function(){
            //         if($(this).val()!=''){
            //             $(this).val('');
            //         }
            //         })
            //         $(".input-search-vigilante").click(function(){
            //         if($(this).val()!=''){
            //             $(this).val('');
            //         }
            //         })
            //     $(".input-search").easyAutocomplete({
            //     url: function(phrase) {
            //     return '{{url("ajax/buscar-mujer")}}';

            // },


            // getValue: function(element) {
            //     console.log(element);
            //     return element.nombres+' '+element.primerApellido+' '+element.segundoApellido+' CI:'+element.numeroCarnet;
            // },

            // ajaxSettings: {
            //     dataType: "json",
            //     method: "GET",
            //     data: {
            //     dataType: "json"
            //     }
            // },
            //     list: {
            //            maxNumberOfElements: 1000,
            //             onClickEvent: function() {

            //                  var value = $(".input-search").getSelectedItemData();

            //                 $(".idmujer").val(value.IdMujer);
            //                 $(".idmujerandroid").val(value.idAndroid);

            //                 },
            //                 onKeyEnterEvent: function() {
            //                 var value = $(".input-search").getSelectedItemData();

            //                 $(".idmujer").val(value.IdMujer);
            //                 $(".idmujerandroid").val(value.idAndroid);

            //                 },
            //             },

            // preparePostData: function(data) {
            //             let tipo='nombres';
            //             if($('#cb-nombres').is(':checked')){
            //                 tipo='nombres'
            //             }
            //             else {
            //                 tipo='carnet';
            //             }
            //           data.phrase = $(".input-search").val();
            //             data.tipo = tipo;
            //             return data;

            // },
            // theme: "plate-dark",

            // requestDelay: 400
            // });

            //   $(".input-search-vigilante").easyAutocomplete({
            //     url: function(phrase) {
            //     return '{{url("ajax/buscar-vigilante")}}';

            // },


            //******************lista de  notificaiones***************


            var page = 1;
            var tipo_notificacion = 'todas';
            var height = $('.noti-container').height();
            var altura = 0;
            var buscar = null;


            addFila(tipo_notificacion);

            contarNotificaciones()
            contarNotificacionesMenu()

            function contarNotificacionesMenu() {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "{{url('notificaciones/contar-notificaciones')}}",

                    beforeSend: function () {
                        $('#spinner').show();
                    },
                    complete: function () {
                        $('#spinner').hide();
                    },
                    success: function (response) {


                        if (response.status) {
                            if (response.contador.todas > 0) {


                                $('.contador-todas').html(response.contador.todas)
                                $('.contador-todas').show();
                            } else {
                                $('.contador-todas').hide();
                            }

                            if (parseInt(response.contador.num_embarazo) > 0) {

                                $('.contador-embarazo').html(response.contador.num_embarazo)
                                $('.contador-embarazo').show();
                            } else {
                                $('.contador-embarazo').hide();
                            }

                            if (parseInt(response.contador.num_muertemujer) > 0) {

                                $('.contador-muertemujer').html(response.contador.num_muertemujer)
                                $('.contador-muertemujer').show();
                            } else {
                                $('.contador-muertemujer').hide();

                            }
                            if (parseInt(response.contador.num_muertebebe) > 0) {

                                $('.contador-muertebebe').html(response.contador.num_muertebebe)
                                $('.contador-muertebebe').show();
                            } else {
                                $('.contador-muertebebe').hide();
                            }
                            if (parseInt(response.contador.num_parto) > 0) {

                                $('.contador-parto').html(response.contador.num_parto)
                                $('.contador-parto').show();
                            } else {
                                $('.contador-parto').hide();

                            }
                            if (parseInt(response.contador.num_teleconsulta) > 0) {

                                $('.contador-teleconsultas').html(response.contador.num_teleconsulta)
                                $('.contador-teleconsultas').show();
                            } else {
                                $('.contador-teleconsultas').hide();

                            }
                        }
                    },

                    error: function () {
                        $('#spinner').show();
                    }
                });
            }

            function addFila(tipo_notificacion) {

                $.ajax({
                    type: "GET",
                    dataType: "html",
                    url: "{{url('notificaciones/listar-notificaciones')}}",
                    data: {page: page, tipo_notificacion: tipo_notificacion, buscar: buscar},
                    beforeSend: function () {
                        $('#spinner').show();
                    },
                    complete: function () {
                        $('#spinner').hide();
                    },
                    success: function (response) {

                        $('.lista-noti').append(response)
                    },

                    error: function () {
                        $('#spinner').show();
                    }
                });
            }

            $('.crear-embarazo').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')
            });

            $('.item-embarazo').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                tipo_notificacion = 'embarazo'
                height = 480;
                addFila(tipo_notificacion)
            })

            $('.item-muerte-mujer').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                height = 480;
                tipo_notificacion = 'muertemujer'
                addFila(tipo_notificacion)
            })

            $('.item-muerte-bebe').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                tipo_notificacion = 'muertebebe'
                height = 480;
                addFila(tipo_notificacion)
            })

            $('.item-parto').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                tipo_notificacion = 'parto'
                height = 480;
                addFila(tipo_notificacion)
            })
            $('.item-teleconsulta').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                tipo_notificacion = 'teleconsulta'
                height = 480;
                addFila(tipo_notificacion)
            })

            $('.item-todas').click(function () {
                var lista = $(this).closest('.list-group').find('.active').removeClass("active");
                $(this).addClass('active')

                $('.lista-noti').empty();
                page = 1;
                tipo_notificacion = 'todas'
                height = 480;
                buscar = null
                addFila(tipo_notificacion)
            })


            $('.noti-container').scroll(function () {
                var scroll = $('.noti-container').scrollTop()
                if ((scroll) > height) {
                    altura = height;
                    height = height + 400;
                    page++;
                    addFila(tipo_notificacion);
                }

            });


            $('body').on('click', '.div-noti', function () {
                var id = $(this).attr('id-noti')
                var _this = $(this);

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "{{url('notificaciones/marcar-visto')}}",
                    data: {id: id},
                    beforeSend: function () {
                        $('#spinner').show();
                    },
                    complete: function () {
                        $('#spinner').hide();
                    },
                    success: function (response) {


                        if (response.status) {
                            $(_this).removeClass('no-visto');
                            contarNotificaciones();
                            contarNotificacionesMenu();

                        }
                    },
                    error: function () {
                        // alert('Ocurrio un error en el servidor')
                    }
                });
            })

            @isset($notificacion_id)
            abririModalVer({{$notificacion_id}})

            @endisset

            function abririModalVer(id) {
                $.ajax({
                    type: "get",
                    url: `{{url('notificaciones/${id}/ver')}}`,
                    dataType: "html",
                    success: function (response) {
                        $('#modal-notificacion .modal-body').html(response)
                        $('#modal-notificacion').modal('show')
                    }
                });
            }

            $('body').on('click', '.btn-ver', function () {
                $('#modal-notificacion .modal-body').empty()
                var id = $(this).attr('id')
                abririModalVer(id);

            })
        });

    </script>

@endsection
