@php
    \Carbon\Carbon::setLocale('es');
@endphp

@foreach($notificaciones as $k=>$v)

    @php
        $notificacion=$v->notificable;
        $vigilante=is_null($notificacion->vigilante)?null:$notificacion->vigilante;
        $inicial='';
        $estilo='';


        switch($v->notificable_type){
        case 'App\Mujer':
        $inicial='RM';
        $estilo='icon-mujer';

        break;

        case 'App\NotificacionEmbarazo':
        $inicial='NME';
        $estilo='icon-embarazo';
        break;
        case 'App\NotificacionMuerteMujer':
        $inicial='NMM';
        $estilo='icon-muerte-mujer';
        break;
        case 'App\NotificacionMuerteBebe':
        $inicial='NMB';
        $estilo='icon-muerte-bebe';
        break;
        case 'App\NotificacionParto':
        $inicial='NP';
        $estilo='icon-parto';
        break;
        case 'App\NotificacionTeleconsulta':
        $inicial='NT';
        $estilo='icon-teleconsulta';
        break;

        }

    @endphp

    <div class="  div-noti
 @if(\Auth::user()->nivel=='nacional' && !$v->vistoNacional)
            no-visto
@endif

    @if(\Auth::user()->nivel=='depto' && !$v->vistoDepartamento)
            no-visto
@endif

    @if(\Auth::user()->nivel=='muni' && !$v->vistoMunicipio)
            no-visto
@endif

    @if(\Auth::user()->nivel=='establ' && !$v->vistoEstablecimiento)
            no-visto
@endif

            list-group-item row" id-noti="{{$v->id}}">

        @if($v->notificable_type=='App\NotificacionTeleconsulta')
            <div style="padding:2px;" class=" col-md-1 col-xs-2 col-sm-2 {{$estilo}}">
                <h5 class="letras-icon">
                    {{$inicial}}

                </h5>
                <center
                        style="font-size:12px; color:antiquewhite;  font-weight: bold;  padding:0">N°
            {{$notificacion->numeroTeleconsulta}}</center>
            </div>
            <div class="col-md-8 ">
                <ul class="">
                    <li><strong>PACIENTE </strong>
                        <span class="small"> <i
                                    class="fa fa-user"></i>{{$notificacion->nombresPaciente}} {{$notificacion->apellidoPaciente}}


                        @isset($v->obtenerEstablecimiento()->nomestabl){{$v->obtenerEstablecimiento()->nomestabl}} /
                        @endisset
                        @isset($v->obtenerMunicipio()->mnc_nombre){{$v->obtenerMunicipio()->mnc_nombre}} / @endisset
                        @isset($v->obtenerDepartamento()->dpt_nombre){{$v->obtenerDepartamento()->dpt_nombre}}@endisset
                        </span>
                    </li>
                    <li><strong>DOCTOR </strong>
                        <span class="small">
                            <i class="fa fa-user"></i>{{$notificacion->nombresMedico}} {{$notificacion->apellidosMedico}}
                        </span>
                    </li>
                    <li>
                        <ul class="list-inline">
                            <li><strong><i class="fa fa-clock-o"></i> </strong> {{$v->created_at->diffForHumans()}}
                                atrás<strong> <i class="fa fa-calendar"></i> {{$v->created_at->format('d/M/Y H:i:s')}}
                                </strong>
                            </li>

                        </ul>
                    </li>

                </ul>

            </div>
            @switch($notificacion->estadoTeleconsulta)
                @case('confirmada')
                @php $alert="info"; @endphp
                @break
                @case('cancelada')
                @php $alert="danger"; @endphp
                @break
                @case('atendida')
                @php $alert="success"; @endphp
                @break
            @endswitch
            <div class="col-md-3 col-lg-3 col-xs-12 alert alert-{{$alert}}">
                <center class="text-uppercase"><strong>{{$notificacion->estadoTeleconsulta}}</strong></center>
            </div>

            @else
            <div style="padding:2px;" class=" col-md-1 col-xs-2 col-sm-2 {{$estilo}}">
                <h5 class="letras-icon">
                    {{$inicial}}

                </h5>
                <span
                        style="font-size:12px; color:antiquewhite; position:absolute; font-weight: bold;  top:60%; left:30%; padding:0">N°
            {{$v->notificable_id}}</span>
            </div>
            <div class="col-md-8 ">
                <ul class="">
                    <li><strong>Vigilante </strong>

                    @if(isset($vigilante->IdVigilante))
                        <span class="small"><a href="{{url('vigilantes/'.$vigilante->IdVigilante.'/ver-perfil')}}"> <i
                                        class="fa fa-user"></i>@isset($vigilante->persona->nombre_completo){{$vigilante->persona->nombre_completo}}
                                @endisset </a></span>
                     @else
                     <span class="text-danger bold">NO TIENE VIGILANTE</span>
                     @endif

                        <span><i class=" fa fa-map-marker"></i>
                    @isset($v->obtenerEstablecimiento()->nomestabl){{$v->obtenerEstablecimiento()->nomestabl}} /
                            @endisset
                            @isset($v->obtenerMunicipio()->mnc_nombre){{$v->obtenerMunicipio()->mnc_nombre}} / @endisset
                            @isset($v->obtenerDepartamento()->dpt_nombre){{$v->obtenerDepartamento()->dpt_nombre}}@endisset
                </span>
                    </li>
                    <li>
                        <ul class="list-inline">
                            <li><strong><i class="fa fa-clock-o"></i> </strong> {{$v->created_at->diffForHumans()}}
                                atrás<strong> <i class="fa fa-calendar"></i> {{$v->created_at->format('d/M/Y H:i:s')}}
                                </strong>
                            </li>

                        </ul>
                    </li>

                </ul>
                @if($notificacion->estado=='anulado')
                    <h4 class="text-center b text-success" style="background-color:crimson;color:#FFF"><i
                                class="fa fa-close"></i>ANULADO</span></h4>

                @else
                    @if($notificacion->cerrado)
                        <h4 class="text-center bg-primary text-success" style="color:#FFF"><i class="fa fa-check"></i>CERRADO
                            EL <span>{{$notificacion->fecha_cerrado}}</span></h4>
                    @endif
                @endif
            </div>
            <div class="div-item-options col-md-3 col-lg-3 col-xs-12">
                <div class="btn-group">
                    @php
                        $ahora=\Carbon\Carbon::now();
                        $diferencia=$ahora->diffInWeeks($notificacion->created_at);
                    @endphp
                    @if(!isset($notificacion->detalle) && $notificacion->estado!='anulado')
                        @if($diferencia<=1)
                            <button type="button" id="{{$v->id}}" class="btn-ver btn btn-primary"><i
                                        class="fa fa-check"></i>Esta semana
                            </button>

                        @endif
                        @if($diferencia==2)
                            <button type="button" id="{{$v->id}}" class=" btn-ver btn btn-warning"><i
                                        class="fa fa-warning"></i>{{$diferencia}} Semanas PRIORIDAD
                            </button>
                        @endif

                        @if($diferencia>=3)
                            <button type="button" id="{{$v->id}}" class="btn-ver btn btn-danger"><i
                                        class="fa fa-warning"></i>{{$diferencia}} Semanas URGENTE
                            </button>
                        @endif
                    @endif
                    <button type="button" id="{{$v->id}}" class=" btn-ver btn btn btn-primary"><i
                                class="fa fa-eye"></i>Registrar Indicadores
                    </button>
                </div>
            </div>
        @endif
    </div>
@endforeach
