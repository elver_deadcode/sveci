
<div role="menu" class="notification-author dropdown-menu animated flipInX">
    <ul id="lista-todas-notificaciones" class=" notification-menu ">

@foreach ($lista_notificaciones as $noti)
 @php
 $notificacion=\App\Notificacion::find($noti['id']);
 $class=$notificacion->obtenerRelacion();
 $abreviacion=$notificacion->obtenerAbreviacionNotificacion();
 $vigilante=!isset($class->vigilante)?'':$class->vigilante->nombre_completo;
 @endphp

<li >
<a  class="col-md-12 col-xs-12 col-sm-12" href="{{url('notificaciones/'.$noti["id"].'/ver-vista')}}" style="margin-bottom:-25px; font-size:10px;">
    <div class=" icon-{{strtolower($abreviacion)}} col-sm-3 col-md-3 col-xs-3">
               <p>{{$abreviacion}}</p>
            </div>
            <div class=" col-sm-8 col-md-8 col-xs-8">
                <span class="message-date"> {{$noti['fecha2']}}  {{$noti['fecha']}} </span>
                <h6>{{$vigilante}}</h6>

            </div>
    </a>
</li>
<div class="clearfix"></div>

@endforeach
</ul>
<div class="notification-view">
<a href="{{url('notificaciones')}}">Ver todas las notificaciones</a>
</div>
</div>
