
@section('styles')
<style>

    .dangerous{
        background-color:#f4424b !important;
        color:#FFF;
    }
    .mini-submenu {
        display: none;
        background-color: rgba(0, 0, 0, 0);
        border: 1px solid rgba(0, 0, 0, 0.9);
        border-radius: 4px;
        padding: 9px;
        /*position: relative;*/
        width: 42px;

    }

    .mini-submenu:hover {
        cursor: pointer;
    }

    .mini-submenu .icon-bar {
        border-radius: 1px;
        display: block;
        height: 2px;
        width: 22px;
        margin-top: 3px;
    }

    .mini-submenu .icon-bar {
        background-color: #000;
    }

    #slide-submenu {
        background: rgba(0, 0, 0, 0.45);
        display: inline-block;
        padding: 0 8px;
        border-radius: 4px;
        cursor: pointer;
    }
</style>

@stop
<div class="col-md-3  col-xs-12 col-xs-12 sidebar bg-primary "  style="padding:10px 2px 0px 10px;">
    <button style="margin-top:-5px" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#side-menu-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-align-justify">Menu</span>

    </button>

    <div class="list-group collapse navbar-collapse" id="side-menu-collapse"style="font-size:12px">
        <span href="#" class="list-group-item ">
        <a href="{{url('/home')}}" class="link"><i class="fa fa-home"></i>REGRESAR</a>
            <span class="pull-right" id="slide-submenu">
                {{-- <i class="fa fa-times"></i> --}}
            </span>
        </span>
        @permission('crear_notificaciones')
        <a href="#"  class="list-group-item crear-embarazo text-warning">
                            <i class="fa fa-plus"></i> REGISTRAR UNA NOTIFICACION
         </a>
         @endpermission

        <a href="#" class="list-group-item active item-todas">
            <i class="fa fa-bell-o"></i> TODAS LAS NOTIFICACIONES     <span class="badge  contador-todas"  style="background-color:#f4424b; display:none; color:aliceblue;" >0</span>
            </a>
        <a href="#" class="list-group-item item-embarazo">
            <i class="fa fa-bell-o"></i>NOTIFICACIONES DE EMBARAZO  <span class="badge contador-embarazo" style="background-color:#f4424b; display:none; color:aliceblue;">0</span>
        </a>

        <a href="#" class="list-group-item item-muerte-mujer">
            <i class="fa fa-bell-o"></i>NOTIFICACIONES  MUERTE DE MUJER  <span class="badge contador-muertemujer" style="background-color:#f4424b; display:none; color:aliceblue;">0</span>
        </a>
        <a href="#" class="list-group-item item-muerte-bebe">
            <i class="fa fa-bell-o"></i> NOTIFICACIONES MUERTE DE BEBE <span class="badge contador-muertebebe" style="background-color:#f4424b; display:none; color:aliceblue;">0</span>
        </a>
        <a href="#" class="list-group-item item-parto">
            <i class="fa fa-bell-o"></i> NOTIFICACIONES PARTO <span class="badge contador-parto" style="background-color:#f4424b; display:none; color:aliceblue;">0</span>
        </a>
        <a href="#" class="list-group-item item-teleconsulta">
            <i class="fa fa-bell-o"></i> NOTIFICACIONES TELECONSULTAS <span class="badge contador-teleconsultas" style="background-color:#f4424b; display:none; color:aliceblue;">0</span>
        </a>

    </div>
</div>

