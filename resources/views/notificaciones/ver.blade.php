@php
\Carbon\Carbon::setLocale('es');
@endphp
<div class="project-details-area mg-b-40 container">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="project-details-wrap shadow-reset">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="project-details-title">
                                <h2><span class="profile-details-name-nn">{{$titulo}} - Nº {{$notificacion->id}}</h2>
                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Tiene datos Para indicador:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                                        @isset($notificable->detalle)
                                        <span class="label label-primary"><i class="fa fa-check"></i>TIENE DATOS
                                            REGISTRADOS</span>
                                        @endisset
                                        @php
                                        $ahora=\Carbon\Carbon::now();
                                        $diferencia=$ahora->diffInWeeks($notificable->fechaRegistro);
                                        @endphp

                                        @if(!isset($notificable->detalle))
                                        @if($diferencia<=1) <span class="label label-success"><i
                                                class="fa fa-pencil"></i>DEBE REGISTRAR DATOS PARA EL INDICADOR</span>
                                            @endif
                                            @if($diferencia==2)
                                            <span class="label label-warning"><i class="fa fa-warning"></i>CUIDADO YA
                                                PASO 2 SEMANAS, <br>DEBE REGISTRAR DATOS PARA INDICADORES</span>
                                            @endif
                                            @if($diferencia>=3)
                                            <span class="label label-danger"><i class="fa fa-warning"></i>URGENTE!!! YA
                                                PASO MAS DE 3 SEMANAS, <br>DEBE REGISTRAR DATOS PARA INDICADORES</span>
                                            @endif
                                            @endif



                                    </div>
                                </div>

                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Vigilante:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            <span>@isset($notificable->vigilante) <a
                                                    href="{{url('vigilantes/'.$notificable->vigilante->IdVigilante.'/ver-perfil')}}">{{$notificable->vigilante->persona->nombre_completo}}</a>@endisset</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Establecimiento:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            @isset($notificacion->obtenerEstablecimiento()->nomestabl){{$notificacion->obtenerEstablecimiento()->nomestabl}}
                                            / @endisset
                                            @isset($notificacion->obtenerMunicipio()->mnc_nombre){{$notificacion->obtenerMunicipio()->mnc_nombre}}
                                            / @endisset
                                            @isset($notificacion->obtenerDepartamento()->dpt_nombre){{$notificacion->obtenerDepartamento()->dpt_nombre}}@endisset

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Coordenadas GPS :</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            @if($notificable->longitud!='' && $notificable->latitud!='')

                                            <a class="btn btn-primary " target="_blank"
                                                href="https://www.google.com/maps/?q={{$notificable->latitud}},{{$notificable->longitud}}">
                                                <i class="fa fa-map-marker"></i>Abrir en Google maps</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="project-details-st">
                                        <span><strong>Anular/Habilitar Notificacion:</strong></span>

                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="btn-group ">
                                        @if($notificable->estado=='recibido')
                                        @permission('anular_notificacion')
                                        <button id="{{$notificacion->id}}"
                                            class="btn btn-danger btn-anular-notificacion"><i class="fa fa-ban"></i>
                                            ANULAR LA NOTIFICACION</button>
                                            @endpermission
                                        @else
                                        <button
                                            class="btn btn-danger btn-xs">{{strtoupper($notificable->estado)}}</button>

                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Fecha Generacion Notificacion:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            <span>@isset($notificable->fechaRegistro) <i class="fa fa-clock-o"></i>
                                                {{$notificable->fechaRegistro->diffforHumans() }} <br> <i
                                                    class="fa fa-calendar"></i>
                                                {{$notificable->fechaRegistro->format('d/m/Y H:i:s')}}@endisset</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Fecha de registro en el sistema:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            <span>@isset($notificacion->created_at) <i class="fa fa-clock-o"></i>
                                                {{$notificacion->created_at->diffforHumans() }} <br> <i
                                                    class="fa fa-calendar"></i>
                                                {{$notificacion->created_at->format('d/m/Y H:i:s')}}@endisset</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="">
                        <div class="col-lg-12">
                            <div class="project-details-tab">
                                <ul class="nav nav-tabs res-pd-less-sm">
                                    <li class="active"><a data-toggle="tab" href="#home"><strong>Datos de la
                                                Mujer</strong></a>
                                    </li>

                                </ul>
                                <div class="tab-content res-tab-content-project ">
                                    <form action="" class="form-datosmujer">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="project-details-mg">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="project-details-st">
                                                                <span><strong>Nombre Completo:</strong></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="btn-group project-list-ad">


                                                                @if($notificable->idMujerAndroid!='' &&
                                                                $notificable->idMujerAndroid!='0' )
                                                                @php

                                                                $nombres_completo=$mujer['nombres'];
                                                                @endphp


                                                                @else
                                                                @php

                                                                $nombres_completo=$notificable->nombres.'
                                                                '.$notificable->primerApellido.'
                                                                '.$notificable->segundoApellido ;
                                                                @endphp
                                                                @endif
                                                                <input type="text" name="nombrecompleto"
                                                                    class=" nombre-completo form-control form-control-sm"
                                                                    value="{{$nombres_completo}}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="project-details-mg">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="project-details-st">
                                                                <span><strong>Edad
                                                                        <small>(Años)</small>:</strong></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="project-details-dt">
                                                                @if($notificable->idMujerAndroid!='' && $notificable->idMujerAndroid!='0')
                                                                    @if(isset($mujer)&&$mujer['edad']!='')
                                                                        @php
                                                                        $edad=$mujer['edad'];
                                                                        @endphp
                                                                        @else
                                                                        @php
                                                                        $edad=$notificable->edad;
                                                                        @endphp

                                                                    @endif
                                                                @else
                                                                @php
                                                                $edad=$notificable->edad;
                                                                @endphp

                                                                @endif
                                                                <input type="text" name="edad" style="width:5em"
                                                                    class=" edad form-control form-control-sm solo-enteros"
                                                                    value="{{$edad}}">


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-details-mg">
                                                    <div class="row">

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="project-details-st">
                                                            <span><strong>Direccion:</strong></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        @if($notificable->idMujerAndroid!='' &&
                                                        $notificable->idMujerAndroid!='0')
                                                        @php
                                                        $direccion=$mujer['direccion'];
                                                        @endphp
                                                        @else
                                                        @php
                                                        $direccion=$notificable->direccion;
                                                        @endphp

                                                        @endif
                                                        <input type="text" name="direccion" style="width:15em"
                                                            class=" direccion form-control form-control-sm "
                                                            value="{{$direccion}}">
                                                    </div>
                                                </div>
                                                <div class="project-details-mg">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="project-details-st">
                                                                <span><strong>Numero celular:</strong></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <div class="project-details-dt">
                                                                <span><i class=" fa fa-phone"></i>
                                                                    @if($notificable->idMujerAndroid!='' &&
                                                                    $notificable->idMujerAndroid!='0')

                                                                    @php
                                                                    $telefono=$mujer['numero_celular'];
                                                                    @endphp

                                                                    @else
                                                                    @php
                                                                    $telefono=$notificable->telefono;
                                                                    @endphp

                                                                    @endif
                                                                    <input type="text" name="telefono"
                                                                        style="width:10em"
                                                                        class="telefono form-control form-control-sm solo-enteros"
                                                                        value="{{$telefono}}">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <input type="hidden" name="notificacionid" class="notificacionid"
                                                        value="{{$notificacion->notificable_id}}">
                                                    <input type="hidden" name="tiponotificacion"
                                                        class="tiponotificacion"
                                                        value="{{$notificacion->notificable_type}}">
                                                    {{-- <button type="button" class="btn-guardar-datos btn btn-success"><i
                                                            class="fa fa-save"></i> Guardar Datos de la mujer</button> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </form>


                                    @if($notificable->estado!='anulado')


                                    <div id="indicadores" class="row" style="margin-top:0px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            @permission('registrar_indicadores')

                                            @include("notificaciones.form-$abreviacion")
                                            @endpermission
                                        </div>
                                    </div>
                                    @endif



                                    <div id="home" class="tab-pane fade in active animated zoomInLeft">

                                        <div class="user-profile-comment-list">



                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $('.btn-guardar-datos').click(function(){
             let data=$('.form-datosmujer').serialize();
             $.ajax({
                type: "POST",
                url: "{{url('ajax/guardar-datosmujer')}}",
                data: data,
                dataType: "json",
                success: function (response) {
                                       
                                                    
                                                }
             }); 
        })

        $('.btn-anular-notificacion').click(function(){
            let _id=$(this).attr('id')
            var _this=$(this)

            $.confirm({
                        title: 'Anular la  notificacion',
                        icon:' fa fa-ban',
                        theme: 'supervan',
                        content: 'Esta seguro de anular la notificacion?',
                        buttons: {
                            confirmar: function () {
                                $.ajax({
                                       type: "GET",
                                       url: "{{url('ajax/anular-notificacion')}}",
                                       data: {id:_id},
                                       dataType: "json",
                                       success: function (response) {
                                           if(response.success){
                                               $(_this).hide();
                                               $('#indicadores').hide();

                                           }
                                                    
                                                }
                                            });                            },
                            cancelar: function () {
                                this.close();
                            }
                           
                        }
                    });
         
        })

        // $('body').on('click','.btn-guardar-detalle-embarazo',function(_evt){
          
        //    _evt.preventDefault();
        //     var datos=$('.form-detalle-embarazo').serialize();
        //     var url=$('.form-detalle-embarazo').attr('action')

        //     console.log('formulario',datos)
        //    $.ajax({
        //        type: "post",
        //        url: url,
        //        data: datos,
        //        dataType: "json",
        //        success: function (response) {

        //            if (response.status){
        //                $.alert({
        //                 title: 'Registro Exitoso',
        //                 content: 'Se guardaron los datos correctamente',
        //             });
        //            }
        //            console.log(response);
        //        }
        //    });
        // });

          $('body').on('click','.btn-guardar-detalle-muertemujer',function(_evt){
           _evt.preventDefault();
            var datos=$('.form-detalle-muertemujer').serialize();
            var url=$('.form-detalle-muertemujer').attr('action')

            console.log('formulario',datos)
           $.ajax({
               type: "post",
               url: url,
               data: datos,
               dataType: "json",
               success: function (response) {

                   if (response.status){
                       $.alert({
                        title: 'Registro Exitoso',
                        content: 'Se guardaron los datos correctamente',
                    });
                   }
                   console.log(response);
               }
           });
        });

           $('body').on('click','.btn-guardar-detalle-muertebebe',function(_evt){
           _evt.preventDefault();
            var datos=$('.form-detalle-muertebebe').serialize();
            var url=$('.form-detalle-muertebebe').attr('action')
         

            console.log('formulario',datos)
           $.ajax({
               type: "post",
               url: url,
               data: datos,
               dataType: "json",
               success: function (response) {

                   if (response.status){
                       $.alert({
                        title: 'Registro Exitoso',
                        content: 'Se guardaron los datos correctamente',
                    });
                   }
                   console.log(response);
               }
           });
        });

         $('body').on('click','.btn-guardar-detalle-parto',function(_evt){
           _evt.preventDefault();
            var datos=$('.form-detalle-parto').serialize();
            var url=$('.form-detalle-parto').attr('action')
          

            console.log('formulario',datos)
           $.ajax({
               type: "post",
               url: url,
               data: datos,
               dataType: "json",
               success: function (response) {

                   if (response.status){
                       $.alert({
                        title: 'Registro Exitoso',
                        content: 'Se guardaron los datos correctamente',
                    });
                   }
                   console.log(response);
               }
           });
        });
  
      
     
    });

</script>