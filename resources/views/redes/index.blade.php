@extends('layouts.app',[
    'active_registros'=>'active',
    'active_red'=>'active',
    'title'=>'Redes'
])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Registros</a> <span class="bread-slash">/</span>
    </li>
  
    <li><span class="bread-blod">Redes</span>
    </li>
</ul>
@stop
@section('content')


<div class="table-responsive">
        <table style="font-size: 10px" class="table datatable">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>RED</th>
                    
                </tr>
            </thead>

        </table>
        </div>
 @section('scripts')
        <script>
        $(document).ready(function () {
            $('.datatable').DataTable({
             dom: "Bfrltip", 
        ajax: {
            url: "{{url('registros/redes/listar')}}",
            type: 'GET'
              },
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'red', name: 'red'},
          
           
        ]
                
            });
        });
        </script>
 @stop
@stop