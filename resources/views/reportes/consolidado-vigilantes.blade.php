@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ])
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">
@stop

@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">CONSOLIDADO VIGILANTES COMUNITARIOS</span>
    </li>
</ul>




@stop
@section('content')
<div>
    <form action="{{url('reportes/consolidado-vigilantes')}}" method="GET">

        @include('includes.localizacion')
        @include('includes.filtro_tiempo',['reportevigilantes'=>true])


</div>
</form>
</div>
<br>
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs  ">
            <li class="active"><a data-toggle="tab" href="#home"><strong> <i class="fa fa-table"></i> Tabla</strong></a>
            </li>
            <li><a data-toggle="tab" href="#menu1"><strong><i class="fa fa-bar-chart"></i> Graficas</strong></a></li>
        </ul>
    </div>
    <div class="panel-body">

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="row table-responsive">
                    <div class=" col-md-10 div-historial">

                    </div>
                    <div class="col-md-10">


                        <table class="table" style="font-size:10px;">
                            <thead>
                                <tr>
                                    <th>LOCALIZACION</th>
                                    <th>Nro vigilantes<br>que SI activaron <br> su celular </th>
                                    <th>Nro vigilantes<br>que NO activaron <br> su celular </th>
                                    <th>Nro Vigilantes<br>dados Baja</th>
                                    <th>Nro Vigilantes<br>Activados que SI <br>envian notificaciones Android </th>
                                    <th>Nro Vigilantes<br>Activados que SI <br>envian notificaciones Papel </th>
                                    <th>Nro Vigilantes<br>Activados que NO <br>envian notificaciones </th>

                                </tr>
                            </thead>
                            <tbody>



                            </tbody>
                            <tfoot>
                                <tr>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">


                <div class="row">
                    <div class="col-md-6">
                        <div id="fila1-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila1-col2" style="height: 400px"></div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div id="fila2-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila2-col2" style="height: 400px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila3-col1" style="height: 400px"></div>
                    </div>

                </div>


            </div>

        </div>
    </div>
</div>

</div>



@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js">
</script>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{asset('js/graficas.js')}}?time=<?=time()?>"></script>
<script src="{{asset('js/helpers.js')}}"></script>
<script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
    var nivel='nacional', fecha_ini='',fecha_fin=''
        var table;
        var activaron_baja=[];
        var envian=[];
        
        




    
        function vaciarDatos(){
            activaron_baja=[];
             envian=[];
        }
    
        function obtenerDatos(table) {
      // loop table rows
        let data=table.rows({selected:  true}).data();
          
            for (var i=0; i < data.length ;i++){
                // vigilantes_si_activados_porcentaje_torta.push([data[i].localizacion,parseFloat(data[i].vigilantes_si_activados_porcentaje)])
                // vigilantes_si_activados_porcentaje_barra.push({"name":data[i].localizacion,"y":parseFloat(data[i].vigilantes_si_activados)})
               activaron_baja.push({"name":data[i].localizacion,"data":[parseFloat(data[i].vigilantes_si_activados),parseFloat(data[i].vigilantes_no_activados),parseFloat(data[i].vigilantes_baja)]})
               envian.push({"name":data[i].localizacion,"data":[parseFloat(data[i].vigilantes_notifican_android),parseFloat(data[i].vigilantes_notifican_papel),parseFloat(data[i].vigilantes_no_notifican)]})
               

                
            }
        
        }
    
        $(document).ready(function () {
            
    
             table=$('.table').DataTable({
                dom: 'B',
                responsive: true,
                aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
                 
                      ajax: {
                url: "{{url('reportes/mujeres-parto')}}",
                type: 'GET'
                  },
                       footerCallback: function () {
    
                        var api = this.api();
    
                        var table = api.table();
    
                        //Sub Total
                        $( table.footer(0) ).html(
                            '<tr>'+
                            '<td><strong>TOTALES</strong></td>'+
                            '<td>'+api.column( 1, {page:'current'} ).data().sum()+'</td>'+
                           '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                           '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td>'+
                           '<td>'+api.column(4, { page: 'current' }).data().sum()+'</td>'+
                           '<td>'+api.column(5, { page: 'current' }).data().sum()+'</td>'+
                           '<td>'+api.column(6, { page: 'current' }).data().sum()+'</td></tr>'
                        //    '<td>'+api.column(7, { page: 'current' }).data().sum()+'</td></tr>'
                                               
                        );
                       
                  },
                    
            processing: true,
            columns:[
                {data:'localizacion'},
                {data:'vigilantes_si_activados',"defaultContent": '0'},
                {data:'vigilantes_no_activados',"defaultContent": '0'},
                {data:'vigilantes_baja',"defaultContent": '0'},
                {data:'vigilantes_notifican_android',"defaultContent": '0'},
                {data:'vigilantes_notifican_papel',"defaultContent": '0'},
                {data:'vigilantes_no_notifican',"defaultContent": '0'},
             
               
            ],
            buttons: [
                {
                   extend: 'copyHtml5',
                    text:'Copiar',
                    title:'REPORTE DE VIGILANTES COMUNITARIOS', 
                },
               
               {
                    extend: 'print',
                    text:'Imprimir',
                    title:'REPORTE DE VIGILANTES COMUNITARIOS',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '6pt' )
                            .prepend(
                                // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            );
     
                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                },
                {
                    extend:'excelHtml5',
                    title:'REPORTE DE VIGILANTES COMUNITARIOS',
    
    
                },
                
                'csvHtml5',
                 {
                    extend: 'pdfHtml5',
                    orientation: 'portrait',
                    pageSize: 'A4',
                    title:'REPORTE DE VIGILANTES COMUNITARIOS',
                    customize: function (doc) {
                        doc.styles.tableHeader.fontSize = 5
                        doc.defaultStyle.fontSize = 5
                        doc.styles.title = {
                            color: '#222',
                            fontSize: '12',
                            background: '#FFF',
                            alignment: 'center',
                            
                            }
                            doc.styles.tableHeader = {
                            alignment: 'center'
                            }
                    }
                
                    
                }
            ]
         
             })
    
             $('.btn-consultar').click(function(_evt){
                let form =$(this).closest('form');
                let datos=$(form).serialize();
                if($('.fecha_inicio').val()===""){
                $.alert('Eliga una fecha de inicio');
                return false;
            }
            if($('.fecha_fin').val()===""){
                $.alert('Eliga una fecha de fin');
                return false;
            }
            fecha_ini=$('.fecha_inicio').val()
            fecha_fin=$('.fecha_fin').val()
                $.ajax({
                    type: "get",
                    url: "{{url('reportes/consolidado-vigilantes')}}",
                    data: datos,
                    dataType: "json",
                    success: function (response) {
                        dibujarHistorial(response.historial);
                         table.clear().draw();
                         table.rows.add(response.data).draw();
                         obtenerDatos(table);
                        // graficarTorta(crearTitulo(`Porcentaje de vigilantes que Si envian notificaciones ${nivel} de ${fecha_ini} hasta ${fecha_fin}`), vigilantes_notifican_porcentaje_torta,'vigilantes_notifican_porcentaje_torta');
                        // graficarBarra2(crearTitulo(`Nro vigilantes que SI activaron su celular`),'','porcentaje', vigilantes_si_activados_porcentaje_barra,'vigilantes_notifican_porcentaje_torta');
                        graficaBarraVertical("Estado  de celulares","Numero Vigilantes","column","percent",activaron_baja,'fila1-col1',[
                                        'Nro de vigilantes que SI activaron su celular', 
                                        'Nro de vigilantes que NO activaron su celular',
                                         'Nro de vigilantes dados de BAJA'
                                        ])

                                          graficarColumna("Envio de Notificaciones","Numero Vigilantes","column","percent",envian,'fila1-col2',[
                                        'Nro Vigilantes Activados que SI envian notificaciones Android', 
                                        'Nro Vigilantes Activados que SI envian notificaciones Papel', 
                                        'Nro Vigilantes Activados que NO envian notificaciones'
                                    ])
                        
                        vaciarDatos();
                    },
                    beforeSend:function(){
                    blocUI('Procesando...')
                },
                complete:function(){
                    $.unblockUI();

                },
                error:function(){
                    $.unblockUI();
                        alert('ocurrio un error en el sistema')
                },
                });
            })
             
              
        });
    
</script>

@stop
@stop