@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
@section('titulo')Municipios

@stop --}} 
@section('breadcome')
{{-- 
<ul class="breadcome-menu">
    <li><a href="#">Roles y Permisos</a> <span class="bread-slash">/</span>
    </li>

    <li><span class="bread-blod">Roles</span>
    </li>
</ul> --}}

@stop 
@section('content')


<div class="table-responsive">
 
    
    <div class="col-md-6 panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">REPORTES CONSOLIDADOS</h4>
        </div>
        <div class="panel-body">
        <ol>
            <li><a href="{{url('reportes/mujeres-embarazadas')}}">MUJERES EMBARAZADAS IDENTIFICADAS  POR VIGILANTE COMUNITARIO</a></li>
            <li><a href="{{url('reportes/mujeres-parto')}}">PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO</a></li>
            <li><a href="{{url('reportes/muertes-vigilante')}}">MUERTES IDENTIFICADAS POR VIGILANTE COMUNITARIO</a></li>
            <li><a href="{{url('reportes/muertes-autopsia')}}">MUERTES IDENTIFICADAS con AUTOPSIA VERBAL Y PLAN DE ACCION (AV PA)</a></li>
            <li><a href="{{url('reportes/indicadores-proceso')}}">INDICADORES DE PROCESO </a></li>
        </ol>
        </div>

    </div>
    <div class="col-md-6 panel panel-default">
        <div class="panel-heading">
           <h4 class="panel-title">REPORTES NOMINALES</h4>
        </div>
        <div class="panel-body">

            <ol>
                <li><a href="{{url('reportes/listavigilantes-comunitarios')}}">LISTA DE  VIGILANTES COMUNITARIOS</a></li>
                <li><a href="{{url('reportes/notificacion-embarazo')}}">NOTIFICACIONES EMBARAZO </a></li>
                <li><a href="{{url('reportes/notificacion-muertemujer')}}">NOTIFICACIONES MUERTE MUJER </a></li>
                <li><a href="{{url('reportes/notificacion-muertebebe')}}">NOTIFICACIONES MUERTE BEBE </a></li>
                <li><a href="{{url('reportes/notificacion-parto')}}">NOTIFICACIONES PARTO </a></li>
                {{-- <li><a href="{{url('reportes/muertes-vigilante')}}">MUJERES EN EDAD  FERTIL</a></li> --}}
                {{-- <li><a href="{{url('reportes/indicadores-proceso')}}">USUARIOS</a></li> --}}
            </ol>

        </div>
    </div>
  
   
</div>

@section('scripts-inner')
<script>
    $(document).ready(function () {
          
        });
</script>

@stop 
@stop