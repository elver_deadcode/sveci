@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
@section('titulo')Municipios

@stop --}} 
@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">INDICADORES DE PROCESO</span>
    </li>
</ul>




@stop 
@section('content')

<form action="{{url('reportes/mujeres-embarazadas')}}" method="GET">
    @include('includes.localizacion')

    <button type="button" class="btn-consultar btn btn-block btn-primary"> <i class="fa fa-search"></i> CONSULTAR</button>

</form>
<div class="col-md-10">
    <br>
    <table class="table">
        <thead>
            <tr>
                <th>LOCALIZACION</th>
                <th>Nro Vigilantes capacitados</th>
                <th>%</th>
                <th>Nro Vigilantes que reportan cada mes</th>
                <th>%</th>
       
            </tr>
        </thead>
        <tbody>



        </tbody>
        <tfoot>
            <tr>
                <th style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>

    </table>




</div>



@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script>
    $(document).ready(function () {
        

        

        var table=$('.table').DataTable({
            dom: 'B',
            responsive: true,
            aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
             
                  ajax: {
            url: "{{url('reportes/indicadores-proceso')}}",
            type: 'GET'
              },
              footerCallback: function () {

                    var api = this.api();

                    var table = api.table();

                    //Sub Total
                    $( table.footer(0) ).html(
                        '<tr>'+
                        '<td><strong>TOTALES</strong></td>'+
                        '<td>'+api.column( 1, {page:'current'} ).data().sum()+'</td>'+
                       '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(4, { page: 'current' }).data().sum()+'</td></TR>'
                      
                    );
                   
              },
       
                 
        processing: true,
        columns:[
            {data:'localizacion'},
            {data:'nmmenores'},
            {data:'porcentaje_menores'},
            {data:'nmrango'},
            {data:'porcentaje_rango'},
            {data:'planparto'},
            {data:'porcentaje_planparto'},
        ],
        buttons: [
            {
               extend: 'copyHtml5',
                text:'Copiar',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO', 
            },
           
           {
                extend: 'print',
                text:'Imprimir',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',


            },
            
            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'A4',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '12',
                        background: '#FFF',
                        alignment: 'center',
                        
                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }
            
                
            }
        ]
     
         })

         $('.btn-consultar').click(function(_evt){
            let form =$(this).closest('form');
            let datos=$(form).serialize();
            $.ajax({
                type: "get",
                url: "{{url('reportes/mujeres-embarazadas')}}",
                data: datos,
                dataType: "json",
                success: function (response) {
                     table.clear().draw();
                     table.rows.add(response.data).draw();
                }
            });
        })
         
          
    });

</script>




@stop 
@stop