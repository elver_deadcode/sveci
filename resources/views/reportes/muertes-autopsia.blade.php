@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ])
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">
@stop
@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO</span>
    </li>
</ul>


@stop
@section('content')

<div class="modal fade" id="modalTazaPeri" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center"><strong>Calculadora Tasa de mortalidad materna municipal</strong>
                </h4>
                <H4 class="text-center"><STRONG class="text-primary">TMMM=(A+C)÷MEF</strong></h4>
                <p>A= <br>
                    C=<br>
                    MEF= Mujeres en edad fertil (15 a 49 años)</p>
            </div>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-4">
                        <label for="">Localizacion</label>
                        <select class=" select-loc form-control" name="" id="">
                            <option value="">Seleccionar</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">A</label>
                        <input readonly type="text" class="aperi form-control">
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">C</label>
                        <input readonly type="text" class="cperi form-control">
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">MEF</label>
                        <input type="text" class="solo-enteros perinv form-control">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <button type="button" class="btn btn-success btn-block btn-calcular-peri">Calcular</button>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <h1>TMMM=<strong class="totalperi">0</strong></h1>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div></div>
<form action="{{url('reportes/muertes-autopsia')}}" method="GET">
    @include('includes.localizacion')
    @include('includes.filtro_tiempo')
</form>
</div>
<br>
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs  ">
            <li class="active"><a data-toggle="tab" href="#home"><strong> <i class="fa fa-table"></i> Tabla</strong></a>
            </li>
            <li><a data-toggle="tab" href="#menu1"><strong><i class="fa fa-bar-chart"></i> Graficas</strong></a></li>
        </ul>
    </div>
    <div class="panel-body">

        <div class="tab-content">
            <div id="home" class=" table-responsive tab-pane fade in active">
                <p class="lead text-danger">
                    <h5 class=" text-danger"><strong>AV=Tiene Autopsia verbal , PA= Tiene plan de accion, MEF= Mujeres
                            en edad fertil (15 a 49 años)</strong></h5>
                </p>
                <div class=" row ">
                    <div class=" col-md-10 div-historial">

                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modalTazaPeri"><i
                                class="fa fa-desktop"></i> calculadora de Tasa de mortalidad materna municipal</button>
                    </div>


                </div>

                <table class="table table-condensed table-bordered" style="font-size:10px">
                    <thead>
                        <tr align="center" style="font-size:1em;text-align:center">
                            <th>LOCALIZACION</th>
                            <th>
                                <h4 class="text-primary">A</h4>Muertes maternas directas (metodologia RAMOS) de mujeres
                                de 10 a 59 años que inicialmente no se sabian si eran maternas o no<br> (AV)
                            </th>

                            <th>
                                <h4 class="text-primary">B</h4>Muertes maternas directas (metodologia RAMOS) de mujeres
                                de 10 a 59 años que inicialmente no se sabian si eran maternas o no<br> (AV+PA)
                            </th>

                            <th>
                                <h4 class="text-primary">C</h4>Muertes maternas directas (metodologia RAMOS) de mujeres
                                de 10 a 59 años que ya se conocian que estaba embarazada<br> (AV)
                            </th>

                            <th>
                                <h4 class="text-primary">D</h4>Muertes maternas directas (metodologia RAMOS) de mujeres
                                de 10 a 59 años que ya se conocian que estaba embarazada<br> (AV+PA)
                            </th>

                            <th>
                                <h4 class="text-primary">E</h4>Fallecio por otras causas con (AV)
                            </th>
                            <th>
                                <h4 class="text-primary"></h4>Cuenta con el dictamen del comite departamental de muerta materna
                            </th>
                            <th>
                                <h4 class="text-primary"></h4>Numero de muertes de mujeres registrados de 10 a 59 años
                            </th>


                            <th>TOTAL</th>
                        </tr>
                    </thead>


                    <tbody>


                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>



                        </tr>
                    </tfoot>

                </table>

            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila1-col1" style="height: 400px"></div>

                    </div>
                    <div class="col-md-6">
                        <div id="fila1-col2" style="height: 400px"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div id="fila2-col1" style="height: 400px"></div>

                    </div>
                    <div class="col-md-6">
                        <div id="fila2-col2" style="height: 400px"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div id="fila3-col1" style="height: 400px"></div>

                    </div>
                    <div class="col-md-6">
                        <div id="fila3-col2" style="height: 400px"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="col-md-10">





</div>



@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js">
</script>




<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="{{asset('js/graficas.js')}}?time=<?=time()?>"></script>
<script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
    var nivel='nacional', fecha_ini='',fecha_fin='';

var table;
var ramos_no_sabia=[]
var ramos_si_sabia=[]
var otras_causas=[]
var tazas=[];
function obtenerDatos(table) {
  const datos = [];


  // loop table rows
    let data=table.rows({selected:  true}).data();

        for (var i=0; i < data.length ;i++){

            tazas.push({
                    localizacion:data[i].localizacion,
                    a:data[i].gav,
                    c:data[i].g1av
                })

            // gav_chart.push([data[i].localizacion,parseFloat(data[i].gav_por)])
            // gav_barras.push({"name":data[i].localizacion,"y":parseFloat(data[i].gav)})
                  if(parseFloat(data[i].gav)>0 || parseFloat(data[i].gavpa)>0){

                      ramos_no_sabia.push({"name":data[i].localizacion,"data":[parseFloat(data[i].gav),parseFloat(data[i].gavpa)]})
                  }
                  if(parseFloat(data[i].g1av)>0 || parseFloat(data[i].g1avpa)>0 )
                ramos_si_sabia.push({"name":data[i].localizacion,"data":[parseFloat(data[i].g1av),parseFloat(data[i].g1avpa)]})

                if(parseFloat(data[i].ninguno)>0)
                otras_causas.push({"name":data[i].localizacion,"data":[parseFloat(data[i].ninguno)]})
        }
    }





    function vaciarDatos(){
    ramos_no_sabia=[]
    ramos_si_sabia=[]
    otras_causas=[]
    tazas=[];

    }

    function datosModales(){
        $('.select-loc').children('option:not(:first)').remove();

        let options=""
        tazas.forEach(function(v,k){
                options+=`<option a="${v.a}" c="${v.c}">${v.localizacion}</option>`
        })

        $('.select-loc').append(options)


    }
    $(document).ready(function () {

        $('.select-loc').change(function(){

          let a =$(this).find('option:selected').attr('a')
          let c =$(this).find('option:selected').attr('c')
          $('.aperi').val(a)
          $('.cperi').val(c)

          $('.perinv').val('')
          $('.perinv').focus()
      })

      $('.btn-calcular-peri').click(function(){
            let a =parseFloat($('.aperi').val())
            let c =parseFloat($('.cperi').val())
            let nv =parseFloat($('.perinv').val())
            let taza=0

            let select=$(this).closest('.modal-body').find('.select-loc')
            if($(select).val()==''){
                $.alert({title:'Alerta!!',content:'Debe seleccionar una localizacion'})

                return false;
            }
            if($('.perinv').val()=='' || nv<=0){
                $.alert({title:'Alerta!!',content:'Ingrese un numero mayor a 0 para MEF'})
                $('.perinv').focus()
                        return false;

            }
            a=50
            c=50
            if(a>0 || c>0){
                taza=(a+c)/nv
                taza=taza.toFixed(2)
            }
            $('.totalperi').html(taza)
        })

        table=$('.table').DataTable({
            dom: 'B',
            responsive: true,
            aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,

                  ajax: {
            url: "{{url('reportes/muertes-autopsia')}}",
            type: 'GET'
              },
                       footerCallback: function () {

                    var api = this.api();

                    var table = api.table();

                    //Sub Total
                    $( table.footer(0) ).html(
                        '<tr>'+
                        '<td><strong>TOTALES</strong></td>'+
                        '<td>'+api.column( 1, {page:'current'} ).data().sum()+'</td>'+
                       '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(4, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(5, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(6, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(7, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(8, { page: 'current' }).data().sum()+'</td>'+
                       '</tr>'

                    );

              },

        processing: true,
        columns:[

            {data:'localizacion'},
            {data:'gav',"defaultContent": '0'},
            {data:'gavpa',"defaultContent": '0'},
            {data:'g1av',"defaultContent": '0'},
            {data:'g1avpa',"defaultContent": '0'},
            {data:'ninguno',"defaultContent": '0'},
            {data:'casos_atendidos',"defaultContent": '0'},
            {data:'casos_registrados',"defaultContent": '0'},
            {data:'total',"defaultContent": '0'},


        ],
        buttons: [
            {
               extend: 'copyHtml5',
                text:'Copiar',
                title:'MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
            },

           {
                extend: 'print',
                text:'Imprimir',
                title:'MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );

                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',


            },

            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'A4',
                title:'MUERTES DE MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '12',
                        background: '#FFF',
                        alignment: 'center',

                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }


            }
        ]

         })


         $('.btn-consultar').click(function(_evt){
            let form =$(this).closest('form');
            let datos=$(form).serialize();
            if($('.fecha_inicio').val()===""){
                $.alert('Eliga una fecha de inicio');
                return false;
            }
            if($('.fecha_fin').val()===""){
                $.alert('Eliga una fecha de fin');
                return false;
            }
            fecha_ini=$('.fecha_inicio').val()
            fecha_fin=$('.fecha_fin').val()
            $.ajax({
                type: "get",
                url: "{{url('reportes/muertes-autopsia')}}",
                data: datos,
                dataType: "json",
                success: function (response) {
                   dibujarHistorial(response.historial)
                     table.clear().draw();
                     table.rows.add(response.data).draw();
                     obtenerDatos(table);
                     datosModales()

                    //  graficarTorta(crearTitulo(`Numero de  muertes maternas por  causas directas que inicialmente no se sabian si eran maternas o no  (AV) ${nivel} `),gav_chart,'gav_chart');
                    //   graficarBarra2(crearTitulo(`Numero de  muertes maternas por  causas directas que inicialmente no se sabian si eran maternas o no  (AV) ${nivel} `),'','porcentaje',gav_barras,'gav_barras');
                    if(ramos_no_sabia.length>0)
                       graficaBarraVertical("Muertes maternas directas de mujeres  de 10 a 59 años que inicialmente no se sabian si estaban embarazadas o no","Notificaciones","column","percent",ramos_no_sabia,'fila1-col1',[
                        'Con Autopsia verbal', 'Autopsia verbal y plan de accion'])

                        if(ramos_si_sabia.length>0)
                        graficaBarraVertical("Muertes maternas directas de mujeres  de 10 a 59 años que ya si estaban embarazadas","Notificaciones","column","percent",ramos_si_sabia,'fila1-col2',[
                        'Con Autopsia verbal', 'Autopsia verbal y plan de accion'])
                        if(otras_causas.lenght>0)
                        graficaBarraVertical("Fallecio por otras causas","Notificaciones","column","percent",otras_causas,'fila2-col1',[
                        'Con Autopsia verbal'])

                     vaciarDatos();
                },
                beforeSend:function(){
                    blocUI('Procesando...')

                },
                complete:function(){
                    $.unblockUI();

                },
                error:function(){
                    $.unblockUI();
                        alert('ocurrio un error en el sistema')
                },
            });
        })


    });

</script>
@stop
@stop
