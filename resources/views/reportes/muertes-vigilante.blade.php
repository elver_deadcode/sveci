@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ])
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">
@stop
@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">MUERTES FETALES Y NEONATALES IDENTIFICADAS POR VIGILANTE COMUNITARIO</span>
    </li>
</ul>

@stop
@section('content')

<!-- Modal -->
<div class="modal fade" id="modalTazaPeri" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center"><strong>Calculadora Tasa de mortalidad perinatal</strong></h4>
                <H4 class="text-center"><STRONG class="text-primary">TMP=[(A+C)÷(NV+A+C)]x1000</strong></h4>
                <p>A=Nro Muertes Fetales (nacidos muertos con AV) <br>
                    C=Nro Muertes < 7 dias con (AV)<br>
                        NV=numero de niños nacidos vivos</p>
            </div>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-4">
                        <label for="">Localizacion</label>
                        <select class=" select-loc form-control" name="" id="">
                            <option value="">Seleccionar</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">A</label>
                        <input readonly type="text" class="aperi form-control">
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">C</label>
                        <input readonly type="text" class="cperi form-control">
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">NV</label>
                        <input type="text" class="solo-enteros perinv form-control">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <button type="button" class="btn btn-success btn-block btn-calcular-peri">Calcular</button>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <h1>TMP=<strong class="totalperi">0</strong></h1>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalTazaNeo" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center"><strong>Calculadora Tasa de mortalidad neonatal temprana</strong>
                </h4>
                <H4 class="text-center"><STRONG class="text-primary">TMT=(C/NV)x1000</strong></h4>
                <p>C=Nro Muertes < 7 dias con (AV)<br>
                        NV=numero de niños nacidos vivos</p>
            </div>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-4">
                        <label for="">Localizacion</label>
                        <select class=" select-loc form-control" name="" id="">
                            <option value="">Seleccionar</option>
                        </select>
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">C</label>
                        <input readonly type="text" class="cneo form-control">
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <label for="" class=" text-center control-label">NV</label>
                        <input type="text" class="solo-enteros neoinv form-control">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <button type="button" class="btn btn-success btn-block btn-calcular-neo">Calcular</button>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <h1>TMT=<strong class="totalneo">0</strong></h1>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>

            </div>
        </div>
    </div>
</div>
<div>
    <form action="{{url('reportes/muertes-mujeres')}}" method="GET">
        @include('includes.localizacion')
        @include('includes.filtro_tiempo')

    </form>
</div>

<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs  ">
            <li class="active"><a data-toggle="tab" href="#home"><strong> <i class="fa fa-table"></i> Tabla</strong></a>
            </li>
            <li><a data-toggle="tab" href="#menu1"><strong><i class="fa fa-bar-chart"></i> Graficas</strong></a></li>
        </ul>
    </div>
    <div class="panel-body">

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="row table-responsive">
                    <div class=" col-md-10 div-historial">

                    </div>
                    <div class="col-md-10">
                        <p class="lead text-danger">
                            <h5 class=" text-danger"><strong>AV=Tiene Autopsia verbal , PA= Tiene plan de accion,
                                    NV=numero de niños nacidos vivos </strong></h5>
                        </p>

                        <div class=" row ">
                            <div class="col-md-4">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalTazaPeri"><i
                                        class="fa fa-desktop"></i> calculadora de tasa de mortalidad perinatal</button>


                                {{-- <H4><STRONG class="text-primary">TMP=[(A+C)÷(NV+A+C)]x1000</strong></h4> --}}
                                {{-- <span class="text-primary" >  TMP=Taza de mortalidad perinatal</span>  --}}
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalTazaNeo"><i
                                        class="fa fa-desktop"></i> calculadora de tasa de mortalidad neonatal
                                    temprana</button>

                                {{-- <H4><STRONG class="text-primary">TMT=(C/NV)x1000</strong></h4>
                       <span class="text-primary" >TMT=Taza de mortalidad neonatal temprana</span>  --}}
                            </div>

                        </div>


                        <table class="table table-condensed table-bordered" style="font-size:10px">
                            <thead>
                                <tr>
                                    <th>LOCALIZACION</th>
                                    <th>
                                        <h4 class="text-primary">A</h4>Nro Muertes <br> Fetales <br>(nacidos muertos
                                        conAV)
                                    </th>
                                    <th>
                                        <h4 class="text-primary">B</h4> Nro Muertes <br> Fetales <br>(nacidos muertoscon
                                        AV+PA)
                                    </th>
                                    <th>
                                        <h4 class="text-primary">C</h4> Nro Muertes <br>
                                        < 7 dias (AV)</th> <th>
                                            <h4 class="text-primary">D</h4> Nro Muertes <br>
                                            < 7 dias (AV+PA)</th> <th>
                                                <h4 class="text-primary">E</h4> Nro Muertes <br> 7 a < 28 dias (AV)</th>
                                                    <th>
                                                    <h4 class="text-primary">F</h4> Nro Muertes <br> 7 a < 28 dias
                                                        (AV+PA)</th> <th>
                                                        <h4 class="text-primary">G</h4> Nro Muertes <br> Fetales
                                                        <br>(nacidos muertos sin AV)
                                    </th>
                                    <th>
                                        <h4 class="text-primary">H</h4> Nro Muertes <br>
                                        < 7 dias (sin AV)</th> <th>
                                            <h4 class="text-primary">I</h4> Nro Muertes <br> 7 a < 28 dias (sin AV)</th>
                                                <th>total
                                    </th>
                                </tr>
                            </thead>


                            <tbody>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            </tfoot>

                        </table>

                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila1-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila1-col2" style="height: 400px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila2-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila2-col2" style="height: 400px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila3-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila3-col2" style="height: 400px"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</div>





@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/helpers.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js">
</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{asset('js/graficas.js')}}?time=<?=time()?>"></script>
<script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
    var nivel='nacional', fecha_ini='',fecha_fin=''
var table
var nacidos_muertos=[];
var muertes_menores_7dias=[];
var muertes_7a28dias=[];
var nacidos_muertos_sinAV=[];

var tazas=[];
    
    
    function vaciarDatos(){
 
    tazas=[]
    nacidos_muertos=[];
    muertes_menores_7dias=[];
    muertes_7a28dias=[];
    nacidos_muertos_sinAV=[];
    
    }

    function obtenerDatos(table) {
    let data=table.rows({selected:  true}).data();
      
        for (var i=0; i < data.length ;i++){

                tazas.push({
                    localizacion:data[i].localizacion,
                    a:data[i].muertesfetales_uv,
                    c:data[i].muertes7dias_uv
                })

                    nacidos_muertos.push({"name":data[i].localizacion,"data":[parseFloat(data[i].muertesfetales_uv),parseFloat(data[i].muertesfetales_uvpa)]})
                    muertes_menores_7dias.push({"name":data[i].localizacion,"data":[parseFloat(data[i].muertes7dias_uv),parseFloat(data[i].muertes7dias_uvpa)]})
                    muertes_7a28dias.push({"name":data[i].localizacion,"data":[parseFloat(data[i].muertes28dias_uv),parseFloat(data[i].muertes28dias_uvpa)]})
                    nacidos_muertos_sinAV.push({"name":data[i].localizacion,"data":[parseFloat(data[i].muertesfetales),parseFloat(data[i].muertes7dias),parseFloat(data[i].muertes28dias)]})

        }
    
    }


    function datosModales(){
        $('.select-loc').children('option:not(:first)').remove();

        let options=""
        tazas.forEach(function(v,k){
                options+=`<option a="${v.a}" c="${v.c}">${v.localizacion}</option>`
        })

        $('.select-loc').append(options)


    }




    $(document).ready(function () {

        $('.select-loc').change(function(){
          
            let a =$(this).find('option:selected').attr('a')
            let c =$(this).find('option:selected').attr('c')
            $('.aperi').val(a)
            $('.cperi').val(c)
            $('.cneo').val(c)
            $('.perinv').val('')
            $('.perinv').focus()
        })

        $('.btn-calcular-peri').click(function(){
            let a =parseFloat($('.aperi').val())
            let c =parseFloat($('.cperi').val())
            let nv =parseFloat($('.perinv').val())
            let taza=0

            let select=$(this).closest('.modal-body').find('.select-loc')
            if($(select).val()==''){
                $.alert({title:'Alerta!!',content:'Debe seleccionar una localizacion'})

                return false;
            }
            if($('.perinv').val()=='' || nv<=0){
                $.alert({title:'Alerta!!',content:'Ingrese un numero mayor a 0 para NV '})
                $('.perinv').focus()
                        return false;

            }
            // a=50
            // c=50
            if(a>0 || c>0){
                taza=((a+c)/(nv+a+c))*1000
                taza=taza.toFixed(2)
            }
            $('.totalperi').html(taza)
        })

        $('.btn-calcular-neo').click(function(){
          
            let c =parseFloat($('.cneo').val())
            let nv =parseFloat($('.neoinv').val())
            let taza=0

            let select=$(this).closest('.modal-body').find('.select-loc')
            if($(select).val()==''){
                $.alert({title:'Alerta!!',content:'Debe seleccionar una localizacion'})

                return false;
            }
            if($('.neoinv').val()=='' || nv<=0){
                $.alert({title:'Alerta!!',content:'Ingrese un numero mayor a 0 para NV '})
                $('.neoinv').focus()
                        return false;

            }
            // a=50
            // c=50
            if(c>0){
                taza=(c/nv)*1000
                taza=taza.toFixed(2)
            }
            $('.totalneo').html(taza)
        })

        

        

        table=$('.table').DataTable({
            dom: 'B',
            responsive: true,
            aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
             
                  ajax: {
            url: "{{url('reportes/muertes-vigilante')}}",
            type: 'GET'
              },

               footerCallback: function () {

                    var api = this.api();

                    var table = api.table();

                    //Sub Total
                    $( table.footer(0) ).html(
                        '<tr>'+
                        '<td><strong>TOTALES</strong></td>'+
                        '<td>'+api.column( 1, {page:'current'} ).data().sum()+'</td>'+
                       '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(4, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(5, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(6, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(7, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(8, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(9, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(10, { page: 'current' }).data().sum()+'</td></tr>'
                     
                    );
                   
              },
                
        processing: true,
        columns:[
            {data:'localizacion'},
            {data:'muertesfetales_uv',"defaultContent": '0'},
            {data:'muertesfetales_uvpa',"defaultContent": '0'},
            {data:'muertes7dias_uv',"defaultContent": '0'},
            {data:'muertes7dias_uvpa',"defaultContent": '0'},
            {data:'muertes28dias_uv',"defaultContent": '0'},
            {data:'muertes28dias_uvpa',"defaultContent": '0'},
            {data:'muertesfetales',"defaultContent": '0'},
            {data:'muertes7dias',"defaultContent": '0'},
            {data:'muertes28dias',"defaultContent": '0'},
            {data:'total',"defaultContent": '0'},
           
        ],
        buttons: [
            {
               extend: 'copyHtml5',
                text:'Copiar',
                title:'MUERTES FETALES Y NEONATALES IDENTIFICADAS POR VIGILANTE COMUNITARIO', 
            },
           
           {
                extend: 'print',
                text:'Imprimir',
                title:'MUERTES FETALES Y NEONATALES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'MUERTES FETALES Y NEONATALES  IDENTIFICADAS POR VIGILANTE COMUNITARIO',


            },
            
            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'A4',
                title:'MUERTES FETALES Y NEONATALES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '12',
                        background: '#FFF',
                        alignment: 'center',
                        
                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }
            
                
            }
        ]
     
         })

         $('.btn-consultar').click(function(_evt){
            let form =$(this).closest('form');
            let datos=$(form).serialize();

            if($('.fecha_inicio').val()===""){
                $.alert('Eliga una fecha de inicio');
                return false;
            }
            if($('.fecha_fin').val()===""){
                $.alert('Eliga una fecha de fin');
                return false;
            }
            fecha_ini=$('.fecha_inicio').val()
            fecha_fin=$('.fecha_fin').val()
            $.ajax({
                type: "get",
                url: "{{url('reportes/muertes-vigilante')}}",
                data: datos,
                dataType: "json",
                success: function (response) {
                    dibujarHistorial(response.historial)
                     table.clear().draw();
                     table.rows.add(response.data).draw();
                     obtenerDatos(table);
                     datosModales()

                    //  graficarTorta(crearTitulo(`Porcentaje de muertes fetales nacidos muertos con AV ${nivel} de ${fecha_ini} hasta ${fecha_fin}`),fetales_uv_torta,'fetales_uv_torta');
                //    graficarBarra2(crearTitulo(`Numero  de muertes fetales nacidos muertos con AV ${nivel} `),'','porcentaje',fetales_uv_barras,'fetales_uv_barras');

                graficaBarraVertical("Numero de muertes Neonatales con AV versus AV+PA","Notificaciones","column","percent",nacidos_muertos,'fila1-col1',[
                'Nacidos muertos con AV', 'Nacidos muertos  con AV+PA'])

                graficaBarraVertical("Numero de muertes neonatales menores a 7 dias","Notificaciones","column","percent",muertes_menores_7dias,'fila1-col2',[
                'Con AV', 'Con AV+PA'])
                 graficaBarraVertical("Numero de muertes neonatales 7 a 28 dias","Notificaciones","column","percent",muertes_7a28dias,'fila2-col1',[
                'Con AV', 'Con AV+PA'])

                 graficaBarraVertical("Numero de muertes neonatales sin AV","Notificaciones","column","percent",muertes_7a28dias,'fila2-col2',[
                'Nacidos muertos', 'Menores 7 dias nacido','Entre 7 a 28 dias nacido'])
                    vaciarDatos();
                },
                beforeSend:function(){
                    blocUI('Procesando...')
                },
                complete:function(){
                    $.unblockUI();

                },
                error:function(){
                    $.unblockUI();
                        alert('ocurrio un error en el sistema')
                },
            });
        })
         
          
    });

</script>

@stop
@stop