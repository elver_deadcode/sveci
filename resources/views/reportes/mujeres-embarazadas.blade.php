@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
@section('titulo')Municipios

@stop --}}
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">


@stop
@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO</span>
    </li>
</ul>



@stop
@section('content')

<div>
    <form class="" action="{{url('reportes/mujeres-embarazadas')}}" method="GET">
        @include('includes.localizacion')
        @include('includes.filtro_tiempo')

    </form>
</div>
<br>
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs  ">
            <li class="active"><a data-toggle="tab" href="#home"><strong> <i class="fa fa-table"></i> Tabla</strong></a>
            </li>
            <li><a data-toggle="tab" href="#menu1"><strong><i class="fa fa-bar-chart"></i> Graficas</strong></a></li>
        </ul>
    </div>
    <div class="panel-body">

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="row table-responsive">

                    <div class=" col-md-10 div-historial">

                    </div>

                    <div class="col-md-10">
                        <br>
                        <p class="lead text-danger">
                            <h5 class=" text-danger"><strong>CPN=Control prenatal </strong></h5>
                        </p>



                        <table class="table datatable" width="100%" style="font-size:10px;">
                            <thead>
                                <tr>
                                    <th>LOCALIZACION</th>
                                    <th>Nro Mujeres 15 a <br> 19 años con al menos 1 CPN</th>
                                    <th>Nro Mujeres 15 a <br> 19 años Sin CPN</th>
                                    <th>Nro Mujeres < 15 años <br> con al menos 1 CPN</th>
                                    <th>Nro Mujeres < 15 años <br> Sin CPN</th>
                                    <th>Nro Mujeres > 19 años <br> con al menos 1 CPN</th>
                                    <th>Nro Mujeres > 19 años <br> Sin CPN</th>
                                    <th>Nro Mujeres <br>15 a 19 años<br> con plan de parto</th>
                                    <th>Nro Mujeres <br>15 a 19 años<br> sin plan de parto</th>
                                    <th>Nro Mujeres <br>menores 15 años<br> con plan de parto</th>
                                    <th>Nro Mujeres <br>menores 15 años<br> sin plan de parto</th>
                                    <th>Nro Mujeres <br>mayores 19 años<br>con plan de parto</th>
                                    <th>Nro Mujeres <br>mayores 19 años<br>sin plan de parto</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align:right">Total:</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila1-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila1-col2" style="height: 600px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila2-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila2-col2" style="height: 600px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila3-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila3-col2" style="height: 600px;"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div id="fila4-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila4-col2" style="height: 600px"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js">
</script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/percent.js"></script>


{{-- <script src="https://code.highcharts.com/stock/highstock.js"></script> --}}
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{asset('js/graficas.js')}}?time=<?=time()?>"></script>
<script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
    var nivel='nacional', fecha_ini='',fecha_fin=''
var table
var _1519_cpn=[]
var _menores15_cpn=[]
var _mayores19_cpn=[]
var _1519_planparto=[]
var _menores15_planparto=[]
var _mayores19_planparto=[]
   

   function vaciarDatos(){
    _1519_cpn=[]
    _menores15_cpn=[]
    _mayores19_cpn=[]
    _1519_planparto=[]
    _menores15_planparto=[]
    _mayores19_planparto=[]
   }
function obtenerDatos(table) {
  // loop table rows
  let data=table.rows({selected:  true}).data();
      
        for (var i=0; i < data.length ;i++){

            // _1519cpn_torta.push([data[i].localizacion,parseFloat(data[i].porcentaje_rango)])
            // _1519scpn_barras.push({"name":data[i].localizacion,"y":parseFloat(data[i].prenatales_15_19_scpn)})
           _1519_cpn.push({"name":data[i].localizacion,"data":[parseFloat(data[i].prenatales_15_19_cpn),parseFloat(data[i].prenatales_15_19_scpn)]})
           _menores15_cpn.push({"name":data[i].localizacion,"data":[parseFloat(data[i].prenatales_menores_15_cpn),parseFloat(data[i].prenatales_menores_15_scpn)]})
           _mayores19_cpn.push({"name":data[i].localizacion,"data":[parseFloat(data[i].prenatales_mayores_19_cpn),parseFloat(data[i].prenatales_mayores_19_scpn)]})
           _1519_planparto.push({"name":data[i].localizacion,"data":[parseFloat(data[i].planparto_15_19_cpp),parseFloat(data[i].planparto_15_19_scpp)]})
           _menores15_planparto.push({"name":data[i].localizacion,"data":[parseFloat(data[i].planparto_menores_15_cpp),parseFloat(data[i].planparto_menores_15_spp)]})
           _mayores19_planparto.push({"name":data[i].localizacion,"data":[parseFloat(data[i].planparto_mayores_19_cpp),parseFloat(data[i].planparto_mayores_19_spp)]})

        }
 
    }


    $(document).ready(function () {

        
        table=$('.datatable').DataTable({
            dom: 'B',
            "scrollX": true,
            responsive: true,
            aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
            
             
                  ajax: {
            url: "{{url('reportes/mujeres-embarazadas')}}",
            type: 'GET'
              },
              footerCallback: function () {

                    var api = this.api();

                    var table = api.table();

                    //Sub Total
                    $( table.footer(0) ).html(
                        '<tr>'+
                        '<td><strong>TOTALES</strong></td>'+
                       '<td>'+api.column(1, {page:'current'} ).data().sum()+'</td>'+
                       '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(4, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(5, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(6, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(7, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(8, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(9, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(10, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(11, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(12, { page: 'current' }).data().sum()+'</td>'+
                       '<td>'+api.column(13, { page: 'current' }).data().sum()+'</td></tr>'
               
                    );
                   
              },
        processing: true,
        columns:[
            {data:'localizacion'},
            {data:'prenatales_15_19_cpn'},
            {data:'prenatales_15_19_scpn'},
            {data:'prenatales_menores_15_cpn'},
            {data:'prenatales_menores_15_scpn'},
            {data:'prenatales_mayores_19_cpn'},
            {data:'prenatales_mayores_19_scpn'},
            {data:'planparto_15_19_cpp'},
            {data:'planparto_15_19_scpp'},
            {data:'planparto_menores_15_cpp'},
            {data:'planparto_menores_15_spp'},
            {data:'planparto_mayores_19_cpp'},
            {data:'planparto_mayores_19_spp'},
            {data:'total'},
        ],
        buttons: [
            {
               extend: 'copyHtml5',
               text:'Copiar',
               title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO', 
            },
           
           {
                extend: 'print',
                text:'Imprimir',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',
            },
            
            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'A4',
                title:'MUJERES EMBARAZADAS IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '12',
                        background: '#FFF',
                        alignment: 'center',
                        
                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }
            
                
            }
        ]
     
         })

      

         $('.btn-consultar').click(function(_evt){
            let form =$(this).closest('form');
            let datos=$(form).serialize();
            let fecha_ini=''
            let fecha_fin=''
            
            $.ajax({
                type: "get",
                url: "{{url('reportes/mujeres-embarazadas')}}",
                data: datos,
                dataType: "json",
                success: function (response) {
                    dibujarHistorial(response.historial)
                     table.clear().draw();
                     table.rows.add(response.data).draw();
                    // nivel=response.data[0].nivel

                    obtenerDatos(table);
                    graficaBarraVertical("Numero de controles prenatales de  mujeres 15 a 19 años","Notificaciones","column","percent",_1519_cpn,'fila1-col1',[ 'Con al menos 1 CPN', 'Sin SPN'])
                    graficaBarraVertical("Numero de controles prenatales de  mujeres menores de 15 años","Notificaciones","column","percent",_menores15_cpn,'fila1-col2',[ 'Con al menos 1 CPN', 'Sin SPN'])
                    graficaBarraVertical("Numero de controles prenatales de  mujeres mayores de 19 años","Notificaciones","column","percent",_mayores19_cpn,'fila2-col1',[ 'Con al menos 1 CPN', 'Sin SPN'])
                    graficaBarraVertical("Plan de Parto de  mujeres 15 a 19 años","Notificaciones","column","percent",_1519_planparto,'fila2-col2',[ 'Con plan de parto', 'Sin plan de parto'])
                    graficaBarraVertical("Plan de Parto de  mujeres menores de 15 años","Notificaciones","column","percent",_menores15_planparto,'fila3-col1',[ 'Con plan de parto', 'Sin plan de parto'])
                    graficaBarraVertical("Plan de Parto de  mujeres mayores de 19 años","Notificaciones","column","percent",_mayores19_planparto,'fila3-col2',[ 'Con plan de parto', 'Sin plan de parto'])
             
                    // if(_mayores19spp_barras.length>0)
                    // // graficarTorta(crearTitulo(`Mujeres Embarazadas de mayores 19 años sin plan de parto  ${nivel}`),_mayores19spp_torta,'mayores19spp_torta');
                    // graficarBarra2(crearTitulo(`Mujeres Embarazadas de mayores 19 años sin plan de parto  ${nivel}`),'','porcentaje',_mayores19spp_barras,'mayores19spp_barras');

                    vaciarDatos();
                },
                beforeSend:function(){
                    blocUI('Procesando...')
                },
                complete:function(){
                    $.unblockUI();

                },
                error:function(){
                    $.unblockUI();
                        alert('ocurrio un error en el sistema')
                },
            });
        })
         
          
    });


</script>



@stop
@stop