@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
@section('titulo')Municipios

@stop --}}
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">
@stop
@section('breadcome')

<ul class="breadcome-menu">
    <li>
        <a href="{{ url('reportes', []) }}">REPORTES</a> <span class="bread-slash">/</span>
    </li>

    <li>
        <span class="bread-blod">PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO</span>
    </li>
</ul>




@stop
@section('content')

<div>
    <form action="{{url('reportes/mujeres-parto')}}" method="GET">

        @include('includes.localizacion')
        @include('includes.filtro_tiempo')
    </form>
</div>

<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs  ">
            <li class="active"><a data-toggle="tab" href="#home"><strong> <i class="fa fa-table"></i> Tabla</strong></a>
            </li>
            <li><a data-toggle="tab" href="#menu1"><strong><i class="fa fa-bar-chart"></i> Graficas</strong></a></li>
        </ul>
    </div>
    <div class="panel-body">

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="row table-responsive">
                    <div class=" col-md-10 div-historial">

                    </div>
                    <div class="col-md-10">


                        <table class="table">
                            <thead>
                                <tr>
                                    <th>LOCALIZACION</th>
                                    <th>Nro. Atendidos <br>por Partera <br>capacitada</th>
                                    <th>Nro. Atendidos<br> por Partera <br>empirica</th>
                                    <th>Total</th>

                                </tr>
                            </thead>
                            <tbody>



                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila1-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila1-col2" style="height: 400px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="fila2-col1" style="height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="fila2-col2" style="height: 400px"></div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>

</div>



@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js">
</script>



<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{asset('js/graficas.js')}}?time=<?=time()?>"></script>
<script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
    var nivel='nacional', fecha_ini='',fecha_fin=''

    var table
    var partos=[]
    

    function vaciarDatos(){
    partos=[]
    
    }

    function obtenerDatos(table) {

        let data=table.rows({selected:  true}).data();

        for (var i=0; i < data.length ;i++){
            // capacitada_torta.push([data[i].localizacion,parseFloat(data[i].porcentaje_capacitada)])
            partos.push({"name":data[i].localizacion,"data":[parseFloat(data[i].capacitada),parseFloat(data[i].empirica)]})
        }
    
    }



    $(document).ready(function () {
         table=$('.table').DataTable({
            dom: 'B',
            responsive: true,
            aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
             
                  ajax: {
            url: "{{url('reportes/mujeres-parto')}}",
            type: 'GET'
              },
                   footerCallback: function () {

                    var api = this.api();

                    var table = api.table();

                    //Sub Total
                    $( table.footer(0) ).html(
                        '<tr>'+
                        '<td><strong>TOTALES</strong></td>'+
                        '<td>'+api.column( 1, {page:'current'} ).data().sum()+'</td>'+
                        '<td>'+api.column(2, { page: 'current' }).data().sum()+'</td>'+
                        '<td>'+api.column(3, { page: 'current' }).data().sum()+'</td></tr>'
                     
                    );
                   
              },
                
        processing: true,
        columns:[
            {data:'localizacion'},
            {data:'capacitada',"defaultContent": '0'},
            {data:'empirica',"defaultContent": '0'},
            {data:'total',"defaultContent": '0'},
           
        ],
        buttons: [
            {
               extend: 'copyHtml5',
                text:'Copiar',
                title:'PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO', 
            },
           
           {
                extend: 'print',
                text:'Imprimir',
                title:'PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',


            },
            
            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'A4',
                title:'PARTO MUJERES IDENTIFICADAS POR VIGILANTE COMUNITARIO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '12',
                        background: '#FFF',
                        alignment: 'center',
                        
                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }
            
                
            }
        ]
     
         })

         $('.btn-consultar').click(function(_evt){
            let form =$(this).closest('form');
            let datos=$(form).serialize();
            if($('.fecha_inicio').val()===""){
                $.alert('Eliga una fecha de inicio');
                return false;
            }
            if($('.fecha_fin').val()===""){
                $.alert('Eliga una fecha de fin');
                return false;
            }
            fecha_ini=$('.fecha_inicio').val()
            fecha_fin=$('.fecha_fin').val()
            $.ajax({
                type: "get",
                url: "{{url('reportes/mujeres-parto')}}",
                data: datos,
                dataType: "json",
                success: function (response) {
                    dibujarHistorial(response.historial)
                     table.clear().draw();
                     table.rows.add(response.data).draw();
                     obtenerDatos(table);
                    //  graficarTorta(crearTitulo(`Porcentaje  de partos atendidos por partera capacitada ${nivel} de ${fecha_ini} hasta ${fecha_fin}`),capacitada_torta,'capacitada_torta');
                   // graficarBarra2(crearTitulo(`Numero de  partos atendidos por partera capacitada `),'','porcentaje',capacitada_barra,'capacitada_barra');

                   graficaBarraVertical("Numeros de partos atendidos","Notificaciones","column","percent",partos,'fila1-col1',[
                                    'Partera Capacitada', 'Partera Empirica'])
                    vaciarDatos();
                },
                beforeSend:function(){
                    blocUI('Procesando...')
                },
                complete:function(){
                    $.unblockUI();

                },
                error:function(){
                    $.unblockUI();
                        alert('ocurrio un error en el sistema')
                },
            });
        })
         
          
    });

</script>




@stop
@stop