@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
@section('titulo')Municipios

@stop --}}
@section('styles')
<link rel="stylesheet" href="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.css')}}">
@stop
@section('breadcome')
<ul class="breadcome-menu">
    <li><a href="{{url('/reportes')}}">Reportes</a> <span class="bread-slash">/</span>
    </li>

    <li><span class="bread-blod">REPORTE NOMINAL NOTIFICACIONES DE PARTO</span>
    </li>
</ul>

@stop
@section('content')


<div class="table-responsive">
    <form action="" class="form">
        @include('includes.localizacion')
        @include('includes.filtro_tiempo',['sinubicacion'=>false])
    </form>
    <br>
    <table class="table display responsive no-wrap " style="font-size:10px;margin-top:15px;">
        <thead>
            <tr>
                <th>CODIGO DE LA NOTIFICACION</th>
                <th>ORIGEN DE LA NOTIFICACION WEB/ANDROID</th>
                <th>USUARIO</th>
                <th>CODIGO </BR>VIGILANTE</th>
                <th>NOMBRES VIGILANTE COMUNITARIO</th>
                <th>ESTABLECIMIENTO</th>
                <th>MUNICIPIO</th>
                <th>RED</th>
                <th>DEPARTAMENTO</th>
                <th>ESTADO DE LA NOTIFICACION</th>
                <th>MUJER <br> REGISTRADA</th>
                <th>NOMBRES MUJER</th>
                <th>APELLIDOS MUJER</th>
                <th>#CARNET MUJER</th>
                <th>#CELULAR MUJER</th>
                <th>EDAD MUJER</th>
                <th>DIRECCION MUJER</th>
                <th>FECHA QUE SE ENVIO LA NOTIFICACION</th>
                <th>FECHA REGISTRO DE LA NOTIFICACION</th>
                <TH>TIENE DATOS PARA <BR> INDICADOR</TH>
                <th>PARTERA <BR> CAPACITADA</th>
                <th>PARTERA </BR> EMPIRICA</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>

            </tr>
        </thead>
    </table>


</div>

@push('custom-scripts')

<script src="{{asset('js/forms.js')}}"></script>

<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="{{asset('js/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>

<script type="text/javascript">
    var table=$('.table').DataTable({
             dom: 'Blrtip',
               language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Registros",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
             responsive: true,
             
                  ajax: {
            url: "{{url('reportes/notificacion-parto')}}",
            type: 'GET'
              },
               deferRender: true,
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "todos"] ],
        
        processing: true,
        buttons: [
            {
               extend: 'copyHtml5',
                text:'Copiar',
                title:'NOTIFICACIONES DE PARTO', 
            },
           
           {
                extend: 'print',
                text:'Imprimir',
                title:'NOTIFICACIONES DE PARTO',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '6pt' )
                        .prepend(
                            // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend:'excelHtml5',
                title:'NOTIFICACIONES DE PARTO',


            },
            
            'csvHtml5',
             {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:'NOTIFICACIONES DE PARTO',
                customize: function (doc) {
                    doc.styles.tableHeader.fontSize = 5
                    doc.defaultStyle.fontSize = 5
                    doc.styles.title = {
                        color: '#222',
                        fontSize: '14',
                        background: '#FFF',
                        alignment: 'center',
                        
                        }
                        doc.styles.tableHeader = {
                        alignment: 'center'
                        }
                }
            
                
            }
        ],
         columns: [
                    {data: 'id'},
                    {data: 'tipo'},
                    { render: function (data, type, row){ if(row.user===null) return 's/usuario'; else return row.user.username;
                    } },
                    {data: 'notificable.codVigilante',"defaultContent": "s/c"},
                    { render: function (data, type, row){ try{if(row.notificable.vigilante===null) return 's/estbl'; else return row.notificable.vigilante.persona.nombre_completo;}catch(e){return 'n/n'}} },
                    { render: function (data, type, row){ if(row.establecimiento===null) return 's/estbl'; else return row.establecimiento.nomestabl;} },
                    { render: function (data, type, row){ if(row.municipio===null) return 's/muni'; else return row.municipio.mnc_nombre; }},
                    { render: function (data, type, row){ if(row.red===null) return 's/r'; else return row.red.are_nombre; } },
                    { render: function (data, type, row){ if(row.departamento===null) return 's/depto'; else return row.departamento.dpt_nombre; } },
                    {data: 'notificable.estado',"defaultContent": "s/e"},
                    { render: function (data, type, row){ try{if(row.notificable.mujer===null) return 'NO'; else return 'SI'; }catch(e){return 'NO'}} },
                    {data: 'notificable.nombres',"defaultContent": "s/n"},
                    {data: 'notificable.primerApellido',"defaultContent": "s/a"},
                    { render: function (data, type, row){ try{if(row.notificable.mujer===null) return 's/carnet'; else return row.notificable.mujer.persona.numeroCarnet; }catch(e){return 'n/n'}} },
                    { render: function (data, type, row){ try{if(row.notificable.mujer===null) return 's/celular'; else return row.notificable.mujer.persona.celular; }catch(e){return 'n/n'}} },
                    { render: function (data, type, row){ try{if(row.notificable.mujer===null) return 's/edad'; else return row.notificable.mujer.persona.edad; }catch(e){return 'n/n'}} },
                    {data: 'notificable.direccion',"defaultContent": "s/d"},
                    {data: 'notificable.fechaRegistro',"defaultContent": "s/f"},
                    {data: 'created_at',"defaultContent": "s/f"},
                    { render: function (data, type, row){ if(row.notificable.detalle===null) return 'NO'; else return 'SI'; } },
                    { render: function (data, type, row){ if(row.notificable.detalle===null) return 's/plan'; else if(row.notificable.detalle.partera=='capacitada')  return '1'; else return '0' } },
                    { render: function (data, type, row){ if(row.notificable.detalle===null) return 's/plan'; else if(row.notificable.detalle.partera=='empirica')  return '1'; else return '0' } },
                    {data: 'notificable.latitud',"defaultContent": ""},
                    {data: 'notificable.longitud',"defaultContent": ""},
           
                   ]
         })
         yadcf.init(table, [
         { column_number: 1, filter_type: "text", filter_default_label: "" },
         { column_number: 2, filter_type: "text", filter_default_label: "" },
         { column_number: 3, filter_type: "text", filter_default_label: "" },
         { column_number: 4, filter_type: "text", filter_default_label: "" },
         { column_number: 5, filter_type: "text", filter_default_label:"" },
         { column_number: 9, filter_type: "text", filter_default_label:"" },
         { column_number: 10, filter_type: "text", filter_default_label:"" },
         { column_number: 11, filter_type: "text", filter_default_label:"" }, 
         { column_number: 12, filter_type: "text", filter_default_label: "" },
         { column_number: 13, filter_type: "text", filter_default_label: "" },
         { column_number: 19, filter_type: "text", filter_default_label: "" }
        ],
        {
            cumulative_filtering: true
        }
        );

 
              

         $('.buscar').click(function(){ 
   let dataform=$('.form').serialize()
   validarLocalizacion();

   $.ajax({
       type: "GET",
       url: "{{url('reportes/notificacion-parto')}}",
       data: dataform,
       dataType: "json",
       success: function (response) {
           console.log(response)
            table.clear().draw();
            table.rows.add(response.data).draw();
          
            // table.ajax.reload();
       }
            });
    });
</script>

@endpush
@stop