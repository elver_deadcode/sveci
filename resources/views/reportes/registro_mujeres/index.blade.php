@extends('layouts.app',[ 'active_tab_reportes'=>'active', 'active_roles'=>'active', 'title'=>'Reportes' ]) {{-- 
    @section('titulo')Municipios
    
    @stop --}}
@section('breadcome')
<ul class="breadcome-menu">
    <li><a href="#">Reportes</a> <span class="bread-slash">/</span>
    </li>

    <li><span class="bread-blod">LISTA DE MUJERES DE 10 A 59 AÑOS</span>
    </li>
</ul>
@stop


@section('content')


<div class="table-responsive">
    <form action="" class="form">
        @include('includes.localizacion')

        <button type="button" class="buscar btn btn-block btn-primary"> <i class="fa fa-search"></i> BUSCAR</button>
    </form>
    <hr>

    <table class="table display responsive no-wrap " style="font-size:10px">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRES</th>
                <th>PRIMER APELLIDO</th>
                <th>SEGUNDO APELLIDO</th>
                <th>#CARNET</th>
                <th>FECHA DE NACIMIENTO</th>
                <th>EDAD</th>
                <th>DIRECCION</th>
                <th>#CELULAR</th>
                <th>EMAIL</th>
                <th>COMUNIDAD</th>
                <th>ESTABLECIMIENTO</th>
                <th>MUNICIPIO</th>
                <th>RED</th>
                <th>DEPTO</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>


</div>

@push('custom-scripts')

<script src="{{asset('js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script type="text/javascript">
    var table=$('.table').DataTable({
                 dom: 'Blfrtip',
                   language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
            "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
            "infoFiltered": "(Filtrado de _MAX_ total Registros)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
               
                
                 responsive: true,
                //  processing: true,
                //  serverSide: true,
                 
                      ajax: {
                        url: "{{url('reportes/lista-mujeres')}}",
                type: 'GET'
                  },
                   deferRender: true,
            "lengthChange": true,
             "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "todos"] ],
            
          
      
            buttons: [
                {
                   extend: 'copyHtml5',
                    text:'Copiar',
                    title:'LISTA DE Mujeres', 
                },
               
               {
                    extend: 'print',
                    text:'Imprimir',
                    title:'LISTA DE Mujeres',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '9pt' )
                            .prepend(
                                // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            );
     
                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                },
                {
                    extend:'excelHtml5',
                    title:'LISTA DE Mujeres',
    
    
                },
                
                'csvHtml5',
                 {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title:'LISTA DE Mujeres',
                    customize: function (doc) {
                        doc.styles.tableHeader.fontSize = 7
                        doc.defaultStyle.fontSize = 7
                        doc.styles.title = {
                            color: '#222',
                            fontSize: '14',
                            background: '#FFF',
                            alignment: 'center',
                            
                            }
                            doc.styles.tableHeader = {
                            alignment: 'center'
                            }
                    }
                
                    
                }
            ],
             columns: [
                        {data: 'IdMujer'},
                        {data: 'nombres'},
                        {data: 'primerApellido'},
                        {data: 'segundoApellido'},
                        {data: 'numeroCarnet'},
                        {data: 'fechaNacimiento'},
                        {data: 'edad'},
                        {data: 'direccion'},
                        {data: 'celular'},
                        {data: 'email'},
                        {data: 'nomComunidad',"defaultContent": "s/c"},
                        {data: 'nomestabl',"defaultContent": "s/e"},
                        {data: 'mnc_nombre',"defaultContent": "s/m"},
                        {data: 'are_nombre',"defaultContent": "s/a"},
                        {data: 'dpt_nombre',"defaultContent": "s/d"},
               
                       ]
             })
             yadcf.init(table, [
           
            
              
                {
                    column_number: 1,
                    filter_type: "text",
                    filter_default_label: ""
    
                },
                {
                    column_number: 2,
                    filter_type: "text",
                    filter_default_label: ""
    
                },
                 {
                    column_number: 3,
                    filter_type: "text",
                    filter_default_label: ""
    
                },
              
                {
                    column_number: 4,
              
                  filter_type: "text",
                  filter_default_label: ""
                },
                {
                   column_number: 7,
                    filter_type: "text",
                    filter_default_label: ""
                  
                },
                 {
                   column_number: 11,
                    filter_type: "text",
                    filter_default_label: ""
                },
                 {
                    column_number: 12,
                    filter_type: "text",
                    filter_default_label: ""
                },
                {
                    column_number: 13,
                    filter_type: "text",
                    filter_default_label: ""
                },
                {
                    column_number: 14,
                    filter_type: "text",
                    filter_default_label: ""
                }
       
               
               
            ],
            {
                cumulative_filtering: true
            }
            );
    
     
                  
    
             $('.buscar').click(function(){ 
       let dataform=$('.form').serialize()
       validarLocalizacion();

       $.ajax({
           type: "GET",
           url: "{{url('reportes/lista-mujeres')}}",
           data: dataform,
           dataType: "json",
           success: function (response) {
            // table.destroy();
            table.clear().draw();
            table.rows.add(response.data).draw();
           }
                });
        }); 
    
        
</script>
@endpush



@stop