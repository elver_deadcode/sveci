@extends('layouts.app',[
     'active_tab_roles'=>'active',
     'active_registrar_rol'=>'active',
      'title'=>'Roles' ]) {{-- 
@section('titulo')Municipios 
@stop --}} 
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Roles y Permisos</a> <span class="bread-slash">/</span></li>
<li><a href="{{url('roles')}}">Roles </a> <span class="bread-slash">/</span></li>
@if(isset($role))
<li><span class="bread-blod">Editar Rol : {{$role->name}}</span></li>
    @else
    <li><span class="bread-blod">Registrar Nuevo rol</span></li>
    @endif
</ul>

@stop 
@section('content')
{{-- <example-component></example-component> --}}

<div class="table-responsive">
  <form  method="post" action="{{url('roles/guardar')}}" class="form-rol ">
        {!! csrf_field() !!}
        <div class="pull-right">

            <button type="button" class="btn btn-guardar  btn-success "><i class="fa fa-save"></i> Guardar</button>
            <a href="{{url('roles')}}" class="btn  btn-warning "><i class="fa fa-times"></i> Cancelar</a>
        </div>
<br>
        @if(isset($role))
            <input type="hidden" name="id" value="{{$role->id}}">
            @endif
        <div class="  form-group-material">
            <label for="" class="control-label"><strong class="text-danger">NOMBRE DEL ROL <span class="text-danger">*</span></strong></label>
            <input  id="nombre" type="text" @if(isset($role)) value="{{$role->name}}" @endif required class="form-control form-material" name="nombre">
        </div>
      

        <table class="table">
             <thead>
                 <tr>
                     <th>N°</th>
                     <th>PERMISO</th>
                     <th>DESCRIPCION</th>
                     <th>MODULO</th>
                     <th>MARCAR</th>
                 </tr>
             </thead>
             <tbody>
                @foreach($permisos->groupBy('modulo') as $k=>$permiso)
                <tr class="bg-primary text-center">
                    <td colspan="5">
                        MODULO {{strtoupper($k)}}
                        
                    </td>
                
                </tr>
                @foreach($permiso as $p)
                <tr >
                    <td>{{$p->id}}</td>
                    <td>{{$p->display_name}}</td>
                    <td>{{$p->description}}</td>
                    <td>{{$p->modulo}}</td>
                    <td>

                      <div class="i-checks ">
                        
                      <input type="checkbox" @if(isset($role) && $role->permissions->contains('id',$p->id))  checked @endif value="{{$p->id}}"  name="permiso[]"/> <i></i>
                        </div>
                       </td>
                
                </tr>
                
                @endforeach
                
                @endforeach
             </tbody>
        </table>
        

        <hr>
        <button  type="button" class="btn btn-guardar  btn-lg btn-block btn-success"><i class="fa fa-save"></i> Guardar</button>




    </form>
</div>

@section('scripts')

{{-- <script src="{{mix('js/forms.js')}}"></script> --}}
<script>
    $(document).ready(function () {
      
        
        $('.btn-guardar').click(function(){
            var countchecked = $(".table input[type=checkbox]:checked").length;
            if(countchecked<=0){
                
                $.alert({ 
                    title: 'Alerta',
                     content: 'Debe seleccionar por lo menos un permiso',
                     type: 'red',
                     theme: 'dark',
                     autoClose: 'ok|100'
                    
                    });
                    return false;
           }
           if($('#nombre').val()==''){
                 $.alert({ 
                    title: 'Alerta',
                     content: 'El nombre de rol es requerido',
                     type: 'red',
                     theme: 'dark',
                     autoClose: 'ok|100'
                    
                    });
                    $('#nombre').focus();
                    return false;
           }
           $('.form-rol').submit();
// $('.table [type="checkbox"]').each(function(i, chk) {
//     if (chk.checked) {
//       console.log("Checked!", i, chk);
//     }
//   });
        })
        });
</script>

@stop 
@stop