@extends('layouts.app',[
    'active_tab_roles'=>'active',
    'active_roles'=>'active',
    'title'=>'Roles'
])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Roles y Permisos</a> <span class="bread-slash">/</span>
    </li>
  
    <li><span class="bread-blod">Roles</span>
    </li>
</ul>
@stop
@section('content')


<div class="table-responsive">
<a href="{{url('roles/nuevo')}}" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i> Registrar  Nuevo Rol</a>
    <hr>
        <table style="font-size: 10px" class="table datatable">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>ROL</th>
                    <th>ACCIONES</th>
               
                    
                </tr>
            </thead>

        </table>
        </div>
 @section('scripts')
        <script>
        $(document).ready(function () {
              $(document).on('click','.btn-borrar',function(_evt){
                  _evt.preventDefault();
                  let _href=$(this).attr('href')

                     $.confirm({
                        title: 'Confirmacion',
                        icon:' fa fa-trash',
                        content: 'Desea eliminar este rol de  usuario?',
                        buttons: {
                            confirmar: function () {
                                window.location=_href
                            },
                            cancelar: function () {
                                this.close();
                            }
                           
                        }
                    });
              })


            $('.datatable').DataTable({
             dom: "Bfrltip", 
        ajax: {
            url: "{{url('roles/listar')}}",
            type: 'GET'
              },
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
             { render: function (val, type, row){ 

                    var links=`
                    <a  class="btn btn-warning btn-xs" href="{{url('roles/${row.id}/editar')}}" ><i class="fa fa-edit"></i> Editar </a>
                    <a  class="btn btn-borrar btn-danger btn-xs" href="{{url('roles/${row.id}/borrar')}}" ><i class="fa fa-trash"></i> Borrar </a>`
                         return links;
                    
                },
             },
           
          
           
        ]
                
            });
        });
        </script>
 @stop
@stop