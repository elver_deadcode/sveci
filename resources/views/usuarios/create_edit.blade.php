@extends('layouts.app',[
'active_tab_usuarios'=>'active',
'active_usuarios_nuevo'=>'active',
'title'=>'Gestion Usuarios'
])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
  <li><a href="#">Usuarios</a> <span class="bread-slash">/</span>
  </li>

  <li>
    @if(isset($user))
    <span class="bread-blod">Editar</span>
    @else
    <span class="bread-blod">Registro de usuarios</span>
    @endif
  </li>
</ul>
@stop
@section('content')

<div class=" container panel panel-default">
  <div class="panel-body">
    <form method="POST" action="{{ url('usuarios/guardar') }}" data-toggle="validator" class="form">
      @csrf
      <input type="hidden" name="persona_id" value="
      @if(isset($user) )
      {{$user->persona->IdPersona}}
      @else
      0

     @endif
     " " class=" persona_id">

      <input type="hidden" class="adscrito_id" name="adscrito_id" value="
            @if(isset($user) )
             {{$user->persona->verificado}}

            @endif
            ">
      @csrf

      @if(isset($user))
      <input type="hidden" name="usuario_id" value="{{$user->id}}">
      @endif
      <fieldset style="padding:5px 5px;">
        <div class="form-group row">
          <div class="col-sm-2">
            <label for="" class=" search col-form-label">Numero de carnet<span
                class="text-danger"><small>*</small></span></label>
            <input type="text" name="numero_carnet" data-error="Completa este campo" required
              class="form-control   solo-enteros numero_carnet" id="" placeholder="Numero de carnet" @if(isset($user))
              value="{{$user->persona->numeroCarnet}}" @endif @if(isset($user) && $user->persona->verificado) readonly
            @endif
            >
            <div class="help-block with-errors"></div>
          </div>
          <div class="col-sm-2">
            <label for="" class="search col-form-label">Fecha Nacimiento<span
                class="text-danger"><small>*</small></span></label>
            <input type="text" name="fecha_nacimiento" required data-validation="date"
              data-validation-format="yyyy-mm-dd" required
              inputmask="'mask': '9999 9999 9999 9999'"
              data-error="selecciona una fecha de tal forma  que la edad este entre 10 y 59 años"
              class="datepicker fecha_nacimiento form-control" required data-error="Completa la fecha de nacimiento"
              placeholder="{{date('d/m/Y')}}" @if(isset($user))
              value="{{$user->persona->fechaNacimiento->format('d/m/Y')}}" @endif @if(isset($user) &&
              $user->persona->verificado) readonly
            @endif
            >
            <div class="help-block with-errors"></div>
          </div>

          <div class="col-sm-2">
            <label for="" class="search col-form-label">Complemento</label>
            <input type="text" name="complemento" @if(isset($user)) value="{{$user->persona->complemento}}" @endif
              @if(isset($user) && $user->persona->verificado) readonly
            @endif
            class="form-control complemento" id="" placeholder="complemento"
            >
          </div>
          <div class="col-sm-3">
            <br>


            <button type="button"
              class="@if(isset($user) && $user->persona->verificado) hide   @endif btn btn-primary mt-3 btn-buscar"
              style="margin-top:5px;"><i class=" fa fa-search "></i>Buscar</button>
            <button type="button"
              class="@if(isset($user) && $user->persona->verificado) hide   @endif btn btn-default mt-3 btn-limpiar"
              style="margin-top:5px;"><i class=" fa fa-refresh "></i>Limpiar</button>
          </div>

        </div>
      </fieldset>
      <fieldset class="@if(!isset($user))  hide @endif fieldset" style="padding:5px 5px; margin-top:5px;">


        <div class="form-group row " style="margin-top:10px;">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Nombres <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
          <div class="col-sm-10">
            <input type="text" @if(isset($user)) value="{{$user->persona->nombres}}" @endif name="nombres"
              data-error="Completa este campo" required class="form-control required nombres" id="inputEmail3"
              placeholder="Nombres">
            <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label">Primer apellido  <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
          <div class="col-sm-10">
            <input type="text" name="primer_apellido" @if(isset($user)) value="{{$user->persona->primerApellido}}"
              @endif required data-error="Completa este campo" class="form-control required primer_apellido" id=""
              placeholder="Primer apellido">
            <div class="help-block with-errors"></div>

          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label">Segundo apellido</label>
          <div class="col-sm-10">
            <input type="text" name="segundo_apellido" class="form-control required primer_apellido" @if(isset($user))
              value="{{$user->persona->segundoApellido}}" @endif id="" placeholder="Segundo apellido">
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label">Direccion</label>
          <div class="col-sm-10">
            <input type="text" name="direccion" class="required direccion form-control" id=""
              placeholder="Direccion actual" @if(isset($user)) value="{{$user->persona->direccion}}" @endif>
          </div>
        </div>
        <div class="form-group row">

          <label for="" class="col-sm-2 col-form-label">Numero celular</label>
          <div class="col-sm-4">
            <input type="text" name="numero_celular" class="form-control num_celular" id=""
              placeholder="Numero de celular" @if(isset($user)) value="{{$user->persona->celular}}" @endif>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label">Correo electronico<span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
          <div class="col-sm-10">
            <input type="email" name="email" class="email form-control required " data-error="Completa este campo"
              required id="" placeholder="Correo electronico" @if(isset($user)) value="{{$user->persona->email}}"
              @endif>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label"> Sexo </label>
          <label for="" class="col-sm-2 col-form-label">
            Hombre <input type="radio" @if(isset($vigilante) )@if($vigilante->persona->sexo=='M') checked @endif @else
            checked @endif name="sexo" value="M" class="masculino">
          </label>
          <label for="" class="col-sm-2 col-form-label">
            Mujer <input type="radio" @if(isset($vigilante) && $vigilante->persona->sexo=='F') checked @endif
            name="sexo" value="F" class="femenino">
          </label>


        </div>



        <div class="form-group row">

          <label for="" class="col-sm-2 col-form-label">Contraseña <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
          <div class="col-sm-5">
            <input type="text" name="password" @if(isset($user)) disabled @else required @endif
              pattern="{{$parametro_seguridad['patron']}}" class="password form-control" id=""
              placeholder="Contraseña"  data-error="Completa este campo correctamente">
            @if(isset($user))
            <label for="" class="text-danger">Cambiar Contraseña</label>
            <input type="checkbox" class="editpassword" name="editpassword" value="1">
            @endif
            <div class="help-block with-errors "></div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class=" col-md-8 col-md-offset-2 label label-warning"><i class="fa fa-info"></i>
              {{$parametro_seguridad['mensaje']}}          </label>

        </div>
        <fieldset class="form-group">

          <div class="">
            <legend class=" col-sm-2"><label for="">Localizacion</label></legend>
            <div class="">
              @if(auth()->user()->nivel=="nacional")
              <div class="col-md-1">
                <label for="">Nacional</label>
                <input type="radio" @if(isset($user) && $user->nivel=='nacional') checked @endif id="cb-nacional"
                value="nacional" name="nivel">
              </div>
              @endif
              @if(auth()->user()->nivel=="nacional"
              || auth()->user()->nivel=="depto")



              <div class="col-md-2">
                <label for="">Departamental</label><br>
                <input type="radio" @if(isset($user) && $user->nivel=='depto') checked @endif id="cb-depto"
                value="depto" name="nivel">
              </div>


              @endif
              @if(auth()->user()->nivel=="nacional"
              || auth()->user()->nivel=="depto"
              || auth()->user()->nivel=="red")
              <div class="col-md-1">
                <label for="">Red</label><br>
                <input type="radio" @if(isset($user) && $user->nivel=='red') checked @endif id="cb-red" value="red"
                name="nivel">
              </div>
              @endif

              @if(auth()->user()->nivel=="nacional"
              || auth()->user()->nivel=="depto"
              || auth()->user()->nivel=="muni"
              )
              <div class="col-md-1">
                <label for="">Municipal</label>
                <input type="radio" id="cb-muni" @if(isset($user) && $user->nivel=='muni') checked @endif value="muni"
                name="nivel">
              </div>
              @endif

              @if(auth()->user()->nivel=="nacional"
              || auth()->user()->nivel=="depto"
              || auth()->user()->nivel=="red"
              || auth()->user()->nivel=="muni"
              || auth()->user()->nivel=="establ"
              )
              <div class="col-md-1">
                <label for="">Establecimiento</label>
                <input type="radio" @if(isset($user) && $user->nivel=='establ') checked @endif
                @if(\Auth::user()->nivel=='establ')checked @endif id="cb-esta" value="establ" name="nivel">
              </div>

              @endif
            </div>
            <div class="col-sm-12">
              <div class="col-md-2 ">
              </div>
              <div @if(auth()->user()->nivel=='nacional' ) style="display:block" @else style="display:none" @endif
                class="col-md-2 div-dptos">
                <label for="">Departamentos</label>
                <select class="departamento form-control" name="dpto" id="">
                  <option value="">Seleccione</option>
                  @foreach($dptos as $k=>$dpto)
                  <option @if(isset($user)&&$dpt_codigo==$dpto->dpt_codigo) selected @endif
                    value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
                  @endforeach
                </select>
              </div>

              <div @if(isset($user) && $user->nivel=='red') style="display:block" @else style="display:none" @endif
                class="col-md-2 div-red">
                <label for="">Redes</label>
                <select class="red form-control" name="red" id="">
                  <option value="">Seleccione</option>
                  @foreach($redes as $k=>$red)
                  <option @if(isset($user)&&$are_codigo==$red->are_codigo) selected @endif dptoid="{{$red->dpt_codigo}}"
                    value="{{$red->are_codigo}}">{{$red->depto->dpt_nombre}} - {{$red->are_nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div @if( isset($user) && ($user->nivel=='nacional' ||$user->nivel=='depto')) style="display:block" @else
                style="display:none" @endif class="col-md-3 form-group div-municipios">
                <label for="">Municipio</label>
                <select class="municipio form-control" name="municipio" id="">
                  <option dptoid="0" value="">Seleccione</option>
                  @foreach($municipios as $k=>$muni)
                  <option @if(isset($user)&&$mnc_codigo==$muni->mnc_codigo) selected @endif
                    dptoid={{$muni->provincia->departamento->dpt_codigo}}
                    value="{{$muni->mnc_codigo}}">{{$muni->mnc_nombre}}</option>
                  @endforeach
                </select>

              </div>
              <div @if(isset($user) && $user->nivel=='establ') style="display:block" @else style="display:none" @endif
                @if(\Auth::user()->nivel!='establ') style="display:none" @endif class="col-md-3 form-group div-estable">
                <label for="">Establecimientos</label>
                @if(\Auth::user()->nivel=='establ')
                <input type="text" readonly class="form-control" value="{{$establecimientos[0]->nomestabl}}">
                <input type="hidden" class="form-control" name="establecimiento"
                  value="{{$establecimientos[0]->codestabl}}">

                @else
                <select class="establecimiento form-control" name="establecimiento" id="">
                  <option value="">Seleccione</option>
                  @foreach($establecimientos as $k=>$v)
                  <option @if(isset($user)&& $user->idnivel==$v->codestabl) selected @endif
                    value="{{$v->codestabl}}">{{$v->nomestabl}}</option>
                  @endforeach

                </select>
                @endif

              </div>
            </div>
          </div>
        </fieldset>
        <fieldset class="form-group">

          <div class="row">
            <legend class=" col-sm-3"><label for="">Rol de Usuario <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
            </legend>
            <div class="col-md-9">
              <label for=""></label>
              <select required data-error="Selecciona un rol" name="role" id="role" class="form-control">
                <option value="">Seleccione un Rol</option>
                @foreach($roles as $k=>$rol)
                <option @if(isset($user) && $user->roles[0]->id==$rol->id) selected @endif
                  value="{{$rol->id}}">{{$rol->display_name}}</option>
                @endforeach
              </select>
              <div class="help-block  with-errors"></div>

            </div>
          </div>
        </fieldset>
        <div class="form-group row">
          <div class="col-sm-10 col-sm-offset-1">
            <button type="submit" class="btn  btn-block btn-primary btn-guardar"><i class="fa fa-save"></i> Registrar
              Usuario</button>
            <a href="{{url('usuarios')}}" class="btn  btn-block btn-default"><i class="fa fa-times"></i> Cancelar</a>
          </div>
        </div>
      </fieldset>
      <input type="hidden" name="departamento_id" @if(isset($user) ) value="{{$user->persona->dpt_codigo}}" @endif
        class="departamento_id">
      <input type="hidden" name="municipio_id" @if(isset($user)) value="{{$user->persona->mnc_codigo}}" @endif
        class="municipio_id">
      <input type="hidden" name="red_id" @if(isset($user)) value="{{$user->persona->are_codigo}}" @endif class="red_id">
      <input type="hidden" name="establecimiento_id" @if(isset($user)) value="{{$user->persona->codEstablecimiento}}"
        @endif class="establecimiento_id">

    </form>
  </div>
</div>


@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script>

  @if(isset($user) )
    @if($user->persona->verificado)
   $('.required').not('.email').prop('readonly',true)
   @else
   $('.required').prop('readonly',false)
   @endif
   @else
   $('.required').not('.email').prop('readonly',true)
@endif

@if(isset($user) )
@if($user->nivel=="nacional")
$('.div-dptos').hide();
$('.div-municipios').hide();
$('.div-redes').hide();
@endif
@if($user->nivel=="depto")
$('.div-dptos').hide();
$('.div-municipios').hide();
$('.div-redes').hide();
@endif
@endif


function llenarFormulario(datos){
       $('.search').prop('readonly',true)
       $('.nombres').val(datos.nombres)
       $('.adscrito_id').val(datos.adscrito_id)
       $('.primer_apellido').val(datos.primerapellido)
       $('.segundo_apellido').val(datos.segundoapellido)
       $('.complemento').val(datos.complemento)
       $('.direccion').val(datos.direccion)
       $('.num_celular').prop('disabled',false)
       $('.num_celular').val(datos.tel_referencia)
       if(datos.email)
       $('.email').val(datos.email)
       if(datos.id)
       $('.persona_id').val(datos.id)
       if(datos.sexo=='Masculino')
       $('.masculino').prop('checked',true)
       if(datos.sexo=='Femenino')
       $('.femenino').prop('checked',true)

       $('.departamento_id').val(datos.departamento_id)
       $('.municipio_id').val(datos.municipio_id)
       $('.red_id').val(datos.red_id)
       $('.establecimiento_id').val(datos.establecimiento_id)



 }
 function limpiarRegistrar(){
     $('.required').prop('readonly',false)
     $('.nombres').focus();
 }


        $(document).ready(function () {
          $('.form').validator()
      $('.editpassword').click(function(){
         if($(this).is(':checked')){
              $('.password').val('');
              $('.password').focus();
               $('.password').attr('disabled',false);
               $('.password').attr('required',true);
         }
         else{
          $('.password').val('');
           $('.password').focus();
             $('.password').attr('disabled',true);
               $('.password').attr('required',false);
         }
      })

          $('#cb-nacional').click(function(){
            if($(this).is(':checked')){
            $('.div-dptos').hide();
            $('.div-municipios').hide();
            $('.div-estable').hide();
            $('.div-red').hide();
            }
          })

              $('#cb-depto').click(function(){
            if($(this).is(':checked')){
            $('.div-dptos').show();
            $('.div-red').hide();
            $('.div-municipios').hide();
            $('.div-estable').hide();
            }
          })

             $('#cb-red').click(function(){
            if($(this).is(':checked')){
              @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto" )
            $('.div-dptos').show();
            @endif
            $('.div-red').show();
            $('.div-municipios').hide();
            $('.div-estable').hide();
            }
          })

             $('#cb-muni').click(function(){
            if($(this).is(':checked')){
            @if(auth()->user()->nivel=="nacional" || auth()->user()->nivel=="depto"  )
            $('.div-dptos').show();
            @endif
            $('.div-municipios').show();
            $('.div-estable').hide();
            $('.div-red').hide();
            }
          })

             $('#cb-esta').click(function(){

            if($(this).is(':checked')){

              @if(auth()->user()->nivel=="nacional")
            $('.div-dptos').show();
            $('.div-municipios').show();
            $('.div-estable').show();
            @endif
            @if(auth()->user()->nivel=="depto")
              $('.div-municipios').show();
            $('.div-estable').show();
            @endif
              @if(auth()->user()->nivel=="red")
            $('.div-estable').show();
            $('.red').val('');
            @endif

             @if(auth()->user()->nivel=="muni")

            $('.div-estable').show();
            @endif

            }
          })

      $('._id').change(function(){
    //   alert('asd')
      var _dpto= $(this).val();
     $('.municipio').val('');
      $(".municipio option").each(function(i,o) {

            if ($(o).attr("dptoid") ===_dpto) {
                $(o).show()
            }
            else{
                $(o).hide()
            }
       });
     $('.red').val('');
      $(".red option").each(function(i,o) {

            if ($(o).attr("dptoid") ===_dpto) {
                $(o).show()
            }
            else{
                $(o).hide()
            }
       });


  })
            $('.departamento').change(function(){
                        let id=$(this).val();
                $('.municipio').val('').trigger('change')
                $('.red').val('').trigger('change')
                $('.municipio > option').each(function(k,v){
                    if($(this).attr('dptoid')==id){
                        $(this).show();
                    }
                    else{
                        $(this).hide();
                    }

                })
                $('.red > option').each(function(k,v){
                    if($(this).attr('dptoid')==id){
                        $(this).show();
                    }
                    else{
                        $(this).hide();
                    }

                })
            })

  $('.municipio').change(function(){
         var id=$(this).val();
         var option="";
     $.ajax({
       url:"{{url('ajax/establecimientos-municipio')}}",
       type:'GET',
       dataType:'json',
       data:{idmuni:id},
       success:function(res){
        $('.establecimiento').find('option').not(':first').remove();

        $('.establecimiento').append(res.options);

       },
     })

  })

    $('.red').change(function(){
         var id=$(this).val();
         var option="";
     $.ajax({
       url:"{{url('ajax/establecimientos-red')}}",
       type:'GET',
       dataType:'json',
       data:{idred:id},
       success:function(res){
        $('.establecimiento').find('option').not(':first').remove();

        $('.establecimiento').append(res.options);

       },
     })

  })

  $('.btn-buscar').click(function(){
      if($('.numero_carnet').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese un numero de carnet',
                    });
                    return false;
      }
      if($('.fecha_nacimiento').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese una Fecha de nacimiento',
                    });
                    return false;
      }

      let numcarnet=$('.numero_carnet').val()
      let fechanacimiento=$('.fecha_nacimiento').val()
      let complemento=$('.complemento').val()
      $.ajax({
          url:"{{url('usuarios/buscar-persona-api')}}",
          type:'GET',
          dataType:'json',
          data:{
              num_carnet:numcarnet,
              fecha_nacimiento:fechanacimiento,
              complemento:complemento,
          },
          beforeSend:function(){
            blocUI('Buscando...')
          },
          complete:function(){
                    $.unblockUI();

                },
          success:function(res){
              console.log(res)
              if(res.success){
                  if(res.data.success && res.data.code==200){
                    llenarFormulario(res.data.data)
                    $('.fieldset').removeClass('hide')

                  }
                  else if( res.status==500){
                    // alert(res.data);
                  }
                  else if(!res.data.success){
                    $.confirm({
            title: res.data.mensaje,
            content: 'No se encontraron datos de la persona.Desea registrar datos de la persona?',
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
                  }
              }
              else{
                $.confirm({
            title: 'No se encontraron datos de la persona',
            content: 'Desea registrar datos de la persona?',
            autoClose: 'Cancelar|8000',
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
              }

          },
          error:function(){}


      })

  })
        });
</script>
@stop
@stop
