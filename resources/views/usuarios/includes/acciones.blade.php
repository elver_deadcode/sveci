<div class="btn-group">

    @permission('editar_usuarios')
    @if($usuario->nivel=='api')
    <a href='{{url("usuarios/$usuario->id/editarapi")}}' type="button" class=" btn btn-xs  btn-warning">Editar</a>
    @else
        <a href='{{url("usuarios/$usuario->id/editar")}}' type="button" class=" btn btn-xs  btn-warning">Editar</a>
    @endif
        @endpermission

    @permission('baja_usuarios')
   @if($usuario->estado=='true')
    <button type="button" id={{$usuario->id}} class=" btn-baja btn btn-xs  btn-danger">Dar de Baja</button>
    @else
    <button type="button" id={{$usuario->id}} class="btn-alta btn btn-xs  btn-success">Dar de Alta</button>
    @endif
    @endpermission
     @if($usuario->nivel=='establ')
    <a  target="_blank" href='{{url("usuarios/$usuario->id/crearvigilante")}}' type="button" class=" btn btn-xs  btn-info">convertir Vigilante</a>
@endif
    {{-- <button type="button" class="btn  btn-xs btn-default">Derecho</button> --}}
</div>
