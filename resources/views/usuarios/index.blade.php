@extends('layouts.app',[ 'active_tab_usuarios'=>'active',
'active_usuarios'=>'active', 'title'=>'Usuarios' ])
{{-- @section('titulo')Municipios @stop --}}
@section('breadcome')

<ul class="breadcome-menu">
  <li><a href="#">Usuarios</a> <span class="bread-slash">/</span></li>

  <li><span class="bread-blod">Lista de Usuarios</span></li>
</ul>
@stop @section('content')

<div class="table-responsive">
  @permission('crear_usuarios')
  <a href="{{ url('usuarios/nuevo') }}" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i> Registrar Nuevo
    Usuario</a>
    @endpermission
  <hr />
  {{-- @include('includes.filter-niveles') --}}

  <table style="font-size: 10px" class="table datatable">
    <thead>
      <tr>
        <th>ACCIONES</th>
        <th>COD</th>
        <th>NOMBRE DE USUARIO</th>
        <th>NOMBRES</th>
        <th>PRIMER APELLIDO</th>
        <th>SEGUNDO APELLIDO</th>
        <th>CARNET</th>
        <th>EMAIL</th>
        <th>NUM CELULAR</th>
        <th>Localizacion</th>

      </tr>
    </thead>
  </table>
</div>
@section('scripts')
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script>

<script>
  $(document).ready(function() {
    function cambiarEstado(id) {
      $.ajax({
        type: "GET",
        url: "{{url('usuarios/cambiar-estado')}}",
        data: { id: id },
        dataType: "json",
        success: function(response) {
          if (response.status) {
            Table.draw();
          }
        },
        error: function(response) {
          $.alert("ocurrio un error");
        }
      });
    }

    $(document).on("click", ".btn-baja", function() {
      var id = $(this).attr("id");
      $.confirm({
        title: "Dar de baja!",
        theme: "supervan",
        content:
          "Al dar de baja al usuario este ya no podra ingresar al sistema!",
        buttons: {
          confirm: {
            text: "Dar de baja",
            action: function() {
              // $.alert('Confirmed!');

              cambiarEstado(id);
            }
          },
          cancel: function() {
            return;
            // $.alert('Canceled!');
          }
          // somethingElse: {
          //     text: 'Something else',
          //     btnClass: 'btn-blue',
          //     keys: ['enter', 'shift'],
          //     action: function(){
          //         $.alert('Something else?');
          //     }
          // }
        }
      });
    });

    $(document).on("click", ".btn-alta", function() {
      var id = $(this).attr("id");
      $.confirm({
        title: "Dar de alta!",
        theme: "supervan",
        content: "Al dar de alta, el usuario volver a a ingresar al sistema ",
        buttons: {
          confirm: {
            text: "Dar de Alta",
            action: function() {
              // $.alert('Confirmed!');

              cambiarEstado(id);
            }
          },
          cancel: function() {
            return;
          }
        }
      });
    });

    $(".departamento").change(function() {
      //   alert('asd')
      var _dpto = $(this).val();
      $(".municipio option").each(function(i, o) {
        //   console.log(o)
        if ($(o).attr("dptoid") === _dpto) {
          $(o).show();
        } else {
          $(o).hide();
        }
      });
    });

    $(document).on("click", ".btn-borrar", function(_evt) {
      _evt.preventDefault();
      let _href = $(this).attr("href");

      $.confirm({
        title: "Confirmacion",
        icon: " fa fa-trash",
        content: "Desea eliminar este rol de  usuario?",
        buttons: {
          confirmar: function() {
            window.location = _href;
          },
          cancelar: function() {
            this.close();
          }
        }
      });
    });

    var Table = $(".datatable").DataTable({
      dom: "<'row '<' ml-2 pl-2 pt-2 mb-1 col-md-2 'l><'px-0   col-md-4'i><' mx-0 col-md-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row txt-small'<'col-sm-5'i><'col-sm-7'p>>",
      ajax: {
        url: "{{url('usuarios/listar')}}",
        type: "GET"
      },
      lengthChange: true,
      lengthMenu: [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
      language: {
        decimal: "",
        emptyTable: "No hay información",
        info: "Mostrando _START_ a _END_ de _TOTAL_ Registros",
        infoEmpty: "Mostrando 0 to 0 of 0 Registros",
        infoFiltered: "(Filtrado de _MAX_ total Registros)",
        infoPostFix: "",
        thousands: ",",
        lengthMenu: "Mostrar _MENU_ Registros",
        loadingRecords: "Cargando...",
        processing: "Procesando...",
        search: "Buscar:",
        zeroRecords: "Sin resultados encontrados",
        paginate: {
          first: "Primero",
          last: "Ultimo",
          next: "Siguiente",
          previous: "Anterior"
        }
      },

      serverSide: true,
      processing: true,
      columns: [
        { data: "acciones" },
        { data: "id", name: "id" },
        { data: "username", name: "username" },
        { data: "persona.nombres", name: "persona.nombres", defaultContent: "s/n" },
        {
          data: "persona.primerApellido",
          name: "persona.primerApellido",
          defaultContent: "s/n"
        },
        {
          data: "persona.segundoApellido",
          name: "persona.segundoApellido",
          defaultContent: "s/n"
        },
        { data: "persona.numeroCarnet", name:"persona.numeroCarnet",defaultContent: "s/n" },
        { data: "persona.email", name: "persona.email", defaultContent: "s/n" },
        {
          data: "persona.celular",
          name: "persona.celular",
          defaultContent: "s/n"
        },
         { data: "localizacion", name: "localizacion", defaultContent: "s/n" },
      ]
    })
    yadcf.init(Table, [

       {
       column_number: 2,
       filter_type: "text",
       filter_default_label: "",
   },

   {
       column_number: 3,
       filter_type: "text",
       filter_default_label: ""
   },
   {
       column_number: 4,
       filter_type: "text",
       filter_default_label: ""
   },
    {
       column_number: 5,
       filter_type: "text",
       filter_default_label: ""
   },

   {
       column_number: 6,

     filter_type: "text",
     filter_default_label: ""
   },
   {
       column_number: 7,

     filter_type: "text",
     filter_default_label: ""
   },

   {
       column_number: 8,

     filter_type: "text",
     filter_default_label: ""
   },

],
{
   cumulative_filtering: true
}
);
  });
</script>
@stop @stop
