@extends('layouts.app',[ 'active_tab_usuarios'=>'active', 'active_usuarios'=>'active', 'title'=>'Registro completo' ]) {{-- 
@section('titulo')Municipios

@stop --}} 
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Usuarios</a> <span class="bread-slash">/</span>
    </li>

    <li><span class="bread-blod">Registro completo</span>
    </li>
</ul>

@stop 
@section('content')

 <div class="container">
     <div class="panel panel-default">
         <div class="panel-heading">
             <h4>REGISTRO COMPLETO</h4>
         </div>
         <div class="panel-body col-md-6">
             <table  class=" col-md-6 table">
                 <tr class="bg-info">
                     <td>Nombre de usuario</td>
                 <td><h3><strong>{{$user->username}}</strong></h3></td>
                 </tr>
                
                 <tr>
                     <td>Nombre completo</td>
                 <td>{{$user->persona->nombre_completo}}</td>
                 </tr>
                 <tr>
                     <td>Numero de carnet</td>
                 <td>{{$user->persona->numeroCarnet}}</td>
                 </tr>
                 <tr>
                     <td>Fecha nacimiento</td>
                 <td>{{$user->persona->fechaNacimiento->format('d-m-Y')}}</td>
                 </tr>
                 <tr>
                        <td>Sexo</td>
                    <td>{{$user->persona->genero}}</td>
                    </tr>
                 <tr>
                        <td>Direccion</td>
                    <td>{{$user->persona->direccion}}</td>
                    </tr>
                 <tr>
                     <td>Email</td>
                 <td>{{$user->persona->email}}</td>
                    </tr>

                 <tr>
                     <td>Numero de celular</td>
                 <td>{{$user->persona->celular}}</td>
                 </tr>

                 <tr>
                    <td>Rol de Usuarior</td>
                    <td>{{$user->roles[0]->display_name}}</td>
                </tr>
             </table>
            <a href="{{url('usuarios')}}" class="btn btn-primary"><i class="fa fa-backward"></i> Regresar</a>
         </div>
     </div>
 </div>



@section('scripts')
<script>
    $(document).ready(function () {

        });
</script>

@stop 
@stop