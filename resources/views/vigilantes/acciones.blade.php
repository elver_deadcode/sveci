<div class="btn-group">
    <a href='{{url("vigilantes/$vigilante->IdVigilante/ver-perfil")}}'  class=" btn btn-xs  btn-info">Perfil</a> 
    @permission('editar_vigilantes')
    <a href='{{url("vigilantes/$vigilante->IdVigilante/editar")}}'  class=" btn btn-xs  btn-warning">Editar</a> 
   @endpermission
   @permission('baja_vigilantes')
    @if($vigilante->estado=='true')
    <button type="button" id={{$vigilante->IdVigilante}} class=" btn-baja btn btn-xs  btn-danger">Dar de Baja</button> 
    @else
    <button type="button" id={{$vigilante->IdVigilante}} class="btn-alta btn btn-xs  btn-success">Dar de Alta</button> 
    @endif 
    @endpermission