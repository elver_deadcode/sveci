@extends('layouts.app',[ 'active_tab_vigilantes'=>'active', 'active_crear_vigilantes'=>'active', 'title'=>'Gestion Usuarios'
]) {{-- 
@section('titulo')Municipios 
@stop --}} 
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="{{ url('vigilantes', []) }}">VIGILANTES COMUNITARIOS</a> <span class="bread-slash">/</span>
    </li>

    <li>
        @if(isset($vigilante))
        <span class="bread-blod">Editar</span> @else
        <span class="bread-blod">REGISTRO DE VIGILANTE COMUNITARIO</span> @endif
    </li>
</ul>


@stop 
@section('content')

<div class=" container panel panel-default">
    <div class="panel-body">
        <form method="POST" action="{{ url('vigilantes/guardar') }}" data-toggle="validator" class="form">
                        <input type="hidden" name="persona_id" value="0" class="persona_id"> 

                   <input type="hidden" class="adscrito_id" name="adscrito_id" value="
            @if(isset($vigilante) ) 
             {{$vigilante->persona->verificado}}
            
            @endif
            "
            >


            @csrf 
            @if(isset($vigilante))
            
            <input type="hidden" name="vigilante_id" value="{{$vigilante->IdVigilante}}"> 
            @endif
            <fieldset   style="padding:5px 5px;" >
                <div class="form-group row">
                        <div class="col-sm-2">
                                <label for="" class=" search col-form-label">Numero de carnet<span class="text-danger"><small>*</small></span></label>
                            <input type="text" name="numero_carnet"   data-error="Completa este campo" required
                                class="form-control solo-enteros numero_carnet" id="" placeholder="Numero de carnet" 
                                @if(isset($vigilante)) value="{{$vigilante->persona->numeroCarnet}}"
                                @endif
                                @if(isset($vigilante) && $vigilante->persona->verificado) readonly
                                @endif
                                >
                            <div class="help-block with-errors"></div>
                        </div>
                    <div class="col-sm-2">
                            <label for="" class="search col-form-label">Fecha Nacimiento<span class="text-danger"><small>*</small></span></label>
                        <input type="text" name="fecha_nacimiento" required data-validation="date" data-validation-format="yyyy-mm-dd" required data-error="selecciona una fecha de tal forma  que la edad este entre 10 y 59 años"  
                        class="datepicker fecha_nacimiento form-control" required data-error="Completa la fecha de nacimiento"
                            placeholder="{{date('d/m/Y')}}"
                             @if(isset($vigilante)) value="{{$vigilante->persona->fechaNacimiento->format('d/m/Y')}}"
                            @endif
                            @if(isset($vigilante) && $vigilante->persona->verificado) readonly
                            @endif
                            >
                        <div class="help-block with-errors"></div>
                    </div>
                
                    <div class="col-sm-2">
                        <label for="" class="search col-form-label">Complemento</label>
                        <input type="text" name="complemento"  
                        @if(isset($vigilante)) value="{{$vigilante->persona->complemento}}"
                        @endif
                        @if(isset($vigilante) && $vigilante->persona->verificado) readonly
                        @endif
                        class="form-control complemento" id="" placeholder="complemento" 
                        >
                    </div>
                    <div class="col-sm-3">
                        <br>
                            
                       
                        <button  type="button" class="@if(isset($vigilante) && $vigilante->persona->verificado) hide   @endif btn btn-primary mt-3 btn-buscar" style="margin-top:5px;"><i class=" fa fa-search "></i>Buscar</button>
                        <button  type="button" class="@if(isset($vigilante) && $vigilante->persona->verificado) hide   @endif btn btn-default mt-3 btn-limpiar" style="margin-top:5px;"><i class=" fa fa-refresh "></i>Limpiar</button>
                    </div>
                    
                </div>
                </fieldset>
            
<fieldset class="@if(!isset($vigilante))hide @endif fieldset" style="padding:5px 5px; margin-top:5px;">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Nombres <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
        <div class="col-sm-10">
            <input type="text" @if(isset($vigilante)) value="{{$vigilante->persona->nombres}}" @endif name="nombres"  data-error="Completa este campo"
                required class=" required form-control nombres" id="inputEmail3" placeholder="Nombres">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Primer apellido <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label>
        <div class="col-sm-10">
            <input type="text" name="primer_apellido" @if(isset($vigilante)) value="{{$vigilante->persona->primerApellido}}" @endif required data-error="Completa este campo"
                class=" required form-control primer_apellido" id="" placeholder="Primer apellido">
            <div class="help-block with-errors"></div>

        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Segundo apellido</label>
        <div class="col-sm-10">
            <input type="text" name="segundo_apellido" class="required segundo_apellido form-control" @if(isset($vigilante)) value="{{$vigilante->persona->segundoApellido}}"
                @endif id="" placeholder="Segundo apellido">
        </div>
    </div>

    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Direccion</label>
        <div class="col-sm-10">
            <input type="text" name="direccion" class="required form-control direccion" id="" placeholder="Direccion actual" @if(isset($vigilante)) value="{{$vigilante->persona->direccion}}"
                @endif>
        </div>
    </div>
 
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Numero celular</label>
        <div class="col-sm-2">
            <input type="text" name="numero_celular" pattern="^[0-9]+$"  data-error="Completa el numero del celular con solo numeros"
                class="num_celular form-control" id="" placeholder="Numero de celular" @if(isset($vigilante)) value="{{$vigilante->persona->celular}}"
                @endif>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="email form-control" id="" placeholder="Correo electronico" @if(isset($vigilante)) value="{{$vigilante->persona->email}}"
                @endif>
        </div>
    </div>

    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label"> Sexo </label>
        <label for="" class="col-sm-2 col-form-label">
            Hombre <input type="radio" @if(isset($vigilante) )@if($vigilante->persona->sexo=='M') checked @endif @else checked @endif name="sexo" value="M" class="masculino">
        </label>
        <label for="" class="col-sm-2 col-form-label">
            Mujer <input type="radio" @if(isset($vigilante) && $vigilante->persona->sexo=='F') checked @endif name="sexo" value="F" class="femenino">
        </label>


    </div>

 

    
    <div class="">
        <legend class=" col-sm-12"><label for="">Localizacion geografica</label></legend>

        <div class="col-sm-12">
            <div class="col-md-2 ">
            </div>

            <div class="col-md-2 div-dptos" @if(auth()->user()->nivel=='nacional') style="display:block" @else style="display:none" @endif>
                <label for="">Departamento</label>
                <select class="departamento form-control" name="dpto" id="">
                                <option value="">Seleccione</option>
                                @foreach($dptos as $k=>$dpto)
                                <option @if(isset($vigilante)&& $vigilante->persona->obtenerDepartamento()->dpt_codigo==$dpto->dpt_codigo) selected @endif  value="{{$dpto->dpt_codigo}}">{{$dpto->dpt_nombre}}</option>
                                @endforeach
                             </select>
            </div>
            <div class="col-md-2 form-group
                            div-municipios" @if(auth()->user()->nivel=='nacional' ||auth()->user()->nivel=='depto') style="display:block" @else style="display:none"
                @endif >
                <label for="">Municipio</label>
                <select class="municipio form-control" name="municipio" id="">
                                <option dptoid="0" value="">Seleccione</option>
                                @foreach($municipios as $k=>$muni)
                                <option 
                                @if(isset($vigilante)&& $vigilante->persona->obtenerMunicipio()->mnc_codigo==$muni->mnc_codigo) selected @endif
                                dptoid={{$muni->provincia->departamento->dpt_codigo}} value="{{$muni->mnc_codigo}}">{{$muni->mnc_nombre}}</option>
                                @endforeach
                             </select>

            </div>
            <div class="col-md-4 form-group div-estable">
                <label for="">Establecimientos de salud <span class="text-danger" style="font-size:10px">(Obligatorio)</span></label> @if(\Auth::user()->nivel=='establ')
                <input type="text" readonly class="form-control" value="{{$establecimientos[0]->nomestabl}}">
                <input type="hidden" class="form-control" name="establecimiento" value="{{$establecimientos[0]->codestabl}}">               
                 @else
                <select class="establecimiento form-control" data-error="Seleccione un Establecimiento" required name="establecimiento" id="">
                                    <option value="">Seleccione</option>
                                    @foreach($establecimientos as $k=>$v)
                                   <option @if(isset($vigilante)&& $vigilante->persona->codEstablecimiento==$v->codestabl) selected @endif value="{{$v->codestabl}}">{{$v->nomestabl}}</option>
                                    @endforeach
        
                             </select>

                <div class="help-block with-errors"></div>
                @endif

            </div>

            {{-- <div class="col-md-3 form-group div-estable">
                <label for="">Comunidad</label>

                <select class="comunidades form-control" data-error="Seleccione una Comunidad" name="comunidad" id="">
                                    <option value="">Seleccione</option>
                                    @foreach($comunidades as $k=>$v)
                                   <option @if(isset($vigilante)&& $vigilante->persona->IdComunidad==$v->IdComunidad) selected @endif  value="{{$v->IdComunidad}}">{{$v->nomComunidad}}</option>
                                    @endforeach
        
                             </select>

            </div> --}}
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-10 col-sm-offset-1">
            <button type="submit" class="btn  btn-block btn-primary"><i class="fa fa-save"></i> Guardar Vigilante Comunitario</button>
        <a href="{{url('vigilantes')}}" class="btn  btn-block btn-default"><i class="fa fa-times"></i> Cancelar</a>
        </div>
    </div>
</fieldset>

            

           

         
        </form>
    </div>
</div>




@section('scripts')
<script src="{{asset('js/forms.js')}}"></script>
<script>
    $(document).ready(function () {

      @if(isset($vigilante) ) 
     
    @if($vigilante->persona->verificado)
   $('.required').prop('readonly',true)
   @else
   $('.required').prop('readonly',false)
   @endif
   @else
   $('.required').prop('readonly',true)
        @endif


          
          $('.form').validator()



      $('.departamento').change(function(){
    //   alert('asd')
      var _dpto= $(this).val();
     $('.municipio').val('');
      $(".municipio option").each(function(i,o) {
        //   console.log(o)
   if ($(o).attr("dptoid") ===_dpto) {
      $(o).show()
   }
   else{
       $(o).hide()
   }
       });
            
  })


    function llenarFormulario(datos){
       
      
        $('.search').prop('readonly',true)
        $('.nombres').val(datos.nombres)
        $('.adscrito_id').val(datos.adscrito_id)
        $('.primer_apellido').val(datos.primerapellido)
        $('.segundo_apellido').val(datos.segundoapellido)
        $('.complemento').val(datos.complemento)
        $('.direccion').val(datos.direccion)
        $('.num_celular').prop('disabled',false)
        $('.num_celular').val(datos.tel_referencia)
        if(datos.email)
        $('.email').val(datos.email)
        if(datos.id)
        $('.persona_id').val(datos.id)
        if(datos.sexo=='Masculino')
        $('.masculino').prop('checked',true)
        if(datos.sexo=='Femenino')
        $('.femenino').prop('checked',true)

        $('.departamento').val(datos.departamento_id).trigger('change')
        $('.municipio').val(datos.municipio_id).trigger('change')
        obtenerMunicipios(datos.municipio_id,datos.establecimiento_id)
        
     
  }
  function limpiarRegistrar(){
      $('.required').prop('readonly',false)
      $('.nombres').focus();
  }
  function obtenerMunicipios(mnc_codigo,establecimiento_id=0){
      $.ajax({
          url:"{{url('ajax/establecimientos-municipio')}}",
          type:'GET',
          dataType:'json',
          data:{idmuni:mnc_codigo},
          beforeSend:function(){
              $('.establecimiento').attr('disabled',true)
              $('.comunidades').attr('disabled',true)
          },
          complete:function(){
               $('.establecimiento').attr('disabled',false)
               $('.comunidades').attr('disabled',false)
          },
          success:function(res){
              $('.establecimiento').find('option').not(':first').remove();
              
              $('.establecimiento').append(res.options);
              if(parseInt(establecimiento_id)>0){
                $('.establecimiento').val(establecimiento_id).trigger('change')
              }

             
              
            },
        }) 
      
  }

  $('.municipio').change(function(){
         var id=$(this).val();
         var option="";
    obtenerMunicipios(id)

  })

  $('.btn-buscar').click(function(){
      if($('.numero_carnet').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese un numero de carnet',
                    });
                    return false;
      } 
      if($('.fecha_nacimiento').val()==''){
        $.alert({
                       type: 'orange',
                        typeAnimated: true,
                        title: 'Dato invalido',
                        content: 'Ingrese una Fecha de nacimiento',
                    });
                    return false;
      }
      
      let numcarnet=$('.numero_carnet').val()
      let fechanacimiento=$('.fecha_nacimiento').val()
      let complemento=$('.complemento').val()
      $.ajax({
          url:"{{url('vigilantes/buscar-persona-api')}}",
          type:'GET',
          dataType:'json',
          data:{
              num_carnet:numcarnet,
              fecha_nacimiento:fechanacimiento,
              complemento:complemento,
          },
          beforeSend:function(){
            blocUI('Buscando...')
          },
          complete:function(){
                    $.unblockUI();

                },
          success:function(res){
              console.log(res)
              if(res.success){
                  console.log('encontrado',res.data.code);

                  if(res.data.success && res.data.code==200){
                    llenarFormulario(res.data.data)
                    $('.fieldset').removeClass('hide')

                  }
                  else{
                    $.confirm({
            title: res.data.mensaje,
            content: 'No se encontraron datos de la persona.Desea registrar datos de la persona?',
           
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
                  }
              }
              else{
                $.confirm({
            title: 'No se encontraron datos de la persona',
            content: 'Desea registrar datos de la persona?',
            autoClose: 'Cancelar|8000',
            containerFluid: true,
            theme: 'material',
            buttons: {

                Cancelar: function() {},
                Registrar: {
                    text: '<i class="fa fa-check"></i> Si registrar',
                    btnClass: 'btn-success',
                    action: function() {
                        limpiarRegistrar()
                        $('.fieldset').removeClass('hide')
                    }
                },
            }
        });
              }

          },
          error:function(){}


      })

  })

            

           
        });

</script>


@stop 
@stop