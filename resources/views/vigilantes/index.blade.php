@extends('layouts.app',[ 'active_tab_vigilantes'=>'active', 'active_listar_vigilantes'=>'active', 'title'=>'Vigilantes Comunitarios' ]) {{-- 
@section('titulo')Municipios

@stop --}} 
@section('breadcome')

<ul class="breadcome-menu">
    <li><a href="#">Vigilantes Comunitarios</a> <span class="bread-slash">/</span>
    </li>

    <li><span class="bread-blod">Lista de Vigilantes Comunitarios</span>
    </li>
</ul>

@stop 
@section('content')




<div class="table-responsive">
        @permission('crear_vigilantes')
    <a href="{{url('vigilantes/crear')}}" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i> REGISTRAR NUEVO VIGILANTE COMUNITARIO</a>
    @endpermission
    <hr> {{--
    @include('includes.filter-niveles') --}}

    <table style="font-size: 10px" class="table datatable">
        <thead>
            <tr>
                <th>ACCIONES</th>
                <th>CODIGO</th>
                <th>NOMBRES</th>
                <th>PRIMER APELLIDO</th>
                <th>SEGUNDO APELLIDO</th>
                <th>CARNET</th>
                <th>FECHA NACIMIENTO</th>
                <th>EDAD</th></th>
                
                <th>SEXO</th>
                <th>CELULAR</th>
                <th>DIRECCION</th>
                <th>ESTABLECIMIENTO</th>
                <th>MUNICIPIO</th>
                <th>RED</th>
                <th>DEPARTAMENTO</th>
            </tr>
        </thead>

    </table>
</div>

@section('scripts')
<script src="{{asset('js/jquery.dataTables.yadcf.js')}}"></script> 

<script>
    $(document).ready(function () {

        function cambiarEstado(id){
                 $.ajax({
                            type: "GET",
                            url: "{{url('vigilantes/cambiar-estado')}}",
                            data: {id:id},
                            dataType: "json",
                            success: function (response) {

                                if (response.status){
                                    Table.draw();
                                }
                                
                            },
                            error:function(response){
                                $.alert('ocurrio un error');
                            }
                        });
            }
            
           $(document).on('click','.btn-baja',function(){
               var id=$(this).attr('id')
                $.confirm({
            title: 'Dar de baja!',
            theme: 'supervan',
            content: 'Al dar de baja a un vigilante este ya no podra enviar informacion al sistema!',
            buttons: {
                confirm:{
                    text:'Dar de baja',
                        action:function () {
                        // $.alert('Confirmed!');

                       cambiarEstado(id)
                    }
                }, 
                cancel: function () {
                    return;
                    // $.alert('Canceled!');
                }
                // somethingElse: {
                //     text: 'Something else',
                //     btnClass: 'btn-blue',
                //     keys: ['enter', 'shift'],
                //     action: function(){
                //         $.alert('Something else?');
                //     }
                // }
            }
        });

            });

              $(document).on('click','.btn-alta',function(){
               var id=$(this).attr('id')
                $.confirm({
            title: 'Dar de alta!',
            theme: 'supervan',
            content: 'Al dar de alta, al vigilante  podra enviar informacion  al sistema ',
            buttons: {
                confirm:{
                    text:'Dar de Alta',
                        action:function () {
                        // $.alert('Confirmed!');

                       cambiarEstado(id)
                    }
                }, 
                cancel: function () {
                    return;
                }
         
            }
        });

            });

   

      $('.departamento').change(function(){
    //   alert('asd')
      var _dpto= $(this).val();
      $(".municipio option").each(function(i,o) {
        //   console.log(o)
   if ($(o).attr("dptoid") ===_dpto) {
      $(o).show()
   }
   else{
       $(o).hide()
   }
       });
            
  })

              $(document).on('click','.btn-borrar',function(_evt){
                  _evt.preventDefault();
                  let _href=$(this).attr('href')

                     $.confirm({
                        title: 'Confirmacion',
                        icon:' fa fa-trash',
                        content: 'Desea eliminar este vigilante del sistema?',
                        buttons: {
                            confirmar: function () {
                                window.location=_href
                            },
                            cancelar: function () {
                                this.close();
                            }
                           
                        }
                    });
              })


            var table=$('.datatable').DataTable({
                dom: "<'row '<' ml-2 pl-2 pt-2 mb-1 col-md-2 'l><'px-0   col-md-4'i><' mx-0 col-md-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row txt-small'<'col-sm-5'i><'col-sm-7'p>>",
        ajax: {
            url: "{{url('vigilantes/listar')}}",
            type: 'GET'
              },
              language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Registros",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
        
        "lengthChange": true,
         "lengthMenu": [ [50, 100, 200, 500,-1], [50, 100, 200, 500, "All"] ],
        serverSide: true,
        processing: true,
        columns: [
            {data:'acciones'},
            {data: 'codigo'},
            {data: 'persona.nombres', name:'persona.nombres'},
            {data: 'persona.primerApellido', name:'persona.primerApellido',"defaultContent": ''},
            {data: 'persona.segundoApellido', name:'persona.segundoApellido',"defaultContent": ''},
            {data: 'persona.numeroCarnet', name:'persona.numeroCarnet'},
            {data: 'fechanacimiento',name:'persona.fechaNacimiento'},
            {data: 'edad'},
            {data: 'genero'},
            {data:'persona.celular',name:'persona.celular'},
            {data:'persona.direccion',name:'persona.direccion'},
            {data:'establecimiento'},
            {data:'municipio'},
            {data:'red'},
            {data:'departamento'},
          
           
        ]
                
            })
            yadcf.init(table, [
       
       {
       column_number: 1,
       filter_type: "text",
       filter_default_label: ""

   },
 
   {
       column_number: 2,
       filter_type: "text",
       filter_default_label: ""

   },
   {
       column_number: 3,
       filter_type: "text",
       filter_default_label: ""

   },
    {
       column_number: 4,
       filter_type: "text",
       filter_default_label: ""

   },
 
   {
       column_number: 5,
 
     filter_type: "text",
     filter_default_label: ""
   },
   {
       column_number: 7,
 
     filter_type: "text",
     filter_default_label: ""
   },
   {
      column_number : 6,
       filter_type: "date",
        date_format: "dd/mm/yyyy",
   filter_plugin_options: {
           changeMonth: true,
           changeYear: true
        },
         filter_default_label: ['dia/mes/anio']

     
   },
   {
       column_number: 9,
 
     filter_type: "text",
     filter_default_label: ""
   },
   {
       column_number: 10,
 
     filter_type: "text",
     filter_default_label: ""
   },
    {
      column_number: 11,
       filter_type: "text",
       filter_default_label: ""
   },
    {
       column_number: 12,
       filter_type: "text",
       filter_default_label: ""
   },
   {
       column_number: 13,
       filter_type: "text",
       filter_default_label: ""
   }

  
  
],
{
   cumulative_filtering: true
}
);
        });
</script>

@stop 
@stop