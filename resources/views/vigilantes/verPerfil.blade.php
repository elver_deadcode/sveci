@extends('layouts.app',[ 'active_tab_vigilantes'=>'active', 'active_tab_vigilantes'=>'active', 'title'=>'Vigilantes - Perfil' ]) {{--
@section('titulo')Municipios

@stop --}}
@section('breadcome')

    <ul class="breadcome-menu">
        <li><a href="{{ url('vigilantes', []) }}">Vigilantes Comunitarios</a> <span class="bread-slash">/</span>
        </li>

        <li><span class="bread-blod">Perfil del Vigilante Comunitario</span>
        </li>
    </ul>

@stop
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>PERFIL DE VIGILANTE COMUNITARIO</h3>
            <a class="btn btn-primary" href="{{url('vigilantes')}}"><i class=" fa fa-list"></i> REGRESAR A LA LISTA DE
                VIGILANTES</a>
        </div>
        <div class="panel-body">
            <div class="col-md-6">


                <table class="  table">
                    <thead>
                    <tr>
                        <th>NOMBRES :</th>
                        <th>{{$vigilante->persona->nombre_completo}}</th>
                    </tr>
                    <tr>
                        <th>SEXO :</th>
                        <th>{{$vigilante->persona->genero}}</th>
                    </tr>
                    <tr class="bg-primary">
                        <th>CODIGO :</th>
                        <th><h3 class="">{{$vigilante->codigo}}</h3></th>
                    </tr>
                    <tr>
                        <th>NUMERO DE CARNET :</th>
                        <th>{{$vigilante->persona->numeroCarnet}}</th>
                    </tr>
                    <tr>
                        <th>FECHA DE NACIMIENTO :</th>
                        <th>{{$vigilante->persona->fechaNacimiento->format('d/m/Y')}} ({{ $vigilante->edad}} AÑOS)</th>
                    </tr>
                    <tr>
                        <th>ACTIVADO :</th>
                        <th>
                            @if($vigilante->activado)
                                <span class="badge badge-success">TELEFONO ACTIVADO @isset($vigilante->fechaActivacion){{$vigilante->fechaActivacion}}@endisset</span>
                            @else
                                <span class="badge badge-default">NO ACTIVO EL TELEFONO</span>

                            @endif

                        </th>
                    </tr>
                    <tr>
                        <th>DIRECCION :</th>
                        <th>{{$vigilante->persona->direccion}}</th>
                    </tr>
                    <tr>
                        <th>NUMERO CELULAR :</th>
                        <th>{{$vigilante->persona->celular}}</th>
                    </tr>
                    <tr>
                        <th>CORREO ELECTRONICO :</th>
                        <th>{{$vigilante->persona->email}}</th>
                    </tr>

                    </thead>
                </table>

            </div>
        </div>
    </div>

@section('scripts')
    <script>
        $(document).ready(function () {


        });


    </script>

@stop
@stop
