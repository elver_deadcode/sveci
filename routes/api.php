<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('1.0/authenticate',  '\App\Http\Controllers\Api\AuthenticateController@authenticate')->name('login');

Route::group(
    [
        'middleware' => ['auth:sanctum'],
        'prefix' => '1.0',
        'namespace' => '\App\Http\Controllers\Api',
        'as' => 'api.',
    ],
    function () {

        Route::post('/user/credentials',  'UserController@userCredentials')->name('credentials');
        Route::get('/personas/medicos',  'PersonaApiController@obtenerMedicosPorNivel')->name('medicos.listar');
        Route::get('/personas/{persona}/medico',  'PersonaApiController@showMedico')->name('medicos.obtener');

        Route::get('/personas/mujeres',  'PersonaApiController@obtenerMujeresPorNivel')->name('mujeres.listar');
        Route::get('/personas/{persona}/mujer',  'PersonaApiController@obtenerMujer')->name('mujer.medico');

        Route::get('/establecimientos/listar',  'EstablecimientoApiController@listar')->name('establecimiento.listar');
        Route::get('/establecimientos/{establecimiento}',  'EstablecimientoApiController@obtenerEstablecimiento')->name('establecimiento.obtener');

        Route::get('/municipios/listar',  'MunicipioApicontroller@listar')->name('municipio.listar');
        Route::get('/municipios/{municipio}',  'MunicipioApicontroller@obtenerMunicipio')->name('municipio.obtener');

        Route::get('/departamentos/listar',  'DepartamentoApiController@listar')->name('departamento.listar');
        Route::get('/departamentos/{departamento}',  'DepartamentoApiController@obtenerDepartamento')->name('departamento.obtener');

        Route::post('/personas/buscar', 'PersonaApiController@buscarPersona');
        //teleconsulas
        Route::post('/teleconsulta',  'TeleconsultaController@guardar')->name('teleconsulta.store');
        Route::patch('/teleconsulta/{teleconsulta}',  'TeleconsultaController@actualizarEstado')->name('teleconsulta.store');


        Route::fallback(function () {
            return response()->json(['error' => 'Not Found'], 404);
        })->name('fallback');
    });
    Route::group(
        [
            'middleware' => [],
            'prefix' => '1.0/lugares',
            'namespace' => '\App\Http\Controllers\Api',
            'as' => 'api.',
        ],
        function () {
            Route::get('/establecimientos/listar',  'EstablecimientoApiController@listar')->name('establecimiento.listar');
            Route::get('/departamentos/listar',  'DepartamentoApiController@listar')->name('departamento.listar');

        });
Route::post('notificar-embarazo', "ApiController@notificarEmbarazo");
Route::post('notificar-muerte-mujer', "ApiController@notificarMuerteMujer");
Route::post('notificar-muerte-bebe', "ApiController@notificarMuerteBebe");
Route::post('notificar-parto', "ApiController@notificarParto");
Route::post('activar', "ApiController@activarTelefono");
Route::post('registrar-mujer', "ApiController@registrarMujer");


Route::post('sync-data', "ApiController@Sincronizar");
Route::post('recuperar-data', "ApiController@recuperarRegistrosPorVigilante");
 Route::get('prueba',"ApiController@pruebas");
Route::get('prueba', function () {
    echo "funcionando";
});
Route::get('test-estado/{id}/{tipo}', "ApiController@testconsultar");
Route::fallback(function () {
    return response()->json(['error' => 'Not Found'], 404);
})->name('fallback');

Route::get('/personas/buscar', '\App\Http\Controllers\Api\PersonaApiController@buscarPersona');

