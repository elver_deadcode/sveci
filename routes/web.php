<?php

use Illuminate\Support\Facades\Route;
//use Maatwebsite\Excel\Facades\Excel;
use App\Mujer;
use App\Persona;
use App\Vigilante;
use App\User;
\Illuminate\Support\Facades\Auth::routes();
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing.index');
});
Route::get('/personas/buscar', '\App\Http\Controllers\Api\PersonaApiController@buscarPersona');

Route::prefix('ajax')->middleware('auth', 'estado')->group(function () {
    Route::get('/establecimientos-municipio', 'AjaxController@establecimientosMunicipio');
    Route::get('/establecimientos-red', 'AjaxController@establecimientosRed');
    Route::get('buscar-mujer', 'AjaxController@buscarMujer');
    Route::get('buscar-vigilante', 'AjaxController@buscarVigilante');
    Route::get('anular-notificacion', 'AjaxController@anularNotificacion');
    Route::post('guardar-datosmujer', 'AjaxController@guardarDatosMujer');

    Route::get('buscar-persona-api', 'ApiClientController@buscarPersonaApi');
});
Route::get('seed', function () {
    App\Persona::create([
        'nombres' => 'elver',
        'primerApellido' => 'vasquez',
        'segundoApellido' => 'segales',
        'numeroCarnet' => '5066349',
        'complemento' => '',
        'nacionalidad' => 'BOLIVIANA',
        'direccion' => '',
        'fechaNacimiento' => '1994-07-21',
        'sexo' => 'M',
        'adscrito_id' => 0,
        'verificado' => true,
        'codEstablecimiento' => 0,
        'IdComunidad' => 0,
        'mnc_codigo' => 0,
        'are_codigo' => 0,
        'dpt_codigo' => 0,
        'email' => 'admin@gmail.com',
        'celular' => '72476178'
    ]);
});

Route::prefix('reportes')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'ReporteController@index');
    Route::get('mujeres-embarazadas', 'ReporteController@mujeresEmbarazadas');
    Route::get('mujeres-parto', 'ReporteController@mujeresParto');


    Route::get('listavigilantes-comunitarios', 'ReporteController@listaVigilantesComunitarios');
    Route::get('lista-mujeres', 'ReporteController@listaMujeres');
    Route::get('notificacion-embarazo', 'ReporteController@notificacionesEmbarazo');
    Route::get('notificacion-muertemujer', 'ReporteController@notificacionesMuerteMujer');
    Route::get('notificacion-muertebebe', 'ReporteController@notificacionesMuerteBebe');
    Route::get('notificacion-parto', 'ReporteController@notificacionesParto');
    Route::get('/mujeres-parto', 'ReporteController@mujeresParto');
    Route::get('/muertes-vigilante', 'ReporteController@muertesVigilante');
    Route::get('/muertes-autopsia', 'ReporteController@muertesAutopsia');
    Route::get('/consolidado-vigilantes', 'ReporteController@consolidadoVigilante');
    Route::get('/consolidado-teleconsultas', 'ReporteController@consolidadoTeleconsulta');
});

Route::get('copiarmujeres', function () {
    $mujeres = Mujer::Where('numeroCarnet', '<>', '')
        ->get();
    try {
        \DB::beginTransaction();
        foreach ($mujeres as $m) {
            $p = new Persona();
            $p->nombres = $m->nombres;
            $p->primerApellido = $m->primerApellido;
            $p->segundoApellido = $m->segundoApellido;
            $p->numeroCarnet = $m->numeroCarnet;
            $p->complemento = '';
            $p->nacionalidad = 'BOLIVIANA';
            $p->direccion = $m->direccion;
            $p->fechaNacimiento = $m->fechaNacimiento;
            $p->sexo = 'm';
            $p->esverificadosegip = 0;
            $p->save();
            $m->IdPersona = $p->IdPersona;
            $m->save();
        }
        \DB::commit();
    } catch (\Exception $e) {
        \DB::rollBack();
    }
});

Route::get('copiarvigilantes', function () {
    $vigilantes = Vigilante::Where('numeroCarnet', '<>', '')
        ->get();
    try {
        \DB::beginTransaction();
        foreach ($vigilantes as $m) {
            $p = new Persona();
            $p->nombres = $m->nombres;
            $p->primerApellido = $m->primerApellido;
            $p->segundoApellido = $m->segundoApellido;
            $p->numeroCarnet = $m->numeroCarnet;
            $p->complemento = '';
            $p->nacionalidad = 'BOLIVIANA';
            $p->direccion = $m->direccion;
            $p->fechaNacimiento = $m->fechaNacimiento;
            $p->sexo = $m->sexo;
            $p->esverificadosegip = 0;
            $p->save();
            $m->IdPersona = $p->IdPersona;
            $m->save();
        }
        \DB::commit();
    } catch (\Exception $e) {
        \DB::rollBack();
        return dd($e);
    }
});
Route::get('copiarusers', function () {
    $users = User::Where('numero_carnet', '<>', '')
        ->get();
    try {
        \DB::beginTransaction();
        foreach ($users as $m) {
            $p = new Persona();
            $p->nombres = $m->nombres;
            $p->primerApellido = $m->primer_apellido;
            $p->segundoApellido = $m->segundo_apellido;
            $p->numeroCarnet = $m->numero_carnet;
            $p->complemento = '';
            $p->nacionalidad = 'BOLIVIANA';
            $p->direccion = '';
            $p->fechaNacimiento = '2000-10-10';
            $p->sexo = 'h';
            $p->esverificadosegip = 0;
            $p->save();
            $m->IdPersona = $p->IdPersona;
            $m->save();
        }
        \DB::commit();
    } catch (\Exception $e) {
        \DB::rollBack();
        return dd($e);
    }
});

Route::prefix('registros')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('municipios',  'MunicipioController@index');
    Route::get('municipios/listar',  'MunicipioController@listar');

    Route::get('redes', 'RedController@index');
    Route::get('redes/listar', 'RedController@listar');

    Route::get('establecimientos', 'EstablecimientoController@index');
    Route::get('establecimientos/listar', 'EstablecimientoController@listar');

    Route::get('/importar-establecimientos', function () {


        \Excel::load('establecimientos.xlsx', function ($reader) {

            try {

                foreach ($reader->get() as $k => $filas) {
                    foreach ($filas as $k2 => $fila) {

                        $e = new \App\Establecimiento();
                        $e->codestabl = trim($fila->codestabl);
                        $e->codinstit = trim($fila->codinstit);
                        $e->codclsest = trim($fila->codclsest);
                        $e->codmunicip = trim($fila->codmunicip);
                        $e->idgestion = trim($fila->idgestion);
                        $e->codarea = trim($fila->codarea);
                        $e->nomestabl = trim($fila->nomestabl);
                        $e->codurbrur = trim($fila->codurbrur);
                        $e->nomrespon = trim($fila->nomrespon);
                        $e->seguro = trim($fila->seguro);
                        $e->num_camas = trim($fila->num_camas);
                        $e->bajalogica = trim($fila->bajalogica);

                        if ($fila->poblacion = "") {
                            $e->poblacion = 0;
                        } else {

                            $e->poblacion = trim($fila->poblacion);
                        }
                        $e->save();
                    }
                }

                // \DB::commit();

                return dd('bien');
            } catch (Exception $e) {

                // \DB::rollBack();

                return dd($e);
            }
        });
    });

    Route::get('/importar-areas', function () {


        \Excel::load('ctg_areas.xls', function ($reader) {

            try {

                foreach ($reader->get() as $k => $filas) {
                    // foreach ($filas as $k2 => $fila) {

                    $e = new \App\Area();
                    // return dd($filas['are_codigo']);
                    $e->are_codigo = trim($filas['are_codigo']);
                    $e->dpt_codigo = trim($filas['dpt_codigo']);
                    $e->are_nombre = trim($filas['are_nombre']);
                    $e->marca_cpd = trim($filas['marca_cpd']);
                    $e->are_direccion = trim($filas['are_direccion']);
                    $e->are_telefono = trim($filas['are_telefono']);
                    $e->are_fax = trim($filas['are_fax']);
                    $e->are_casilla = trim($filas['are_casilla']);
                    $e->are_director = trim($filas['are_director']);
                    $e->are_planificacion = trim($filas['are_planificacion']);
                    $e->are_admcpd = trim($filas['are_admcpd']);

                    $e->save();


                    // }
                }

                // \DB::commit();

                return dd('bien');
            } catch (Exception $e) {

                // \DB::rollBack();

                return dd($e);
            }
        });
    });

    Route::get('/importar-muni', function () {


        \Excel::load('muni.xlsx', function ($reader) {

            try {

                foreach ($reader->get() as $k => $fila) {
                    // foreach ($filas as $k2 => $fila) {

                    DB::connection("snis")->table("ctg_municipios")->insert([


                        "mnc_codigo" => trim($fila["mnc_codigo"]),
                        "prv_codigo" => trim($fila["prv_codigo"]),
                        "mnc_nombre" => trim($fila["mnc_nombre"]),
                        "mnc_poblacion" => trim($fila["mnc_poblacion"]),
                        "anio_creacion" => trim($fila["anio_creacion"]),

                    ]);


                    // // }
                }

                // \DB::commit();

                return dd('bien');
            } catch (Exception $e) {
                // \DB::rollBack();
                return dd($e);
            }
        });
    });

    Route::get('/importar-provi', function () {


        \Excel::load('ctg_provincia.xlsx', function ($reader) {

            try {

                foreach ($reader->get() as $k => $fila) {
                    // foreach ($filas as $k2 => $fila) {

                    // return dd($fila);

                    DB::connection("snis")->table("ctg_provincia")->insert([


                        "coddepto" => trim($fila["coddepto"]),
                        "codprovi" => trim($fila["codprovi"]),
                        "nomprovi" => trim($fila["nomprovi"])


                    ]);


                    // // }
                }

                // \DB::commit();

                return dd('bien');
            } catch (Exception $e) {
                // \DB::rollBack();
                return dd($e);
            }
        });
    });
});

Route::get('importar-comunidades', function () {
    \Excel::load('comunidades.xlsx', function ($reader) {

        try {
            \DB::beginTransaction();
            foreach ($reader->get() as $k => $fila) {
                dd($fila);
                if (!is_null($fila->codlocalidad) && $fila->codlocalidad != '') {
                    $codloc = (int) $fila->codlocalidad;
                    $codmuni = (int) $fila->codmunicip;


                    \App\Comunidad::create([

                        "codLocalidad" => $codloc,
                        "codMunicipio" => $codmuni,
                        "nomLocalidad" => $fila->nomlocalidad,
                        "nomComunidad" => $fila->nombre,

                    ]);
                } else {
                    return dd($k);
                }
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return dd($e);
        }
    });
    return redirect('/');
});
//  VIEOTUTORIALES
Route::prefix('manual')->middleware('auth', 'estado','apiroute')->group(function () {

    Route::get('/', 'ManualController@index');

    Route::get('/ingreso', function () {
        return view('manual.generales.ingresosistema');
    });

    Route::get('/menu-notificaciones', function () {
        return view('manual.generales.notificaciones');
    });

    Route::get('/menu-salir', function () {
        return view('manual.generales.salir');
    });

    Route::get('/roles-listar', function () {
        return view('manual.roles.listar');
    });
    Route::get('/roles-crear', function () {
        return view('manual.roles.crear');
    });
    Route::get('roles-editar', function () {

        return view('manual.roles.editar');
    });
    Route::get('/roles-borrar', function () {
        return view('manual.roles.borrar');
    });
});
// NOTIFICACION ROLES DE USUARIO

Route::prefix('roles')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'RolesController@index');
    Route::get('/listar', 'RolesController@listar');
    Route::get('/nuevo', 'RolesController@nuevo');
    Route::post('/guardar', 'RolesController@guardar');
    Route::get('/{role}/editar', 'RolesController@editar');
    Route::get('/{role}/borrar', 'RolesController@borrar');
});

# VIGILANTES

Route::prefix('vigilantes')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'VigilanteController@index');
    Route::get('/listar', 'VigilanteController@listarVigilantes');
    Route::get('/crear', 'VigilanteController@crearVigilante');
    Route::post('/guardar', 'VigilanteController@guardar');
    Route::get('/cambiar-estado', 'VigilanteController@cambiarEstado');
    Route::get('/{vigilante}/ver-perfil', 'VigilanteController@verPerfil');
    Route::get('/{vigilante}/editar', 'VigilanteController@editarVigilante');
    Route::get('/{vigilante}/borrar', 'VigilanteController@borrar');
//        Route::get('/buscar-persona-api', 'VigilanteController@buscarPersona');
    Route::get('/buscar-persona-api', 'ApiClientController@buscarPersonaApi');
});


// NOTIFICACION NOTIFICACIONES
Route::prefix('notificaciones')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'NotificacionController@index');
    Route::get('/listar-notificaciones', 'NotificacionController@listarNotificaciones');
    Route::get('/contar-notificaciones', 'NotificacionController@contarNotificaciones');
    Route::get('/contar-todas-notificaciones', 'NotificacionController@contarTodasNotificaciones');
    Route::get('/marcar-visto', 'NotificacionController@marcarVisto');
    Route::get('/mostrar-cabecera', 'NotificacionController@mostrarTodasNotificacionesEnCabecera');
    Route::get('/{notificacion}/ver', 'NotificacionController@verNotificacion');
    Route::get('/{notificacion}/ver-vista', 'NotificacionController@verNotificacionVista');
    Route::post('/guardar', 'NotificacionController@guardar');
});
// NOTIFICACION EMBARAZO
Route::prefix('notificacion-embarazo')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'NotificacionEmbarazoController@index');
    Route::get('/listar', 'NotificacionEmbarazoController@listar');
    Route::post('/guardar-detalle', 'NotificacionEmbarazoController@guardarDetalle');
});
// NOTIFICACION MUERTE MUJER
Route::prefix('notificacion-muertemujer')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::post('/guardar-detalle', 'NotificacionMuerteMujerController@guardarDetalle');
});
// NOTIFICACION MUERTE BEBE
Route::prefix('notificacion-muertebebe')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::post('/guardar-detalle', 'NotificacionMuerteBebeController@guardarDetalle');
});

// NOTIFICACION PARTO
Route::prefix('notificacion-parto')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::post('/guardar-detalle', 'NotificacionPartoController@guardarDetalle');
});

// MUJERES

Route::prefix('mujeres')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'MujerController@index');
    Route::get('/listar', 'MujerController@listarMujeres');
    Route::get('/create', 'MujerController@create')->middleware('permission:crear_mujeres');
    Route::post('/guardar', 'MujerController@guardar');
    Route::get('/{mujer}/edit', 'MujerController@editar')->middleware('permission:editar_mujeres');
    Route::get('/{mujer}/delete', 'MujerController@borrar');
});

// USUARIOS
// Route::get('usuarios/nuevo', 'UserController@nuevo')->middleware('permission:crear_usuarios');

Route::prefix('usuarios')->middleware('auth', 'estado','apiroute')->group(function () {
    Route::get('/', 'UserController@index');
    Route::get('/listar', 'UserController@listar');
    Route::get('/nuevo', 'UserController@nuevo')->middleware('permission:crear_usuarios');
    Route::get('/nuevoapi', 'UserController@nuevoApi')->middleware('permission:crear_usuarios');
    Route::post('/guardar', 'UserController@guardar');
    Route::post('/guardarapi', 'UserController@guardarApi');
    Route::post('/guardar-perfil', 'UserController@guardarPerfil');
    Route::get('/{user}/editar', 'UserController@editar')->middleware('permission:editar_usuarios');
    Route::get('/{user}/editarapi', 'UserController@editarApi')->middleware('permission:editar_usuarios');
    Route::get('/{user}/editarperfil', 'UserController@editarPerfil');
    Route::get('/{user}/crearvigilante', 'UserController@crearVigilante');
    Route::get('/cambiar-estado', 'UserController@darBaja');
    Route::get('/buscar-persona-api', 'VigilanteController@buscarPersona');
    Route::get('/buscar-persona-api', 'VigilanteController@buscarPersona');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth','apiroute');
Route::get('/logout', function(){
    \Auth::logout();
    return redirect('/');

});
Route::get('error/{tipo}', 'CommonController@error');

Route::view('/login/recuperar', 'auth.reset');
Route::post('/login/generar-codigo', 'Auth\LoginController@generarCodigo');
Route::post('/login/validar-codigo', 'Auth\LoginController@validarCodigoRecuperacion');
Route::post('/login/guardar-credenciales', 'Auth\LoginController@guardarCredenciales');
