const mix = require('laravel-mix');

mix.browserSync({
    proxy: "sveci.test"
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles(
    [
        "resources/assets/template/css/bootstrap.css",
        "resources/assets/template/css/font-awesome.min.css",
        "resources/assets/template/css/adminpro-custon-icon.css",
        "resources/assets/template/css/meanmenu.min.css",
        "resources/assets/template/css/jquery.mCustomScrollbar.min.css",
        "resources/assets/template/css/animate.css",
        "resources/assets/template/css/normalize.css",
        "resources/assets/template/css/form.css",
        "resources/assets/template/css/switcher/color-switcher.css",
        "resources/assets/template/css/style.css",
        "resources/assets/template/css/responsive.css",
        "resources/assets/template/DataTables-1.10.18/css/jquery.dataTables.css",
        "resources/assets/template/DataTables-1.10.18/css/dataTables.bootstrap4.css",
        "resources/assets/template/css/Lobibox.min.css",
        "resources/assets/plugins/jquery-confirm/jquery-confirm.min.css",
        "node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"
    ],
    "public/css/app.css"
);

mix.styles(
    [
        "resources/assets/login/css/util.css",
        "resources/assets/login/css/main.css"
    ],
    "public/css/login.css"
);

mix.styles(["resources/assets/sass/error.scss"], "public/css/error.css");
mix.styles(["node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"], "public/css/datepicker.css");

mix.copy("resources/assets/template/fonts", "public/fonts");
mix.copy("resources/assets/plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css", "public/css");
mix.copy("resources/assets/plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css", "public/css");
mix.copy("resources/assets/plugins/yadcf-0.9.3/jquery.dataTables.yadcf.css", "public/css");
mix.copy("resources/assets/plugins/select2/select2.min.css", "public/css");

// mix.js('resources/assets/js/app.js', 'public/js/main.js')
mix.scripts(
    [
        // 'resources/assets/js/app.js',
        "resources/assets/template/js/vendor/jquery-1.11.3.min.js",
        "resources/assets/template/js/bootstrap.min.js",
        // "resources/assets/template/DataTables-1.10.18/js/jquery.dataTables.js",
        // "resources/assets/template/DataTables-1.10.18/js/dataTables.bootstrap4.js",
        "resources/assets/template/js/jquery.meanmenu.js",
        "resources/assets/template/js/jquery.mCustomScrollbar.concat.min.js",
        "resources/assets/template/js/jquery.sticky.js",
        "resources/assets/template/js/jquery.scrollUp.min.js",
        "resources/assets/template/js/jquery.validate.min.js",
        "resources/assets/template/js/form-active.js",
        "resources/assets/template/js/switcher/styleswitch.js",
        "resources/assets/template/js/switcher/switch-active.js",
        "resources/assets/template/js/switcher/switch-active.js",
        "resources/assets/template/js/Lobibox.js",
        "resources/assets/template/js/main.js",
        "resources/assets/plugins/jquery-confirm/jquery-confirm.min.js"
        // "resources/assets/plugins/slide-panel/js/jquery.slidereveal.min.js"
    ],
    "public/js/app.js"
);

mix.scripts(
    [
        "resources/assets/template/js/icheck/icheck.min.js",
        "resources/assets/template/js/icheck/icheck-active.js",
        "node_modules/bootstrap-validator/dist/validator.js",
        "resources/assets/template/js/moment.min.js",
        "resources/assets/plugins/select2/select2.min.js"
    ],
    "public/js/forms.js"
);

// mix.copy("node_modules/push.js/bin/push.js", "public/js/push.js");
// mix.copy("node_modules/push.js/bin/serviceWorker.min.js", "public/js/serviceWorker.min.js");
mix.copy("resources/assets/plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js", "public/js/");
mix.copy("resources/assets/plugins/yadcf-0.9.3/jquery.dataTables.yadcf.js", "public/js/");
mix.copy("resources/assets/template/DataTables-1.10.18/js/jquery.dataTables.js", "public/js/");
mix.copy("resources/assets/template/DataTables-1.10.18/js/dataTables.bootstrap4.js", "public/js/");
// "resources/assets/template/DataTables-1.10.18/js/dataTables.bootstrap4.js",, "public/js/");

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
